from db import initialize_db
from models import User,Organization,Countries,States,Cities,User1,Fields,Numbering,Module_name,User_module_name

# translation for sorting between datatables api and mongodb



class LastDataId():
    def getOrganizationLastDataId(col):
        try:
            col1 = '-'+col
            if len(list(Organization.objects.filter())) is not 0:
                last_user = list(Organization.objects.filter({}).order_by(col1).limit(1))
                return int(last_user[0][col])
            else:
                return 0
        except Organization.DoesNotExist:
            return 0
    def getLastDataId(col):
        try:
            col1 = '-'+col
            if len(list(Organization.objects.filter())) is not 0:
                last_user = list(Organization.objects.filter({}).order_by(col1).limit(1))
                return int(last_user[0][col])
            else:
                return 0
        except Organization.DoesNotExist:
            return 0

class DataTables_Handler(object):
    def paging(self):
        pages = namedtuple('pages', ['start', 'length'])
        if (self.request_values['iDisplayStart'] != "") and (self.request_values['iDisplayLength'] != -1):
            pages.start = int(self.request_values['iDisplayStart'])
            pages.length = int(self.request_values['iDisplayLength'])
        return pages