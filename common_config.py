import datetime
from uuid import uuid1
from collections import defaultdict
from flask import Flask ,render_template,make_response
import pdfkit
import app_config
import mimetypes

class Pdf():
    
    def PDFGenerator(html,filename):
        config_path = 'C:\\Program Files\\wkhtmltopdf\\bin\\wkhtmltopdf.exe'
        config = pdfkit.configuration(wkhtmltopdf=config_path)
        pdf = pdfkit.from_string(html,False,configuration=config,options={
            'page-size': 'A4',
            'margin-top': '10',
            'margin-right': '8',
            'margin-left': '5',
            'margin-bottom': '10',
            'zoom': '1.2',
            'encoding': "UTF-8",
        })
        response = make_response(pdf)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'inline;filename='+filename
        return response


    def html2pdf(open_files):
        import six
        from xhtml2pdf import pisa
        pisa.showLogging()


        data = '<html><body><p>Hello <strong style="color: #f00;">World</strong><hr><table border="1" style="background: #eee; padding: 0.5em;"><tr><td>Amount</td><td>Description</td><td>Total</td></tr><tr><td>1</td><td>Good weather</td><td>0 EUR</td></tr><tr style="font-weight: bold"><td colspan="2" align="right">Sum</td><td>0 EUR</td></tr></table></body></html>'
        filename = "test.pdf"
        pdf = pisa.CreatePDF(six.StringIO(data),file(filename, "wb"))
        open_file = False
        if open_file and (not pdf.err):
            pisa.startViewer(filename)
    
        return not pdf.err

    def render_pdf(data, filename, open_file):
        from xhtml2pdf import pisa
        from io import StringIO,BytesIO
        pdf = pisa.CreatePDF(
        six.StringIO(data),
        file(filename, "wb"))
 
        if open_file and (not pdf.err):
            pisa.startViewer(filename)
 
        return not pdf.err

class MailAttach():
    
    def getAttachment(attachmentFilePath):
   
        contentType, encoding = mimetypes.guess_type(attachmentFilePath)

        if contentType is None or encoding is not None:
            contentType = 'application/octet-stream'
        mainType, subType = contentType.split('/', 1)
        file = open(attachmentFilePath, 'rb')
        if mainType == 'text':
            attachment = MIMEText(file.read())
        elif mainType == 'message':
            attachment = email.message_from_file(file)
        elif mainType == 'image':
            attachment = MIMEImage(file.read(),_subType=subType)
        elif mainType == 'audio':
            attachment = MIMEAudio(file.read(),_subType=subType)
        else:
            attachment = MIMEBase(mainType, subType)

        attachment.set_payload(file.read())
        encode_base64(attachment)
        file.close()
        attachment.add_header('Content-Disposition', 'attachment',   filename=os.path.basename(attachmentFilePath))
        return attachment




class Uid():
    def idData():
        number = datetime.datetime.now().microsecond
        return number
    def generateUUID():
        return str(uuid1())

    def fix_array1(info):
        item1 = []
        for item in info:
            for key, value in item.items():
                if not isinstance(value, dict) or len(value) != 1:
                    continue
                (subkey, subvalue), = value.items()
                if not subkey.startswith('$'):
                    continue
                elif key=='profile_image':
                    
                    item[key] = str(app_config.BASE_URL)+str(app_config.UPLOAD_PROFILE_FOLDER)+str(subvalue)
                elif key!='password':
                    item[key] = subvalue
                # Id = 'userId'
                # if key=='_id':
                #     item['Id'] = subvalue
                #     print (subvalue)
                
            item1.append(item)
            item4 = {}
            item25 = {}
            for item2 in item1:
                item25
                # item3 = {item2:item1[item2]}
                # item4.append(item2)

        # response=json.dumps(item1)
            # item1[] = item
        return item2

    def fix_array(info):
        item1 = []
        for item in info:
            for key, value in item.items():
                if not isinstance(value, dict) or len(value) != 1:
                    continue
                (subkey, subvalue), = value.items()
                if not subkey.startswith('$'):
                    continue
                elif item=='field_setting':
                    settings[item] = info[item] 
                elif key=='profile_image':
                    
                    item[key] = str(app_config.BASE_URL)+str(UPLOAD_PROFILE_FOLDER)+str(subvalue)
                elif key!='password':
                    item[key] = subvalue
                # Id = 'userId'
                # if key=='_id':
                #     item['Id'] = subvalue
                #     print (subvalue)
                
            item1.append(item)
            item4 = {} 
            item25 = {}
            # for item2 in item1:
            #     item25.append(item2[item1])
                # item3 = {item2:item1[item2]}
                # item4.append(item2)

        # response=json.dumps(item1)
            # item1[] = item
        return item1

    def fix_array_sub(info):
        item1 = []
        for item in info:
            for key, value in item.items():
                if not isinstance(value, dict) or len(value) != 1:
                    continue
                (subkey, subvalue), = value.items()
                # print(item)
                item12 = ''
                item13 = ''
                item14 = ''
                item15 = ''
                item16 = ''
                for k in item:
                    if k=='sub_checklist':
                        item12 = item[k]
                    if k=='sub_process':
                        item13 = item[k]
                    if k=='process_list':
                        item14 = item[k]
                    if k=='account':
                        item15 = item[k]
                    if k=='reporting_to':
                        item16 = item[k]
                if item12:
                    item['sub_checklist'] = Uid.fix_array(item12)
                if item13:
                    item['sub_process'] = Uid.fix_array(item13)
                if item14:
                    item['process_list'] = Uid.fix_array(item14)
                if item15:
                    item['account'] = Uid.fix_array(item15)
                if item16:
                    item['reporting_to'] = Uid.fix_array(item16)
                if not subkey.startswith('$'):
                    continue                    
                elif key=='profile_image':
                    
                    item[key] = str(app_config.BASE_URL)+str(UPLOAD_PROFILE_FOLDER)+str(subvalue)
                elif key!='password':
                    item[key] = subvalue
                # Id = 'userId'
                # if key=='_id':
                #     item['Id'] = subvalue
                #     print (subvalue)
                
            item1.append(item)
            item4 = {} 
            item25 = {}
            # for item2 in item1:
            #     item25.append(item2[item1])
                # item3 = {item2:item1[item2]}
                # item4.append(item2)

        # response=json.dumps(item1)
            # item1[] = item
        return item1

    def parse_json(data):
        return json.loads(json_util.dumps(data))
    
    def fix_array2(info):
        item1 = []
        settings = []
        for item in info:
            for key, value in item.items():
                if not isinstance(value, dict) or len(value) != 1:
                    continue
                (subkey, subvalue), = value.items()
                if not subkey.startswith('$'):
                    continue
                elif item=='field_setting':
                    settings[item] = info[item] 
                elif key=='profile_image':
                    
                    item[key] = str(app_config.BASE_URL)+str(UPLOAD_PROFILE_FOLDER)+str(subvalue)
                elif key!='password':
                    item[key] = subvalue
               
            settings.append(item)
        return settings
        
    def fix_array_role(info):
        settings = defaultdict(list)
        for item in info:
            # print (item)
            if item=='_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='template_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='source':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='stage':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='category':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='product':
                item3 = info[item]
                settings[item] = item3
            elif item=='deal':
                item3 = info[item]
                settings[item] = item3
            elif item=='tag':
                item3 = info[item]
                settings[item] = item3
            elif item=='images':
                item3 = info[item]
                settings[item] = item3
            
            elif item=='qty':
                item3 = info[item]
                settings[item] = item3
            elif item=='customer_type':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='assigned_to':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='company_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='contact_id':
                item3 = info[item]
                # print (item3)
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='field_setting':
                settings[item] = info[item] 
            elif item=='user_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='deal_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='account':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            elif item=='contact_detail':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            elif item=='report_to':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            else:
                
                settings[item]=str(info[item])
        return settings
    def fix_array3(info):
        settings = defaultdict(list)
        for item in info:
            # print (item)
            if item=='_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='template_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='quote_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='proforma_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='product_id':
                print('ttttttttttttt')
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='source':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='stage':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='proforma_stage':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='quote_stage':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='category':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='product':
                item3 = info[item]
                settings[item] = item3
            elif item=='deal':
                item3 = info[item]
                settings[item] = item3
            elif item=='tag':
                item3 = info[item]
                settings[item] = item3
            elif item=='images':
                item3 = info[item]
                settings[item] = item3
            elif item=='contact':
                item3 = info[item]
                settings[item] = ''
            
            elif item=='qty':
                item3 = info[item]
                settings[item] = item3
            elif item=='customer_type':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='assigned_to':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            # elif item=='pricelist':
            #     item3 = info[item]
            #     for item1 in item3:
            #         settings[item]=str(item3['$oid'])
            elif item=='company_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='contact_id':
                item3 = info[item]
                # print (item3)
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='field_setting':
                settings[item] = info[item] 
            elif item=='user_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='deal_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='account':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            elif item=='contact_detail':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            elif item=='report_to':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            elif item=='custom_fields':
                item3 = info[item]
                settings[item]=Uid.fix_array(info[item])
            else:
                settings[item]=str(info[item])
        return settings
    def fix_array4(info):
        settings = defaultdict(list)
        for item in info:
            # print (item)
            if item=='_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='field_setting':
                settings[item] = info[item]  
            elif item=='roleData':
                roleData = info[item]
                # print(roleData)
                for roleData1 in roleData:
                    settings[item] = roleData1
                    # print (roleData1)
                # for roleData1 in roleData:
                #     print(roleData)
                #     settings[item] = roleData[roleData1]

            elif item=='profile_image':
                item3 = info[item]
                settings[item] = str(app_config.BASE_URL)+str(app_config.UPLOAD_PROFILE_FOLDER)+str(item3)
            elif item=='org_id':
                item3 = info[item]
                settings[item] = int(item3)
            else:
                settings[item]=str(info[item])
        return settings
    def fix_array5(info):
        settings = defaultdict(list)
        for item in info:
            # print (item)
            if item=='_id':
                item3 = info[item]
                for item1 in item3:
                    settings[item]=str(item3['$oid'])
            elif item=='source':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='stage':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='category':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='product':
                item3 = info[item]
                settings[item] = item3
            elif item=='deal':
                item3 = info[item]
                settings[item] = item3
            elif item=='tag':
                item3 = info[item]
                settings[item] = item3
            elif item=='images':
                item3 = info[item]
                settings[item] = item3
            elif item=='contact':
                item3 = info[item]
                settings[item] = ''
            
            elif item=='qty':
                item3 = info[item]
                settings[item] = item3
            elif item=='customer_type':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='assigned_to':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='company_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='contact_id':
                item3 = info[item]
                # print (item3)
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='field_setting':
                settings[item] = info[item] 
            elif item=='user_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            elif item=='deal_id':
                item3 = info[item]
                if item3:
                    for item1 in item3:
                        settings[item]=str(item3['$oid'])
                else:
                    settings[item] = ''
            else:
                
                settings[item]=str(info[item])
        return settings