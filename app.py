from models import Follow_up , Accounttmp ,Contacttmp, Producttemp
from flask import Flask ,render_template,make_response
from flask import request, jsonify, json, Response, Flask,url_for, send_from_directory,redirect
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, fresh_jwt_required,get_raw_jwt
)

from flask_restful import Resource
from flask_cors import CORS
from flask_mail import Mail, Message
import socket, smtplib,ssl, time, select

from bson import ObjectId
from bson import json_util
import datetime,pytz
from datetime import date
from db import initialize_db
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
from os.path import join, dirname, realpath
import os
from werkzeug.security import generate_password_hash, check_password_hash
from common_config import Uid,Pdf,MailAttach

import app_config 
from collections import defaultdict
from flask_weasyprint import HTML, render_pdf
import mimetypes
import magic
from mongoengine.errors import NotUniqueError
import requests
# from datetime import datetime
# import json

from csv import reader
from pytz import timezone 
import base64
from operator import attrgetter






app = Flask(__name__, static_url_path='/static')

# app.config['MAIL_SERVER']='smtp.elasticemail.com'
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USERNAME'] = 'support@rabbitcrm.com'
# app.config['MAIL_PASSWORD'] = 'da3676c8-7d64-4b6b-beed-b71a432da8dd'
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True

# Gmail
# app.config['MAIL_SERVER']='smtp.gmail.com'
# app.config['MAIL_PORT'] = 465
# app.config['MAIL_USERNAME'] = 'support@farazon.com'
# app.config['MAIL_PASSWORD'] = '!QAZ2wsx'
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True

#Elastic Mail
app.config['MAIL_SERVER']='smtp.elasticemail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'support@farazon.com'
app.config['MAIL_PASSWORD'] = '8EFB529583174E21CF4120ABEEEC6B3CB1D3'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True

# app.config['MAIL_USERNAME'] = 'rabbitdev2020@gmail.com'
# app.config['MAIL_PASSWORD'] = 'Intel123'



mail = Mail(app)

mail.init_app(app)



app.secret_key = "secret key"

app.config['UPLOAD_PRODUCT_FOLDER'] = app_config.UPLOAD_PRODUCT_FOLDER

app.config['UPLOAD_PROFILE_FOLDER'] = app_config.UPLOAD_PROFILE_FOLDER
app.config['UPLOAD_COMPANY_FOLDER'] = app_config.UPLOAD_COMPANY_FOLDER
app.config['UPLOAD_FOLDER'] = app_config.UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

app.config['UPLOAD_OPEN_FOLDER'] = app_config.UPLOAD_OPEN_FOLDER
app.config['UPLOAD_OPEN_PROFILE_FOLDER'] = app_config.UPLOAD_OPEN_PROFILE_FOLDER
app.config['UPLOAD_OPEN_COMPANY_FOLDER'] = app_config.UPLOAD_OPEN_COMPANY_FOLDER

app.config['UPLOAD_EMAIL_FOLDER'] = app_config.UPLOAD_EMAIL_FOLDER
app.config['UPLOAD_DEAL_FOLDER'] = app_config.UPLOAD_DEAL_FOLDER
app.config['UPLOAD_RESUME_FOLDER'] = app_config.UPLOAD_RESUME_FOLDER
app.config['UPLOAD_OPEN_EMAIL_FOLDER'] = app_config.UPLOAD_OPEN_EMAIL_FOLDER

app.config['UPLOAD_DOCUMENT_FOLDER'] = app_config.UPLOAD_DOCUMENT_FOLDER
app.config['UPLOAD_OPEN_DOCUMENT_FOLDER'] = app_config.UPLOAD_OPEN_DOCUMENT_FOLDER
app.config['UPLOAD_CSV_FOLDER'] = app_config.UPLOAD_CSV_FOLDER


app.config['UPLOAD_OPEN_CRM_DOCS_FOLDER'] = app_config.UPLOAD_OPEN_CRM_DOCS_FOLDER
app.config['UPLOAD_CRM_DOCS_FOLDER'] = app_config.UPLOAD_CRM_DOCS_FOLDER
app.config['TEMPLATE_FOLDER'] = app_config.TEMPLATE_FOLDER
app.config['UPLOAD_PDF_IMAGE_FOLDER'] = app_config.UPLOAD_PDF_IMAGE_FOLDER
app.config['UPLOAD_COMMON_DOCUMENT_FOLDER'] = app_config.UPLOAD_COMMON_DOCUMENT_FOLDER
app.config['FIREBASE_TOKEN'] = 'AAAAPWXk2XI:APA91bE1AtOT0CE5ywMcPUU1tGbDdkDCMsn-kn7pewSJrSKR_e-e7w9OaVeL3VyNmv_bR2JGAs-4h-bC_4l4QpDLH9gJS2NR8rA46d4S-FFIW0p5CSy4Jih4O_H5zWInMSOto_QsBp6t'




from mongodb import MongoAPI,TokenRefresh
from adminapi import AdminAPI

CORS(app)
app.config['JWT_SECRET_KEY'] = 'qwertyuioplkjhgfdsazxcvbnmdwxnmbvsjesduh'  # Change this!
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['ACTIVATION_LINK'] = app_config.REDIRECT_URL+'activateEmail/'
app.config['RESET_PASSWORD_LINK'] = 'https://farazon.com/crm/#/reset_password/'
app.config['CONTACT_DETAIL_URL'] = 'https://farazon.com/crm/#/contact-detail/'
app.config['DEAL_DETAIL_URL'] = 'https://farazon.com/crm/#/deal-detail/'
app.config['COMPANY_DETAIL_URL'] = 'https://farazon.com/crm/#/company-detail/'
app.config['SITE_LOGIN_URL'] = 'https://farazon.com/crm/#/login'
app.config['SUPPORT_MAIL_ID'] = 'developer2@skyzon.com'
# app.config['MAIL_SERVER']='smtp.elasticemail.com'
# app.config['MAIL_PORT'] = 25
# app.config['MAIL_USERNAME'] = 'support@rabbitcrm.com'
# app.config['MAIL_PASSWORD'] = 'da3676c8-7d64-4b6b-beed-b71a432da8dd'
# app.config['MAIL_USE_TLS'] = False
# app.config['MAIL_USE_SSL'] = True

mail = Mail(app)



jwt = JWTManager(app)



company_field  = defaultdict(list)
company_field['company_name']='Company Name'
company_field['phone']='Phone'
company_field['email']='Email'
company_field['gstin']='GSTIN'
company_field['website']='Website'
company_field['address1']='Address1'
company_field['address2']='Address2'
company_field['state']='State'
company_field['city']='City'
company_field['pincode']='Pincode'
company_field['country']='Country'

product_field  = defaultdict(list)
product_field['product_name']='Product Name'
product_field['model_no']='Model No'
product_field['uom']='UOM'
product_field['hsn_code']='HSN Code'
product_field['price']='Selling Price'
product_field['description']='Description'
product_field['cgst']='CGST %'
product_field['sgst']='SGST %'
product_field['igst']='igst'



contact_field  = defaultdict(list)
contact_field['name']='Name'
contact_field['company_name']='Company Name'
contact_field['phone_number']='Phone'
contact_field['email']='Email'
contact_field['description']='Description'
contact_field['whatsapp_no']='Whatsapp No'
contact_field['secondary_email']='Secondary Email'
contact_field['designation']='Designation'
contact_field['website']='Website'
contact_field['facebook']='facebook'
contact_field['linkedin']='linkedin'
contact_field['twitter']='twitter'
contact_field['skype']='skype'
contact_field['address1']='address1'
contact_field['address2']='address2'
contact_field['country']='country'
contact_field['state']='state'
contact_field['city']='city'
contact_field['pincode']='pincode'



@app.route('/', methods=['GET', 'POST'])
def pdf_template1():
    name='karthik'
    location='location'
    return render_template('certificate.html',name=name,location=location)
      

@app.route('/users', methods=['GET'])
def get_users():
    output = User.objects().to_json()
    users = json.loads(json_util.dumps(output))
    return Response(output, mimetype="application/json", status=200)

@app.route('/user/<id>', methods=['GET'])
def get_user(id):
    output = User.objects.get(id=id).to_json()
    users = json.loads(json_util.dumps(output))
    return Response(users, mimetype="application/json", status=200)

@app.route('/date', methods=['GET'])
def date():
    dat = dt.datetime.now()
    return Response(dat, mimetype="application/json", status=200)



@app.route('/login', methods=['GET','POST'])
# @cross_origin()
def login():
    data = request.json
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})

    # data = request.form
    
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    device_id =  request.json.get('device_id', None)
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    if not device_id:
        device_id=''
        app_type='web'
    else:
        app_type='mobile'
        device_id=device_id

    password = base64.b64encode(password.encode())
    password=password.decode('utf-8')
    response = MongoAPI.selectLogin(email,password)

  
    if response=='no such name':
       
        response = MongoAPI.emailCheck(email)
        if response=='Yes':
            return jsonify({"msg": "Invalid Password.","code": 400})
        else:
             return jsonify({"msg": "Invalid Email","code": 400})
    else:
        if response['status']=='active':

            addDeviceId=MongoAPI.updateDeviceID(response['org_id'],response['id'],device_id)
            current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
            addUserAudit=MongoAPI.userAudit(response['id'],response['org_id'],current_time,app_type)
            if response['role']==None:
                role_id = MongoAPI.userAdministratorRole(response['org_id'])
                if role_id==0:
                    roleNewSubmit(response['org_id'],response['id'])
                    role_id = MongoAPI.userAdministratorRole(response['org_id'])
                MongoAPI.userRoleUpdate(response['id'],role_id)
                role = MongoAPI.getRolesListDetails(response['org_id'],role_id)
                roleData = Uid.fix_array_role(role)
            else:
                role = MongoAPI.getRolesListDetails(response['org_id'],response['role'])
                roleData = Uid.fix_array_role(role)
            rename = ''
            rename = MongoAPI.getRenameDetails(response['org_id'])
            rename1 = Uid.fix_array3(rename)
            # print (rename1)
            if not rename1['id']:
                renameSubmit(response['org_id'])
                rename = MongoAPI.getRenameDetails(response['org_id'])
                rename1 = Uid.fix_array3(rename)

                
            # response4 =json.dumps(response)
            response4 = Uid.fix_array3(response)
            expires = datetime.timedelta(days=365)
            organization = MongoAPI.organizationInfo(response['org_id'])
            organization = Uid.fix_array1(organization)
            access_token = create_access_token(identity=str(response['id']), expires_delta=expires)
            refresh_token = create_refresh_token(identity=str(response['id']), expires_delta=expires)
            addon = MongoAPI.addOnList(str(response['org_id']))
            data = {'result': response4,'access_token': access_token,'refresh_token': refresh_token,'roleData': roleData,'rename': rename1,'organization':organization,'addon':addon}
        elif response['status']=='inactive':
            return jsonify({"msg": "Your account is inactive, Pls contact Admin","code": 400,"data": data})

        return jsonify({"msg": "You have been successfully Logged in.","code": 200,"data": data})

@app.route('/gmailLoginWeb/<user_id>', methods=['GET'])
# @cross_origin()
def gmailLoginWeb(user_id):


    response = MongoAPI.selectLoginMail(user_id)
    # return response
    device_id = ''
    app_type = 'web'
    if response=='no such name':
        response1 = MongoAPI.userCheck(user_id)
        if response1=='Yes':
            return jsonify({"msg": "Invalid user.","code": 400})
        else:
            return jsonify({"msg": "User doesn't exist","code": 400})
    else:
        if response['status']=='active':

            addDeviceId=MongoAPI.updateDeviceID(response['org_id'],response['id'],device_id)
            current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
            addUserAudit=MongoAPI.userAudit(response['id'],response['org_id'],current_time,app_type)
            if response['role']==None:
                role_id = MongoAPI.userAdministratorRole(response['org_id'])
                if role_id==0:
                    roleNewSubmit(response['org_id'],response['id'])
                    role_id = MongoAPI.userAdministratorRole(response['org_id'])
                MongoAPI.userRoleUpdate(response['id'],role_id)
                role = MongoAPI.getRolesListDetails(response['org_id'],role_id)
                roleData = Uid.fix_array_role(role)
            else:
                role = MongoAPI.getRolesListDetails(response['org_id'],response['role'])
                roleData = Uid.fix_array_role(role)
            rename = ''
            rename = MongoAPI.getRenameDetails(response['org_id'])
            rename1 = Uid.fix_array3(rename)
            # print (rename1)
            if not rename1['id']:
                renameSubmit(response['org_id'])
                rename = MongoAPI.getRenameDetails(response['org_id'])
                rename1 = Uid.fix_array3(rename)

                
            # response4 =json.dumps(response)
            response4 = Uid.fix_array3(response)
            expires = datetime.timedelta(days=365)
            organization = MongoAPI.organizationInfo(response['org_id'])
            organization = Uid.fix_array1(organization)
            access_token = create_access_token(identity=str(response['id']), expires_delta=expires)
            refresh_token = create_refresh_token(identity=str(response['id']), expires_delta=expires)
            data = {'result': response4,'access_token': access_token,'refresh_token': refresh_token,'roleData': roleData,'rename': rename1,'organization':organization}
        elif response['status']=='inactive':
            return jsonify({"msg": "Your account is inactive, Pls contact Admin","code": 400})

        return jsonify({"msg": "You have been successfully Logged in.","code": 200,"data": data})

@app.route('/signUp', methods=['POST'])
def userSignUp():

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    name =  request.json.get('name', None)
    device_id =  request.json.get('device_id', None)
    if not name:
        return jsonify({"msg": "Missing name parameter","code": 400})
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    if not device_id:
        device_id=''
        app_type='web'
    else:
        app_type='mobile'

    user = MongoAPI.userCheck(email)
    if user=='Yes':
        return jsonify({"msg": "Email id is already registered","code": 400})
    else:
        org_id = MongoAPI.createOrganizationData(email,name)
        org_id = int(org_id)
        # dataEnc = MongoAPI.dataEncryption(password)
        # encPassword = dataEnc['encString'].decode('utf-8')
        # password=password.encode('base64')
        password = base64.b64encode(password.encode())
        response1 = MongoAPI.createUser(email,password,name,org_id)
        password=password.decode('utf-8')
        response = MongoAPI.selectLogin(email,password)
        # print(response)
        addDeviceId=MongoAPI.updateDeviceID(response['org_id'],response['id'],device_id)
        current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
        addUserAudit=MongoAPI.userAudit(response['id'],response['org_id'],current_time,app_type)


    if response1:
        response4 =json.dumps(response)
        newCompany(response['org_id'],response['id'])
        role_id = MongoAPI.userAdministratorRole(response['org_id'])
        MongoAPI.userRoleUpdate(response['id'],role_id)
        role = MongoAPI.getRolesListDetails(response['org_id'],response['id'])

        roleData = Uid.fix_array3(role)


        # roleNewSubmit(response['org_id'],response['id'])
        role_id = MongoAPI.userAdministratorRole(response['org_id'])
        MongoAPI.userRoleUpdate(response['id'],role_id)
        role = MongoAPI.getRolesListDetails(response['org_id'],role_id)
        roleData = Uid.fix_array_role(role)


        expires = datetime.timedelta(days=365)
        access_token = create_access_token(identity=str(response['id']), expires_delta=expires)
        refresh_token = create_refresh_token(identity=str(response['id']), expires_delta=expires)
        
        currency = MongoAPI.fieldsSettingsData('currency',response['org_id'])
        currency = Uid.fix_array(currency)
        timezones = MongoAPI.fieldsSettingsData('timezones',response['org_id'])
        timezones = Uid.fix_array(timezones)
        str_id=str(response['org_id'])
        encode_id=str_id
        activation_link=app.config['ACTIVATION_LINK']
        encoded_link=activation_link+encode_id

        signUpTemplate=MongoAPI.getAdminEmailTemplate('signup')
        signupMail=signUpTemplate[0]['template_content']
    
        signupMailContent = signupMail.replace("{{user_name}}",response['name'])
        signupMailContent1 = signupMailContent.replace("{{activationlink}}",encoded_link)

        organization = MongoAPI.organizationInfo(response['org_id'])
        organization = Uid.fix_array1(organization)
        
        data1 = defaultdict(list)
        data1['subject']='Farazon CRM Account Activation'
        data1['content']=signupMailContent1
        data1['to']=response['email']
        data1['cc']=''
        data1['bcc']='swami@skyzon.com,developer2@skyzon.com,tester@skyzon.com'
        # data1['attachment_list']=''
        emailId='info@farazon.com'
        attachment=''
        data1['associate_id']=response['id']
        data1['associate_to']='signup'
        sendMail=MongoAPI.emailSubmit(response['org_id'],response['id'],data1,emailId,attachment)
        createdirectory = MongoAPI.createDirectory(response['org_id'])
        addon = MongoAPI.addOnList(response['org_id'])
      
        data = {'user': response,'currency':currency,'timezones':timezones,'access_token': access_token,'refresh_token': refresh_token,'roleData': roleData,'organization':organization,'addon':addon}
        reresult =  {'data': data,"code": 200,"msg":"Account registered successfully."}
        return json.dumps(reresult)
        # return jsonify({"msg": "account registered successfully","code": 200,"data": data})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/signUpUpdate', methods=['PUT'])
@jwt_required
def signUpUpdate():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
            
    organization_name =  request.json.get('organization_name', None)
    currency =  request.json.get('currency', None)
    time_zone =  request.json.get('time_zone', None)
    date_format =  request.json.get('date_format', None)
    phone =  request.json.get('phone', None)
    website =  request.json.get('website', None)
    

    
    if not organization_name:
        return jsonify({"msg": "Missing organization name parameter","code": 400})
    if not currency:
        return jsonify({"msg": "Missing currency parameter","code": 400})
    if not time_zone:
        return jsonify({"msg": "Missing time zone parameter","code": 400})
    
    response = MongoAPI.signUpUpdate(organization_name,currency,time_zone,date_format,phone,website,ordId)
    if response:
        return jsonify({"msg": "Organization successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/mobileLogout', methods=['GET'])
@jwt_required
def mobileLogout():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    MongoAPI.updateDeviceID(ordId,current_user,'')
    return jsonify(msg="Logout successfully",code=200)

@app.route('/mobileEndTime', methods=['GET'])
@jwt_required
def mobileEndTime():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
    userLogout=MongoAPI.userLogoutMobile(current_user,ordId,current_time)
    return jsonify(msg="End Time Updated",code=200)

# @app.route('/webLogoutAuto', methods=['GET'])
# @jwt_required
# def webLogoutAuto():
#     current_user = get_jwt_identity()
#     user = MongoAPI.authorizationCheck(current_user)
#     ordId = ''
#     for user2 in user:
#         if user2=='org_id':
#             ordId = user[user2]
#     current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d')
#     auto_logout_time=current_time+' 23:59:00.000'
#     t=datetime.datetime.strptime(auto_logout_time, '%Y-%m-%d %H:%M:%S.%f')
#     userLogout=MongoAPI.userLogoutWeb(current_user,ordId,t)
#     return jsonify(msg="Logout successfully",code=200)

@app.route('/webLogout', methods=['GET'])
@jwt_required
def webLogout():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    current_time=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
    userLogout=MongoAPI.userLogoutWeb(current_user,ordId,current_time)
    return jsonify(msg="End Time Updated",code=200)


@app.route('/sendNotification', methods=['GET'])
def sendNotification():
    # serverToken = 'AAAAPWXk2XI:APA91bE1AtOT0CE5ywMcPUU1tGbDdkDCMsn-kn7pewSJrSKR_e-e7w9OaVeL3VyNmv_bR2JGAs-4h-bC_4l4QpDLH9gJS2NR8rA46d4S-FFIW0p5CSy4Jih4O_H5zWInMSOto_QsBp6t'
    # deviceToken = 'cT2_FrYcTw-NnUuOVI2DvT:APA91bGAvGO5z-_4iFxYYJj6f76gmkSWX_bsjttLrER3axjIj45LU5UzRWNmLNzTXuCMDy_J-FHya1q2rVsqR-SqxKu5jAWBLqTkQz2T8TCplmumjWgtGVTCGywtl7dh7VWOctIxIGUi'
    serverToken = app.config['FIREBASE_TOKEN']
    headers = {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + serverToken,
            }
    followupList=MongoAPI.getFollowUpAll()
    if len(followupList)!=0:
        today=datetime.datetime.today()
        for data in followupList:
            user_data=MongoAPI.getUserDetails(data['org_id'],data['owner'])
            deviceToken=user_data['device_id']
            company_name=''
            link=''
            if data['associate_to']=='contact':
                    contact_detail=MongoAPI.getContactDetails(data['org_id'],data['associate_id'])
                    company_name=contact_detail['company_name']
                    link=app.config['CONTACT_DETAIL_URL']+data['associate_id']
            elif data['associate_to']=='company':
                    company_detail=MongoAPI.getCompanyDetails(data['org_id'],data['associate_id'])
                    company_name=company_detail['company_name']
                    link=app.config['COMPANY_DETAIL_URL']+data['associate_id']
            date_string=data['date']+' '+data['time']
            body_string=company_name+' | '+data['time']

            # Send Email Notification
            # encoded_link=activation_link+encode_id
            Template=MongoAPI.getAdminEmailTemplate('task_reminder')
            resetMail=Template[0]['template_content']
            
            resetMailContent = resetMail.replace("{{user_name}}",user_data['name'])
            resetMailContent1 = resetMailContent.replace("{{follow_up_title}}",data['name'])
            resetMailContent2 = resetMailContent1.replace("{{follow_up_date}}",data['date'])
            resetMailContent3 = resetMailContent2.replace("{{follow_up_time}}",data['time'])
            resetMailContent4 = resetMailContent3.replace("{{follow_up_priority}}",data['priority'])
            resetMailContent5 = resetMailContent4.replace("{{company_name}}",company_name)
            resetMailContent6 = resetMailContent5.replace("{{detail_link}}",link)
            
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - Task Reminder'
            data1['content']=resetMailContent6
            data1['to']=user_data['email']
            data1['cc']=''
            data1['bcc']=''
            data1['attachment_list']=''
            emailId='info@skyzon.com'
            attachment=''
            data1['associate_id']=data['_id']
            data1['associate_to']='follow_up'
            datetime_object = datetime.datetime.strptime(date_string, '%d/%m/%Y %I:%M %p')
            minutes_diff = (datetime_object - today).total_seconds() / 60.0
            if int(minutes_diff) < 20 and int(minutes_diff) > 0:
                sendMail=MongoAPI.emailSubmit(data['org_id'],data['_id'],data1,emailId,attachment)
                follow_up_update = Follow_up.objects(id=ObjectId(data['_id'])).update_one(notification='1')
            # Send Notification
            if deviceToken !='':
                datetime_object = datetime.datetime.strptime(date_string, '%d/%m/%Y %I:%M %p')
                minutes_diff = (datetime_object - today).total_seconds() / 60.0
                if int(minutes_diff) < 20 and int(minutes_diff) > 0:
                    body = {
                    'notification': {'title': data['name'],
                                    'body':  body_string,
                                    },
                    'to':deviceToken,
                    'priority': 'high',
                    'data':{
                        'notification_type':'followup',
                        'associate_id':data['associate_id'],
                        'associate_to':data['associate_to']
                        }
                        }
                    response = requests.post("https://fcm.googleapis.com/fcm/send",headers = headers, data=json.dumps(body))
                    if response.status_code==200:
                        response_data=response.json()
                        if response_data['success']==1:
                            follow_up_update = Follow_up.objects(id=ObjectId(data['_id'])).update_one(notification='2',notification_data=response_data)
                else:
                    return jsonify({"msg": "No Notification","code": 200})
            else:
                return jsonify({"msg": "No Device ID","code": 200})
    else:
        return jsonify({"msg": "No Notification","code": 200})



@app.route('/activateEmail/<org_id>', methods=['GET'])
def activateEmail(org_id):
    
    response = MongoAPI.activateEmail(org_id)
    if response:
        url = app.config['SITE_LOGIN_URL']
        return redirect(url, code=302)
        # return render_template('emailactivation.html')
    else:
        return render_template('wentwrong.html')

@app.route('/sendPasswordLink/<email_id>', methods=['GET'])
def sendPasswordLink(email_id):

    user = MongoAPI.userCheck(email_id)
    if user=='Yes':
        get_user=MongoAPI.getUserWithEmail(email_id)
        response=get_user[0]
        str_id=get_user[0]['_id']
        encode_id=str_id
        activation_link=app.config['RESET_PASSWORD_LINK']
        encoded_link=activation_link+encode_id
        Template=MongoAPI.getAdminEmailTemplate('reset_password')
        resetMail=Template[0]['template_content']
    
        resetMailContent = resetMail.replace("{{user_name}}",get_user[0]['name'])
        resetMailContent1 = resetMailContent.replace("{{resetlink}}",encoded_link)
        
        data1 = defaultdict(list)
        data1['subject']='Reset Farazon Account Password'
        data1['content']=resetMailContent1
        data1['to']=response['email']
        data1['cc']=''
        data1['bcc']='swami@skyzon.com,developer2@skyzon.com,tester@skyzon.com'
        data1['attachment_list']=''
        emailId='info@farazon.com'
        attachment=''
        data1['associate_id']=response['_id']
        data1['associate_to']='forgot_password'
        sendMail=MongoAPI.emailSubmit(response['org_id'],response['_id'],data1,emailId,attachment)
        return jsonify({"msg": "Reset Password Link is sent successfully","code": 200})
        
    else:
        return jsonify({"msg": "Email id is not registered","code": 400})

@app.route('/resetPassword', methods=['POST'])
def resetPassword():

    user_id =  request.json.get('user_id', None)
    password =  request.json.get('password', None)
    response = MongoAPI.resetPassword(user_id,password)
    if response:
        return jsonify({"msg": "Password reset successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/countries', methods=['GET'])
@jwt_required
def countries():
    current_user = get_jwt_identity()
    # current = authorizationCheck(current_user)
    data = MongoAPI.countriesList(current_user)
    countries = Uid.fix_array(data)
    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        return jsonify(msg="Countries List",code=200,data= countries)

@app.route('/states/<id>', methods=['GET'])
@jwt_required
def states(id):
    current_user = get_jwt_identity()
    # current = authorizationCheck(current_user)
    data = MongoAPI.statesList(current_user,id)
    states = Uid.fix_array(data)
    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        return jsonify(msg="States List",code=200,data= states)



@app.route('/statesName/<name>', methods=['GET'])
@jwt_required
def statesName(name):
    current_user = get_jwt_identity()
    # current = authorizationCheck(current_user)
    data = MongoAPI.countriesNameList(name)

    states = Uid.fix_array(data)
    states1 = states
    for states2 in states1:
        states3 = ''
    # print (states2['id'])

    data = MongoAPI.statesList(current_user,states2['id'])

    states = Uid.fix_array(data)

    # data = MongoAPI.statesList(current_user,id)


    # states = Uid.fix_array(data)
    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        return jsonify(msg="States List",code=200,data= states)


@app.route('/citiesName/<name>', methods=['GET'])
@jwt_required
def citiesName(name):
    current_user = get_jwt_identity()
    
    data = MongoAPI.statesNameList(name)

    states = Uid.fix_array(data)
    states1 = states
    for states2 in states1:
        states3 = ''


    data = MongoAPI.citiesList(current_user,states2['id'])
    cities = Uid.fix_array(data)
    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        return jsonify(msg="Cities List",code=200,data= cities)



@app.route('/cities/<id>', methods=['GET'])
@jwt_required
def cities(id):
    current_user = get_jwt_identity()
    # current = authorizationCheck(current_user)
    data = MongoAPI.citiesList(current_user,id)
    cities = Uid.fix_array(data)
    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        return jsonify(msg="Cities List",code=200,data= cities)

@app.route('/addonList', methods=['GET'])
@jwt_required
def addonList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    addon = MongoAPI.addOnList(ordId)
    addon_list = Uid.fix_array(users1)
    if addon_list:
        return jsonify({"msg": "Add on List","code": 200,"data": addon_list})
    else:
        return jsonify({"msg": "No Addon","code": 400})

@app.route('/usersList', methods=['GET'])
@jwt_required
def usersList():
    current_user = get_jwt_identity()
    

    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    users1 = MongoAPI.usersList(ordId)
    users = Uid.fix_array(users1)
    # print (users)
    if users1:
        return jsonify({"msg": "User List","code": 200,"data": users})
    else:
        return jsonify({"msg": "No User List","code": 400})
        
        

@app.route('/addUser', methods=['POST'])
@jwt_required
def addUser():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    # print (user)
    # return jsonify({"msg": "Oops,Something went wrong !","code": 400,"user": user})
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if countries=='No':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        email =  request.json.get('email', None)
        password =  request.json.get('password', None)
        name =  request.json.get('name', None)
        phone = request.json.get('phone', None)
        role = request.json.get('role', None)
        report_to = request.json.get('report_to', None)
        
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        if not email:
            return jsonify({"msg": "Missing email parameter","code": 400})
        if not password:
            return jsonify({"msg": "Missing password parameter","code": 400})
        if not phone:
            return jsonify({"msg": "Missing phone parameter","code": 400})
        if not role:
            return jsonify({"msg": "Missing role parameter","code": 400})
        
        password = base64.b64encode(password.encode())
        password=password.decode('utf-8')
        user1 = MongoAPI.userCheck(email)
        if user1=='Yes':
            return jsonify({"msg": "Email id is already registered","code": 400})
        else:
            org_id = int(ordId)
            # password = generate_password_hash(password)
            response = MongoAPI.createOrganizationUser(email,password,name,org_id,phone,role,report_to)
            response1 = json.loads(json_util.dumps(response))
        if response:
            return jsonify({"msg": "account registered successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/userDetails/<id>', methods=['GET'])
@jwt_required
def userDetails(id):
    current_user = get_jwt_identity()
    user1 = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user1:
        if user2=='org_id':
            ordId = user1[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        user = MongoAPI.getUserDetails(ordId,id)
        # print (user)
        # user = Uid.fix_array(user)
        user['profile_image'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_PROFILE_FOLDER'])+str(user['profile_image'])
        if not user1['email']:
            return jsonify({"msg": "Invalid authorization","code": 400})
        else:
            if user:
                return jsonify({"msg": "User details","code": 200,"user": user})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/userUpdate/<id>', methods=['PUT'])
@jwt_required
def userUpdate(id):

    email =  request.json.get('email', None)
    name =  request.json.get('name', None)
    phone = request.json.get('phone', None)
    role = request.json.get('role', None)
    report_to = request.json.get('report_to', None)
    
    if not name:
        return jsonify({"msg": "Missing name parameter","code": 400})
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not phone:
        return jsonify({"msg": "Missing phone parameter","code": 400})
    if not role:
        return jsonify({"msg": "Missing role parameter","code": 400})

    response = MongoAPI.userUpdate(email,name,phone,id,role,report_to)
    if response=='Yes':
        return jsonify({"msg": "User successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/regionalSettings', methods=['PUT'])
@jwt_required
def regionalSettings():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    currency =  request.json.get('currency', None)
    time_zone =  request.json.get('time_zone', None)
    date_format =  request.json.get('date_format', None)
    
    if not currency:
        return jsonify({"msg": "Missing currency parameter","code": 400})
    if not time_zone:
        return jsonify({"msg": "Missing time zone parameter","code": 400})
    if not date_format:
        return jsonify({"msg": "Missing date format parameter","code": 400})
    
    response = MongoAPI.regionalSettingsUpdate(current_user,currency,time_zone,date_format)
    if response=='Yes':
        return jsonify({"msg": "User regional settings successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/mailSignature', methods=['PUT'])
@jwt_required
def mailSignature():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    mail_signature =  request.json.get('mail_signature', None)
    
    if not mail_signature:
        return jsonify({"msg": "Missing mail signature parameter","code": 400})
        
    response = MongoAPI.mailSignatureUpdate(current_user,mail_signature)
    if response=='Yes':
        return jsonify({"msg": "User mail signature successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/changePassword', methods=['PUT'])
@jwt_required
def changePassword():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    old_password =  request.json.get('old_password', None)
    new_password =  request.json.get('new_password', None)

    
    if not old_password:
        return jsonify({"msg": "Missing old password parameter","code": 400})
    if not new_password:
        return jsonify({"msg": "Missing new password parameter","code": 400})


    old_password = base64.b64encode(old_password.encode())
    old_password = old_password.decode('utf-8')
    user = MongoAPI.getUserPasswordDetails(current_user,old_password)
    if not user:
        return jsonify({"msg": "Invalid old password","code": 400})
    else:
        new_password = base64.b64encode(new_password.encode())
        new_password=new_password.decode('utf-8')
        response = MongoAPI.changePassword(current_user,new_password)

    if response=='Yes':
        return jsonify({"msg": "User password successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/profileImage', methods=['PUT'])
@jwt_required 
def profile_image():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if 'file' not in request.files:
        resp = jsonify({'msg' : 'No file part in the request','code':'400'})
        return resp
    file = request.files['file']

    # print (file)

    if file.filename == '':
        resp = jsonify({'msg' : 'No file selected for uploading','code':'400'})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename,app_config.PROFILE_ALLOWED_EXTENSIONS):


        filename = secure_filename(file.filename)
        # filename = 'ggg'
        file_type = file.filename

        file_format = splitpart(file_type,-1,'.')
        file_name = splitpart(file_type,0,'.')
        filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
        file.save(os.path.join(app.config['UPLOAD_PROFILE_FOLDER'], filename1))

        response = MongoAPI.updateUserLogo(current_user,filename1)
        user = MongoAPI.getProfile(current_user)
        # user = Uid.fix_array1(user)
        user['profile_image'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_PROFILE_FOLDER'])+str(user['profile_image'])
        data = user

        resp = jsonify({'msg' : 'File successfully uploaded',"data":data,'code':'200'})
        return resp
    else:
        resp = jsonify({'msg' : 'Allowed file types are png, jpg, jpeg','code':'400'})
    return resp


@app.route('/profile', methods=['GET'])
@jwt_required
def profile():
	current_user = get_jwt_identity()
	
	
	if current_user:
		user = MongoAPI.getProfile(current_user)
		if user:

			# user = Uid.fix_array1(user)
			if not user['profile_image']:
				user['profile_image'] = ''
			else:
				user['profile_image'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_PROFILE_FOLDER'])+str(user['profile_image'])

			data = user 
			print(data)
			return jsonify({"msg": "User Info","data":data,"code": 200})
		else:
			return jsonify({"msg": "Oops,Something went wrong !","code": 400})
	else:
		return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/profile', methods=['PUT'])
@jwt_required
def profileUpdate():
	current_user = get_jwt_identity()
	if not request.is_json:
		return jsonify({"msg": "Missing JSON in request","code": 400})
	name =  request.json.get('name', None)
	phone =  request.json.get('phone', None)

	if not name:
		return jsonify({"msg": "Missing name parameter","code": 400})
	if not phone:
		return jsonify({"msg": "Missing phone parameter","code": 400})

	if current_user:
		user1 = MongoAPI.profileUpdate(current_user,name,phone)
		# print(current_user)
		user = MongoAPI.getProfile(current_user)
		# print(user)
        

		if user1:
			user = Uid.fix_array1(user)
			user['profile_image'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_PROFILE_FOLDER'])+str(user['profile_image'])
			data = user
            
			return jsonify({"msg": "User profile successfully updated !","data": data,"code": 200})
		else:
			return jsonify({"msg": "Oops,Something went wrong !","code": 400})
	else:
		return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/companyUpdate', methods=['PUT'])
@jwt_required 
def companyUpdate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if 'file' not in request.files:
        resp = jsonify({'msg' : 'No file part in the request','code':'400'})
        return resp
    file = request.files['file']

    # print (file)

    if file.filename == '':
        resp = jsonify({'msg' : 'No file selected for uploading','code':'400'})
        resp.status_code = 400
        return resp
    if file and allowed_file(file.filename,app_config.PROFILE_ALLOWED_EXTENSIONS):


        filename = secure_filename(file.filename)
        # filename = 'ggg'
        file_type = file.filename

        file_format = splitpart(file_type,1,'.')
        file_name = splitpart(file_type,0,'.')
        filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
        file.save(os.path.join(app.config['UPLOAD_COMPANY_FOLDER'], filename1))

        response = MongoAPI.updateOrganizationLogo(ordId,filename1)

        logo = str(app_config.BASE_URL)+str(app.config['UPLOAD_COMPANY_FOLDER'])+str(filename1)

        resp = jsonify({'msg' : 'File successfully uploaded','code':'200','logo':logo})
        return resp
    else:
        resp = jsonify({'msg' : 'Allowed file types are png, jpg, jpeg','code':'400'})
    return resp





@app.route('/company_profile', methods=['PUT'])
@jwt_required 
def company_profile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    elif not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    else:
        company_name =  request.json.get('company_name', None)
        phone =  request.json.get('phone', None)
        email =  request.json.get('email', None)
        website =  request.json.get('website', None)
        

        if not company_name:
            return jsonify({"msg": "Missing company name parameter","code": 400})
        if not phone:
            return jsonify({"msg": "Missing phone parameter","code": 400})
        if not email:
            return jsonify({"msg": "Missing email parameter","code": 400})
        if not website:
            return jsonify({"msg": "Missing website parameter","code": 400})

       
        response = MongoAPI.updateOrganization(ordId,company_name,phone,email,website)

        resp = jsonify({'msg' : 'Company settings successfully updated !','code':'200'})
        return resp
    return resp




@app.route('/company_profile', methods=['GET'])
@jwt_required 
def getCompanyProfile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        company = MongoAPI.getCompanyProfile(ordId)
        data = {'company':company}
        resp = jsonify({'msg' : 'Company profile !','code':'200','data':data})
        return resp




@app.route('/companyProfile', methods=['PUT'])
@jwt_required 
def companyProfile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    elif not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    else:
        address =  request.json.get('address', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        pcode =  request.json.get('pcode', None)
        country =  request.json.get('country', None)
        gstin =  request.json.get('gstin', None)
        pan_no =  request.json.get('pan_no', None)

        

        if not address:
            return jsonify({"msg": "Missing address parameter","code": 400})
        if not address2:
            return jsonify({"msg": "Missing address2 parameter","code": 400})
        if not city:
            return jsonify({"msg": "Missing city parameter","code": 400})
        if not state:
            return jsonify({"msg": "Missing state parameter","code": 400})
        if not pcode:
            return jsonify({"msg": "Missing pcode parameter","code": 400})
        if not country:
            return jsonify({"msg": "Missing country parameter","code": 400})
 

       
        response = MongoAPI.updateOrganizationData(ordId,address,address2,city,state,pcode,country,gstin,pan_no)

        resp = jsonify({'msg' : 'Company settings successfully updated !','code':'200'})
        return resp
    return resp

@app.route('/getEmailDetail/<id>', methods=['GET'])
@jwt_required 
def getEmailDetail(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        email = MongoAPI.getEmailDetail(id)
        data = {'email':email}
        resp = jsonify({'msg' : 'Email Details','code':'200','data':data})
        return resp

@app.route('/regional_settings', methods=['PUT'])
@jwt_required 
def regional_settings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    elif not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    else:
        currency =  request.json.get('currency', None)
        business_days =  request.json.get('business_days', None)
        time_zone =  request.json.get('time_zone', None)
        start_time =  request.json.get('start_time', None)
        date_format =  request.json.get('date_format', None)
        end_time =  request.json.get('end_time', None)
        
        

        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        # if not business_days:
        #     return jsonify({"msg": "Missing business days parameter","code": 400})
        if not time_zone:
            return jsonify({"msg": "Missing time zone parameter","code": 400})
        # if not start_time:
        #     return jsonify({"msg": "Missing start time parameter","code": 400})
        if not date_format:
            return jsonify({"msg": "Missing date format parameter","code": 400})
        # if not end_time:
        #     return jsonify({"msg": "Missing end time parameter","code": 400})

       
        response = MongoAPI.organizationUpdateData(ordId,currency,business_days,time_zone,start_time,date_format,end_time)

        resp = jsonify({'msg' : 'Company settings successfully updated !','code':'200'})
        return resp
    return resp


@app.route('/organization', methods=['GET'])
@jwt_required 
def organization():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
	# check if the post request has the file part
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])
        type = {'currency':'currency','country':'country','days':'days','date_format':'date_format','timezones':'timezones'}
        settings = defaultdict(list)
        for key in type:
            item = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array2(data)
            item = {key:data1}
            settings[key] = data1

        settings2 = {'currency':settings['currency'],'country':settings['country'],'days':settings['days'],'date_format':settings['date_format'],'timezones':settings['timezones']}

        data = {'organization':organization,'settings':settings2}
        resp = jsonify({'msg' : 'Organization Info','data':data,'code':'200'})
        return resp
    return resp



@app.route('/userChangeStatus/<id>', methods=['PUT'])
@jwt_required
def userChangeStatus(id):
    status =  request.json.get('status', None)
    
    if not status:
        return jsonify({"msg": "Missing status parameter","code": 400})
    
    response = MongoAPI.userChangeStatus(status,id)
    if response=='Yes':
        return jsonify({"msg": "User status successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/removePricelistCompany/<id>', methods=['GET'])
@jwt_required
def removePricelistCompany(id):
    response = MongoAPI.removePricelistCompany(id)
    if response=='Yes':
        return jsonify({"msg": "Company Removed Successfully !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    



# @app.route('/profile', methods=['PUT'])
# @jwt_required
# def profileUpdate():
#     current_user = get_jwt_identity()
#     current = authorizationCheck(current_user)
#     print (current)

    




@app.route('/pdfSettingsNew', methods=['POST'])
@jwt_required
def pdfSettingsNew():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        doc_type =  request.json.get('doc_type', None)
        name =  request.json.get('name', None)
       
        if not type:
            return jsonify({"msg": "Missing type parameter","code": 400})
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        
        org_id = int(ordId)

        check = MongoAPI.checkPDFSettings(type,org_id)
        # print (check)
        if check=='No':
            data1 = json.loads(json_util.dumps(request.json))
            response = MongoAPI.addPDFSettingsNew(org_id,data1)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings add successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings Already Added","code": 400})
            # data1 = json.loads(json_util.dumps(request.json))
            # response = MongoAPI.updatePDFSettingsNew(org_id,type,data1)
            # response1 = json.loads(json_util.dumps(response))

            # if response:
            #     return jsonify({"msg": "Settings Updated","code": 200,"response": response1})
            # else:
                # return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/pdfSettingsNew', methods=['PUT'])
@jwt_required
def pdfSettingsUpdate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
      
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.updatePDFSettings(org_id,data)
            response = json.loads(json_util.dumps(response1))
            data = {'type':response}
            return jsonify({"msg": "Settings Updated !","code": 200,"data": data})

        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/pdfSettingsNew/<doc_type>', methods=['GET'])
@jwt_required
def getpdfSettings(doc_type):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        # type =  request.args.get('type', None)
       
        if not doc_type:
            return jsonify({"msg": "Missing type parameter","code": 400})
        
        if ordId:
            response1 = MongoAPI.getPdfSettings(ordId,doc_type)
            response4 = Uid.fix_array(response1)
            # print(response4[''])
            # for res in response4:
            #     if not res['prepared_by_path']:

            #     print(res['prepared_by_path'])

            # if not user['p']:
			# 	user['profile_image'] = ''
			# else:
			# 	user['profile_image'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_PROFILE_FOLDER'])+str(user['profile_image'])
            data = response4
            if data:
                return jsonify({"msg": "PDF Settings!","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 500})

@app.route('/quoteBuilderItem', methods=['GET'])
@jwt_required
def getQuoteBuilderItem():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
       

        
        if ordId:
            response1 = MongoAPI.getQuoteBuilderSettings(ordId)
            response4 = Uid.fix_array(response1)
            data = response4
            if data:
                return jsonify({"msg": "PDF Settings!","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 500})

@app.route('/quoteBuilderItem', methods=['PUT'])
@jwt_required
def quoteBuilderItem():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
      
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.quoteBuilderItem(org_id,data)
            response = json.loads(json_util.dumps(response1))
            data = {'type':response}
            return jsonify({"msg": "Settings Updated !","code": 200,"data": data})

        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/settingsNew', methods=['POST'])
@jwt_required
def addSettingsNew():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        type =  request.json.get('type', None)
        name =  request.json.get('name', None)
        default = 0
        default =  request.json.get('default', default)

        
        if not type:
            return jsonify({"msg": "Missing user field type parameter","code": 400})
        if not name:
            return jsonify({"msg": "Missing user field name parameter","code": 400})
        
        org_id = int(ordId)

        check = MongoAPI.checkSettings(type,name,org_id)
        if check=='No':
            count = MongoAPI.fieldsSettingsCount(type,org_id)

            data1 = json.loads(json_util.dumps(request.json))
            
            response = MongoAPI.addSettingsNew(type,name,org_id,count,data1,default)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings add successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})

@app.route('/settingsDefault', methods=['POST'])
@jwt_required
def settingsDefault():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        type =  request.json.get('type', None)
        id = request.json.get('id',None)
        default = 0
        default =  request.json.get('default', default)

        
        if not type:
            return jsonify({"msg": "Missing user field type parameter","code": 400})
        if not id:
            return jsonify({"msg": "Missing id parameter","code": 400})
            
        
        org_id = int(ordId)
        data1 = json.loads(json_util.dumps(request.json))
        response = MongoAPI.settingsDefault(type,org_id,id)
        response1 = json.loads(json_util.dumps(response))
        if response:
            return jsonify({"msg": "Default set successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/settingsDefault/<field_type>', methods=['GET'])
@jwt_required
def getsettingsDefault(field_type):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
              
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})
           
        
        org_id = int(ordId)
        data1 = json.loads(json_util.dumps(request.json))
        response = MongoAPI.getDefault(org_id,field_type)
        response1 = json.loads(json_util.dumps(response))
        if response:
            return jsonify({"msg": "Default set successfully","code": 200,"response": response1[0]})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminEmailTemplates', methods=['POST'])
@jwt_required
def addAdminEmailTemplates():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    template_name =  request.json.get('template_name', None)
    template_content =  request.json.get('template_content', None)
    response = MongoAPI.addAdminEmailTemplates(template_name,template_content)
    response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "Template add successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/getAdminEmailTemplates/<template_name>', methods=['GET'])
@jwt_required
def getAdminEmailTemplate(template_name):

    response = MongoAPI.getAdminEmailTemplate(template_name)
    response1 = json.loads(json_util.dumps(response))
    # print(response1)
    if response:
        return jsonify({"msg": "Default set successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/emailTemplateDefault/<id>', methods=['GET'])
@jwt_required
def emailTemplateDefault(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not id:
            return jsonify({"msg": "Missing id parameter","code": 400})
            
        
        org_id = int(ordId)
        data1 = json.loads(json_util.dumps(request.json))
        response = MongoAPI.emailTemplateDefault(org_id,id)
        response1 = json.loads(json_util.dumps(response))
        if response:
            return jsonify({"msg": "Default set successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessDefault/<id>', methods=['GET'])
@jwt_required
def salesProcessDefault(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not id:
            return jsonify({"msg": "Missing id parameter","code": 400})
            
        
        org_id = int(ordId)
        data1 = json.loads(json_util.dumps(request.json))
        response = MongoAPI.salesProcessDefault(org_id,id)
        response1 = json.loads(json_util.dumps(response))
        if response:
            return jsonify({"msg": "Default set successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/settings', methods=['POST'])
@jwt_required
def addSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        type =  request.json.get('type', None)
        name =  request.json.get('name', None)
        info =  request.json.get('info', None)
        default = 0
        default =  request.json.get('default', default)
        weightage = 0
        weightage =  request.json.get('weightage', weightage)

        if not type:
            return jsonify({"msg": "Missing user field type parameter","code": 400})
        if not name:
            return jsonify({"msg": "Missing user field name parameter","code": 400})
        
        org_id = int(ordId)

        check = MongoAPI.checkSettings(type,name,org_id)
        if check=='No':
            count = MongoAPI.fieldsSettingsCount(type,org_id)

            data1 = json.loads(json_util.dumps(request.json))
            
            response = MongoAPI.addSettings(type,name,weightage,info,org_id,count,default)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings add successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})


@app.route('/emailTemplate', methods=['POST'])
@jwt_required
def emailTemplate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId =int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        name =  request.json.get('name', None)
        default = 0
        default =  request.json.get('default', default)
        template =  request.json.get('template', None)
        subject =  request.json.get('subject', None)


        if not name:
            return jsonify({"msg": "Missing user field name parameter","code": 400})
        if not template:
            return jsonify({"msg": "Missing template parameter","code": 400})
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        
        org_id = int(ordId)

        check = MongoAPI.checkEmailTemplate(name,org_id)
        if check=='No':
            count = MongoAPI.emailTemplateCount(org_id)

            data1 = json.loads(json_util.dumps(request.json))
            
            response = MongoAPI.addEmailTemplate(name,template,subject,org_id,count,default)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Email template add successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Email template is already add","code": 400})



@app.route('/emailTemplate/<id>', methods=['PUT'])
@jwt_required
def updateEmailTemplate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId =int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        name =  request.json.get('name', None)
        default = 0
        default =  request.json.get('default', default)
        template =  request.json.get('template', None)
        subject =  request.json.get('subject', None)


        if not name:
            return jsonify({"msg": "Missing user field name parameter","code": 400})
        if not template:
            return jsonify({"msg": "Missing template parameter","code": 400})
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        
        org_id = int(ordId)

        # check = MongoAPI.checkEmailTemplate(name,org_id)
        check='No'
        if check=='No':
           
            data1 = json.loads(json_util.dumps(request.json))

            id = ObjectId(id)
            
            response = MongoAPI.updateEmailTemplate(id,name,template,subject,org_id,default)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Email template update successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Email template is already update","code": 400})

@app.route('/emailTemplate/<id>', methods=['GET'])
@jwt_required
def getEmailTemplate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getEmailTemplateDetails(ordId,id)
            response = Uid.fix_array3(response1)
            data = {'template':response}

            if data:
                return jsonify({"msg": "Email template details!","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/rename', methods=['GET'])
@jwt_required
def getRenameDetails():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getRenameDetails(ordId)
            response = Uid.fix_array3(response1)
            data = {'rename':response}

            if data:
                return jsonify({"msg": "Rename details!","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/emailTemplate', methods=['GET'])
@jwt_required
def getEmailTemplates():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getEmailTemplates(ordId)
            response = Uid.fix_array(response1)
            data = {'template':response}

            if data:
                return jsonify({"msg": "Email template list!","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/emailTemplate/<id>', methods=['DELETE'])
@jwt_required
def deleteEmailTemplate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteEmailTemplate(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Email template delete successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/settings/<id>', methods=['PUT'])
@jwt_required
def updateSettings(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        type =  request.json.get('type', None)
        name =  request.json.get('name', None)
        default = 0
        default =  request.json.get('default', default)
        weightage = 0
        weightage =  request.json.get('weightage', weightage)

        info = '0'
        info =  request.json.get('info', info)


        
        if not type:
            return jsonify({"msg": "Missing user field type parameter","code": 400})
        if not name:
            return jsonify({"msg": "Missing user field name parameter","code": 400})
        
        org_id = int(ordId)

        # check = MongoAPI.checkEditSettings(type,name,org_id,id)
        check = 'No'
        if check=='No':         
            response = MongoAPI.updateSettings(org_id,name,weightage,id,type,default,info)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings update successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})



@app.route('/settings/<id>', methods=['DELETE'])
@jwt_required
def deleteSettings(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        
        org_id = int(ordId)
     
        response = MongoAPI.deleteSettings(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Settings delete successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        

@app.route('/contactSettingsList', methods=['GET'])
@jwt_required
def contactSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'source':'source','contact_tag':'contact_tag'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='source':
                title = 'Source'
            elif key=='contact_tag':
                title = 'Tag'
            item = {key:data1}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings': settings1,"titlelist":titlelist,"keylist":keylist,"code": 200})

@app.route('/tag/<name>', methods=['GET'])
@jwt_required
def tag(name):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if name=='deal':
            data = MongoAPI.fieldsSettingsData('deal_tag',ordId)
        elif name=='contact':
            data = MongoAPI.fieldsSettingsData('contact_tag',ordId)
        elif name=='company':
            data = MongoAPI.fieldsSettingsData('company_tag',ordId)
        data1 = Uid.fix_array(data)
        return jsonify({"msg": "Tag List",'tag': data1,"code": 200})

@app.route('/dealSettingsList', methods=['GET'])
@jwt_required
def dealSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'deal_stage':'deal_stage','deal_tag':'deal_tag','won_reason':'won_reason','lost_reason':'lost_reason'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            item1 = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='deal_stage':
                title = 'Deal Stage'
            elif key=='deal_tag':
                title = 'Deal Tag'
            elif key=='won_reason':
                title = 'Won Reason'
            elif key=='lost_reason':
                title = 'Lost Reason'
            item = {key:data1,'title':title}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings1': settings1,"titlelist":titlelist,"keylist":keylist,"code": 200,'settings':settings})


@app.route('/settingsList/<field_type>', methods=['GET'])
@jwt_required
def settingsList(field_type):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data = MongoAPI.fieldsSettingsData(field_type,ordId)
        data1 = Uid.fix_array(data)
        # type = {'deal_stage':'deal_stage','deal_tag':'deal_tag','won_reason':'won_reason','lost_reason':'lost_reason'}
        # settings = []
        # settings1 = []
        # titlelist = []
        # keylist = []
        # for key in type:
        #     item = []
        #     item1 = []
        #     data = MongoAPI.fieldsSettingsData(type[key],ordId)
        #     data1 = Uid.fix_array(data)
        #     if key=='deal_stage':
        #         title = 'Deal Stage'
        #     elif key=='deal_tag':
        #         title = 'Deal Tag'
        #     elif key=='won_reason':
        #         title = 'Won Reason'
        #     elif key=='lost_reason':
        #         title = 'Lost Reason'
        #     item = {key:data1,'title':title}
        #     settings.append(item)
        #     settings1.append(data1)
        #     titlelist.append(title)
        #     keylist.append(key)
        
        return jsonify({"msg": "Settings List",'data': data1,"code": 200})


@app.route('/accountSettingsList', methods=['GET'])
@jwt_required
def accountSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'customer_type':'customer_type'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            item1 = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='customer_type':
                title = 'Customer Type'
            item = {key:data1,'title':title}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings1': settings1,"titlelist":titlelist,"keylist":keylist,"code": 200,'settings':settings})

@app.route('/quoteSettingsList', methods=['GET'])
@jwt_required
def quoteSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'category':'category','validity':'validity','delivery_period':'delivery_period','shipping':'shipping','payment_terms':'payment_terms','bank_details':'bank_details','terms_condition':'terms_condition'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            item1 = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='quote_stage':
                title = 'Quote Stage'
            elif key=='category':
                title = 'Category'
            elif key=='validity':
                title = 'Validity'
            elif key=='shipping':
                title = 'Shipping'
            elif key=='payment_terms':
                title = 'Payment Terms'
            elif key=='delivery_period':
                title = 'Delivery Period'
            elif key=='bank_details':
                title = 'Bank Details'
            elif key=='terms_condition':
                title = 'Terms And Conditions'

            item = {key:data1,'title':title}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings': settings1,"titlelist":titlelist,"keylist":keylist,"code": 200})

@app.route('/proformaInvoiceSettingsList', methods=['GET'])
@jwt_required
def proformaInvoiceSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'proforma_stage':'proforma_stage','invoice_stage':'invoice_stage'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            item1 = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='proforma_stage':
                title = 'Proforma Stage'
            elif key=='invoice_stage':
                title = 'Invoice Stage'

            item = {key:data1,'title':title}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings': settings1,"titlelist":titlelist,"keylist":keylist,"code": 200})


@app.route('/productSettingsList', methods=['GET'])
@jwt_required
def productSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'product_category':'product_category','tax_type':'tax_type','tax_percentage':'tax_percentage','currency':'currency','uom':'uom'}
        settings = []
        settings1 = []
        titlelist = []
        keylist = []
        for key in type:
            item = []
            item1 = []
            data = MongoAPI.fieldsSettingsData(type[key],ordId)
            data1 = Uid.fix_array(data)
            if key=='product_category':
                title = 'Product Category'
            elif key=='tax_type':
                title = 'Tax Type'
            elif key=='tax_percentage':
                title = 'Tax %'
            elif key=='currency':
                title = 'Currency'
            elif key=='uom':
                title = 'UOM'
            item = {key:data1,'title':title}
            settings.append(item)
            settings1.append(data1)
            titlelist.append(title)
            keylist.append(key)
        
        return jsonify({"msg": "Settings List",'settings1': settings1,'settings': settings,"titlelist":titlelist,"keylist":keylist,"code": 200})

@app.route('/numberingSettingsList', methods=['GET'])
@jwt_required
def numberingSettingsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

        
    numbering = MongoAPI.numberingSettingsData(ordId)
    numbering = Uid.fix_array(numbering)  
    return jsonify({"msg": "Settings List",'settings': numbering,"code": 400}) 


@app.route('/addNumberingSettings', methods=['POST'])
@jwt_required
def addNumberingSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        type =  request.json.get('type', None)
        name =  request.json.get('name', None)
        prefix =  request.json.get('prefix', None)
        sequence =  request.json.get('sequence', None)

        
        if not type:
            return jsonify({"msg": "Missing type parameter","code": 400})
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        if not prefix:
            return jsonify({"msg": "Missing prefix parameter","code": 400})
        if not sequence:
            return jsonify({"msg": "Missing sequence parameter","code": 400})
        
        org_id = int(ordId)

        check = MongoAPI.addNumberingSettings(type,name,prefix,sequence,org_id)
        if check:
            count = MongoAPI.fieldsSettingsCount(type,org_id)
            
            response = MongoAPI.addSettings(type,name,org_id,count)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings add successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})




# @app.route('/contact', methods=['GET'])
# @jwt_required
# def contactList():
#     current_user = get_jwt_identity()
#     user = MongoAPI.authorizationCheck(current_user)
#     for user2 in user:
#         if user2=='org_id':
#             ordId = user[user2]

    
        
#     org_id = int(ordId)

#     check = MongoAPI.idData()
#     return jsonify({"msg": "Oops,Something went wrong !","code": 400,"check": check})


@app.route('/contact', methods=['POST'])
@jwt_required
def contactSubmit():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        name =  request.json.get('name', None)
        company_name =  request.json.get('company_name', None)
        company_id =  request.json.get('company_id', None)
        phone_number =  request.json.get('phone_number', None)
        email =  request.json.get('email', None)
        description =  request.json.get('description', None)
        assigned_to =  request.json.get('assigned_to', None)
        source =  request.json.get('source', None)
        whatsapp_no =  request.json.get('whatsapp_no', None)
        secondary_email =  request.json.get('secondary_email', None)
        designation =  request.json.get('designation', None)
        website =  request.json.get('website', None)
        facebook =  request.json.get('facebook', None)
        linkedin =  request.json.get('linkedin', None)
        twitter =  request.json.get('twitter', None)
        skype =  request.json.get('skype', None)
        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        country =  request.json.get('country', None)
        state =  request.json.get('state', None)
        city =  request.json.get('city', None)
        pincode =  request.json.get('pincode', None)

        
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        if not company_name:
            return jsonify({"msg": "Missing company name parameter","code": 400})
        if not phone_number:
            return jsonify({"msg": "Missing phone number parameter","code": 400})
        # if not email:
        #     return jsonify({"msg": "Missing email parameter","code": 400})
        # if not description:
        #     return jsonify({"msg": "Missing description parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned_to parameter","code": 400})

        if source=='':
            source = ''
        else:
            source = ObjectId(source)
        if pincode=='': 
            pincode = ''
        else:
            pincode = pincode
        
       
        id = TokenRefresh.idData()

        if not company_id:

            companies = {
                'company_name':company_name,
                'phone':phone_number,
                'email':email,
                'account_id':str(id),
                'assigned_to':assigned_to,
                'website':website,
                'address1':address1,
                'address2':address2,
                'country':country,
                'state':state,
                'city':city,
                'create_by':current_user,
                'org_id':ordId
                }
            if source:
                companies['source'] = source
            if pincode:
                companies['pincode'] = pincode
            

            data5 = json.loads(json_util.dumps(companies))
            response1 = MongoAPI.companyNewSubmit(ordId,current_user,data5)
            CompanyId =  ObjectId(response1)
                    
            action = 'CREATE'
            associate_to = 'company'
            associate_id = CompanyId
            via = 0
            extra_info = json.loads(json_util.dumps(data5))
            text_info = app_config.CREATE_INFO
            title = app_config.CREATE_TITLE
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

        else:
            CompanyId = ObjectId(company_id)
        
        company_id =  CompanyId
        data1 = json.loads(json_util.dumps(request.json))
        data = data1
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.contactSubmit(org_id,current_user,data,company_id,pincode,source)
            response2 = response1
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            
            response1 = MongoAPI.getContactDetails(org_id,response)
            # print (response1)
            response = Uid.fix_array3(response1)
            # contact = response

            # print (response)

            data = {'contact':response}

            if data:
                       
                action = 'CREATE'
                associate_to = 'contact'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Contact successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/contact/<id>', methods=['PUT'])
@jwt_required
def contactUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        name =  request.json.get('name', None)
        company_name =  request.json.get('company_name', None)
        company_id =  request.json.get('company_id', None)
        phone_number =  request.json.get('phone_number', None)
        email =  request.json.get('email', None)
        description =  request.json.get('description', None)
        assigned_to =  request.json.get('assigned_to', None)
        source =  request.json.get('source', None)
        whatsapp_no =  request.json.get('whatsapp_no', None)
        secondary_email =  request.json.get('secondary_email', None)
        designation =  request.json.get('designation', None)
        website =  request.json.get('website', None)
        facebook =  request.json.get('facebook', None)
        linkedin =  request.json.get('linkedin', None)
        twitter =  request.json.get('twitter', None)
        skype =  request.json.get('skype', None)
        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        country =  request.json.get('country', None)
        state =  request.json.get('state', None)
        city =  request.json.get('city', None)
        pincode =  request.json.get('pincode', None)

        
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        if not company_name:
            return jsonify({"msg": "Missing company name parameter","code": 400})
        if not phone_number:
            return jsonify({"msg": "Missing phone number parameter","code": 400})
        # if not email:
        #     return jsonify({"msg": "Missing email parameter","code": 400})
        # if not description:
        #     return jsonify({"msg": "Missing description parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned_to parameter","code": 400})

       
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.contactUpdate(org_id,current_user,data,id)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}

            if data:
                
                contact = MongoAPI.getContactDetails(ordId,id)
                # print (contact)
       
                action = 'UPDATE'
                associate_to = 'contact'
                associate_id = ObjectId(response)
                via = 0
                extra_info = json.loads(json_util.dumps(request.json))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)


                response = Uid.fix_array3(contact)
                data = {'contact':response}


                return jsonify({"msg": "Contact successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/setKeyContact/<id>', methods=['GET'])
@jwt_required
def setKeyContact(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        org_id = int(ordId)
        if org_id:
            contact_data = MongoAPI.getContactDetails(ordId,id)
            contact_data = Uid.fix_array3(contact_data)
            response1 = MongoAPI.setKeyContact(ordId,contact_data['company_id'],id)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}

            if data:
                
                contact = MongoAPI.getContactDetails(ordId,id)
                # print (contact)
       
                action = 'UPDATE'
                associate_to = 'contact'
                associate_id = ObjectId(response)
                via = 0
                extra_info = json.loads(json_util.dumps(request.json))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)


                response = Uid.fix_array3(contact)
                data = {'contact':response}


                return jsonify({"msg": "Contact successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/contact/<id>', methods=['GET'])
@jwt_required
def getContact(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getContactDetails(ordId,id)
            response = Uid.fix_array3(response1)
            data = {'contact':response}

            if data:
                return jsonify({"msg": "Contact successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})

@app.route('/getCompanyContact/<id>', methods=['GET'])
@jwt_required
def getCompanyContact(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getCompanyContacts(ordId,id)
            response = Uid.fix_array(response1)
            data = {'contact':response}

            if data:
                return jsonify({"msg": "Contact List !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})

@app.route('/cleardemodata', methods=['GET'])
@jwt_required
def clearDemoData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.clearDemoData(ordId)
            return jsonify({"msg": 'Demo Data Cleared',"code": 200})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
            
     
@app.route('/delete', methods=['GET'])
@jwt_required
def delete():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        
        
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
               
 
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.delete(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'followUp':response}

            if associate_to=='contact':
                mes = 'Contact successfully deleted !'
            elif associate_to=='company':
                mes = 'Company successfully deleted !'
            elif associate_to=='deal':
                mes = 'Deal successfully deleted !'
            elif associate_to=='quote':
                mes = 'Quote successfully deleted !'
            elif associate_to=='proforma':
                mes = 'Proforma successfully deleted !'
            elif associate_to=='invoice':
                mes = 'Invoice successfully deleted !'
            elif associate_to=='product':
                mes = 'Product successfully deleted !'
            else:
                mes = 'Successfully deleted !'


            if data:
                action = 'DELETE'
                associate_to = associate_to
                associate_id = id
                via = 0
                extra_info = json.loads(json_util.dumps(request.args))
                text_info = 'DELETE'
                title = 'DELETE'


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

                return jsonify({"msg": mes,"code": 200})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeStatus', methods=['GET'])
@jwt_required
def changeStatus():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        status =  request.args.get('status', None)
        
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        if not status:
            return jsonify({"msg": "Missing status","code": 400})
        
        changeData=defaultdict(list)
        changeData['associate_to']=associate_to
        changeData['associate_id']=id
        changeData['status']=status
 
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.changeStatus(org_id,changeData)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'followUp':response}

            if associate_to=='contact':
                mes = 'Contact status changed !'
            elif associate_to=='company':
                mes = 'Company status changed !'
            elif associate_to=='deal':
                mes = 'Deal status changed !'
            elif associate_to=='quote':
                mes = 'Quote status changed !'
            elif associate_to=='product':
                mes = 'Product status changed !'
            else:
                mes = 'Status changed successfully !'


            if data:
                action = 'STATUS_CHANGE'
                associate_to = associate_to
                associate_id = id
                via = 0
                extra_info = json.loads(json_util.dumps(request.args))
                text_info = 'STATUS_CHANGE'
                title = 'STATUS_CHANGE'


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

                return jsonify({"msg": mes,"code": 200})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/contactList', methods=['GET'])
@jwt_required
def contactList():
    current_user = get_jwt_identity()
    # print(current_user)
   
    user = MongoAPI.authorizationCheck(current_user)
    
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        tag =  request.args.get('tag', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not tag:
            tag=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getContactsList(ordId,length,page,search,user,source,tag,date_from,date_to,sort,order,current_user,city,state,country)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        contact_total = MongoAPI.getContactsCount(ordId)


        data = {'total_count':contact_total,'items':response1}
        return data

        # if data:
        #     return jsonify({'total_count':contact_total,'items':response1})
        # else:
        #     return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/contact', methods=['GET'])
@jwt_required
def contacts():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        tag =  request.args.get('tag', None)
        user =  request.args.get('user', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', None)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)
        orderdata = 'desc'
        order =  request.args.get('order', orderdata)

        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        if not order:
            order=orderdata
        
        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if not source:
            source=''
        if not tag:
            tag=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not sort_by:
            sort_by=sortBy

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by,order,current_user,city,state,country)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'contacts':response1}

        if data:
            return jsonify({"msg": "Contact List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
       
@app.route('/accountName/<name>', methods=['GET'])
@jwt_required
def accountName(name):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    if name=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        response1 = MongoAPI.getAccountNameSearch(ordId,name)
        
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        data = {'accounts':response1}

        if data:
            return jsonify({"msg": "Contact successfully created !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
            

@app.route('/accountSettings', methods=['GET'])
@jwt_required
def accountSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        users = MongoAPI.usersList(ordId)
        countries = MongoAPI.countriesList(current_user)
        source = MongoAPI.fieldsSettingsData('source',ordId)
        customer_type = MongoAPI.fieldsSettingsData('customer_type',ordId)
        pricelist = MongoAPI.getPricelistData(ordId)
        currency = MongoAPI.fieldsSettingsData('currency',ordId)  

        users = Uid.fix_array2(users)
        countries = Uid.fix_array2(countries)
        source = Uid.fix_array2(source)
        customer_type = Uid.fix_array2(customer_type)
        pricelist = Uid.fix_array2(pricelist)
        currencylist = Uid.fix_array2(currency)
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        settings = {'users':users,
                    'countries':countries,
                    'source':source,
                    'customer_type':customer_type,
                    'pricelist':pricelist,
                    'currencylist':currencylist,
                    'organization':organization
                    }


        # settings = {'users':users,'countries':countries,'source':source,'customer_type':customer_type}

    return jsonify({"msg": "Settings List",'data': settings,"code": 200})

@app.route('/accounts', methods=['POST'])
@jwt_required
def accounts():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        company_name =  request.json.get('company_name', None)
        phone =  request.json.get('phone', None)
        email =  request.json.get('email', None)
        assigned_to =  request.json.get('assigned_to', None)
        customer_type =  request.json.get('customer_type', None)
        gstin =  request.json.get('gstin', None)
        website =  request.json.get('website', None)
        source =  request.json.get('source', None)
        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        country =  request.json.get('country', None)
        state =  request.json.get('state', None)
        city =  request.json.get('city', None)
        pincode =  request.json.get('pincode', None)
        pricelist =  request.json.get('pricelist', None)
        currency =  request.json.get('currency', None)

        
        if not company_name:
            return jsonify({"msg": "Missing account parameter","code": 400})
        if not phone:
            return jsonify({"msg": "Missing phone parameter","code": 400})
        if not email:
            return jsonify({"msg": "Missing email parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned to parameter","code": 400})
        if not customer_type:
            return jsonify({"msg": "Missing customer type parameter","code": 400})
        
        if pincode=='':
            pincode = ''
        else:
            pincode = pincode
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.companySubmit(org_id,current_user,data,pincode)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            id = response
            

            if data:
                # print (response)

       
                action = 'CREATE'
                associate_to = 'company'
                associate_id = ObjectId(response)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

                response1 = MongoAPI.getCompanyDetails(org_id,response)
                response = Uid.fix_array3(response1)
                data = {'id':id,'company':response}


                return jsonify({"msg": "Company successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/accounts/<id>', methods=['GET'])
@jwt_required
def getAccount(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getCompanyDetails(ordId,id)
            # response1 = MongoAPI.getAccountsDetails(ordId,id)
            response = Uid.fix_array3(response1)
            # response = response1.to_json()
            # print (response)
            data = response

            if data:
                return jsonify({"msg": "Account details !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})





@app.route('/accounts/<id>', methods=['PUT'])
@jwt_required
def updateAccounts(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId=''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 600})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        company_name =  request.json.get('company_name', None)
        phone =  request.json.get('phone', None)
        email =  request.json.get('email', None)
        assigned_to =  request.json.get('assigned_to', None)
        customer_type =  request.json.get('customer_type', None)
        gstin =  request.json.get('gstin', None)
        website =  request.json.get('website', None)
        source =  request.json.get('source', None)
        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        country =  request.json.get('country', None)
        state =  request.json.get('state', None)
        city =  request.json.get('city', None)
        pincode =  request.json.get('pincode', None)
        pricelist =  request.json.get('pricelist', None)
        currency =  request.json.get('currency', None)

        
        if not company_name:
            return jsonify({"msg": "Missing account parameter","code": 400})
        if not phone:
            return jsonify({"msg": "Missing phone parameter","code": 400})
        if not email:
            return jsonify({"msg": "Missing email parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned to parameter","code": 400})
        if not customer_type:
            return jsonify({"msg": "Missing customer type parameter","code": 400})
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.companyUpdate(org_id,current_user,data,id)
            response = json.loads(json_util.dumps(response1))
            if data:
                action = 'UPDATE'
                associate_to = 'company'
                associate_id = ObjectId(id)
                via = 0
                extra_info = data1
                text_info = 'Company Updated'
                title = 'UPDATE'
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                response1 = MongoAPI.getCompanyDetails(org_id,id)
                response = Uid.fix_array3(response1)
                data = {'id':id,'company':response}
                return jsonify({"msg": "Company successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/companyContact/<id>', methods=['GET'])
@jwt_required
def companyContact(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getCompanyContacts(ordId,id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'contacts':response1}

        if data:
            return jsonify({"msg": "Contact List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})





@app.route('/numberingSettings/<id>', methods=['PUT'])
@jwt_required
def updateNumberingSettings(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        prefix =  request.json.get('prefix', None)
        sequence =  request.json.get('sequence', None)

        
        if not prefix:
            return jsonify({"msg": "Missing prefix parameter","code": 400})
        if not sequence:
            return jsonify({"msg": "Missing sequence name parameter","code": 400})
        
        org_id = int(ordId)

        # check = MongoAPI.checkEditSettings(type,name,org_id,id)
        check = 'No'
        if check=='No':         
            response = MongoAPI.updateNumberingSettings(prefix,sequence,id)
            response1 = json.loads(json_util.dumps(response))

            if response:
                return jsonify({"msg": "Settings update successfully","code": 200,"response": response1})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})


@app.route('/followUpCompleted', methods=['POST'])
@jwt_required
def followUpCompleted():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})


        id =  request.json.get('id', None)
        notes =  request.json.get('notes', None)
        
        
        
        if not id:
            return jsonify({"msg": "Missing followUp id parameter","code": 400})
        if not notes:
            return jsonify({"msg": "Missing notes parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.followUpCompleted(org_id,current_user,id,notes)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'COMPLETED'
                associate_to = 'associate_to'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = 'FOLLOW_UP_COMPLETED'
                title = 'FOLLOW_UP'


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Follow up successfully completed !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/followUpReschedule', methods=['POST'])
@jwt_required
def followUpReschedule():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})


        id =  request.json.get('id', None)
        time =  request.json.get('time', None)
        date =  request.json.get('date', None)
        
        
        
        if not id:
            return jsonify({"msg": "Missing followUp id parameter","code": 400})
        if not time:
            return jsonify({"msg": "Missing time parameter","code": 400})
        if not date:
            return jsonify({"msg": "Missing date parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.followUpReschedule(org_id,current_user,id,time,date)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'RESCHEDULE'
                associate_to = 'followUp'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.RESCHEDULE_INFO
                title = app_config.RESCHEDULE_TITLE


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Follow up successfully Rescheduled !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/follow_up', methods=['POST'])
@jwt_required
def follow_up():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})


       


        name =  request.json.get('name', None)
        follow_type =  request.json.get('follow_type', None)
        time =  request.json.get('time', None)
        date =  request.json.get('date', None)
        owner =  request.json.get('owner', None)
        sms =  request.json.get('sms', None)
        email =  request.json.get('email', None)
        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)
        
        
        if not name:
            return jsonify({"msg": "Missing name parameter","code": 400})
        if not follow_type:
            return jsonify({"msg": "Missing follow type parameter","code": 400})
        if not time:
            return jsonify({"msg": "Missing time parameter","code": 400})
        if not date:
            return jsonify({"msg": "Missing date parameter","code": 400})
        if not owner:
            return jsonify({"msg": "Missing owner parameter","code": 400})
        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.followUpSubmit(org_id,current_user,data)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'CREATE'
                associate_to = associate_to
                associate_id = associate_id
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Follow up successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/todayFollowUp', methods=['GET'])
@jwt_required
def todayFollowUp():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)

        assigned_to = current_user
 
        response1 = MongoAPI.getTodayFollowUpData(ordId,length,page,assigned_to,search_time,nextday,sort,order,'all')

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        deals_total = MongoAPI.getTodayFollowUpCount(ordId,current_user,search_time,nextday,'all')
        data = {'total_count':deals_total,'items':response1}
        return data


@app.route('/updateFieldSettings', methods=['POST'])
@jwt_required
def updateFieldSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.updateFieldSettings(org_id,current_user,data)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}
            return jsonify({"msg": "Field Settings Updated Successfully","code": 200})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/updatePriceList/<id>', methods=['PUT'])
@jwt_required
def updatePriceList(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.updatePriceList(id,data)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}
            return jsonify({"msg": "Pricelist Title Updated Successfully","code": 200})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/todayFollowUpOverdue', methods=['GET'])
@jwt_required
def todayFollowUpOverdue():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)

        assigned_to = current_user
 
        response1 = MongoAPI.getTodayFollowUpData(ordId,length,page,assigned_to,search_time,nextday,sort,order,'Overdue')

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        deals_total = MongoAPI.getDealsCountData(ordId)
        data = {'total_count':deals_total,'items':response1}
        return data


@app.route('/openEndDealsData', methods=['GET'])
@jwt_required
def openEndDealsData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=5
        if not order:
            order=''
        if not sort:
            sort=''
        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
      
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)

        assigned_to = current_user
 
        response1 = MongoAPI.getEndOpenDealsData(ordId,length,page,assigned_to,sort,order,search_time,nextday)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        deals_total = MongoAPI.getEndOpenDealsDataCount(ordId,assigned_to,search_time,nextday)
        data = {'total_count':deals_total,'items':response1}
        return data


@app.route('/overdueDealsData', methods=['GET'])
@jwt_required
def overdueDealsData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=5


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)

        assigned_to = current_user
 
        response1 = MongoAPI.overdueDealsData(ordId,length,page,assigned_to,sort,order,search_time)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        overdueCount = MongoAPI.getOverdueCount(ordId,current_user,search_time)
        data = {'total_count':overdueCount['count'],'items':response1}
        return data

@app.route('/unnoticedDealsData', methods=['GET'])
@jwt_required
def unnoticedDealsData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=5


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)

        assigned_to = current_user
 
        response1 = MongoAPI.unnoticedDealsData(ordId,length,page,assigned_to,sort,order,search_time)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        unnoticedDealsCount = MongoAPI.getUnnoticedCount(ordId,current_user,search_time)

        data = {'total_count':unnoticedDealsCount['count'],'items':response1}
        return data

@app.route('/dealsCounts', methods=['GET'])
@jwt_required
def dealsCount():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        search_time_from = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        search_time_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        open_deals_count = MongoAPI.openDealsCount(ordId,current_user)
        created_today_deals_count = MongoAPI.createdThisMonthDealsCount(ordId,current_user,search_time_from,search_time_end)
        won_deals_count = MongoAPI.wonDealsCount(ordId,current_user,search_time_from,search_time_end,'this_month')
        lost_deals_count = MongoAPI.lostDealsCount(ordId,current_user,search_time_from,search_time_end,'this_month')
        if not open_deals_count:
            open_deals_count = {'count':0,'total':0}
        if not created_today_deals_count:
            created_today_deals_count = {'count':0,'total':0}
        if not won_deals_count:
            won_deals_count = {'count':0,'total':0}
        if not lost_deals_count:
            lost_deals_count = {'count':0,'total':0}
        data = {
            'open_deals':open_deals_count,
            'created_today':created_today_deals_count,
            'won_deals':won_deals_count,
            'lost_deals':lost_deals_count
            }
        return data


@app.route('/openQuotesData', methods=['GET'])
@jwt_required
def openQuotesData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=5


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        # print('ttttttttttttttttttttttttt')
        # print(length)
        assigned_to = current_user
 
        response1 = MongoAPI.getOpenQuotesData(ordId,length,page,assigned_to,sort,order)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        quotesCount = MongoAPI.getDashboardQuotesCount(ordId,current_user)
        data = {'total_count':quotesCount['count'],'items':response1}
        return data

@app.route('/reorderSettings', methods=['POST'])
@jwt_required
def reorderSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        settings_type=  request.json.get('settings_type', None)
        settings_list =  request.json.get('settings_list', None)
        if not settings_type:
            return jsonify({"msg": "Missing Settings Type Parameter","code": 400})
        if not settings_list:
            return jsonify({"msg": "Missing Settings List parameter","code": 400})
        data1 = json.loads(json_util.dumps(request.json))
        data = data1
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.reorderSettings(org_id,current_user,data)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}
            return jsonify({"msg": "Reorder done!","code": 200,"data": data})

        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/dashboardMoble', methods=['GET'])
@jwt_required
def dashboardMoble():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        ordId = int(ordId)
        # today = date.now().date('%Y-%m-%d')
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)
        todayFollowUp = MongoAPI.getTodayFollowUp(ordId,current_user,search_time,nextday,'all')

        todayFollowUpComplete = MongoAPI.getTodayFollowUp(ordId,current_user,search_time,nextday,'Completed')
        todayFollowUpOverdue = MongoAPI.getTodayFollowUp(ordId,current_user,search_time,nextday,'Overdue')

        openDeals = MongoAPI.getOpenDeals(ordId,current_user)

        openDeals = Uid.fix_array(openDeals)

        unnoticedDeals = MongoAPI.getUnnoticed(ordId,current_user,search_time)
        unnoticedDeals = Uid.fix_array(unnoticedDeals)


        overdue = MongoAPI.getOverdue(ordId,current_user,search_time)
        overdueDeals = Uid.fix_array(overdue)
       
        quotes = MongoAPI.getOpenQuotes(ordId,current_user)
        OpenQuotes = Uid.fix_array(quotes)

        currency = MongoAPI.fieldsSettingsData('currency',ordId)
        currency = Uid.fix_array(currency)
        timezones = MongoAPI.fieldsSettingsData('timezones',ordId)
        timezones = Uid.fix_array(timezones)

        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)

        org_id = int(ordId)
        if org_id:
            # response1 = MongoAPI.getFollowUp(org_id,current_user,ObjectId(id),associate_to)
            # response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)

            roleData = user['roleData']

            rename = MongoAPI.getRenameDetails(ordId)
            rename1 = Uid.fix_array3(rename)
            
            data = {
                'roleData':roleData,
                'rename1':rename1,
                'todayFollowUp':todayFollowUp,
                'openQuotes':OpenQuotes,
                'overdueDeals':overdueDeals,
                'todayFollowUpComplete':todayFollowUpComplete,
                'todayFollowUpOverdue':todayFollowUpOverdue,
                'openDeals':openDeals,
                'unnoticedDeals':unnoticedDeals,
                'currency_list':currency,
                'timezone_list':timezones,
                'organization':organization
                }

            if data:
                # print (response)              
                return jsonify({"msg": "dashboard","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/dashboard', methods=['GET'])
@jwt_required
def dashboard():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        # today = date.now().date('%Y-%m-%d')
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        search_time = datetime.datetime(int(y),int(m),int(d),0,0)
        nextday = datetime.datetime(int(y),int(m),int(d),23,59)

        # This month        
        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        search_time_from = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        search_time_end = datetime.datetime(int(ey),int(em),int(ed),23,59)



        todayFollowUpCount = MongoAPI.getTodayFollowUpCount(ordId,current_user,search_time,nextday,'all')
        todayFollowUpOverdueCount = MongoAPI.getTodayFollowUpCount(ordId,current_user,search_time,nextday,'Overdue')
        openDealsCount = MongoAPI.getOpenDealsCount(ordId,current_user)

        unnoticedDealsCount = MongoAPI.getUnnoticedCount(ordId,current_user,search_time)

        todayDealsCount = MongoAPI.getTodayUnnoticedCount(ordId,current_user,search_time,nextday)

        overdueCount = MongoAPI.getOverdueCount(ordId,current_user,search_time)

        quotesCount = MongoAPI.getDashboardQuotesCount(ordId,current_user)

        wondeals = MongoAPI.wonDealsCount(ordId,current_user,search_time_from,search_time_end,'this_month')
        lostdeals = MongoAPI.lostDealsCount(ordId,current_user,search_time_from,search_time_end,'this_month')

        todayFollowUpComplete = MongoAPI.getTodayFollowUp(ordId,current_user,search_time,nextday,'Completed')
        todayFollowUpOverdue = MongoAPI.getTodayFollowUp(ordId,current_user,search_time,nextday,'Overdue')

        
        org_id = int(ordId)
        if org_id:
            data = {
                'todayFollowUpComplete':todayFollowUpComplete,
                'todayFollowUpOverdue':todayFollowUpOverdue,
                'quotesCount':quotesCount,
                'todayFollowUpCount':todayFollowUpCount,
                'todayFollowUpOverdueCount':todayFollowUpOverdueCount,
                'openDealsCount':openDealsCount,
                'unnoticedDealsCount':unnoticedDealsCount,
                'overdueCount':overdueCount,
                'todayDealsCount':todayDealsCount,
                'wondeals':wondeals,
                'lostdeals':lostdeals
                }
            if data:        
                return jsonify({"msg": "dashboard","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/reports/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def reports(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

          # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)

        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end
        
        if selected_user!='all':
            selected_user=ObjectId(selected_user)
        else:
            selected_user='all'

        
        total_deals_won = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'won',current_user)
        total_deals_lost = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'lost',current_user)
        total_deals = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'all',current_user)
        pipeline_deals = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'pipeline',current_user)
        new_business = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'new_business',current_user)
        repeat_business = MongoAPI.getDealsCountReport(ordId,selected_user,startDate,endDate,'repeat_business',current_user)
        quotes_total = MongoAPI.getQuotesCountReport(ordId,selected_user,startDate,endDate,'all',current_user)
        followup_open = MongoAPI.getFollowUpCountReport(ordId,selected_user,startDate,endDate,'open',current_user)
        followup_all = MongoAPI.getFollowUpCountReport(ordId,selected_user,startDate,endDate,'all',current_user)


        # Sales Revenue
        total_days = endDate - startDate
        sales_revenue=[]
        for i in range(total_days.days + 1):
            day_start=startDate + datetime.timedelta(days=i)
            day_end_convert=str(day_start).replace('00:00:00' , '23:59:00')
            day_end=datetime.datetime.strptime(day_end_convert, '%Y-%m-%d %H:%M:%S')
            this_day_count = MongoAPI.getDealsCountReport(ordId,selected_user,day_start,day_end,'won',current_user)
            this_day_unix=int(time.mktime(day_start.utctimetuple()) * 1000 + day_start.microsecond / 1000)
            this_array=[int(this_day_unix),int(this_day_count['total'])]
            sales_revenue.append(this_array)

        # Deals Pipeline
        deal_stage=MongoAPI.fieldsSettingsData('deal_stage',org_id)
        deal_stage=json.loads(json_util.dumps(deal_stage))
        deal_stage = list(deal_stage)
        sorted_array = sorted(deal_stage, key=lambda h: (h['sort_order']))
        deals_funnel = []
        for arr in sorted_array:
            for ar in arr:
                
                if ar == '_id':
                    stage_id = str(arr[ar]['$oid'])
                    this_stage_data = MongoAPI.getDealsCountReportStage(ordId,selected_user,startDate,endDate,stage_id,current_user)
                    if arr['name']!='won' and arr['name']!='Won' and arr['name']!='lost' and arr['name']!='Lost' and arr['name']!='Revise' and arr['name']!='revise':
                        this_array=[arr['name'],int(this_stage_data['total'])]
                        deals_funnel.append(this_array)
        
        # Contact Tags:
        contact_tag=MongoAPI.fieldsSettingsData('contact_tag',org_id)
        contact_tag=json.loads(json_util.dumps(contact_tag))
        contact_tag = list(contact_tag)
        
        tag_data=[]
        for tag in contact_tag:
            this_tag=MongoAPI.getTagContactCount(ordId,selected_user,tag['name'],current_user)
            this_array=[tag['name'],this_tag['count']]
            tag_data.append(this_array)

        if org_id:
            data = {
                'total_deals_won':total_deals_won,
                'total_deals_lost':total_deals_lost,
                'total_deals':total_deals,
                'pipeline_deals':pipeline_deals,
                'new_business':new_business,
                'repeat_business':repeat_business,
                'quotes_total':quotes_total,
                'followup_open':followup_open,
                'followup_all':followup_all,
                'sales_revenue':sales_revenue,
                'deals_funnel':deals_funnel,
                'tag_data':tag_data
                   }

        
            if data:        
                return jsonify({"msg": "Reports","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/revenue_by_owner/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def revenue_by_owner(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)

        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end

        user_data=[]

        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
            deal_users=[]
            deal_users.append(selected_user)
        else:
            selected_user = 'all'
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            deal_users=reporting_users['deal_user_list']
            

        for d_id in deal_users:
            this_user=MongoAPI.getUserDetails(ordId,d_id)
            this_user_deals=MongoAPI.getDealsCountReport(ordId,d_id,startDate,endDate,'all',current_user)
            this_user_quotes=MongoAPI.getQuotesCountReport(ordId,d_id,startDate,endDate,'all',current_user)
            this_followup_open = MongoAPI.getFollowUpCountReport(ordId,d_id,startDate,endDate,'open',current_user)
            this_followup_all = MongoAPI.getFollowUpCountReport(ordId,d_id,startDate,endDate,'all',current_user)
            this_notes = MongoAPI.getNotesCountReport(ordId,d_id,startDate,endDate)
            this_user_deals_won = MongoAPI.getDealsCountReport(ordId,d_id,startDate,endDate,'won',current_user)
            this_data={
                'name':this_user['name'],
                'deals_count':this_user_deals['count'],
                'deals_total':this_user_deals['total_amt'],
                'quotes_count':this_user_quotes['count'],
                'quotes_total':this_user_quotes['total'],
                'followup_total':this_followup_all['count'],
                'followup_open':this_followup_open['count'],
                'notes':this_notes['count'],
                'deals_won_count':this_user_deals_won['count'],
                'deals_won_total':this_user_deals_won['total_amt'],


            }
            user_data.append(this_data)


        

        # Revenuu by Owner:
        

        if org_id:
            data = {
                'revenue_by_owner':user_data,
                   }

        
            if data:        
                return jsonify({"msg": "Reports","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/expected_sales/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def expected_sales(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)
    
        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end


        # Revenue by Owner:
        # reporting_users=MongoAPI.getReportingUsers(ordId,selected_user)
        # deal_users=reporting_users['deal_user_list']
        # user_data=[]


        user_data=[]

        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
            deal_users=[]
            deal_users.append(selected_user)
        else:
            selected_user = 'all'
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            deal_users=reporting_users['deal_user_list']

        # print(deal_users)
        for d_id in deal_users:
            this_user=MongoAPI.getUserDetails(ordId,d_id)
            this_user_deals_nego = MongoAPI.getDealsCountReport(ordId,d_id,startDate,endDate,'Negotiation',current_user)
            this_user_deals_expect = MongoAPI.getDealsCountReport(ordId,d_id,startDate,endDate,'Expect',current_user)
            # print(this_user_deals_nego)
            # print(this_user_deals_expect)
            this_count = this_user_deals_nego['count'] + this_user_deals_expect['count']
            this_total = this_user_deals_nego['total'] + this_user_deals_expect['total']
            this_total = TokenRefresh.human_format(this_total)

            this_data={
                'name':this_user['name'],
                'deals_count':this_count,
                'deals_total':this_total
                

            }
            # print(this_data)
            user_data.append(this_data)

        if org_id:
            data = {
                'expected_sales':user_data,
                   }

        
            if data:        
                return jsonify({"msg": "Reports","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/quotes_report/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def quotes_report(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)

        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end
        
        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
        else:
            selected_user = 'all'
        quotes_data=MongoAPI.getQuotesListReport(ordId,selected_user,current_user)

        if org_id:
            data = {
                'quotes_data':quotes_data,
                   }

            if data:        
                return jsonify({"msg": "Reports","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/follow_up_report/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def follow_up_report(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)

        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end

        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
        else:
            selected_user='all'

        
        followup_data=MongoAPI.getFollowUpReports(ordId,selected_user,startDate,endDate,'Completed',current_user)

        if org_id:
            data = {
                'followup_data':followup_data,
                   }

            if data:        
                return jsonify({"msg": "Follow up Report","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/deal_business_report/<date_period>/<business_type>/<selected_user>', methods=['GET'])
@jwt_required
def deal_business_report(date_period,business_type,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)

        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end
        
        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
        else:
            selected_user = 'all'

        
        deals_data=MongoAPI.getDealsBusinessReport(ordId,selected_user,startDate,endDate,business_type,current_user)

        if org_id:
            data = {
                'deals_data':deals_data,
                   }

            if data:        
                return jsonify({"msg": "Deal Business Data","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/lost_deals_report/<date_period>/<selected_user>', methods=['GET'])
@jwt_required
def lost_deals_report(date_period,selected_user):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        org_id = int(ordId)
        # today

        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days

        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)


        # Last Month
        last_month_end = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=1)
        lmey = last_month_end.strftime('%Y')
        lmem = last_month_end.strftime('%m')
        lmed = last_month_end.strftime('%d')
        l_month_end = datetime.datetime(int(lmey),int(lmem),int(lmed),0,0)
        last_month_start = datetime.datetime.today().replace(day=1) - datetime.timedelta(days=last_month_end.day)
        lmsy = last_month_start.strftime('%Y')
        lmsm = last_month_start.strftime('%m')
        lmsd = last_month_start.strftime('%d')
        l_month_start = datetime.datetime(int(lmsy),int(lmsm),int(lmsd),23,59)
      


        if date_period == 'this_month':
            startDate = month_start
            endDate = month_end
        elif date_period == 'this_week':
            startDate = week_start
            endDate = week_end
        elif date_period =='today':
            startDate = today_start
            endDate = today_end
        elif date_period =='last_90_days':
            startDate = n_start
            endDate = today_end
        elif date_period =='last_month':
            startDate = l_month_start
            endDate = l_month_end
        if selected_user != 'all':
            selected_user=ObjectId(selected_user)
        else:
            selected_user='all'

        
        deals_data=MongoAPI.getDealsLostReport(ordId,selected_user,startDate,endDate,current_user)

        if org_id:
            data = {
                'deals_data':deals_data,
                   }

            if data:        
                return jsonify({"msg": "Deal Lost Data","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/follow_up', methods=['GET'])
@jwt_required
def getFollowUp():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})
        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        
        
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getFollowUp(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'followUp':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Company successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/notes', methods=['POST'])
@jwt_required
def notes():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
     


        note =  request.json.get('note', None)
        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)
        
        
        if not note:
            return jsonify({"msg": "Missing note parameter","code": 400})
        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.noteSubmit(org_id,current_user,data)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'ADD_NOTE'
                associate_to = associate_to
                associate_id = associate_id
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Notes successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/helpSupport', methods=['POST'])
@jwt_required
def helpSupport():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='email':
            emailId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        content =  request.json.get('content', None)
        # print('testtt')
        # org_id = 
        associate_id =  current_user
        associate_to =  'help_support'
        attachment_list =  request.json.get('attachment_list', None)
        request.json.get('fromEmail', emailId)
        

        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not content:
            return jsonify({"msg": "Missing email message parameter","code": 400})
        
       

        EmailData = defaultdict(list)
        EmailData['to']=app.config['SUPPORT_MAIL_ID']
        EmailData['subject']=subject
        EmailData['content']=content
        # EmailData['associate_to']='support_ticket'
        # EmailData['associate_id']=ticket_id
        
        data1 = json.loads(json_util.dumps(EmailData))

        data = data1
        
        org_id = int(ordId)
        
        numbering = MongoAPI.getNumberingSettings('ticket',0)
        if not numbering:
            numbering = MongoAPI.getNumberingSettings2('ticket',0)

        no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
        ticket = MongoAPI.ticketSubmit(ordId,current_user,no,data,emailId,attachment_list)
        ticket_id = json.loads(json_util.dumps(ticket))
        update_no = int(numbering['sequence'])+1
        MongoAPI.numberingUpdate(numbering['id'],update_no)

        EmailData['subject']=no+'-'+subject
        EmailData['associate_id']=ticket_id
        EmailData['associate_to']='support_ticket'
        EmailData['cc']='tester@skyzon.com'
        EmailData['bcc']='swami@skyzon.com,developer2@skyzon.com'

        data1 = json.loads(json_util.dumps(EmailData))
        data = data1
        

        if org_id:
            emailId='info@farazon.com'
            response1 = MongoAPI.emailSubmit(org_id,current_user,data,emailId,attachment_list)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'SEND_EMAIL'
                associate_to = associate_to
                associate_id = associate_id
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.SEND_EMAIL_INFO
                title = app_config.SEND_EMAIL


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Ticket Raised, Farazon team will contact you soon","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route("/sendEmail1")
def sendEmail1():

#    msg = Message('Hello', sender = 'tester@skyzon.com', recipients = ['developer2@skyzon.com'])
#    msg.body = "This is the email body"
#    mail.send(msg)
   return "Sent"


@app.route('/sendEmail', methods=['GET'])
# @jwt_required
def sendEmail():
    ordId = '1'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = '1'

        if org_id:
            response1 = MongoAPI.getEmailAll()
            response = json.loads(json_util.dumps(response1))
            if len(response) !=0:
                for mail_data in response:
                    msg = Message(
                        subject=mail_data['subject'],
                        recipients=mail_data['to'], 
                        # body=mail_data['content'], 
                        sender=mail_data['fromEmail'], 
                        cc=mail_data['cc'], 
                        bcc=mail_data['bcc'], 
                        html=mail_data['content']
                    )
        
                    if mail_data['attachment'] !='':
                        if mail_data['attachment'] and len(mail_data['attachment']) > 0:
                            
                            for files in mail_data['attachment']:
                                if files != '':
                                    file_data=files[files.find('uploads'):]
                                    if file_data and file_data !='':
                                        mime = magic.from_file(file_data, mime=True)
                                        with open(file_data,'rb') as f:
                                            fname=file_data.rsplit('/', 1)
                                            msg.attach(filename=fname[1], content_type=mime, data=f.read(), disposition=None, headers=None) 
                    try:
                        mail.send(msg)
                        MongoAPI.emailSent(mail_data['_id'])
                        return jsonify({"msg": "Mail sent","code": 200})
                    except NotUniqueError as e:
                        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
            else:
                return jsonify({"msg": "No Emails to send","code": 200})
            

@app.route('/email', methods=['POST'])
@jwt_required
def email():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='email':
            emailId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.form.get:
            return jsonify({"msg": "Missing JSON in request"})

        to =  request.form.get('to', None)
        cc =  request.form.get('cc', None)
        bcc =  request.form.get('bcc', None)
        subject =  request.form.get('subject', None)
        content =  request.form.get('content', None)
        # emailId = request.form.get('fromMail',None)
        associate_id =  request.form.get('associate_id', None)
        associate_to =  request.form.get('associate_to', None)
        attachment_list =  request.form.get('attachment_list', None)
        request.form.get('fromEmail', emailId)
        to_list = to.split(',')
        EmailData = defaultdict(list)
        EmailData = request.form

        if not to:
            return jsonify({"msg": "Missing to email id parameter","code": 400})
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not content:
            return jsonify({"msg": "Missing email content parameter","code": 400})
        if not emailId:
            return jsonify({"msg": "Missing from content parameter","code": 400})
        if not associate_id:
            return jsonify({"msg": "Missing from id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing from associate to parameter","code": 400})


        allowed = app_config.ALLOWED_EXTENSIONS

        # if 'attachment[]' not in request.files:
        #     attachment = []
        # else:
        #     attachment = []
        #     files = request.files.getlist('attachment[]')
        #     for file in files:
        #         exists = file.filename in attachment_list
        #         if exists == True:
        #             if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
        #                 filename = secure_filename(file.filename)
        #                 file.save(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename))
        #                 attachment.append(str(filename))
        
        # msg = Message('Hello',sender =emailId,recipients = [to]) 
        # msg.body = 'Hello Flask message sent from Flask-Mail'
        # print (msg)
        # mail.send(msg)

        
        data1 = json.loads(json_util.dumps(EmailData))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.emailSubmit(org_id,current_user,data,emailId,attachment_list)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                # print (response)

       
                action = 'SEND_EMAIL'
                associate_to = associate_to
                associate_id = associate_id
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.SEND_EMAIL_INFO
                title = app_config.SEND_EMAIL


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Email successfully sent !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/email', methods=['GET'])
@jwt_required
def getEmail():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})


        # page =  request.args.get('page', None)
        # length =  request.args.get('length', None)
        # search =  request.args.get('search', None)


        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        
        
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getEmail(org_id,current_user,ObjectId(id),associate_to)
            
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'email':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Company successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/emailListAssociate', methods=['GET'])
@jwt_required
def getEmailAssociate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        associate_id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        sort='create_date'
        if not page:
            page=1
        if not length:
            length=10

        length = int(length)
        page = int(page)



        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        
        
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})

        response1 = MongoAPI.getEmailAssociate(ordId,length,page,sort,order,associate_id,associate_to)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        email_total = MongoAPI.getEmailAssociateCount(ordId,associate_id,associate_to)
        data = {'total_count':email_total,'items':response1}
        return data


@app.route('/documentCommon', methods=['POST'])
@jwt_required 
def documentCommon():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':
       
        associate_to =  'common'
        org_info=MongoAPI.organizationInfo(ordId)
        id=org_info[0]['_id']
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
     
        if 'document[]' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            filenameData = []
            files = request.files.getlist('document[]')
            for file in files:
                if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
                    file.save(os.path.join(app.config['UPLOAD_COMMON_DOCUMENT_FOLDER']+'/'+str(ordId), filename1))
                    filenameData.append(filename1)
                    response1 = MongoAPI.documentSubmit(ordId,current_user,id,associate_to,filename1,filename)
                    response1 = str(response1)
                    data = {'document':response1}

                    if data:
                        # print (response)
                        action = 'UPLOAD_DOCUMENT'
                        associate_to = associate_to
                        associate_id = id
                        via = 0
                        extra_info = json.loads(json_util.dumps(request.form))
                        text_info = "Upload Document"
                        title = "Upload Document"
                        MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                        return jsonify({"msg": "Document upload successfully !","code": 200,"data": data})
                    else:
                        mes = "Something Went Wrong"
                        return jsonify({"msg": mes,"code": 400})
                else:
                        mes = str('Allowed file types are ')+str(app_config.ALLOWED_EXTENSIONS)
                        return jsonify({"msg": mes,"code": 400})

@app.route('/documentCommon', methods=['GET'])
@jwt_required
def getDocumentCommon():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        associate_to =  'common'
        org_info=MongoAPI.organizationInfo(ordId)
        id=org_info[0]['_id']
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getDocumentCommon(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'document':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Document list","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/documentCommon/<id>', methods=['DELETE'])
@jwt_required
def deleteDocumentCommon(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteDocumentCommon(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Document Deleted successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/document', methods=['POST'])
@jwt_required
def document():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='email':
            emailId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.form.get:
            return jsonify({"msg": "Missing from in request"})
        id =  request.form.get('associate_id', None)
        associate_to =  request.form.get('associate_to', None)

        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
        if 'document' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        
        file = request.files['document']

        if file.filename == '':
            resp = jsonify({'msg' : 'No file selected for uploading','code':'400'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
            
            
            filename = secure_filename(file.filename)
            # filename = 'ggg'
            file_type = file.filename
        
            file_format = splitpart(file_type,1,'.')
            file_name = splitpart(file_type,0,'.')
            filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
            file.save(os.path.join(app.config['UPLOAD_DOCUMENT_FOLDER'], filename1))


            response1 = MongoAPI.documentSubmit(ordId,current_user,id,associate_to,filename1,filename)
            response1 = str(response1)
            data = {'document':response1}


        if data:
            # print (response)
            action = 'UPLOAD_DOCUMENT'
            associate_to = associate_to
            associate_id = id
            via = 0
            extra_info = json.loads(json_util.dumps(request.form))
            text_info = "Upload Document"
            title = "Upload Document"
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
            return jsonify({"msg": "Document upload successfully !","code": 200,"data": data})
        else:
            mes = str('Allowed file types are ')+str(app_config.ALLOWED_EXTENSIONS)
            return jsonify({"msg": mes,"code": 200})

@app.route('/document', methods=['GET'])
@jwt_required
def getDocument():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
               
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getDocument(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'document':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Document list","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/document/<id>', methods=['DELETE'])
@jwt_required
def deleteDocument(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteDocument(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Document Deleted successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/emailDocuments', methods=['POST'])
@jwt_required
def emailDocuments():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'document[]' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            filenameData = []
            files = request.files.getlist('document[]')
            for file in files:
                if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    file.save(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename1))
                    this_file={
                        'name':filename,
                        'url':os.path.join(str(app_config.BASE_URL),app.config['UPLOAD_EMAIL_FOLDER'], filename1)
                    }
                    filenameData.append(this_file)
            return jsonify({"msg": "File(s) Added","code": 200,"filelist":filenameData})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})
    
@app.route('/dealDocuments', methods=['POST'])
@jwt_required
def dealDocuments():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'document[]' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            filenameData = []
            files = request.files.getlist('document[]')
            for file in files:
                if file and allowed_file(file.filename,app_config.PROFILE_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    file.save(os.path.join(app.config['UPLOAD_DEAL_FOLDER'], filename1))
                    filenameData.append(filename1)
                    data={
                        'file_name':file_name,
                        'file_path':str(app_config.BASE_URL)+str(app.config['UPLOAD_DEAL_FOLDER'])+str(filename1),
                        'file_name_src':filename1
                    }
            process_data = json.loads(json_util.dumps(request.form))
            process_data['list_value']=str(app_config.BASE_URL)+str(app.config['UPLOAD_DEAL_FOLDER'])+str(filename1)
            if process_data['process_type']=='main':
                response1 = MongoAPI.updateCheckListData(ordId,current_user,process_data)
            elif process_data['process_type']=='sub':
                response1 = MongoAPI.updateCheckListSubData(ordId,current_user,process_data)

            
            return jsonify({"msg": "File(s) Added","code": 200,"data":data})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})

@app.route('/dealDocuments/<process_id>/<index>/<process_type>', methods=['DELETE'])
@jwt_required
def dealDocumentsRemove(process_id,index,process_type):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if process_type=='main':
        response1 = MongoAPI.dealDocumentsRemove(ordId,process_id,index)
    elif process_type=='sub':
        response1 = MongoAPI.dealDocumentsSubRemove(ordId,process_id,index)

    return jsonify({"msg": "File(s) Deleted","code": 200})


@app.route('/pdfDocument', methods=['POST'])
@jwt_required
def pdfDocument():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='email':
            emailId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.form.get:
            return jsonify({"msg": "Missing from in request"})
        doc_id =  request.form.get('doc_id', None)
        field_type =  request.form.get('field_type', None)

        if not doc_id:
            return jsonify({"msg": "Missing id parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing Field Type parameter","code": 400})
        
        if 'document' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        
        file = request.files['document']

        if file.filename == '':
            resp = jsonify({'msg' : 'No file selected for uploading','code':'400'})
            resp.status_code = 400
            return resp
        if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
            
            
            filename = secure_filename(file.filename)
            # filename = 'ggg'
            file_type = file.filename
        
            file_format = splitpart(file_type,1,'.')
            file_name = splitpart(file_type,0,'.')
            filename1 = doc_id + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format

            file.save(os.path.join(app.config['UPLOAD_PDF_IMAGE_FOLDER'],str(ordId),filename1))

       
            response1 = MongoAPI.pdfDocumentUpdate(ordId,doc_id,field_type,filename1)
            response1 = str(response1)
            data = {'document':response1}


        if data:
            # print (response)
            action = 'UPLOAD_DOCUMENT'
            associate_to = 'PDF'
            associate_id = doc_id   
            via = 0
            extra_info = json.loads(json_util.dumps(request.form))
            text_info = "Upload Document"
            title = "Upload Document"
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
            return jsonify({"msg": "Document upload successfully !","code": 200,"data": data})
        else:
            mes = str('Allowed file types are ')+str(app_config.ALLOWED_EXTENSIONS)
            return jsonify({"msg": mes,"code": 400})

@app.route('/notes', methods=['GET'])
@jwt_required
def getNotes():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
               
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getNotes(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'notes':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Notes list","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/productsSettings', methods=['GET'])
@jwt_required
def productsSettings():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    category = MongoAPI.fieldsSettingsData('product_category',ordId)
    category = Uid.fix_array(category)
    data = {'category':category}
    return jsonify({"msg": "Settings List",'data': data,"code": 200})

@app.route('/products', methods=['GET'])
@jwt_required
def productsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page1 = 1
        page =  request.args.get('page', page1)
        length1 = 50000
        length =  request.args.get('length', length1)
        search =  request.args.get('search', None)
        category =  request.args.get('category', None)
        sortBy = 'name'
        sort_by =  request.args.get('sort_by', sortBy)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        status =  request.args.get('status', None)
     
        if not status:
            status=''

        if status=='all':
            status=''

         
        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''

        if not sort_by:
            sort_by=sortBy
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getProducts(ordId,length,page,search,category,sort_by,date_from,date_to,status)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'products':response1}

        if data:
            return jsonify({"msg": "products List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/products/<id>', methods=['GET'])
@jwt_required
def getProduct(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            response1 = MongoAPI.getProductDetails(ordId,id)
            response = Uid.fix_array3(response1)
            data = response

            if data:
                return jsonify({"msg": "Contact successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})

@app.route('/products/<id>', methods=['PUT'])
@jwt_required
def productUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        product_name =  request.json.get('product_name', None)
        price =  request.json.get('price', None)
        category =  request.json.get('category', None)
        model_no =  request.json.get('model_no', None)
        hsn_code =  request.json.get('hsn_code', None)
        uom =  request.json.get('uom', None)
        description =  request.json.get('description', None)
        
        
        
        if not product_name:
            return jsonify({"msg": "Missing product name parameter","code": 400})
        if not price:
            return jsonify({"msg": "Missing price parameter","code": 400})
        if not category:
            return jsonify({"msg": "Missing category parameter","code": 400})
        # if not model_no:
        #     return jsonify({"msg": "Missing model no parameter","code": 400})
        # if not hsn_code:
        #     return jsonify({"msg": "Missing hsn code parameter","code": 400})
        # if not uom:
        #     return jsonify({"msg": "Missing uom parameter","code": 400})
        # if not description:
        #     return jsonify({"msg": "Missing description parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.productUpdate(org_id,current_user,data1,id)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                action = 'UPDATE'
                associate_to = 'product'
                associate_id = ObjectId(id)
                via = 0
                extra_info = data1
                text_info = 'Product Updated'
                title = 'Updated'


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Product successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/rename/<id>', methods=['PUT'])
@jwt_required
def renameUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        contact_singular =  request.json.get('contact_singular', None)
        contact_plural =  request.json.get('contact_plural', None)
        company_singular =  request.json.get('company_singular', None)
        company_plural =  request.json.get('company_plural', None)
        deal_singular =  request.json.get('deal_singular', None)
        deal_plural =  request.json.get('deal_plural', None)

        
        
        
        if not contact_singular:
            return jsonify({"msg": "Missing contact singular parameter","code": 400})
        if not contact_plural:
            return jsonify({"msg": "Missing contact plural parameter","code": 400})
        if not company_singular:
            return jsonify({"msg": "Missing company singular parameter","code": 400})
        if not company_plural:
            return jsonify({"msg": "Missing company plural parameter","code": 400})
        if not deal_singular:
            return jsonify({"msg": "Missing deal singular parameter","code": 400})
        if not deal_plural:
            return jsonify({"msg": "Missing deal plural parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.renameUpdate(org_id,current_user,data1,id)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                action = 'UPDATE'
                associate_to = 'rename'
                associate_id = ObjectId(id)
                via = 0
                extra_info = data1
                text_info = 'Rename Updated'
                title = 'Updated'


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Rename successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

            
@app.route('/productsList', methods=['GET'])
@jwt_required
def getProductsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page1 = 1
        page =  request.args.get('page', page1)
        length1 = 10
        length =  request.args.get('length', length1)
        search =  request.args.get('search', None)
        category =  request.args.get('category', None)
        sortBy = 'name'
        sort =  request.args.get('sort', sortBy)
        order =  request.args.get('order', None)
        status =  request.args.get('status', None)
        
        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        if not status:
            status=''

        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getProductsListData(ordId,length,page,search,category,sort,order,status)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        products_total = MongoAPI.getProductsCount(ordId)


        data = {'total_count':products_total,'items':response1}
        return data

@app.route('/productsListPricelist', methods=['GET'])
@jwt_required
def productsListPricelist():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page1 = 1
        page =  request.args.get('page', page1)
        length1 = 10
        length =  request.args.get('length', length1)
        search =  request.args.get('search', None)
        category =  request.args.get('category', None)
        sortBy = 'name'
        sort =  request.args.get('sort', sortBy)
        order =  request.args.get('order', None)
        status =  request.args.get('status', None)
        pricelist_id =  request.args.get('pricelist_id', None)
        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        if not status:
            status=''

        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = MongoAPI.productsListPricelist(ordId,length,page,search,category,sort,order,status,pricelist_id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        products_total = MongoAPI.getProductsCount(ordId)


        data = {'total_count':products_total,'items':response1}
        return data

@app.route('/products', methods=['POST'])
@jwt_required
def products():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        product_name =  request.json.get('product_name', None)
        price =  request.json.get('price', None)
        category =  request.json.get('category', None)
        model_no =  request.json.get('model_no', None)
        hsn_code =  request.json.get('hsn_code', None)
        uom =  request.json.get('uom', None)
        description =  request.json.get('description', None)
        
        
        
        if not product_name:
            return jsonify({"msg": "Missing product name parameter","code": 400})
        if not price:
            return jsonify({"msg": "Missing price parameter","code": 400})
        if not category:
            return jsonify({"msg": "Missing category parameter","code": 400})
        # if not model_no:
        #     return jsonify({"msg": "Missing model no parameter","code": 400})
        # if not hsn_code:
        #     return jsonify({"msg": "Missing hsn code parameter","code": 400})
        # if not uom:
        #     return jsonify({"msg": "Missing uom parameter","code": 400})
        # if not description:
        #     return jsonify({"msg": "Missing description parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.productSubmit(org_id,current_user,data1)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'id':response}

            if data:
                action = 'CREATE'
                associate_to = 'product'
                associate_id = ObjectId(response)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE


                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Product successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})




@app.route('/dealsSettings', methods=['GET'])
@jwt_required
def addDeals():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
  
    users = MongoAPI.usersList(ordId)
    stage = MongoAPI.fieldsSettingsData('stage',ordId)
    # products = MongoAPI.getProductsList(ordId)
    stage = Uid.fix_array(stage)
    users = Uid.fix_array(users)

    currency = MongoAPI.fieldsSettingsData('currency',ordId)
    currency = Uid.fix_array(currency)

    deal_tag = MongoAPI.fieldsSettingsData('deal_tag',ordId)
    deal_tag = Uid.fix_array(deal_tag)
    
    organization = MongoAPI.organizationInfo(ordId)
    organization = Uid.fix_array1(organization)
    if organization['logo']:
        organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])
    
    data = {'users':users,'stage':stage,'currency':currency,'deal_tag':deal_tag,'organization':organization}
    return jsonify({"msg": "Settings List",'data': data,"code": 200})



@app.route('/quoteCount', methods=['GET'])
@jwt_required
def getQuoteCount():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data = MongoAPI.fieldsSettingsData('deal_stage',ordId)
        data = Uid.fix_array2(data)
        settings6=[]
        this_month=MongoAPI.getQuoteCreatedThisMonth(ordId,current_user)
        won_this_month=MongoAPI.getQuoteWonThisMonth(ordId,current_user)
        lost_this_month=MongoAPI.getQuoteLostThisMonth(ordId,current_user)
        if data:
            return jsonify({
                "data":
                    {
                        "created_this_month": this_month,
                        "won_this_month":won_this_month,
                        "lost_this_month":lost_this_month
                    },
                "code": 200,
                "msg": "Quote List",
                "created_this_month": this_month,
                "won_this_month":won_this_month,
                "lost_this_month":lost_this_month

                }
            )
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/quote', methods=['GET'])
@jwt_required
def getQuotes():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('quote_stage', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)
        quote_category =  request.args.get('quote_category', None)

        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not sort_by:
            sort_by=sortBy
        if not quote_category:
            quote_category=''
            
        length = int(length)
        page = int(page)

        response1 = MongoAPI.getQuotes(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort_by,current_user,quote_category)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        data = {'quotes':response1}


        if data:
            return jsonify({"msg": "Quotes List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/dealsQuoteList', methods=['GET'])
@jwt_required
def getDealsQuoteList():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        associate_id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)


        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('stage', None)
        assigned_to =  request.args.get('assigned_to', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=1000
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not assigned_to:
            assigned_to=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)



        settingsData = defaultdict(list)

        settingsData['ordId'] = ordId
        settingsData['length'] = length
        settingsData['page'] = page
        settingsData['search'] = search
        settingsData['assigned_to'] = assigned_to
        settingsData['stage'] = stage
        settingsData['sort'] = sort
        settingsData['order'] = order
        settingsData['associate_id'] = associate_id
        settingsData['associate_to'] = associate_to

        
        
        response1 = MongoAPI.getDelasQuotesList(settingsData)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        deals_total = MongoAPI.getDealsCount1(settingsData)
        # data = {'total_count':deals_total,'items':response1}
        # return data

        data = {'quotes':response1,'total_count':deals_total,'items':response1}


        if data:
            return jsonify({"msg": "Quotes List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})




@app.route('/quoteList', methods=['GET'])
@jwt_required
def getQuoteList():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:


        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('quote_stage', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        quote_category =  request.args.get('quote_category', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not quote_category:
            quote_category=''


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        length = int(length)
        page = int(page)
        # print(length)

        response1 = MongoAPI.getQuotesList(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user,quote_category)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        deals_total = MongoAPI.getQuotesCountData(ordId,search,assigned_to,stage,date_from,date_to,current_user,quote_category)
        data = {'total_count':deals_total,'items':response1}
        return data


        # if data:
        #     return jsonify({"msg": "Quotes List","code": 200,"data": data})
        # else:
        #     return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/quoteListAssociate', methods=['GET'])
@jwt_required
def getQuoteListAssociate():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:


        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        associate_to =  request.args.get('associate_to', None)
        associate_id =  request.args.get('associate_id', None)

        if not page:
            page=1
        if not length:
            length=10
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        sort='create_date'

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getQuotesListAssociate(ordId,length,page,sort,order,associate_id,associate_to)
       
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        deals_total = MongoAPI.getQuotesCountDataAssociate(ordId,associate_id,associate_to)
        data = {'total_count':deals_total,'items':response1}
        return data

@app.route('/quoteProforma/<id>', methods=['GET'])
@jwt_required
def quoteProforma(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        quote = MongoAPI.getFullQuoteDetails(ordId,id)
        quote = Uid.fix_array3(quote)

        # print (quote)

        proforma_stage = MongoAPI.fieldsSettingsDataDefault('proforma_stage',ordId)


        numbering = MongoAPI.getNumberingSettings('proforma',ordId)
        if not numbering:
            numbering = MongoAPI.getNumberingSettings2('proforma',ordId)
        proforma_no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
        no = int(numbering['sequence'])+1
        MongoAPI.numberingUpdate(numbering['id'],no)


        

        response1 = MongoAPI.convertToProforma(ordId,quote,id,proforma_no,current_user,proforma_stage)
        
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)

        p_id = str(response1)
        response1 = MongoAPI.getProformaDetails(ordId,p_id)

        # print (response1)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array3(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        data = response1


        if data:
            action = 'CREATE'
            associate_to = 'proforma'
            associate_id = ObjectId(p_id)
            via = 0
            extra_info = json.loads(json_util.dumps(quote))
            text_info = 'Create Proforma'
            title = 'Create Proforma'
            
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
            return jsonify({"msg": "Proforma successfully created !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/quoteInvoice/<id>', methods=['GET'])
@jwt_required
def quoteInvoice(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        quote = MongoAPI.getFullQuoteDetails(ordId,id)
        quote = Uid.fix_array3(quote)

        # print (quote)

        invoice_stage = MongoAPI.fieldsSettingsDataDefault('invoice_stage',ordId)


        numbering = MongoAPI.getNumberingSettings('invoice',ordId)
        if not numbering:
            numbering = MongoAPI.getNumberingSettings2('invoice',ordId)
        invoice_no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
        no = int(numbering['sequence'])+1
        MongoAPI.numberingUpdate(numbering['id'],no)


        response1 = MongoAPI.convertToInvoice(ordId,quote,id,invoice_no,current_user,invoice_stage,'quote')
        
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)

        p_id = str(response1)
        response1 = MongoAPI.getInvoiceDetails(ordId,p_id)

        # print (response1)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array3(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        data = response1


        if data:
            action = 'CREATE'
            associate_to = 'invoice'
            associate_id = ObjectId(p_id)
            via = 0
            extra_info = json.loads(json_util.dumps(quote))
            text_info = 'Create Invoice'
            title = 'Create Invoice'
            
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
            return jsonify({"msg": "Invoice successfully created !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/proformaInvoice/<id>', methods=['GET'])
@jwt_required
def proformaInvoice(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        quote = MongoAPI.getFullProformaDetails(ordId,id)
        quote = Uid.fix_array3(quote)

        # print (quote)

        invoice_stage = MongoAPI.fieldsSettingsDataDefault('invoice_stage',ordId)


        numbering = MongoAPI.getNumberingSettings('invoice',ordId)
        if not numbering:
            numbering = MongoAPI.getNumberingSettings2('invoice',ordId)
        no = str(numbering['prefix'])+'/'+str(numbering['sequence'])

        response1 = MongoAPI.convertToInvoice(ordId,quote,id,no,current_user,invoice_stage,'proforma')
        
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)

        p_id = str(response1)
        response1 = MongoAPI.getInvoiceDetails(ordId,p_id)

        # print (response1)

        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array3(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        data = response1


        if data:
            action = 'CREATE'
            associate_to = 'invoice'
            associate_id = ObjectId(p_id)
            via = 0
            extra_info = json.loads(json_util.dumps(quote))
            text_info = 'Create Invoice'
            title = 'Create Invoice'
            
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
            return jsonify({"msg": "Invoice successfully created !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/proformaCount', methods=['GET'])
@jwt_required
def getProformaCount():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data = MongoAPI.fieldsSettingsData('proforma_stage',ordId)
        data = Uid.fix_array2(data)
        settings6=[]
        for data1 in data:
            
            stage_data = MongoAPI.getProformaCount(ordId,data1['_id'])
            settings = defaultdict(list)
            if stage_data:
                for stageData in stage_data:
                    settings['stage_name'] = data1['name']
                    if stageData=='total':
                        settings['total'] = stage_data['total']
                    elif stageData=='count':
                        settings['count'] = stage_data['count']
                    elif stageData=='_id':
                        settings['id'] = data1['_id']
            else:
                settings['stage_name'] = data1['name']
                settings['total'] = '0'
                settings['count'] = '0'
                settings['id'] = data1['_id']
            settings6.append(settings)
        if data:
            return jsonify({"msg": "Proforma Count","code": 200,"data": settings6})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/proforma/<id>', methods=['GET'])
@jwt_required
def getProformaDetails(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        
        response1 = MongoAPI.getProformaDetails(ordId,id)
        response4 = Uid.fix_array3(response1)
        data = {'proforma':response4}


        if data:
            return jsonify({"msg": "Proforma Details","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/proforma/<id>', methods=['PUT'])
@jwt_required
def proformaUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !1","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)

        ref_no =  request.json.get('ref_no', None)
        # valid_till =  request.json.get('valid_till', None)

        deal_id =  request.json.get('deal_id', None)
        deal_id = ObjectId(deal_id)
        
        request.json.get('deal_id', deal_id)
        
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1      
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.proformasUpdate(ordId,current_user,data1,id)
            response1 = MongoAPI.getProformaDetails(ordId,id)
            response4 = Uid.fix_array3(response1)
            data = {'proforma':response4}
            if data:
                action = 'UPDATE'
                associate_to = 'proforma'
                associate_id = ObjectId(id)
                via = 0
                # print (data1)
                extra_info = data1
                text_info = 'Update Proforma'
                title = 'Update Proforma'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Proforma successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !2","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !3","code": 400})


@app.route('/proforma', methods=['GET'])
@jwt_required
def getProforma():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('proforma_stage', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)

        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not sort_by:
            sort_by=sortBy

        length = int(length)
        page = int(page)

        response1 = MongoAPI.getProformas(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort_by,current_user)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        data = {'proformas':response1}


        if data:
            return jsonify({"msg": "Proformas List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/proformaSettings', methods=['GET'])
@jwt_required
def addProforma():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId=''
    state=''
    country=''

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='state':
            state = user[user2]
        elif user2=='country':
            country = user[user2]



    
  
    users = MongoAPI.usersList(ordId)
    users = Uid.fix_array(users)

    currency = MongoAPI.fieldsSettingsData('currency',ordId)
    currency = Uid.fix_array(currency)

    countries = MongoAPI.countriesList(current_user)
    countries = Uid.fix_array(countries)


    tax_type = MongoAPI.fieldsSettingsData('tax_type',ordId)
    tax_type = Uid.fix_array(tax_type)

    tax_percentage = MongoAPI.fieldsSettingsData('tax_percentage',ordId)
    tax_percentage = Uid.fix_array(tax_percentage)

    bank_details = MongoAPI.fieldsSettingsData('bank_details',ordId)
    bank_details = Uid.fix_array(bank_details)

    terms_condition = MongoAPI.fieldsSettingsData('terms_condition',ordId)
    terms_condition = Uid.fix_array(terms_condition)

    payment_terms = MongoAPI.fieldsSettingsData('payment_terms',ordId)
    payment_terms = Uid.fix_array(payment_terms)

    shipping = MongoAPI.fieldsSettingsData('shipping',ordId)
    shipping = Uid.fix_array(shipping)

    delivery_period = MongoAPI.fieldsSettingsData('delivery_period',ordId)
    delivery_period = Uid.fix_array(delivery_period)
    
    validity = MongoAPI.fieldsSettingsData('validity',ordId)
    validity = Uid.fix_array(validity)
    

    settings = defaultdict(list)

    settings['valid_till'] = 30
    settings['auto_tax_percentage'] = 1
    settings['auto_tax_percentage'] = 1
    settings['state'] = 1
    settings['country'] = 1
    

    
    data = {'users':users,'currency':currency,'settings':settings,'validity':validity,'shipping':shipping,'delivery_period':delivery_period,'payment_terms':payment_terms,'countries':countries,'tax_type':tax_type,'tax_percentage':tax_percentage,'bank_details':bank_details,'terms_condition':terms_condition}
    return jsonify({"msg": "Settings List",'data': data,"code": 200})


@app.route('/quotePDF/<id>', methods=['GET'])
# @jwt_required
def pdf_template(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    # print(user)
    ordId = 1
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = 1
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = 'quote'
        pdfData = check = MongoAPI.checkPDFSettings(type,ordId)
        pdfData = Uid.fix_array3(pdfData)
        quote = MongoAPI.getQuoteDetails(ordId,id)
        quote = Uid.fix_array3(quote)
        # print (quote)
        quote['quote_no']

        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])


        html = render_template('QuotePDFView.html',pdfData=pdfData,quote=quote,organization=organization)
        filename = str('quote_')+quote['quote_no']+str('.pdf')
        # print (filename)
        response = Pdf.PDFGenerator(html,filename)
        return response
        # return html
        # return jsonify({"quote": quote,'pdfData': pdfData})
        


@app.route('/quote/<id>', methods=['GET'])
@jwt_required
def getQuoteDetails(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        
        response1 = MongoAPI.getQuoteDetails(ordId,id)
        response4 = Uid.fix_array3(response1)
        data = {'quote':response4}


        if data:
            return jsonify({"msg": "Quotes List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/quotePDF11/<id>', methods=['GET'])
@jwt_required
def getQuotePDF(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        HTMLTEST = '<html><body><p>Hello <strong style="color: #f00;">World</strong><hr><table border="1" style="background: #eee; padding: 0.5em;"><tr><td>Amount</td><td>Description</td><td>Total</td></tr><tr><td>1</td><td>Good weather</td><td>0 EUR</td></tr><tr style="font-weight: bold"><td colspan="2" align="right">Sum</td><td>0 EUR</td></tr></table></body></html>'

        #pdf = StringIO()
        # business_name ='ddfd'
        # tin='asdas'
        # html = render_template('certificate.html', business_name=business_name, tin=tin)
        file_class = Pdf()
        pdf = file_class.html2pdf()
        

        headers = {
            'content-type': 'application.pdf',
            'content-disposition': 'attachment; filename=certificate.pdf'}
        return pdf, 200, headers



@app.route('/quote', methods=['POST'])
@jwt_required
def quoteSubmit():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)

        ref_no =  request.json.get('ref_no', None)
        valid_till =  request.json.get('valid_till', None)

        deal_id =  request.json.get('deal_id', None)
        quote_stage =  request.json.get('quote_stage', None)

        # quote_stage = MongoAPI.fieldsSettingsDataDefault('deal_stage',ordId)
        
        
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})
        if not valid_till:
            return jsonify({"msg": "Missing quote valid till parameter","code": 400})


        data1 = json.loads(json_util.dumps(request.json))

        if not deal_id:
            deal_stage = MongoAPI.fieldsSettingsDataDefault('deal_stage',ordId)
            numbering = MongoAPI.getNumberingSettings('deal',ordId)
            if not numbering:
                numbering = MongoAPI.getNumberingSettings2('deal',ordId)

            no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
            deal_id = MongoAPI.dealSubmitNew(ordId,current_user,data1,deal_stage,no)
            no = int(numbering['sequence'])+1
            MongoAPI.numberingUpdate(numbering['id'],no)

            action = 'CREATE'
            associate_to = 'quote'
            associate_id = ObjectId(deal_id)
            via = 0
            extra_info = json.loads(json_util.dumps(data1))
            text_info = app_config.CREATE_INFO
            title = app_config.CREATE_TITLE
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)   
            quote_stage = deal_stage 

            # print(quote_stage)
        request.json.get('deal_id', deal_id)
        # quote_stage = MongoAPI.fieldsSettingsDataDefault('quote_stage',ordId)   
        
        data = data1      
        org_id = int(ordId)
        if org_id:
            numbering = MongoAPI.getNumberingSettings('quote',ordId)
            
            if not numbering:
                numbering = MongoAPI.getNumberingSettings2('quote',ordId)
            no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
            response1 = MongoAPI.quoteSubmit(ordId,current_user,quote_stage,data1,deal_id,no,'parent')
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getQuoteDetails(ordId,response2)

            no = int(numbering['sequence'])+1
            MongoAPI.numberingUpdate(numbering['id'],no)

            
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = {'quote':response4}
            
            if data:
                action = 'CREATE'
                associate_to = 'quote'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = 'Create Quote'
                title = 'Create Quote'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Quote successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/invoice/<id>', methods=['PUT'])
@jwt_required
def invoiceUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)

       
        
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})


        data1 = json.loads(json_util.dumps(request.json))

        data = data1      
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.invoiceUpdate(ordId,current_user,data1,id)
            response1 = MongoAPI.getInvoiceDetails(ordId,id)
            response4 = Uid.fix_array3(response1)
            data = {'invoice':response4}
            if data:
                action = 'UPDATE'
                associate_to = 'invoice'
                associate_id = ObjectId(id)
                via = 0
                extra_info = data1
                text_info = 'Update Invoice'
                title = 'Update Invoice'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Invoice successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/invoice', methods=['GET'])
@jwt_required
def getInvoiceList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('invoice_stage', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getInvoiceListData(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        deals_total = MongoAPI.getInvoiceCountData(ordId)
        data = {'total_count':deals_total,'items':response1}
        return data


@app.route('/invoice', methods=['POST'])
@jwt_required
def invoiceSubmit():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)



         
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})
        


        data1 = json.loads(json_util.dumps(request.json))

         
        invoice_stage = MongoAPI.fieldsSettingsDataDefault('invoice_stage',ordId)   
        
        data = data1      
        org_id = int(ordId)
        if org_id:
            numbering = MongoAPI.getNumberingSettings('invoice',ordId)
            
            if not numbering:
                numbering = MongoAPI.getNumberingSettings2('invoice',ordId)
            no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
            
            response1 = MongoAPI.invoiceSubmit(ordId,current_user,invoice_stage,data1,no)


            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getInvoiceDetails(ordId,response2)

            no = int(numbering['sequence'])+1
            MongoAPI.numberingUpdate(numbering['id'],no)

            
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = {'invoice':response4}
            
            if data:
                action = 'CREATE'
                associate_to = 'invoice'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = 'Create invoice'
                title = 'Create invoice'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Invoice successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/invoice/<id>', methods=['GET'])
@jwt_required
def getInvoiceDetails(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        
        response1 = MongoAPI.getInvoiceDetails(ordId,id)
        response4 = Uid.fix_array3(response1)
        data = {'invoice':response4}


        if data:
            return jsonify({"msg": "Invoice Deatils","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
            



@app.route('/proformaList', methods=['GET'])
@jwt_required
def getproformaList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('proforma_stage', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getProformaListData(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        deals_total = MongoAPI.getProformaCountData(ordId)
        data = {'total_count':deals_total,'items':response1}
        return data



@app.route('/quoteRevised', methods=['POST'])
@jwt_required
def quoteRevised():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)

        ref_no =  request.json.get('ref_no', None)
        valid_till =  request.json.get('valid_till', None)

        deal_id =  request.json.get('deal_id', None)

        quote_revised =  request.json.get('quote_revised', None)

        # print (quote_revised)
        
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})
        if not valid_till:
            return jsonify({"msg": "Missing quote valid till parameter","code": 400})
        if not quote_revised:
            return jsonify({"msg": "Missing quote revised id parameter","code": 400})

        revised = MongoAPI.getQuoteDetails(ordId,quote_revised)
        revised = Uid.fix_array3(revised)

        # count = MongoAPI.getQuotesCount(ordId,quote_revised)


        # if not count:
        #     count=1
        # elif count==0:
        #     count=1
        # else:
        #     count = int(count)+1
     
        count = int(revised['revise_count'])+1

        revised_no = str(revised['quote_no'])+'.'+str(count)

        data1 = json.loads(json_util.dumps(request.json))

        if ordId:

            quote_stage_revised = MongoAPI.fieldsSettingsDataDefaultRevised('deal_stage',ordId,'Revised')
               
            print(ordId)
            print(quote_revised)
            print(quote_stage_revised)
            response1 = MongoAPI.quoteUpdateStage(ordId,quote_revised,quote_stage_revised)
            revise_quote_count = MongoAPI.quoteUpdateReviseCount(ordId,quote_revised,count)
            
            # quote_stage = MongoAPI.fieldsSettingsDataDefault('quote_stage',ordId)
            quote_stage = revised['quote_stage']
            no = revised_no
            response1 = MongoAPI.quoteSubmit(ordId,current_user,quote_stage,data1,deal_id,no,'child')
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getQuoteDetails(ordId,response2)
            response4 = Uid.fix_array3(response1)


            


            data = '1'
            data = {'quote':response4}
            
            if data:
                action = 'CREATE_REVISED'
                associate_to = 'quote'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = 'Create Quote'
                title = 'Create Quote'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Quote successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})











@app.route('/numbering', methods=['GET'])
@jwt_required
def numbering():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        numbering = MongoAPI.getNumberingSettings('quote',ordId)
        if not numbering:
            numbering = MongoAPI.getNumberingSettings2('quote',ordId)
        no = str(numbering['prefix'])+'/'+str(numbering['sequence'])
        return jsonify({"msg": "Quote successfully updated !","code": 200,"numbering": numbering})



@app.route('/quote/<id>', methods=['PUT'])
@jwt_required
def quoteUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        subject =  request.json.get('subject', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        gstin =  request.json.get('gstin', None)
        product =  request.json.get('product', None)


        discount_amount =  request.json.get('discount_amount', None)
        discount_percentage =  request.json.get('discount_percentage', None)

        packing_amount =  request.json.get('packing_amount', None)
        packing_percentage =  request.json.get('packing_percentage', None)

        shipping_charges =  request.json.get('shipping_charges', None)
        installation_charges =  request.json.get('installation_charges', None)

        tax_type1 =  request.json.get('tax_type1', None)
        tax_percentage1 =  request.json.get('tax_percentage1', None)
        tax_amount1 =  request.json.get('tax_amount1', None)

        tax_type2 =  request.json.get('tax_type2', None)
        tax_percentage2 =  request.json.get('tax_percentage2', None)
        tax_amount2 =  request.json.get('tax_amount2', None)

        total =  request.json.get('total', None)

        terms_condition =  request.json.get('terms_condition', None)
        bank_details =  request.json.get('bank_details', None)

        remark =  request.json.get('remark', None)

        address1 =  request.json.get('address1', None)
        address2 =  request.json.get('address2', None)
        city =  request.json.get('city', None)
        state =  request.json.get('state', None)
        country =  request.json.get('country', None)
        postal_code =  request.json.get('postal_code', None)

        shipping_address1 =  request.json.get('address1', None)
        shipping_address2 =  request.json.get('address2', None)
        shipping_city =  request.json.get('city', None)
        shipping_state =  request.json.get('state', None)
        shipping_country =  request.json.get('country', None)
        shipping_postal_code =  request.json.get('postal_code', None)

        ref_no =  request.json.get('ref_no', None)
        valid_till =  request.json.get('valid_till', None)

        deal_id =  request.json.get('deal_id', None)
        deal_id = ObjectId(deal_id)
        
        request.json.get('deal_id', deal_id)
        
        if not subject:
            return jsonify({"msg": "Missing subject parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company parameter","code": 400})

        if not contact_id:
            return jsonify({"msg": "Missing contact parameter","code": 400})
        if not product:
            return jsonify({"msg": "Missing product parameter","code": 400})
        if not total:
            return jsonify({"msg": "Missing total parameter","code": 400})
        if not valid_till:
            return jsonify({"msg": "Missing quote valid till parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1      
        org_id = int(ordId)
        if org_id:
            response1 = MongoAPI.quoteUpdate(ordId,current_user,data1,id)
            response1 = MongoAPI.getQuoteDetails(ordId,id)
            response4 = Uid.fix_array3(response1)
            data = {'quote':response4}
            if data:
                action = 'UPDATE'
                associate_to = 'quote'
                associate_id = ObjectId(id)
                via = 0
                # print (data1)
                extra_info = data1
                text_info = 'Update Quote'
                title = 'Update Quote'
                
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Quote successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})




@app.route('/quoteSettings', methods=['GET'])
@jwt_required
def addQuote():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId=''
    state=''
    country=''

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
        elif user2=='state':
            state = user[user2]
        elif user2=='country':
            country = user[user2]



    
  
    users = MongoAPI.usersList(ordId)
    users = Uid.fix_array(users)

    currency = MongoAPI.fieldsSettingsData('currency',ordId)
    currency = Uid.fix_array(currency)

    countries = MongoAPI.countriesList(current_user)
    countries = Uid.fix_array(countries)


    tax_type = MongoAPI.fieldsSettingsData('tax_type',ordId)
    tax_type = Uid.fix_array(tax_type)

    tax_percentage = MongoAPI.fieldsSettingsData('tax_percentage',ordId)
    tax_percentage = Uid.fix_array(tax_percentage)

    bank_details = MongoAPI.fieldsSettingsData('bank_details',ordId)
    bank_details = Uid.fix_array(bank_details)

    terms_condition = MongoAPI.fieldsSettingsData('terms_condition',ordId)
    terms_condition = Uid.fix_array(terms_condition)

    payment_terms = MongoAPI.fieldsSettingsData('payment_terms',ordId)
    payment_terms = Uid.fix_array(payment_terms)

    shipping = MongoAPI.fieldsSettingsData('shipping',ordId) 
    shipping = Uid.fix_array(shipping)

    delivery_period = MongoAPI.fieldsSettingsData('delivery_period',ordId)
    delivery_period = Uid.fix_array(delivery_period)
    
    validity = MongoAPI.fieldsSettingsData('validity',ordId)
    validity = Uid.fix_array(validity)

    quote_category = MongoAPI.fieldsSettingsData('category',ordId)
    quote_category = Uid.fix_array(quote_category)
    
    organization = MongoAPI.organizationInfo(ordId)
    organization = Uid.fix_array1(organization)
    if organization['logo']:
        organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])
    

    settings = defaultdict(list)

    settings['valid_till'] = 30
    settings['auto_tax_percentage'] = 1
    settings['auto_tax_percentage'] = 1
    settings['state'] = 1
    settings['country'] = 1
    

    
    data = {'users':users,
           'currency':currency,'settings':settings,'shipping':shipping,'validity':validity,
           'delivery_period':delivery_period,'payment_terms':payment_terms,'countries':countries,'tax_type':tax_type,
           'tax_percentage':tax_percentage,'bank_details':bank_details,
           'terms_condition':terms_condition,'quote_category':quote_category,
           'organization':organization
           }
    return jsonify({"msg": "Settings List",'data': data,"code": 200})





@app.route('/deals/<id>', methods=['PUT'])
@jwt_required
def dealsUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        deal_name =  request.json.get('deal_name', None)
        amount =  request.json.get('amount', None)
        target_date =  request.json.get('target_date', None)
        stage =  request.json.get('stage', None)
        assigned_to =  request.json.get('assigned_to', None)
        product =  request.json.get('product', None)
        # qty =  request.json.get('qty', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        currency =  request.json.get('currency', None)
        
        
        if not deal_name:
            return jsonify({"msg": "Missing deal name parameter","code": 400})
        if not amount:
            return jsonify({"msg": "Missing amount parameter","code": 400})
        if not target_date:
            return jsonify({"msg": "Missing amount parameter","code": 400})
        # if not stage:
        #     return jsonify({"msg": "Missing stage parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned to parameter","code": 400})
        # if not product:
        #     return jsonify({"msg": "Missing product parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not contact_id:
            return jsonify({"msg": "Missing contact id parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company id parameter","code": 400})
        
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
         
            response1 = MongoAPI.dealUpdate(org_id,id,data)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getDealDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'deal'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Deal successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/closeDeal/<id>', methods=['PUT'])
@jwt_required
def closeDeal(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.closeDeal(org_id,id,data,current_user)
            # response = json.loads(json_util.dumps(response1))
            # response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            # if data:
            #     action = 'UPDATE'
            #     associate_to = 'deal'
            #     associate_id = ObjectId(id)
            #     via = 0
            #     extra_info = json.loads(json_util.dumps(data))
            #     text_info = app_config.CREATE_INFO
            #     title = app_config.CREATE_TITLE

            #     MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
            return jsonify({"msg": "Deal successfully updated !","code": 200,"data": data})
            # else:
            #     return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/reassign', methods=['PUT'])
@jwt_required
def reassign():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)

        if associate_to=='deal':
            assigned_to =  request.json.get('assigned_to', None)
            mes = "Deal successfully reassigned !"
        elif associate_to=='contact':
            assigned_to =  request.json.get('assigned_to', None)
            mes = "Contact successfully reassigned !"
        elif associate_to=='company':
            assigned_to =  request.json.get('assigned_to', None)
            mes = "Company successfully reassigned !"
        else:
            assigned_to =  request.json.get('assigned_to', None)
            mes = str(associate_to) + str(" successfully reassigned !")

        new_assigned_name =  request.json.get('new_assigned_name', None)
        old_assigned_name =  request.json.get('old_assigned_name', None)
       
        
        
        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned to parameter","code": 400})
        if not new_assigned_name:
            return jsonify({"msg": "Missing new assigned name parameter","code": 400})
        if not old_assigned_name:
            return jsonify({"msg": "Missing old assigned name parameter","code": 400})
        
        
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.reassignUpdate(org_id,associate_id,associate_to,assigned_to)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            if associate_to == 'deal':
                response1 = MongoAPI.getDealDetails(org_id,response2)
                url=app.config['DEAL_DETAIL_URL']+str(response1['_id']['$oid'])
                MongoAPI.assignMail(org_id,response1['_id'],associate_to,response2,old_assigned_name,assigned_to,url)
            elif associate_to == 'contact':
                response1 = MongoAPI.getContactDetails(org_id,response2)

                url=app.config['CONTACT_DETAIL_URL']+str(response1['_id']['$oid'])
                MongoAPI.assignMail(org_id,response1['_id'],associate_to,response2,old_assigned_name,assigned_to,url)
                
            elif associate_to == 'company':
                response1 = MongoAPI.getCompanyDetails(org_id,response2)
                url=app.config['COMPANY_DETAIL_URL']+str(response1['_id']['$oid'])
                MongoAPI.assignMail(org_id,response1['_id'],associate_to,response2,old_assigned_name,assigned_to,url)
          
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'REASSIGN'
                associate_to = associate_to
                associate_id = ObjectId(associate_id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = str('Reassigned this from ')+str(old_assigned_name)
                title = 'Reassigned'

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": mes,"code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong2 !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong3 !","code": 400})


@app.route('/tagUpdate', methods=['PUT'])
@jwt_required
def tagUpdate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)

        tag =  request.json.get('tag', None)
        mes = "Tag successfully Update !"
     
        
        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
        
        
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.tagUpdate(org_id,associate_id,associate_to,tag)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            if associate_to=='deal':
                response1 = MongoAPI.getDealDetails(org_id,associate_id)
                responseData = Uid.fix_array3(response1)
            elif associate_to=='contact':
                response1 = MongoAPI.getContactDetails(org_id,associate_id)
                responseData = Uid.fix_array3(response1)
            elif associate_to=='company':
                response1 = MongoAPI.getCompanyDetails(ordId,associate_id)
                responseData = Uid.fix_array3(response1)
            else:
                responseData=[]
            
            data = responseData
            # data = response1
            if data:
                action = 'TAG_UPDATE'
                associate_to = associate_to
                associate_id = ObjectId(associate_id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = str('Tag Update')
                title = 'Tag update'

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": mes,"code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/proformaStatusChange/<id>', methods=['PUT'])
@jwt_required
def proformaStatusChange(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        stage =  request.json.get('proforma_stage', None)
             
        
        if not stage:
            return jsonify({"msg": "Missing stage parameter","code": 400})

        old_stage =  request.json.get('old_status', None)
        new_stage =  request.json.get('new_status', None)
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.proformaStatusChange(org_id,id,data)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getProformaDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'UPDATE_STAGE'
                associate_to = 'proforma'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = str('Stage has been changed from  ')+str(old_stage)+str(' to ')+str(new_stage)
                title = 'Proforma Stage Change'

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Proforma stage successfully changed !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/invoiceStatusChange/<id>', methods=['PUT'])
@jwt_required
def invoiceStatusChange(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        stage =  request.json.get('invoice_stage', None)
             
        
        if not stage:
            return jsonify({"msg": "Missing stage parameter","code": 400})

        old_stage =  request.json.get('old_status', None)
        new_stage =  request.json.get('new_status', None)
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.invoiceStatusChange(org_id,id,data)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getInvoiceDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'UPDATE_STAGE'
                associate_to = 'invoice'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = str('Stage has been changed from  ')+str(old_stage)+str(' to ')+str(new_stage)
                title = 'Invoice Stage Change'

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Invoice stage successfully changed !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeStage/<id>', methods=['PUT'])
@jwt_required
def changeStage(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        stage =  request.json.get('stage', None)
             
        
        if not stage:
            return jsonify({"msg": "Missing stage parameter","code": 400})

        old_stage =  request.json.get('old_stage', None)
        new_stage =  request.json.get('new_stage', None)
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.dealChangeStage(org_id,id,data)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getDealDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'UPDATE_STAGE'
                associate_to = 'deal'
                associate_id = ObjectId(id)
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                text_info = str('Stage has been changed from  ')+str(old_stage)+str(' to ')+str(new_stage)
                title = 'Deal Change Stage'

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Deal stage successfully changed !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/deals', methods=['POST'])
@jwt_required
def deals():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        deal_name =  request.json.get('deal_name', None)
        amount =  request.json.get('amount', None)
        target_date =  request.json.get('target_date', None)
        stage =  request.json.get('stage', None)
        assigned_to =  request.json.get('assigned_to', None)
        product =  request.json.get('product', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        
        # stage=MongoAPI.getDefault(ordId,'deal_stage')
       
        if not deal_name:
            return jsonify({"msg": "Missing deal name parameter","code": 400})
        if not amount:
            return jsonify({"msg": "Missing amount parameter","code": 400})
        if not target_date:
            return jsonify({"msg": "Missing target date","code": 400})
        if not assigned_to:
            return jsonify({"msg": "Missing assigned to parameter","code": 400})
        # if not product:
        #     return jsonify({"msg": "Missing product parameter","code": 400})
        if not currency:
            return jsonify({"msg": "Missing currency parameter","code": 400})
        if not contact_id:
            return jsonify({"msg": "Missing contact id parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company id parameter","code": 400})
        
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            numbering = MongoAPI.getNumberingSettings('quote',ordId)
            if not numbering:
                numbering = MongoAPI.getNumberingSettings2('quote',ordId)
            no = str(numbering['prefix'])+'/'+str(numbering['sequence'])

            deal_submit = MongoAPI.dealSubmit(org_id,current_user,data1,no)

            no = int(numbering['sequence'])+1
            MongoAPI.numberingUpdate(numbering['id'],no)

            
            response = json.loads(json_util.dumps(deal_submit))
            response2 = str(response)
            response1 = MongoAPI.getDealDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'CREATE'
                associate_to = 'deal'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)   

                process1 = MongoAPI.getSalesProcessTemplateDefault(ordId)
                process = json.loads(json_util.dumps(process1))
                process1 = Uid.fix_array_sub(process)
                
                checkList = MongoAPI.insertCheckList(ordId,process,associate_to,associate_id) 
                response = json.loads(json_util.dumps(deal_submit))
                response2 = str(response)
                response1 = MongoAPI.getDealDetails(org_id,response2)
                response4 = Uid.fix_array3(response1)
                data = response4

                        
                return jsonify({"msg": "Deal successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/dealQuickAdd', methods=['POST'])
@jwt_required
def dealsQuickAdd():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        deal_name =  request.json.get('deal_name', None)
        amount =  request.json.get('amount', None)
        target_date =  request.json.get('target_date', None)
        stage =  request.json.get('stage', None)
        assigned_to =  request.json.get('assigned_to', None)
        product =  request.json.get('product', None)
        currency =  request.json.get('currency', None)
        contact_id =  request.json.get('contact_id', None)
        company_id =  request.json.get('company_id', None)
        
       
        if not deal_name:
            return jsonify({"msg": "Missing deal name parameter","code": 400})
        if not amount:
            return jsonify({"msg": "Missing amount parameter","code": 400})
        if not contact_id:
            return jsonify({"msg": "Missing contact id parameter","code": 400})
        if not company_id:
            return jsonify({"msg": "Missing company id parameter","code": 400})
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            numbering = MongoAPI.getNumberingSettings('quote',ordId)
            if not numbering:
                numbering = MongoAPI.getNumberingSettings2('quote',ordId)
            no = str(numbering['prefix'])+'/'+str(numbering['sequence'])

            response1 = MongoAPI.dealSubmit(org_id,current_user,data1,no)

            no = int(numbering['sequence'])+1
            MongoAPI.numberingUpdate(numbering['id'],no)

            
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)
            response1 = MongoAPI.getDealDetails(org_id,response2)
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'CREATE'
                associate_to = 'deal'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = data1
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)   

                process1 = MongoAPI.getSalesProcessTemplateDefault(ordId)
                process = json.loads(json_util.dumps(process1))
                process1 = Uid.fix_array_sub(process)
                checkList = MongoAPI.insertCheckList(ordId,process,associate_to,associate_id) 

                return jsonify({"msg": "Deal successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/deals/<id>', methods=['GET'])
@jwt_required
def getDealsDetails(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        # print (org_id)

        if org_id:
            response1 = MongoAPI.getDealDetails(org_id,id)

            # print (response1)

            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                return jsonify({"msg": "Deal successfully Details !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 500})


@app.route('/deals', methods=['GET'])
@jwt_required
def getDeals():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('stage', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)

        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not sort_by:
            sort_by=sortBy

        length = int(length)
        page = int(page)

        response1 = MongoAPI.getDeals(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort_by,current_user,city,state,country)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}

        data = {'deals':response1}

        if data:
            return jsonify({"msg": "Deals List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/usersNewList', methods=['GET'])
@jwt_required
def getUsersNewList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        status =  request.args.get('status', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not status:
            status='active'

        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort:
            sort = sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getUsersNewList(ordId,length,page,search,status,sort,order)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)

        # data = {'deals':response1,'stage':stage_data2}

        user_total = MongoAPI.getUsersNewListCountData(ordId,status)
        data = {'total_count':user_total,'items':response1}
        return data


@app.route('/dealsList', methods=['GET'])
@jwt_required
def getDealsList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('stage', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getDealsListAll(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort,order,current_user,city,state,country)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        deals_total = MongoAPI.getDealsCountData(ordId,search,assigned_to,stage,tag,date_from,date_to,current_user)
        # deals_total=len(response1)
        data = {'total_count':deals_total,'items':response1}
        return data

@app.route('/dealsListDataDb', methods=['GET']) #Datatable format
@jwt_required
def getDealsListDb():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        associate_id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        sort='create_date'
        if not page:
            page=1
        if not length:
            length=10

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getDealsListDataDb(ordId,length,page,sort,order,associate_id,associate_to)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        deals_total = MongoAPI.getDealsAssociateCountData(ordId,associate_id,associate_to)
        data = {'total_count':deals_total,'items':response1}
        return data

@app.route('/dealsListNew', methods=['GET'])
@jwt_required
def getDealsListNew():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        stage =  request.args.get('stage', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        sort =  request.args.get('sort', None)
        associate_id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)

        order ='desc'

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not stage:
            stage=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = MongoAPI.getDealsListData(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort,order,associate_id,associate_to)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        # deals_total = MongoAPI.getDealsCountData(ordId)
        data = {'deals':response1}
        
        if data:
            return jsonify({"msg": "Deals List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/dealsStage', methods=['GET'])
@jwt_required
def getDealsStage():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        search =  request.args.get('search', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)

    

        if not search:
            search=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = 20

        settings = {}
        
        dealsStage = MongoAPI.fieldsSettingsData('stage',ordId)
        dealsStage = Uid.fix_array2(dealsStage)
        skip = 0
        stage_name = []
        stages = []
        for key in dealsStage:
            stage = key['_id']
            id = key['_id']
            name = key['name']

            count = MongoAPI.getDealStageCount(ordId,stage)

            # print (count)

            # count = Uid.fix_array3(count)

            name_data = {'key':name,'value':count}
            stage_name.append(name_data)
            response = MongoAPI.getDealsStage(ordId,length,skip,search,assigned_to,stage,tag,date_from,date_to,sort_by)
            response1 = Uid.fix_array(response)
            stages.append(response1)
        data = {'deals':stages,'stage_name':stage_name}

        if data:
            return jsonify({"msg": "Deals stages List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/dealsStageList', methods=['GET'])
@jwt_required
def getDealsStageList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        search =  request.args.get('search', None)
        tag =  request.args.get('tag', None)
        assigned_to =  request.args.get('assigned_to', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        skip =  request.args.get('skip', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)

        stage =  request.args.get('stage', sortBy)
        

        skip = int(skip)
    

        if not search:
            search=''
        if not tag:
            tag=''
        if not assigned_to:
            assigned_to=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        

        length = 20

        response = MongoAPI.getDealsStage(ordId,length,skip,search,assigned_to,stage,tag,date_from,date_to,sort_by)
        response1 = Uid.fix_array(response)

        data = {'deals':response1}

        if data:
            return jsonify({"msg": "Deals stages List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/dealsCount', methods=['GET'])
@jwt_required
def getDealsCount():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data = MongoAPI.fieldsSettingsData('deal_stage',ordId)
        data = Uid.fix_array2(data)
        settings6=[]
        for data1 in data:
            
            stage_data = MongoAPI.getDealsCount(ordId,data1['_id'])
            settings = defaultdict(list)
            if stage_data:
                for stageData in stage_data:
                    if stageData=='stage_name':
                        settings['stage_name'] = stage_data['stage_name']
                    elif stageData=='total':
                        settings['total'] = stage_data['total']
                    elif stageData=='count':
                        settings['count'] = stage_data['count']
                    elif stageData=='_id':
                        settings['id'] = data1['_id']
            else:
                settings['stage_name'] = data1['name']
                settings['total'] = '0'
                settings['count'] = '0'
                settings['id'] = data1['_id']

            settings6.append(settings)
        if data:
            return jsonify({"msg": "Deals List","code": 200,"data": settings6})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/qutoe', methods=['POST'])
@jwt_required
def qutoeSubmit():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    data = json.loads(json_util.dumps(request.json))
    return jsonify({"msg": "Settings List",'data': data,"code": 400})

@app.route('/modulesList', methods=['GET'])
@jwt_required
def modulesList():
    sort
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

        
    module = MongoAPI.modulesList(ordId)
    modules = Uid.fix_array(module)  
    return jsonify({"msg": "Settings List",'settings': modules,"code": 400})
    


@app.route('/profile1', methods=['POST'])
@jwt_required
def profile1():
	current_user = get_jwt_identity()
	# print (current_user)
	# check if the post request has the file part
	if 'file' not in request.files:
		resp = jsonify({'msg' : 'No file part in the request','code':'400'})
		return resp
	file = request.files['file']

	if file.filename == '':
		resp = jsonify({'msg' : 'No file selected for uploading','code':'400'})
		resp.status_code = 400
		return resp
	if file and allowed_file(file.filename,app_config.PROFILE_ALLOWED_EXTENSIONS):
        
        
		filename = secure_filename(file.filename)
        # filename = 'ggg'
		file_type = file.filename
	
		file_format = splitpart(file_type,1,'.')
		file_name = splitpart(file_type,0,'.')
		filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
		file.save(os.path.join(app.config['UPLOAD_PROFILE_FOLDER'], filename1))
		resp = jsonify({'msg' : 'File successfully uploaded','code':'200'})
		return resp
	else:
		resp = jsonify({'msg' : 'Allowed file types are png, jpg, jpeg','code':'400'})
		return resp

@app.route('/q', methods=['GET'])
@jwt_required
def candidate_list_json():
    context=''
    request=''
    columns = [ 'id_number', 'first_name', 'email', 'mobile_number']
    index_column = "id_number"
    collection = "candidates"

    results = DataTables_Handler(request, columns, index_column, collection).output_result()

    # return the results as a string for the datatable
    return {"results": results}



@app.route('/accounts', methods=['GET'])
@jwt_required
def getAccounts():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('user', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sortBy = 'create_date'
        sort_by =  request.args.get('sort_by', sortBy)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)

        if not page:
            page=1
        if not length:
            length=2
        if not search:
            search=''
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if not sort_by:
            sort_by=sortBy

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getAccounts(ordId,length,page,search,user,source,customer_type,date_from,date_to,sort_by,current_user,city,state,country)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'accounts':response1}

        if data:
            return jsonify({"msg": "Accounts List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/accountsList', methods=['GET'])
@jwt_required
def getAccountsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        city =  request.args.get('city', None)
        state =  request.args.get('state', None)
        country =  request.args.get('country', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)




       
        response1 = MongoAPI.getAccountsList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user,city,state,country)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        
        # data = {'accounts':response1}


        accounts_total = MongoAPI.getAccountsCountData(ordId)
        data = {'total_count':accounts_total,'items':response1}
        return data

@app.route('/companiesListPriceList', methods=['GET'])
@jwt_required
def companiesListPriceList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        pricelist_id =request.args.get('pricelist_id', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getAccountsListPriceList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        accounts_total = MongoAPI.getAccountsCountData(ordId)
        data = {'total_count':accounts_total,'items':response1}
        return data

@app.route('/companiesListPriceList1', methods=['GET'])
@jwt_required
def companiesListPriceList1():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        pricelist_id =request.args.get('pricelist_id', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getAccountsListPriceList1(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id)
        response = json.loads(json_util.dumps(response1))
        company_list=[]
        for item in response:
            company_list.append(item['_id'])
        # print(company_list)
        data = {'data':company_list}
        return data
        # response1 = Uid.fix_array(response)
        # accounts_total = MongoAPI.getAccountsCountData(ordId)
        # data = {'total_count':accounts_total,'items':response1}
        # return data


@app.route('/contactSettings', methods=['GET'])
@jwt_required
def contactSettings():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        type = {'source':'source','tag':'contact_tag','users':'users','countries':'countries'}
        # settings = []
        settings = defaultdict(list)
        for key in type:
            # item = []
            if key=='users':
                users = MongoAPI.usersList(ordId)
            elif key=='countries':
                countries = MongoAPI.countriesList(current_user)
            elif key=='source':
                source = MongoAPI.fieldsSettingsData(type[key],ordId)
            elif key=='tag':
                tags = MongoAPI.fieldsSettingsData(type[key],ordId)
          

        users = Uid.fix_array2(users)
        countries = Uid.fix_array2(countries)
        source = Uid.fix_array2(source)
        tags = Uid.fix_array2(tags)
            

        settings = {'users':users,'countries':countries,'source':source,'tags':tags}

    return jsonify({"msg": "Settings List",'data': settings,"code": 200})



@app.route('/filter_by', methods=['GET'])
@jwt_required
def filterBy():
    r1 = '5ff5619fbf6a031d2c13cfa2'
    r = 'karthik Gk'
    role = MongoAPI.filterBy()
    role = Uid.fix_array(role)
    # print (role)
    return jsonify({"msg": "Filters List",'role': role,"code": 200})


@app.route('/reporting_users', methods=['GET'])
@jwt_required
def reporting_users():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
        user1 = MongoAPI.getFilterUser(ordId,reporting_users['deal_user_list'])
        user1 = Uid.fix_array(user1)
        data = {'user':user1}
        return jsonify({"msg": "Filters List",'data': data,"code": 200})




@app.route('/filters/<name>', methods=['GET'])
@jwt_required
def filters(name):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if name=='contact':
            source = MongoAPI.fieldsSettingsData('source',ordId)
            source = Uid.fix_array(source)
            tag = MongoAPI.fieldsSettingsData('contact_tag',ordId)
            tag = Uid.fix_array(tag)
            user = MongoAPI.usersList(ordId)
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            user1 = MongoAPI.getFilterUser(ordId,reporting_users['contact_user_list'])
            user1 = Uid.fix_array(user1)
            user = Uid.fix_array(user)
            city_list=MongoAPI.selectedCitiesList(ordId,'contact')
            state_list=MongoAPI.selectedStatesList(ordId,'contact')
            country_list=MongoAPI.selectedCountriesList(ordId,'contact')
            data = {'user':user1,'source':source,'tag':tag,'city':city_list,'state':state_list,'country':country_list}
        elif name=='company':
            source = MongoAPI.fieldsSettingsData('source',ordId)
            source = Uid.fix_array(source)
            customer_type = MongoAPI.fieldsSettingsData('customer_type',ordId)
            customer_type = Uid.fix_array(customer_type)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            user1 = MongoAPI.getFilterUser(ordId,reporting_users['contact_user_list'])
            user1 = Uid.fix_array(user1)
            city_list=MongoAPI.selectedCitiesList(ordId,'company')
            state_list=MongoAPI.selectedStatesList(ordId,'company')
            country_list=MongoAPI.selectedCountriesList(ordId,'company')
            data = {'user':user1,'source':source,'customer_type':customer_type,'city':city_list,'state':state_list,'country':country_list}
        elif name=='deal':
            stage=[]
            active={'_id':'active','name':'Active','selected':'selected'}
            stage.append(active)
            stage1 = MongoAPI.fieldsSettingsData('deal_stage',ordId)
            stage1 = Uid.fix_array(stage1)
            for st in stage1:
                if st['name']=='Revised':
                    # del stage1[st]
                    stage1.remove(st)
                st['selected']=''
                stage.append(st)
                # print(stage)

            tag = MongoAPI.fieldsSettingsData('deal_tag',ordId)
            tag = Uid.fix_array(tag)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            user1 = MongoAPI.getFilterUser(ordId,reporting_users['contact_user_list'])
            user1 = Uid.fix_array(user1)
            city_list=MongoAPI.selectedCitiesList(ordId,'company')
            state_list=MongoAPI.selectedStatesList(ordId,'company')
            country_list=MongoAPI.selectedCountriesList(ordId,'company')
            data = {'user':user1,'stage':stage,'tag':tag,'city':city_list,'state':state_list,'country':country_list}
        elif name=='quote':
            # quote_stage = MongoAPI.fieldsSettingsData('deal_stage',ordId)
            # quote_stage = Uid.fix_array(quote_stage)
            # tag = MongoAPI.fieldsSettingsData('quote_tag',ordId)
            # tag = Uid.fix_array(tag)
            stage=[]
            active={'_id':'active','name':'Active','selected':'selected'}
            stage.append(active)
            stage1 = MongoAPI.fieldsSettingsData('deal_stage',ordId)
            stage1 = Uid.fix_array(stage1)
            for st in stage1:
                st['selected']=''
                stage.append(st)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            reporting_users=MongoAPI.getReportingUsers(ordId,current_user)
            user1 = MongoAPI.getFilterUser(ordId,reporting_users['contact_user_list'])
            user1 = Uid.fix_array(user1)
            data = {'user':user1,'quote_stage':stage}
        elif name=='proforma':
            # proforma_stage = MongoAPI.fieldsSettingsData('proforma_stage',ordId)
            # proforma_stage = Uid.fix_array(proforma_stage)
            # tag = MongoAPI.fieldsSettingsData('quote_tag',ordId)
            # tag = Uid.fix_array(tag)
            stage=[]
            active={'_id':'active','name':'Active','selected':'selected'}
            stage.append(active)
            stage1 = MongoAPI.fieldsSettingsData('proforma_stage',ordId)
            stage1 = Uid.fix_array(stage1)
            for st in stage1:
                st['selected']=''
                stage.append(st)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            
            data = {'proforma_stage':stage}
        elif name=='product':
            category = MongoAPI.fieldsSettingsData('product_category',ordId)
            category = Uid.fix_array(category)
            data = {'category':category}
        elif name=='quotes':
            currency = MongoAPI.fieldsSettingsData('currency',ordId)
            currency = Uid.fix_array(currency)
            tax_type = MongoAPI.fieldsSettingsData('tax_type',ordId)
            tax_type = Uid.fix_array(tax_type)
            tax_percentage = MongoAPI.fieldsSettingsData('tax_percentage',ordId)
            tax_percentage = Uid.fix_array(tax_percentage)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            data = {'user':user,'currency':currency,'tax_type':tax_type,'tax_percentage':tax_percentage}
        elif name=='invoice':
            # invoice_stage = MongoAPI.fieldsSettingsData('invoice_stage',ordId)
            # invoice_stage = Uid.fix_array(invoice_stage)
            stage=[]
            active={'_id':'active','name':'Active','selected':'selected'}
            stage.append(active)
            stage1 = MongoAPI.fieldsSettingsData('invoice_stage',ordId)
            stage1 = Uid.fix_array(stage1)
            for st in stage1:
                st['selected']=''
                stage.append(st)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            data = {'invoice_stage':stage}

        elif name=='organization':
            source = MongoAPI.fieldsSettingsData('source',ordId)
            source = Uid.fix_array(source)
            customer_type = MongoAPI.fieldsSettingsData('customer_type',ordId)
            customer_type = Uid.fix_array(customer_type)
            user = MongoAPI.usersList(ordId)
            user = Uid.fix_array(user)
            data = {'user':user,'source':source,'customer_type':customer_type}

    return jsonify({"msg": "Filters List",'data': data,"code": 200})


@app.route('/<path:filename>')
def image(filename):
    try:
        w = int(request.args['w'])
        h = int(request.args['h'])
    except (KeyError, ValueError):
        return send_from_directory('.', filename)

    try:
        im = Image.open(filename)
        im.thumbnail((w, h), Image.ANTIALIAS)
        io = StringIO.StringIO()
        im.save(io, format='JPEG')
        return Response(io.getvalue(), mimetype='image/jpeg')

    except IOError:
        abort(404)

    return send_from_directory('.', filename)


@app.route('/dealsListData', methods=['GET'])
@jwt_required
def dealsListData():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)

        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        

        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getDealsNewListData(ordId,length,page,sort,order,id,associate_to)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        # data = {'deals':response1,'stage':stage_data2}

        deals_total = MongoAPI.getDealsCountData(ordId)
        deals_total = MongoAPI.getDealsAssociateCountData(ordId,id,associate_to)
        
        data = {'total_count':deals_total,'items':response1}
        return data



@app.route('/dealsListNewData', methods=['GET'])
@jwt_required
def dealsList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
               
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.getDealsList(org_id,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            response = Uid.fix_array(response)
            # response = response1.to_json()
            # print (response)
            data = {'deals':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Deals list","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/timeline', methods=['GET'])
@jwt_required
def timeline():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)

        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})

        
        response1 = MongoAPI.getTimeline(ordId,id,associate_to)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'timeline':response1}

        if data:
            return jsonify({"msg": "Timeline List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/companyStatusChange', methods=['PUT'])
@jwt_required
def companyStatusChange():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        id =  request.json.get('id', None)
        status =  request.json.get('status', None)

        if not id:
            return jsonify({"msg": "Missing company id parameter","code": 400})
        if not status:
            return jsonify({"msg": "Missing status parameter","code": 400})

        if status=='Active':
            old_status='Inactive'
            
        elif status=='Inactive':
            old_status='Active'
        
        response = MongoAPI.companyStatusChangeUpdate(id,status)
        data1 = json.loads(json_util.dumps(request.json))

        action = 'STATUS_CHANGE'
        associate_to = 'company'
        associate_id = ObjectId(id)
        via = 0
        extra_info = json.loads(json_util.dumps(data1))
        text_info='Company  Status has been changed from '+old_status+' to '+status
        title = 'Status Changed'
        MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

        if response:
            return jsonify({"msg": text_info,"code": 200,"data": response})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/productUpload/<id>', methods=['POST'])
@jwt_required 
def productUpload(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'files[]' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            
            files = request.files.getlist('files[]')
            for file in files:
                filenameData = []
                if file and allowed_file(file.filename,app_config.PROFILE_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    if os.path.exists(os.path.join(app.config['UPLOAD_PRODUCT_FOLDER'],str(ordId)))==True:
                        path='true'
                    else:
                        access =  0o755
                        path='uploads/product/'+str(ordId)
                        os.mkdir(path, access)
                    file.save(os.path.join(app.config['UPLOAD_PRODUCT_FOLDER'],str(ordId),filename1))
                    filenameData.append(filename1)
                    response = MongoAPI.productUpload(ordId,id,filenameData)

            data1 = json.loads(json_util.dumps(request.args))
            
            action = 'PRODUCT_UPLOAD'
            associate_to = 'product'
            associate_id = ObjectId(id)
            via = 0
            extra_info = json.loads(json_util.dumps(data1))
            text_info = 'New Image has been added'
            title = 'Product Upload'
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

            return jsonify({"msg": "File(s) successfully uploaded","code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})


@app.route('/productImageRemove/<product_id>/<index>', methods=['DELETE'])
@jwt_required
def productImageRemove(product_id,index):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    response1 = MongoAPI.productImageRemove(ordId,product_id,index)

    return jsonify({"msg": "File(s) Deleted","code": 200})


@app.route('/statusChange', methods=['PUT'])
@jwt_required
def statusChange():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})
        status =  request.json.get('status', None)
             
        
        if not status:
            return jsonify({"msg": "Missing status parameter","code": 400})

        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)

        old_status =  request.json.get('old_status', None)
        new_status =  request.json.get('new_status', None)
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:
            response1 = MongoAPI.changeStatus(org_id,data)
            response = json.loads(json_util.dumps(response1))
            response2 = str(response)

            id = associate_id

            if associate_to == 'quote':
                action = 'UPDATE_STAGE'
                text_info = str('Stage has been changed from  ')+str(old_status)+str(' to ')+str(new_status)
                title = 'Quote Change Stage'
                associate_id = ObjectId(id)
                msg = "Quote stage successfully changed !"

                response1 = MongoAPI.getQuoteDetails(org_id,id)
                
            elif associate_to == 'proforma':
                action = 'UPDATE_STAGE'
                text_info = str('Stage has been changed from  ')+str(old_status)+str(' to ')+str(new_status)
                title = 'Proforma Change Stage'
                associate_id = ObjectId(id)
                msg = "Proforma stage successfully changed !"
                response1 = MongoAPI.getProformaDetails(org_id,id)
                
            elif associate_to == 'invoice':
                action = 'UPDATE_STAGE'
                text_info = str('Stage has been changed from  ')+str(old_status)+str(' to ')+str(new_status)
                title = 'Invoice Change Stage'
                associate_id = ObjectId(id)
                msg = "Invoice stage successfully changed !"
                response1 = MongoAPI.getInvoiceDetails(org_id,id)
            elif associate_to == 'deal':
                action = 'UPDATE_STAGE'
                text_info = str('Stage has been changed from  ')+str(old_status)+str(' to ')+str(new_status)
                title = 'Deal Change Stage'
                associate_id = ObjectId(id)
                msg = "Deal stage successfully changed !"
                response1 = MongoAPI.getDealDetails(org_id,id)

            
            response4 = Uid.fix_array3(response1)
            data = '1'
            data = response4
            if data:
                action = 'UPDATE_STAGE'
                via = 0
                extra_info = json.loads(json_util.dumps(data))
                

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": msg,"code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/status/<name>', methods=['GET'])
@jwt_required
def status(name):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if name=='deal':
            stage = MongoAPI.fieldsSettingsData('stage',ordId)
            stage = Uid.fix_array(stage)
            data = stage
        elif name=='quote':
            quote_stage = MongoAPI.fieldsSettingsData('quote_stage',ordId)
            quote_stage = Uid.fix_array(quote_stage)
            data = quote_stage
        elif name=='proforma':
            proforma_stage = MongoAPI.fieldsSettingsData('proforma_stage',ordId)
            proforma_stage = Uid.fix_array(proforma_stage)
            data = proforma_stage
        elif name=='invoice':
            invoice_stage = MongoAPI.fieldsSettingsData('invoice_stage',ordId)
            invoice_stage = Uid.fix_array(invoice_stage)
            data = invoice_stage
    return jsonify({"msg": "Stage List",'data': data,"code": 200})


@app.route('/productUpload/<id>', methods=['PUT'])
@jwt_required 
def productImageUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})

    files =  request.json.get('files', None)

    if not files:
        return jsonify({"msg": "Missing files parameter","code": 400})           
                    
    response = MongoAPI.productUpload(id,files)

    if response=='Yes':
        data1 = json.loads(json_util.dumps(request.is_json))
        action = 'PRODUCT_UPLOAD'
        associate_to = 'product'
        associate_id = ObjectId(id)
        via = 0
        extra_info = json.loads(json_util.dumps(data1))
        text_info = 'Image has been updated'
        title = 'Product image updated'
        MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
        return jsonify({"msg": "File(s) successfully updated","code": 400})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

	


@app.route('/productImageDefault/<id>', methods=['PUT'])
@jwt_required
def productImageDefault(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        default_image =  request.json.get('default_image', None)

        if not id:
            return jsonify({"msg": "Missing product id parameter","code": 400})
        if not default_image:
            return jsonify({"msg": "Missing default image parameter","code": 400})

        
        
        response = MongoAPI.productImageDefault(id,default_image)
        data1 = json.loads(json_util.dumps(request.json))

        action = 'STATUS_CHANGE'
        associate_to = 'company'
        associate_id = ObjectId(id)
        via = 0
        extra_info = json.loads(json_util.dumps(data1))
        text_info ='Product Image Default '+default_image
        title = 'Status Changed'
        MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)

        if response:
            return jsonify({"msg": text_info,"code": 200,"data": response})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/sendMail1', methods=['POST'])
@jwt_required
def sendMail1():
    # print (request.args)
    name = request.form.get('name', None)
    return jsonify(name = name)
    # return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/sendMail', methods=['POST'])
@jwt_required
def sendMail():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        if request.method == 'POST':
            
            # print (request.form.get('associate_id', None))

            # msg = Message('Hello',sender =emailId,recipients = [to]) 

            # msg.body = 'Hello Flask message sent from Flask-Mail'


            if 'attachment[]' not in request.files:
                resp = jsonify({'msg' : 'No file part in the request','code':'400'})
                return resp
            else:
                filenameData = []
                filenameData2 = []
                path = str(app_config.BASE_URL)+app.config['UPLOAD_OPEN_EMAIL_FOLDER']
                files = request.files.getlist('attachment[]')
                for file in files:
                    if file and allowed_file(file.filename,app_config.ALLOWED_EXTENSIONS):
                        filename = secure_filename(file.filename)
                        file_type = file.filename
                        file_format = splitpart(file_type,-1,'.')
                        file_name = splitpart(file_type,0,'.')
                        filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                        file.save(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename1))
                        filenameData.append(filename1)
                        filename2 = str(path)+str(filename1)
                        filenameData2.append(filename2)

                        
                associate_id =  request.form.get('associate_id', None)
                associate_to =  request.form.get('associate_to', None)


                to =  request.form.get('to', None)
                cc =  request.form.get('cc', None)
                bcc =  request.form.get('bcc', None)
                fromEmail =  request.form.get('from', None)
                subject =  request.form.get('subject', None)
                pdf =  request.form.get('pdf', None)
                crm_docs =  request.form.get('crm_docs', None)
                email_message =  request.form.get('email_message', None)

                docs = crm_docs.split(',')

                docData = []


                path = str(app_config.BASE_URL)+app.config['UPLOAD_OPEN_CRM_DOCS_FOLDER']
                
                for doc in docs:
                    doc1 =  str(path)+str(doc)
                    docData.append(doc1) 
                
                if not to:
                    return jsonify({"msg": "Missing to id parameter","code": 400})
                if not fromEmail:
                    return jsonify({"msg": "Missing from parameter","code": 400})
                if not subject:
                    return jsonify({"msg": "Missing subject parameter","code": 400})
                if not email_message:
                    return jsonify({"msg": "Missing email message parameter","code": 400})

            

                action = 'SEND_MAIL'
                associate_to = associate_to
                associate_id = ObjectId(associate_id)
                via = 0
                data1 = json.loads(json_util.dumps(request.form))
                extra_info = json.loads(json_util.dumps(request.form))
                text_info = 'Send Mail'
                title = 'Send Mail'
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)


                if associate_to=='quote':
                    mes = 'Email has been sent'
                elif associate_to=='proforma':
                    mes = 'Email has been sent'
                elif associate_to=='invoice':
                    mes = 'Email has been sent'

                if mes:
                    return jsonify({"msg": mes,"code": 200})
                else:
                    return jsonify({"msg": "Oops,Something went wrong !","code": 400})

# @app.route('/salesProcess', methods=['POST'])
# @jwt_required
# def salesProcess():
#     current_user = get_jwt_identity()
#     user = MongoAPI.authorizationCheck(current_user)

#     ordId = ''
#     for user2 in user:
#         if user2=='org_id':
#             ordId = user[user2]

#     if ordId=='':
#         return jsonify({"msg": "Oops,Something went wrong !","code": 400})
#     else:
#         if not request.is_json:
#             return jsonify({"msg": "Missing JSON in request"})

#         process_name =  request.json.get('process_name', None)
#         field_type =  request.json.get('field_type', None)
#         values =  request.json.get('list_value', None)
            
#         if not process_name:
#             return jsonify({"msg": "Missing process name parameter","code": 400})
#         if not field_type:
#             return jsonify({"msg": "Missing field type parameter","code": 400})

#         data1 = json.loads(json_util.dumps(request.json))

#         data = data1
        
#         org_id = int(ordId)

#         if org_id:

#             response1 = MongoAPI.salesProcess(org_id,current_user,data1)
#             response = json.loads(json_util.dumps(response1))

#             response2 = str(response)
#             # response1 = MongoAPI.getDealDetails(org_id,response2)
#             # response4 = Uid.fix_array3(response1)
#             # data = '1'
#             data = response
#             if data:
#                 action = 'CREATE'
#                 associate_to = 'sales_process'
#                 associate_id = ObjectId(response2)
#                 via = 0
#                 extra_info = json.loads(json_util.dumps(data1))
#                 text_info = app_config.CREATE_INFO
#                 title = app_config.CREATE_TITLE

#                 MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
#                 return jsonify({"msg": "Sales Process successfully created !","code": 200,"data": data})
#             else:
#                 return jsonify({"msg": "Oops,Something went wrong !","code": 400})
#         else:
#             return jsonify({"msg": "Oops,Something went wrong !","code": 400})



@app.route('/customFields', methods=['POST'])
@jwt_required
def addCustomFields():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        field_name =  request.json.get('field_name', None)
        field_type =  request.json.get('field_type', None)
        values =  request.json.get('list_value', None)
            
        if not field_name:
            return jsonify({"msg": "Missing field name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.addCustomFields(org_id,current_user,data1)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            data = response
            if data:
                action = 'CREATE'
                associate_to = 'custom_fields'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Custom Fields successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/customFields/<module>', methods=['GET'])
@jwt_required
def getCustomFields(module):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getCustomFields(ordId,module)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'custom_fields':response1}

        if data:
            return jsonify({"msg": "Custom Fields List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/customFields/<id>', methods=['PUT'])
@jwt_required
def updateCustomFields(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        field_name =  request.json.get('field_name', None)
        field_type =  request.json.get('field_type', None)
            
        if not field_name:
            return jsonify({"msg": "Missing field name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.updateCustomFields(org_id,data1,id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            data = '1'
            if data:
                action = 'Update'
                associate_to = 'custom_fields'
                associate_id = ObjectId(response2)
                via = 0
                extra_info =''
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Custom Fields successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/customFieldsDetail/<id>', methods=['GET'])
@jwt_required
def customFieldsDetail(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getCustomFieldsDetails(ordId,id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'custom_fields':response1}

        if data:
            return jsonify({"msg": "Custom Fields List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})




@app.route('/salesProcess', methods=['POST'])
@jwt_required
def salesProcess():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        process_name =  request.json.get('process_name', None)
        field_type =  request.json.get('field_type', None)
        values =  request.json.get('list_value', None)
            
        if not process_name:
            return jsonify({"msg": "Missing process name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesProcess(org_id,current_user,data1)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            # data = '1'
            data = response
            if data:
                action = 'CREATE'
                associate_to = 'sales_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Process successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessTemplate', methods=['POST'])
@jwt_required
def salesProcessTemplate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        template_name =  request.json.get('template_name', None)
        process_list =  request.json.get('process_list', None)
            
        if not template_name:
            return jsonify({"msg": "Missing template name parameter","code": 400})
        if not process_list:
            return jsonify({"msg": "Missing process list parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesProcessTemplate(org_id,current_user,data1)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'CREATE'
                associate_to = 'sales_process_template'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Process Template successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessTemplate', methods=['GET'])
@jwt_required
def getSalesProcessTemplate():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesProcessTemplate(ordId)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'sales_process_template':response1}

        if data:
            return jsonify({"msg": "Sales Process Template List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessTemplate/<id>', methods=['GET'])
@jwt_required
def getSalesProcessTemplateDetail(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesProcessTemplateDetail(ordId,id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'sales_process_template':response1}

        if data:
            return jsonify({"msg": "Sales Process Template Detail","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessTemplateDefault/', methods=['GET'])
@jwt_required
def getSalesProcessTemplateDetailDefault():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesProcessTemplateDefault(ordId)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'sales_process_template':response1}

        if data:
            return jsonify({"msg": "Sales Process Template Default","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcessTemplate/<id>', methods=['PUT'])
@jwt_required
def salesProcessTemplateUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        template_name =  request.json.get('template_name', None)
        process_list =  request.json.get('process_list', None)
            
        if not template_name:
            return jsonify({"msg": "Missing template name parameter","code": 400})
        if not process_list:
            return jsonify({"msg": "Missing process list parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesProcessTemplateUpdate(org_id,data1,id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'Update'
                associate_to = 'sales_process_template'
                associate_id = ObjectId(response2)
                via = 0
                extra_info =''
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Process Template successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        
@app.route('/salesProcessTemplate/<id>', methods=['DELETE'])
@jwt_required
def deleteSalesProcessTemplate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteSalesTemplate(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Sales process Template deleted successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/changeSalesProcessTemplate/<associate_id>/<id>', methods=['GET'])
@jwt_required
def changeSalesProcessTemplate(associate_id,id):
    associate_to='deal'
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        # response1 = MongoAPI.getSalesProcessTemplateDetail(ordId,id)
        process1 = MongoAPI.getSalesProcessTemplateDetail(ordId,id)
        process = json.loads(json_util.dumps(process1))
        process1 = Uid.fix_array_sub(process)
        checkList = MongoAPI.insertCheckList(ordId,process,associate_to,associate_id) 
        response = json.loads(json_util.dumps(process1))
        response1 = Uid.fix_array_sub(response)
        data = {'sales_process_template':response1}

        if data:
            return jsonify({"msg": "Sales Process Template Detail","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/salesProcess', methods=['GET'])
@jwt_required
def getSalesProcess():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesProcess(ordId)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'sales_process':response1}

        if data:
            return jsonify({"msg": "Sales Process List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcess/<id>', methods=['GET'])
@jwt_required
def getSalesProcessDetail(id):


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesProcessDetail(ordId,id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'sales_process':response1}

        if data:
            return jsonify({"msg": "Sales Process Detail","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcess/<id>', methods=['PUT'])
@jwt_required
def salesProcessUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        process_name =  request.json.get('process_name', None)
        field_type =  request.json.get('field_type', None)
        values =  request.json.get('list_value', None)
            
        if not process_name:
            return jsonify({"msg": "Missing process name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesProcessUpdate(org_id,current_user,data1,id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'sales_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Process successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesProcess/<id>', methods=['DELETE'])
@jwt_required
def deleteSalesProcess(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteSalesProcess(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Sales process deleted successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcess', methods=['POST'])
@jwt_required
def salesSubProcess():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        process_name =  request.json.get('process_name', None)
        field_type =  request.json.get('field_type', None)
        values =  request.json.get('list_value', None)
            
        if not process_name:
            return jsonify({"msg": "Missing process name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesSubProcess(org_id,current_user,data1)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'CREATE'
                associate_to = 'sales_sub_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Sub Process successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcess', methods=['GET'])
@jwt_required
def getSalesSubProcess():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesSubProcess(ordId)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'sales_sub_process':response1}

        if data:
            return jsonify({"msg": "Sales Sub Process List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcess/<id>', methods=['GET'])
@jwt_required
def getSalesSubProcessDetail(id):


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesSubProcessDetail(ordId,id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'sales_process':response1}

        if data:
            return jsonify({"msg": "Sales Process Detail","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcessList/<parent_id>', methods=['GET'])
@jwt_required
def getsalesSubProcessList(parent_id):


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getSalesSubProcessList(ordId,parent_id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'sales_process':response1}

        if data:
            return jsonify({"msg": "Sales Process Detail","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcess/<id>', methods=['PUT'])
@jwt_required
def salesSubProcessUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        process_name =  request.json.get('process_name', None)
        field_type =  request.json.get('field_type', None)
        values =  request.json.get('list_value', None)
            
        if not process_name:
            return jsonify({"msg": "Missing process name parameter","code": 400})
        if not field_type:
            return jsonify({"msg": "Missing field type parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.salesSubProcessUpdate(org_id,current_user,data1,id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'sales_sub_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "Sales Sub Process successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/salesSubProcess/<id>', methods=['DELETE'])
@jwt_required
def deleteSalesSubProcess(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)
        id = ObjectId(id)
     
        response = MongoAPI.deleteSalesSubProcess(id)
        response1 = json.loads(json_util.dumps(response))

        if response:
            return jsonify({"msg": "Sales sub process deleted successfully","code": 200,"response": response1})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/getSalesProcessTest', methods=['GET'])
@jwt_required
def getSalesProcessTest():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        process1 = MongoAPI.getSalesProcess(ordId)
        process = json.loads(json_util.dumps(process1))
        process1 = Uid.fix_array(process)



        data = {'sales_process':process,'sales_sub_process':sub_process}
       
        test = MongoAPI.insertCheckList(process,sub_process)



        if data:
            return jsonify({"msg": "Sales Sub Process List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/getChecklist', methods=['GET'])
@jwt_required
def getCheckList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
       
        associate_to =  request.args.get('associate_to', None)
        associate_id =  request.args.get('associate_id', None)

        if not associate_to:
            return jsonify({"msg": "Missing associate_to parameter","code": 400})
        if not associate_id:
            return jsonify({"msg": "Missing associate_id parameter","code": 400})

        response1 = MongoAPI.getCheckList(ordId,associate_id,associate_to)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)
        data = {'check_list':response1}

        if data:
            return jsonify({"msg": "Check List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/updateCheckList/<deal_id>/<id>', methods=['PUT'])
@jwt_required
def updateCheckList(deal_id,id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.updateCheckList(org_id,current_user,id,data,deal_id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'checklist_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "CheckList successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/updateCheckListQuote/<associate_id>/<process_id>', methods=['PUT'])
@jwt_required
def updateCheckListQuote(associate_id,process_id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.updateCheckListQuote(org_id,associate_id,process_id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                      
                return jsonify({"msg": "CheckList successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/updateCheckListSub/<id>', methods=['PUT'])
@jwt_required
def updateCheckListSub(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.updateCheckListSub(org_id,current_user,id,data)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'checklist_process'
                associate_id = ObjectId(response2)
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "CheckList successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/updateDealStage/<deal_id>', methods=['GET'])
@jwt_required
def updateDealStage(deal_id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        
        org_id = int(ordId)

        if org_id:

            response1 = MongoAPI.updateDealStage(org_id,deal_id)
            response = json.loads(json_util.dumps(response1))

            response2 = str(response)
            # response1 = MongoAPI.getDealDetails(org_id,response2)
            # response4 = Uid.fix_array3(response1)
            data = '1'
            # data = response4
            if data:
                action = 'UPDATE'
                associate_to = 'checklist_process'
                associate_id = '1'
                via = 0
                extra_info = ''
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE

                # MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)            
                return jsonify({"msg": "CheckList successfully updated !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/roles', methods=['POST'])
@jwt_required
def roleSubmit():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        role_name =  request.json.get('role_name', None)
        description =  request.json.get('description', None)
        

        if not role_name:
            return jsonify({"msg": "Missing role name parameter","code": 400})
        if not description:
            return jsonify({"msg": "Missing description parameter","code": 400})
        
        data1 = json.loads(json_util.dumps(request.json))
        response1 = MongoAPI.roleSubmit(ordId,current_user,data1)
        data = response1

        if data:
            action = 'CREATE'
            associate_to = 'roles'
            associate_id = ObjectId(response1)
            via = 0
            extra_info = json.loads(json_util.dumps(data1))
            text_info = app_config.CREATE_INFO
            title = app_config.CREATE_TITLE
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)   

            return jsonify({"msg": "Role successfully created !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/roles/<id>', methods=['PUT'])
@jwt_required
def roleUpdate(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        role_name =  request.json.get('role_name', None)
        description =  request.json.get('description', None)
        

        if not role_name:
            return jsonify({"msg": "Missing role name parameter","code": 400})
        if not description:
            return jsonify({"msg": "Missing description parameter","code": 400})
        
        data1 = json.loads(json_util.dumps(request.json))
        response1 = MongoAPI.roleUpdate(ordId,data1,id)
        data = response1

        if data:
            action = 'UPDATE'
            associate_to = 'role'
            associate_id = ObjectId(response1)
            via = 0
            extra_info = data1
            text_info = 'Update Role'
            title = 'Update Role'
            MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)   

            return jsonify({"msg": "Role successfully updated !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/roles/<id>', methods=['GET'])
@jwt_required
def getRoleDetails(id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        response1 = MongoAPI.getRolesListDetails(ordId,id)
        response4 = Uid.fix_array_role(response1)
        data = response4
        if response1:
            return jsonify({"msg": "Role successfully updated !","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/roles', methods=['GET'])
@jwt_required
def getRolesList():

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''



        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='role_name':
            sort='role_name'
        elif sort:
            sort=sort
        else:
            sort='role_name'

        length = int(length)
        page = int(page)




        response1 = MongoAPI.getRolesList(ordId,length,page,search,sort,order)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        # data = {'deals':response1,'stage':stage_data2}
        # response1=''
        deals_total = MongoAPI.getRolesCountData(ordId)
        data = {'total_count':deals_total,'items':response1}
        return data


def allowed_file(filename,type):
	return '.' in filename and filename.rsplit('.', 1)[1].lower() in type

def splitpart (value, index, char = ','):
    return value.split(char)[index]

@app.route('/test', methods=['POST'])
@jwt_required
def test():
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    name =  request.json.get('name', None)        
    response = MongoAPI.test(email,name,2,password)
    return jsonify({"msg": "Missing password parameter","code": 400})


def roleNewSubmit(org_id,user_id):

    data1 = {
                "role_name":"Administrator",
                "description":"Administrator",
                "contact":1,
                "contact_view_all":1,
                "contact_view_own":1,
                "contact_edit":1,
                "contact_delete":1,
                "company":1,
                "company_view_all":1,
                "company_view_own":1,
                "company_edit":1,
                "company_delete":1,
                "deal":1,
                "deal_view_all":1,
                "deal_view_own":1,
                "deal_edit":1,
                "deal_delete":1,
                "product":1,
                "product_view_all":1,
                "product_view_own":1,
                "product_edit":1,
                "product_delete":1,
                "quote":1,
                "quote_view_all":1,
                "quote_view_own":1,
                "quote_edit":1,
                "quote_delete":1,
                "proforma":1,
                "proforma_view_all":1,
                "proforma_view_own":1,
                "proforma_edit":1,
                "proforma_delete":1,
                "invoice":1,
                "invoice_view_all":1,
                "invoice_view_own":1,
                "invoice_edit":1,
                "invoice_delete":1,
                "settings":1,
                "default":1,
                "document":1

            }

    response1 = MongoAPI.roleSubmit(org_id,user_id,data1)

def renameSubmit(org_id):
    response1 = MongoAPI.renameSubmit(org_id)

def newCompany(org_id,user_id):

    source = {'Direct Mail','Facebook','Linked In','Referral','Whatsapp','Indiamart','Cold Call','Trade India','Trade Fair'}
    tag = {'Working - Contacted','Interested','Not Qualified','Future Requirement','No Response'}
    customer_type = {'End Customer','Partner','Distributor','OEM'}
    application = {'Application 1','Application 2'}
    industry = {'Agriculture And Allied Industries','Automobiles','Auto Components','Aviation','Banking','Biotechnology','Cement','Chemicals','Consumer Durables','Defence Manufacturing','E-commerce','Education And Training','Electronics System Design & Manufacturing','Engineering And Capital Goods','Financial Services','Fmcg','Gems And Jewellery','Healthcare','Infrastructure','Insurance','It & Bpm','Manufacturing','Media And Entertainment','Medical Devices','Metals And Mining','Msme','Oil And Gas','Pharmaceuticals','Ports','Power','Railways','Real Estate','Renewable Energy','Retail','Roads','Science And Technology','Services','Steel','Telecommunications','Textiles','Tourism And Hospitality'}
    # Deal
    stage = {'New Opportunity','Quote Sent','Expect','Negotiation','Commit','Order in hand','Won','Lost','Revised'}
    won_reason = {'Brand Name','Repeat business','Quick Delivery','Referred By Existing Customer'}
    lost_reason = {'Price is High','Project Postponed','Budget not available','Old Enquiries','No Response'}
    # Product & Tax
    product_category= {'Category A','Category B'}
    tax_type= {'SGST','CGST','IGST'}
    tax_percentage = {'5','6','9','12','14','18','28'}
    currency ={'INR','USD','EURO','AUD','GBP','MYR','AED','QR',}
    # Quote
    quote_stage = {'Created','Sent','Revised','Won','Lost'}
    category= {'Sales','Service'}
    # validity= {'30 Days','15 Days','60 Days','1 Week'}
    shipping= {'By Road','Air Cargo','By Sea','Deliver by hand','Customer Pickup'}
    payment_terms= {'100% Advance','70 % advance 30% before dispatch','50% Advance with PO & Balance 50% before dispatch','Against Delivery','Net 30 Days'}
    # Proforma & Invoice
    invoice_stage = {'Created','Sent','Won','Lost'}
    proforma_stage = {'Created','Sent','Won','Lost'}
    # print (source)

    date_format = {'dd-mm-yyyy','mm-dd-yyyy','mm-dd-yy'}
    days = {'Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'}
    deal_stage_weightage={
    'New Opportunity':0,
    'Quote Sent':20,
    'Expect':50,
    'Negotiation':70,
    'Commit':90,
    'Order in hand':98,
    'Won':100,
    'Lost':0,
    'Revised':0
   }

    deal_tag={
            'Immediate Purchase',
            'Project On Hold',
            'Future Requirement',
            'No Proper Response'
    }

    contact_tag={
        'Interested',
        'Not Interested',
        'Potential Customer',
        'Future Requirement'

    }

    uom={
        'Nos',
        'Kg',
        'Pc',
        'Cm',
        'Inch'
    }


    # carrier = {'Air Cargo','By Air Door to Door','By Road','By Sea','Customer Pickup','Deliver by hand'}

    delivery_period = {'Immediate','1 Week','2 Days','2 Weeks','3 Weeks','4 Weeks','5 weeks','6 Weeks','8 to 10 weeks','6 to 8 Weeks','10 to 12 Weeks'}

    validity = {'30'}
    
    

    settings = {'delivery_period':delivery_period,
    'validity':validity,
    'date_format':date_format,
    'days':days,
    'source':source,
    'tag':tag,
    'customer_type':customer_type,
    'application':application,
    'industry':industry,
    'stage':stage,
    'deal_stage':stage,
    'won_reason':won_reason,
    'lost_reason':lost_reason,
    'product_category':product_category,
    'tax_type':tax_type,
    'tax_percentage':tax_percentage,
    'currency':currency,
    'quote_stage':quote_stage,
    'category':category,
    'shipping':shipping,
    'payment_terms':payment_terms,
    'invoice_stage':invoice_stage,
    'proforma_stage':proforma_stage,
    'deal_tag':deal_tag,
    'contact_tag':contact_tag,
    'uom':uom
    }
    for key in settings:
        settings1 = settings[key]
        for key2 in settings1:
            count = MongoAPI.fieldsSettingsCount(key,org_id)
            if key=='deal_stage':
                response = MongoAPI.addSettings(key,key2,int(deal_stage_weightage[key2]),'',org_id,count,0)
            else:
                response = MongoAPI.addSettings(key,key2,0,'',org_id,count,0)

    reorder=MongoAPI.reorderDealStage(org_id)

    # type = 'deal'
    # name = 'deal_no'
    # prefix = 'DE'
    # sequence = '1001'
    # response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,org_id)

    type = 'quote'
    name = 'quote_no'
    prefix = 'QUO'
    sequence = '1001'
    response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,org_id)


    type = 'proforma'
    name = 'proforma_no'
    prefix = 'PRO'
    sequence = '1001'
    response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,org_id)

    type = 'invoice'
    name = 'invoice_no'
    prefix = 'INV'
    sequence = '1001'
    response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,org_id)

    response1 = MongoAPI.renameSubmit(org_id)

    data1 = {
                "role_name":"Administrator",
                "description":"Administrator",
                "contact":1,
                "contact_view_all":1,
                "contact_view_own":1,
                "contact_edit":1,
                "contact_delete":1,
                "company":1,
                "company_view_all":1,
                "company_view_own":1,
                "company_edit":1,
                "company_delete":1,
                "deal":1,
                "deal_view_all":1,
                "deal_view_own":1,
                "deal_edit":1,
                "deal_delete":1,
                "product":1,
                "product_view_all":1,
                "product_view_own":1,
                "product_edit":1,
                "product_delete":1,
                "quote":1,
                "quote_view_all":1,
                "quote_view_own":1,
                "quote_edit":1,
                "quote_delete":1,
                "proforma":1,
                "proforma_view_all":1,
                "proforma_view_own":1,
                "proforma_edit":1,
                "proforma_delete":1,
                "invoice":1,
                "invoice_view_all":1,
                "invoice_view_own":1,
                "invoice_edit":1,
                "invoice_delete":1,
                "settings":1,
                "pricelist":1,
                "default":1,
                "document":1

            }

    sales_executive = {
                "role_name":"Sales Executive",
                "description":"Sales Executive",
                "contact":1,
                "contact_view_all":1,
                "contact_view_own":1,
                "contact_edit":1,
                "contact_delete":1,
                "company":1,
                "company_view_all":1,
                "company_view_own":1,
                "company_edit":1,
                "company_delete":1,
                "deal":1,
                "deal_view_all":1,
                "deal_view_own":1,
                "deal_edit":1,
                "deal_delete":1,
                "product":1,
                "product_view_all":1,
                "product_view_own":1,
                "product_edit":1,
                "product_delete":1,
                "quote":1,
                "quote_view_all":1,
                "quote_view_own":1,
                "quote_edit":1,
                "quote_delete":1,
                "proforma":1,
                "proforma_view_all":1,
                "proforma_view_own":1,
                "proforma_edit":1,
                "proforma_delete":1,
                "invoice":1,
                "invoice_view_all":1,
                "invoice_view_own":1,
                "invoice_edit":1,
                "invoice_delete":1,
                "settings":1,
                "pricelist":0,
                "default":0,
                "document":1

            }

    sales_manager = {
                "role_name":"Sales Manager",
                "description":"Sales Manager",
                "contact":1,
                "contact_view_all":1,
                "contact_view_own":1,
                "contact_edit":1,
                "contact_delete":1,
                "company":1,
                "company_view_all":1,
                "company_view_own":1,
                "company_edit":1,
                "company_delete":1,
                "deal":1,
                "deal_view_all":1,
                "deal_view_own":1,
                "deal_edit":1,
                "deal_delete":1,
                "product":1,
                "product_view_all":1,
                "product_view_own":1,
                "product_edit":1,
                "product_delete":1,
                "quote":1,
                "quote_view_all":1,
                "quote_view_own":1,
                "quote_edit":1,
                "quote_delete":1,
                "proforma":1,
                "proforma_view_all":1,
                "proforma_view_own":1,
                "proforma_edit":1,
                "proforma_delete":1,
                "invoice":1,
                "invoice_view_all":1,
                "invoice_view_own":1,
                "invoice_edit":1,
                "invoice_delete":1,
                "settings":1,
                "pricelist":0,
                "default":0,
                "document":1

            }

    response1 = MongoAPI.roleSubmit(org_id,user_id,data1)
    response2 = MongoAPI.roleSubmit(org_id,user_id,sales_executive)
    response3 = MongoAPI.roleSubmit(org_id,user_id,sales_manager)

    response4 = MongoAPI.settingsDefaultName('proforma_stage',org_id,'Created')
    response5 = MongoAPI.settingsDefaultName('invoice_stage',org_id,'Created')
    response6 = MongoAPI.settingsDefaultName('customer_type',org_id,'End Customer')

    tname1 = 'General Enquiry'
    tsubject1 = 'Thanks for your valuable Enquiry !'
    tcontent1='<p>Thank you for your interest in our products. In response to your inquiry, please find the attached list of our products in which we are dealing with. A separate list of prices is also attached to the item we are manufacturing.  We also offer huge discounts on special occasions.</p><p>I hope you are satisfied with the above information. In case you would like to have more information, we are happy to arrange a call for further queries.</p>'
    response7 = MongoAPI.addEmailTemplate(tname1,tcontent1,tsubject1,org_id,1,0)


    tname2 = 'Quote Proposal'
    tsubject2 = 'Quote Proposal'
    tcontent2='<p>Thank you very much for allowing us the opportunity to send a quote proposal for your requirements. Based on our last conversation last week, I have attached the detailed Quote for your kind perusal.</p><p>We hope to hear from you soon and we also hope this is the start of a fruitful working relationship between our companies.</p>'
    response8 = MongoAPI.addEmailTemplate(tname2,tcontent2,tsubject2,org_id,2,0)

    tname3 = 'Sales Follow Up'
    tsubject3 = 'Follow up to our last conversation'
    tcontent3='<p>Hope you are doing well. I’m writing to follow up on our last conversation. Have you given any additional thought to my proposal? I’d be happy to do a quick review of it on the phone and answer any pending questions.</p>'
    response9 = MongoAPI.addEmailTemplate(tname3,tcontent3,tsubject3,org_id,3,0)

    p1 = 'Self Introduction'
    f1 = 'none'
    s1 = MongoAPI.addsalesProcessDefault(org_id,user_id,p1,f1)

    p2='Understanding Requirement'
    f2='none'
    s2=MongoAPI.addsalesProcessDefault(org_id,user_id,p2,f2)

    p3='Preparation of Proposal'
    f3='quote'
    s3=MongoAPI.addsalesProcessDefault(org_id,user_id,p3,f3)

    p4='Presentation of proposal'
    f4='none'
    s4=MongoAPI.addsalesProcessDefault(org_id,user_id,p4,f4)

    p5='Finalise the option with revised quote'
    f5='none'
    s5=MongoAPI.addsalesProcessDefault(org_id,user_id,p5,f5)

    p6='Finalise target closure date'
    f6='datepicker'
    s6=MongoAPI.addsalesProcessDefault(org_id,user_id,p6,f6)

    p7='Follow up'
    f7='followup'
    s7=MongoAPI.addsalesProcessDefault(org_id,user_id,p7,f7)

    p8='Get PO confirmation'
    f8='upload'
    s8=MongoAPI.addsalesProcessDefault(org_id,user_id,p8,f8)

    p9='WON / LOST'
    f9='won_lost'
    s9=MongoAPI.addsalesProcessDefault(org_id,user_id,p9,f9)

    sp1='Context of Requirement'
    sf1='textbox'
    ss1=MongoAPI.addsalesSubProcessDefault(org_id,user_id,sp1,sf1)

    sp2='Purpose of Enquiry'
    sf2='picklist'
    ss2=MongoAPI.addsalesSubProcessDefault(org_id,user_id,sp2,sf2)

    sp3='Time Frame to purchase'
    sf3='datepicker'
    ss3=MongoAPI.addsalesSubProcessDefault(org_id,user_id,sp3,sf3)

    sp4='Make a Call'
    sf4='call'
    ss4=MongoAPI.addsalesSubProcessDefault(org_id,user_id,sp4,sf4)

    sp5='Send Email'
    sf5='email'
    ss5=MongoAPI.addsalesSubProcessDefault(org_id,user_id,sp5,sf5)

    quote_pdf_settings={
        "name":"Quotation",
        "layout": "portrait",
        "logo": "1",
        "logoAlignment": "1",
        "logo_alignment": "left",
        "width": "150",
        "company_alignment": "1",
        "org_color_name": "#0c0c0d",
        "quotation_border": " ",
        "org_size": "20",
        "company_name": "Farazon",
        "org_alignment": "1",
        "org_address": "right",
        "org_address_alignment": "top",
        "bill_to_name": "Bill To",
        "account_name": "1",
        "ship_to": "1",
        "ship_to_name": "Ship To",
        "buyer_tin": "1",
        "buyer_cst": "1",
        "quote_ref_no": "1",
        "quote_ref_name": "Reference No",
        "ref_date": "0",
        "ref_date_name": "Ref Date",
        "due_date": "1",
        "due_date_name": "Due Date",
        "org_tin": "1",
        "org_cst": "1",
        "installed": "1",
        "committed": "1",
        "committed_date_name": "Commited Date",
        "delay": "1",
        "delay_name": "Delay",
        "installed_date_name": "installed_date_name ",
        "gstin": "1",
        "org_pan_no": "1",
        "ed_no": "0",
        "org_service_tax_no": "0",
        "org_cin_no": "0",
        "quote_subject": "1",
        "item_sno": "SNo",
        "item_description": "Description",
        "description_align": "center",
        "price1": "1",
        "part_no": "0",
        "part_no1": "1",
        "part_no_name": "Model",
        "packing": "0",
        "packing1": "0",
        "Taxable": "1",
        "taxable_name": "Taxable Value",
        "packing_name1": "Packing Amnt",
        "packing_name": "Packing %",
        "packing_align": "Left",
        "price": "Price",
        "qty": "1",
        "qty_name": "Qty",
        "discount": "0",
        "discount_name": "Disc %",
        "discount_price": "0",
        "discount_price_name": "Disc Price",
        "tax": "0",
        "item_tax_name": "Item Tax",
        "tax_amount": "0",
        "item_tax_amount_name": "Tax Amount",
        "item_custom": "1",
        "item_custom_field": "UOM",
        "item_amount_name": "Amount",
        "sub_total": "1",
        "sub_total_name": "Sub Total",
        "overall_discount": "1",
        "over_all_discount": "Total Discount",
        "overall_tax_amount": "1",
        "total_tax": "1",
        "total_tax_name": "",
        "tax2": "0",
        "item_tax2_name": "item_tax2_name",
        "tax2_amount": "0",
        "item_tax2_amount_name": "item_tax2_amount_name",
        "shipping": "1",
        "shipping_name": "Shipping",
        "installation": "1",
        "installation_name": "Installation",
        "amount": "Amount",
        "amount_in_words": "1",
        "proforma_recevied": "1",
        "total": "1",
        "total_round_off": "1",
        "total_name": "Total",
        "terms_conditions": "1",
        "terms_conditions_name": "Terms & Conditions",
        "bank_details": "1",
        "bank_details_name": "Bank Details",
        "remarks": "1",
        "remarks_name": "Remarks",
        "org_for": "1",
        "org_for_color": "#2a24bb",
        "prepared_by": "1",
        "prepared_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_40.jpeg",
        "checked_by": "1",
        "checked_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_43.jpeg",
        "authorized_signatory": "1",
        "authorized_signatory_path": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_16_39_41.png",
        "prepared_by_img": "602d1b9f2afaf81f68b91a96_10_2021_12_20_40.jpeg",
        "checked_by_img": "1",
        "authorized_signatory_img": "1",
        "footer_img": "0",
        "footer_img_file": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_19_42_21.jpg",
        "footer_content": "1",
        "footer_content_text": "**This is a computer generated document and it does not require any signature**",
        "colspan_count": "3",
        "colspan_count1": "4",
        "font_color": "#1c0202",
        "border_color": "#5fd1eb",
        "border_font_color": "#b8483c",
        "doc_type": "quote",
        "hsn": "0",
        "hsn_name": "HSN"
        }

    invoice_pdf_settings={
        "name":"Invoice",
        "layout": "portrait",
        "logo": "1",
        "logoAlignment": "1",
        "logo_alignment": "left",
        "width": "150",
        "company_alignment": "1",
        "org_color_name": "#0c0c0d",
        "quotation_border": " ",
        "org_size": "20",
        "company_name": "Farazon",
        "org_alignment": "1",
        "org_address": "right",
        "org_address_alignment": "top",
        "bill_to_name": "Bill To",
        "account_name": "1",
        "ship_to": "1",
        "ship_to_name": "Ship To",
        "buyer_tin": "1",
        "buyer_cst": "1",
        "quote_ref_no": "1",
        "quote_ref_name": "Reference No",
        "ref_date": "0",
        "ref_date_name": "Ref Date",
        "due_date": "1",
        "due_date_name": "Due Date",
        "org_tin": "1",
        "org_cst": "1",
        "installed": "1",
        "committed": "1",
        "committed_date_name": "Commited Date",
        "delay": "1",
        "delay_name": "Delay",
        "installed_date_name": "installed_date_name ",
        "gstin": "1",
        "org_pan_no": "1",
        "ed_no": "0",
        "org_service_tax_no": "0",
        "org_cin_no": "0",
        "quote_subject": "1",
        "item_sno": "SNo",
        "item_description": "Description",
        "description_align": "center",
        "price1": "1",
        "part_no": "0",
        "part_no1": "1",
        "part_no_name": "Model",
        "packing": "0",
        "packing1": "0",
        "Taxable": "1",
        "taxable_name": "Taxable Value",
        "packing_name1": "Packing Amnt",
        "packing_name": "Packing %",
        "packing_align": "Left",
        "price": "Price",
        "qty": "1",
        "qty_name": "Qty",
        "discount": "0",
        "discount_name": "Disc %",
        "discount_price": "0",
        "discount_price_name": "Disc Price",
        "tax": "0",
        "item_tax_name": "Item Tax",
        "tax_amount": "0",
        "item_tax_amount_name": "Tax Amount",
        "item_custom": "1",
        "item_custom_field": "UOM",
        "item_amount_name": "Amount",
        "sub_total": "1",
        "sub_total_name": "Sub Total",
        "overall_discount": "1",
        "over_all_discount": "Total Discount",
        "overall_tax_amount": "1",
        "total_tax": "1",
        "total_tax_name": "",
        "tax2": "0",
        "item_tax2_name": "item_tax2_name",
        "tax2_amount": "0",
        "item_tax2_amount_name": "item_tax2_amount_name",
        "shipping": "1",
        "shipping_name": "Shipping",
        "installation": "1",
        "installation_name": "Installation",
        "amount": "Amount",
        "amount_in_words": "1",
        "proforma_recevied": "1",
        "total": "1",
        "total_round_off": "1",
        "total_name": "Total",
        "terms_conditions": "1",
        "terms_conditions_name": "Terms & Conditions",
        "bank_details": "1",
        "bank_details_name": "Bank Details",
        "remarks": "1",
        "remarks_name": "Remarks",
        "org_for": "1",
        "org_for_color": "#2a24bb",
        "prepared_by": "1",
        "prepared_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_40.jpeg",
        "checked_by": "1",
        "checked_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_43.jpeg",
        "authorized_signatory": "1",
        "authorized_signatory_path": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_16_39_41.png",
        "prepared_by_img": "602d1b9f2afaf81f68b91a96_10_2021_12_20_40.jpeg",
        "checked_by_img": "1",
        "authorized_signatory_img": "1",
        "footer_img": "0",
        "footer_img_file": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_19_42_21.jpg",
        "footer_content": "1",
        "footer_content_text": "**This is a computer generated document and it does not require any signature**",
        "colspan_count": "3",
        "colspan_count1": "4",
        "font_color": "#1c0202",
        "border_color": "#5fd1eb",
        "border_font_color": "#b8483c",
        "doc_type": "invoice",
        "hsn": "0",
        "hsn_name": "HSN"
        }

    proforma_pdf_settings={
        "name":"Proforma",
        "layout": "portrait",
        "logo": "1",
        "logoAlignment": "1",
        "logo_alignment": "left",
        "width": "150",
        "company_alignment": "1",
        "org_color_name": "#0c0c0d",
        "quotation_border": " ",
        "org_size": "20",
        "company_name": "Farazon",
        "org_alignment": "1",
        "org_address": "right",
        "org_address_alignment": "top",
        "bill_to_name": "Bill To",
        "account_name": "1",
        "ship_to": "1",
        "ship_to_name": "Ship To",
        "buyer_tin": "1",
        "buyer_cst": "1",
        "quote_ref_no": "1",
        "quote_ref_name": "Reference No",
        "ref_date": "0",
        "ref_date_name": "Ref Date",
        "due_date": "1",
        "due_date_name": "Due Date",
        "org_tin": "1",
        "org_cst": "1",
        "installed": "1",
        "committed": "1",
        "committed_date_name": "Commited Date",
        "delay": "1",
        "delay_name": "Delay",
        "installed_date_name": "installed_date_name ",
        "gstin": "1",
        "org_pan_no": "1",
        "ed_no": "0",
        "org_service_tax_no": "0",
        "org_cin_no": "0",
        "quote_subject": "1",
        "item_sno": "SNo",
        "item_description": "Description",
        "description_align": "center",
        "price1": "1",
        "part_no": "0",
        "part_no1": "1",
        "part_no_name": "Model",
        "packing": "0",
        "packing1": "0",
        "Taxable": "1",
        "taxable_name": "Taxable Value",
        "packing_name1": "Packing Amnt",
        "packing_name": "Packing %",
        "packing_align": "Left",
        "price": "Price",
        "qty": "1",
        "qty_name": "Qty",
        "discount": "0",
        "discount_name": "Disc %",
        "discount_price": "0",
        "discount_price_name": "Disc Price",
        "tax": "0",
        "item_tax_name": "Item Tax",
        "tax_amount": "0",
        "item_tax_amount_name": "Tax Amount",
        "item_custom": "1",
        "item_custom_field": "UOM",
        "item_amount_name": "Amount",
        "sub_total": "1",
        "sub_total_name": "Sub Total",
        "overall_discount": "1",
        "over_all_discount": "Total Discount",
        "overall_tax_amount": "1",
        "total_tax": "1",
        "total_tax_name": "",
        "tax2": "0",
        "item_tax2_name": "item_tax2_name",
        "tax2_amount": "0",
        "item_tax2_amount_name": "item_tax2_amount_name",
        "shipping": "1",
        "shipping_name": "Shipping",
        "installation": "1",
        "installation_name": "Installation",
        "amount": "Amount",
        "amount_in_words": "1",
        "proforma_recevied": "1",
        "total": "1",
        "total_round_off": "1",
        "total_name": "Total",
        "terms_conditions": "1",
        "terms_conditions_name": "Terms & Conditions",
        "bank_details": "1",
        "bank_details_name": "Bank Details",
        "remarks": "1",
        "remarks_name": "Remarks",
        "org_for": "1",
        "org_for_color": "#2a24bb",
        "prepared_by": "1",
        "prepared_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_40.jpeg",
        "checked_by": "1",
        "checked_by_path": "602d1b9f2afaf81f68b91a96_10_2021_14_19_43.jpeg",
        "authorized_signatory": "1",
        "authorized_signatory_path": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_16_39_41.png",
        "prepared_by_img": "602d1b9f2afaf81f68b91a96_10_2021_12_20_40.jpeg",
        "checked_by_img": "1",
        "authorized_signatory_img": "1",
        "footer_img": "0",
        "footer_img_file": "https://farazon.com:8444/uploads/pdf_image/100/60bdbf30eb493d8029f405b9_14_2021_19_42_21.jpg",
        "footer_content": "1",
        "footer_content_text": "**This is a computer generated document and it does not require any signature**",
        "colspan_count": "3",
        "colspan_count1": "4",
        "font_color": "#1c0202",
        "border_color": "#5fd1eb",
        "border_font_color": "#b8483c",
        "doc_type": "proforma",
        "hsn": "0",
        "hsn_name": "HSN"
        }

    insert_pdf_settings1 = MongoAPI.insertPDFSettings(org_id,user_id,quote_pdf_settings)
    insert_pdf_settings2 = MongoAPI.insertPDFSettings(org_id,user_id,proforma_pdf_settings)
    insert_pdf_settings3 = MongoAPI.insertPDFSettings(org_id,user_id,invoice_pdf_settings)
    MongoAPI.insertDemoData(org_id,user_id)

    # Field Settings

    field_settings={
    "name": "field_settings",
    "type": "field_settings",
    "sort_order": 1,
    "default": 1,
    }
    # field_settings = json.loads(json_util.dumps(field_settings))
    settings_data= {
        "bank": "true",
        "discount": "none",
        "discount_mode": "select",
        "installation": "true",
        "packing": "none",
        "packing_mode": "select",
        "round_off": "true",
        "shipping": "true",
        "tax": "overall",
        "total_in_pdf": "true",
        "remarks": "true"
    }
    insert_field_settings = MongoAPI.insertFieldSettings(org_id,user_id,field_settings)
    update_field_settings = MongoAPI.updateFieldSettings(org_id,user_id,settings_data)
    return jsonify({"msg": "Yes"})



@app.route('/demodata',methods=['GET'])
@jwt_required
def insertDemoData():
    org_id = 135
    user_id = '60e6961ae27b241571a04992'
    MongoAPI.insertDemoData(org_id,user_id)
    


@app.route('/testpdf/<id>')
# @jwt_required
def hello_pdf(id):
    # html = render_template('hello.html', name=name)
    # pdf = HTML(string=html).write_pdf()
    # f = open(os.path.join(app.config['TEMPLATE_FOLDER'], 'mypdf.pdf'), 'wb')
    # f.write(pdf)
    # return render_pdf(HTML(string=html))
    # current_user = get_jwt_identity()
    # user = MongoAPI.authorizationCheck(current_user)
    # print(user)
    # ordId = 25
    # for user2 in user:
    #     if user2=='org_id':
    #         ordId = user[user2]
    ordId = 25
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        doc_type = 'quote'
        pdfData = check = MongoAPI.checkPDFSettings(doc_type,ordId)
        pdfData = Uid.fix_array3(pdfData)
        quote = MongoAPI.getQuoteDetails(ordId,id)
        quote = Uid.fix_array3(quote)
        # print (quote)
        quote['quote_no']

        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])

        # print(pdfData)
        settings_calculate = MongoAPI.pdfCalculate(pdfData,quote)
        settings = settings_calculate['pageSettings']
        quote['sub_total']=settings_calculate['sub_total']
        quote['amt_in_words']=settings_calculate['amt_in_words']

        # print(settings)
        html = render_template('QuotePDFView.html',settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        # print(pdfData)
        # return html
        # filename = str('quote_')+id+str('.pdf')
        # response = HTML(string=html).write_pdf()
        # f = open(os.path.join(app.config['TEMPLATE_FOLDER'], filename), 'wb')
        # f.write(response)
        return render_pdf(HTML(string=html))
        # return 1
        # return html


# @jwt_required 
# def account_mapping():
#     current_user = get_jwt_identity()
#     user = MongoAPI.authorizationCheck(current_user)

@app.route('/previewQuote',methods=['POST'])
@jwt_required
def previewQuote():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        html_string =  request.json.get('html_string', None)
        date=datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
        filename=str(date)+'.pdf'
        template = 'itemtable.html'
        item_table = render_template(template)
        quote = MongoAPI.getQuoteDetails(ordId,'610e5a058a4bd6e260183f16')
        quote = Uid.fix_array3(quote)
        quote=json.loads(json_util.dumps(quote))
        print(quote)
        # html = render_template(template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        # html_string=html_string+item_table
        # result=item_table+html_string
        html_string = html_string.replace("{{company_name}}",quote['company_name'])
        html_string = html_string.replace("{{subject}}",quote['subject'])
        html_string = html_string.replace("{{date}}",quote['create_date'])
        result = html_string
        response = HTML(string=result).write_pdf()

        # response = HTML(string=html_string).write_pdf()
        f = open(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename), 'wb')
        f.write(response)
        saved_path=os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename)
        saved_file=str(app_config.BASE_URL)+saved_path
        return {"code":200,"data" : {'filename':filename,'path':saved_file}}


@app.route('/generateQuotePDF/<org_id>/<doc_type>/<id>')
# @jwt_required
def quote_pdf(org_id,doc_type,id):
    # current_user = get_jwt_identity()
    # user = MongoAPI.authorizationCheck(current_user)
    ordId = int(org_id)
    # for user2 in user:
    #     if user2=='org_id':
    #         ordId = user[user2]
    # print(user)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])

        doc_type = doc_type
        pdfData = check = MongoAPI.checkPDFSettings(doc_type,ordId)
        pdfData = Uid.fix_array3(pdfData)
        # print(pdfData)
   
        if doc_type=='quote':
            quote = MongoAPI.getQuoteDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            template = 'QuotePDFView.html'
        elif doc_type=='proforma':
            quote = MongoAPI.getProformaDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            template = 'ProformaPDFView.html'
        elif doc_type=='invoice':
            quote = MongoAPI.getInvoiceDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            template = 'InvoicePDFView.html'
        settings_calculate = MongoAPI.pdfCalculate(pdfData,quote)
        settings = settings_calculate['pageSettings']
        quote['sub_total']=settings_calculate['sub_total']
        quote['amt_in_words']=settings_calculate['amt_in_words']
        quote['total_roundoff']=settings_calculate['total_roundoff']
        html = render_template(template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        # return html
        return render_pdf(HTML(string=html))

@app.route('/generateCustomQuotePDF/<org_id>/<doc_type>/<id>')
# @jwt_required
def custom_quote_pdf(org_id,doc_type,id):
    ordId = int(org_id)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])

        doc_type = doc_type
        pdfData = MongoAPI.checkCustomPDFSettings(ordId)
        pdfData = Uid.fix_array3(pdfData)
        # return pdfData
        if doc_type=='quote':
            quote = MongoAPI.getQuoteDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            item_table_template = 'itemtable.html'
        settings_calculate = MongoAPI.pdfCalculate(pdfData,quote)
        settings = settings_calculate['pageSettings']
        quote['sub_total']=settings_calculate['sub_total']
        quote['amt_in_words']=settings_calculate['amt_in_words']
        quote['total_roundoff']=settings_calculate['total_roundoff']
        footer_template='footer.html'
        style_template='style.html'
        # html = render_template(template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        style_html = render_template(style_template,settings=settings,pdfData=pdfData)
        item_table = render_template(item_table_template,quote=quote,settings=settings,pdfData=pdfData)
        html_end='</body></html>'
        footer = render_template(footer_template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        # return html
        html=style_html+pdfData['header']+pdfData['body']+footer+html_end
        html = html.replace("{{company_name}}",quote['company_name'])
        html = html.replace("{{subject}}",quote['subject'])
        html = html.replace("{{date}}",quote['create_date'])
        html = html.replace("{{address_line1}}",quote['address1'])
        html = html.replace("{{address_line2}}",quote['address2'])
        html = html.replace("{{city}}",quote['city'])
        html = html.replace("{{state}}",quote['state'])
        html = html.replace("{{country}}",quote['country'])
        html = html.replace("{{pincode}}",quote['pincode'])
        html = html.replace("{{item_table}}",item_table)
        terms = quote['terms_condition']
        html = html.replace("{{terms_conditions}}",terms)
        # return html
        return render_pdf(HTML(string=html))

@app.route('/sendQuotePDF/<org_id>/<doc_type>/<id>')
def send_quote_pdf(org_id,doc_type,id):
    ordId = int(org_id)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])

        doc_type = doc_type
        pdfData = check = MongoAPI.checkPDFSettings(doc_type,ordId)
        pdfData = Uid.fix_array3(pdfData)
        
        if doc_type=='quote':
            quote = MongoAPI.getQuoteDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            quote = json.loads(json_util.dumps(quote))
            template = 'QuotePDFView.html'
            company_name=quote['company_name'].replace(" ","_")
            filename = str(company_name)+str('.pdf')
        elif doc_type=='proforma':
            quote = MongoAPI.getProformaDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            template = 'ProformaPDFView.html'
            company_name=quote['company_name'].replace(" ","_")
            filename = str(company_name)+str('.pdf')
        elif doc_type=='invoice':
            quote = MongoAPI.getInvoiceDetails(ordId,id)
            quote = Uid.fix_array3(quote)
            template = 'InvoicePDFView.html'
            company_name=quote['company_name'].replace(" ","_")
            filename = str(company_name)+str('.pdf')
        

        settings_calculate = MongoAPI.pdfCalculate(pdfData,quote)
        settings = settings_calculate['pageSettings']
        quote['sub_total']=settings_calculate['sub_total']
        html = render_template(template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        response = HTML(string=html).write_pdf()
        f = open(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename), 'wb')
        f.write(response)
        saved_path=os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename)
        saved_file=str(app_config.BASE_URL)+saved_path
        return {"code":200,"data" : {'filename':filename,'path':saved_file}}


@app.route('/sendCustomQuotePDF/<org_id>/<doc_type>/<id>')
def send_custom_quote_pdf(org_id,doc_type,id):
    ordId = int(org_id)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        organization = MongoAPI.organizationInfo(ordId)
        organization = Uid.fix_array1(organization)
        if organization['logo']:
            organization['logo'] = str(app_config.BASE_URL)+str(app.config['UPLOAD_OPEN_COMPANY_FOLDER'])+str(organization['logo'])

        doc_type = doc_type
        pdfData = check = MongoAPI.checkPDFSettings(doc_type,ordId)
        pdfData = Uid.fix_array3(pdfData)
        
        doc_type = doc_type
        pdfData = MongoAPI.checkCustomPDFSettings(ordId)
        pdfData = Uid.fix_array3(pdfData)
        quote = MongoAPI.getQuoteDetails(ordId,id)
        quote = Uid.fix_array3(quote)
        item_table_template = 'itemtable.html'
        settings_calculate = MongoAPI.pdfCalculate(pdfData,quote)
        settings = settings_calculate['pageSettings']
        quote['sub_total']=settings_calculate['sub_total']
        quote['amt_in_words']=settings_calculate['amt_in_words']
        quote['total_roundoff']=settings_calculate['total_roundoff']
        footer_template='footer.html'
        style_template='style.html'
        # html = render_template(template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        style_html = render_template(style_template,settings=settings,pdfData=pdfData)
        item_table = render_template(item_table_template,quote=quote,settings=settings,pdfData=pdfData)
        html_end='</body></html>'
        footer = render_template(footer_template,settings=settings,pdfData=pdfData,quote=quote,organization=organization)
        # return html
        html=style_html+pdfData['header']+pdfData['body']+footer+html_end
        html = html.replace("{{company_name}}",quote['company_name'])
        html = html.replace("{{subject}}",quote['subject'])
        html = html.replace("{{date}}",quote['create_date'])
        html = html.replace("{{address_line1}}",quote['address1'])
        html = html.replace("{{address_line2}}",quote['address2'])
        html = html.replace("{{city}}",quote['city'])
        html = html.replace("{{state}}",quote['state'])
        html = html.replace("{{country}}",quote['country'])
        html = html.replace("{{pincode}}",quote['pincode'])
        html = html.replace("{{item_table}}",item_table)
        terms = quote['terms_condition']
        html = html.replace("{{terms_conditions}}",terms)
        response = HTML(string=html).write_pdf()
        # return response
        company_name=quote['company_name'].replace(" ","_")
        filename = str(company_name)+time.strftime('_%d_%Y_%H_%M_%S')+str('.pdf')
        f = open(os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename), 'wb')
        f.write(response)
        saved_path=os.path.join(app.config['UPLOAD_EMAIL_FOLDER'], filename)
        saved_file=str(app_config.BASE_URL)+saved_path
        return {"code":200,"data" : {'filename':filename,'path':saved_file}}

@app.route('/addPricelist', methods=['POST'])
@jwt_required
def addPricelist():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    # ordId = '1'
    # user = '5ff5619fbf6a031d2c13cfa2'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    title =  request.json.get('title', None)
    
    if not title:
        return jsonify({"msg": "Missing title parameter","code": 400})
    user1 = MongoAPI.PricelistCheck(title,ordId)
    if user1=='Yes':
        return jsonify({"msg": "Title is already registered","code": 400})
    else:
        org_id = ordId
        # password = generate_password_hash(password)
        #print (current_user)
        response = MongoAPI.createPricelist(title,org_id,current_user)
        #response = 1
        
        order=1
        sort='product_name'

        Products = MongoAPI.getProductsListNew(ordId,sort,order)
        Products1 = Uid.fix_array(Products)
        response1 = json.loads(json_util.dumps(response))

        response = MongoAPI.createPricelistProducts(Products1,response1)


    if response:
        return jsonify({"msg": "Pricelist registered successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/pricelistDetail/<id>', methods=['GET'])
@jwt_required
def pricelistDetail(id):
    current_user = get_jwt_identity()
    user1 = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user1:
        if user2=='org_id':
            ordId = user1[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        order = 1
        sort = 'product.product_name'

        pricelist = MongoAPI.getPricelistDetail(ordId,id)
        print (pricelist)
        Products = MongoAPI.getPricelistProductsList(ordId,sort,order,id)
        pricelist = Uid.fix_array(pricelist)
        # Products = Uid.fix_array3(Products)
        if pricelist:
            return jsonify({"msg": "Pricelist details","code": 200,"Products": Products,"pricelist": pricelist})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/addPricelistProduct/<id>', methods=['GET'])
@jwt_required
def addPricelistProduct(id):
    current_user = get_jwt_identity()
    user1 = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user1:
        if user2=='org_id':
            ordId = user1[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        order = 1
        sort = 'product.product_name'

        PricelistProducts = MongoAPI.getPricelistProductsList(ordId,sort,order,id)

        order=1
        sort='product_name'
        products = MongoAPI.getPricelistProductsDataList(ordId,sort,order)
        
        pricelist = Uid.fix_array2(PricelistProducts)
        #print (pricelist)

        

        if pricelist:
            return jsonify({"msg": "Pricelist details","code": 200,"products": pricelist,"PricelistProducts": PricelistProducts})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/pricelistProductUpdate', methods=['POST'])
@jwt_required
def pricelistProductUpdate():

    current_user = get_jwt_identity()
    user1 = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user1:
        if user2=='org_id':
            ordId = user1[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        data = request.json
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        # data = request.form
        
        id =  request.json.get('id', None)
        column =  request.json.get('column', None)
        value =  request.json.get('value', None)

        if not id:
            return jsonify({"msg": "Id parameter missing","code": 400})
        if not column:
            return jsonify({"msg": "Column parameter missing","code": 400})
        if not value:
            return jsonify({"msg": "Value parameter missing","code": 400})

        response = MongoAPI.pricelistProductUpdate(id,column,value)
        if response:
            return jsonify({"msg": "Value updated successfully !","code": 200})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/pricelist_list', methods=['GET'])
@jwt_required
def pricelist_list():
    current_user = get_jwt_identity()
    # print(current_user)
   
    user = MongoAPI.authorizationCheck(current_user)
    
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        tag =  request.args.get('tag', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)

        if not page:
            page=1
        if not length:
            length=100
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='title':
            sort='pricelist.title'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not tag:
            tag=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = MongoAPI.getPricelistList(ordId,length,page,search,user,source,tag,date_from,date_to,sort,order,current_user)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)

        contact_total = len(response1)


        data = {'total_count':contact_total,'items':response1}
        return data

@app.route('/pricelistCompaniesList', methods=['GET'])
@jwt_required
def pricelistCompaniesList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:



        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        pricelist_id =  request.args.get('id', None)
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        length = int(length)
        page = int(page)
        response1 = MongoAPI.pricelistCompaniesList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        accounts_total = MongoAPI.getPriceListAccountsCountData(ordId,pricelist_id)
        data = {'total_count':accounts_total,'items':response1}
        return data

@app.route('/priceListNewProduct', methods=['POST'])
@jwt_required
def priceListNewProduct():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    else:
        price_id =  request.json.get('price_id', None)
        product_id =  request.json.get('product_id', None)

    response = MongoAPI.priceListNewProduct(price_id,product_id)
    response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "Product added successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/updateCompanyPricelist', methods=['POST'])
@jwt_required
def updateCompanyPricelist():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    else:
        pricelist_id =  request.json.get('pricelist_id', None)
        company_list =  request.json.get('company_list', None)

    response = MongoAPI.updateCompanyPricelist(pricelist_id,company_list)
    response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "Company Updated successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/productsNew', methods=['GET'])
@jwt_required
def productsNew():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if ordId:
            id =  request.args.get('id', None)
            response1 = MongoAPI.getProductDetails(ordId,id)
            response = Uid.fix_array3(response1)

            company_id =  request.args.get('company_id', None)
            response1 = MongoAPI.getCompanyDetails(ordId,company_id)
            response = Uid.fix_array3(response1)
            pricelist = response['pricelist']
            currency = response['currency']
            if not currency:
                currency='INR'
            if not pricelist:
                pricelist=''
            # print(pricelist)
            response1 = MongoAPI.getProductNewDetails(ordId,id,pricelist,currency)
            response = Uid.fix_array3(response1)

            

            data = response

            if data:
                return jsonify({"msg": "Contact successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Settings is already add","code": 400})

@app.route('/receiveEnquiry', methods=['POST'])
def receiveEnquiry():
    if not request.form:
        return json.dumps({"msg": "Missing Data"})
    else:
        data = json.loads(json_util.dumps(request.form))
        gcaptcha=data['gcaptcha'].replace(" ","")
        captcha=data['captcha']
        if captcha != gcaptcha:
            return json.dumps({"code":600,"msg":"Wrong Captcha"})
        else:
            
            Template=MongoAPI.getAdminEmailTemplate('sales_enquiry')
            resetMail=Template[0]['template_content']
            if data['enquiry_type']=='job_enquiry':
                data['company_name']='-'
                file = request.files['upload_file']

                if file.filename == '':
                    return json.dumps({'msg' : 'No file selected for uploading','code':400})
             
                if file and allowed_file(file.filename,app_config.RESUME_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    # print(file_format)
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format      
                    file.save(os.path.join(app.config['UPLOAD_RESUME_FOLDER'], filename1))
                    saved_path=os.path.join(app.config['UPLOAD_RESUME_FOLDER'], filename1)
                    saved_file=str(app_config.BASE_URL)+saved_path
                    data['file_name']=saved_file
                else:
                    return json.dumps({'msg' : 'File Format Not Supported','code':400})
                    

            # resetMailContent = resetMail.replace("{{enquiry_type}}",'Sales Enquiry')
            resetMailContent1 = resetMail.replace("{{name}}",data['name'])
            resetMailContent2 = resetMailContent1.replace("{{company_name}}",data['company_name'])
            resetMailContent3 = resetMailContent2.replace("{{mobile}}",data['mobile'])
            resetMailContent4 = resetMailContent3.replace("{{email}}",data['email'])
            resetMailContent5 = resetMailContent4.replace("{{message}}",data['message'])
            if data['enquiry_type']=='sales_enquiry':
                resetMailContent6 = resetMailContent5.replace("{{city}}",data['city'])
                resetMailContent7 = resetMailContent6.replace("{{state}}",data['state'])
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Sales Enquiry')
                mailSubject = 'Sales Enquiry'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='technical_support':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Technical Support Enquiry')
                mailSubject = 'Technical Support'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='customer_service':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Customer Service Enquiry')
                mailSubject = 'Customer Service'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='feedback':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Feedback')
                mailSubject = 'Feedback'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='book_a_call':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Book a Call')
                mailSubject = 'Book a Call'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='demo':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Demo Request')
                mailSubject = 'Demo Request'
                mailcontent=resetMailContent8
            elif data['enquiry_type']=='job_enquiry':
                resetMailContent6 = resetMailContent5.replace("{{city}}",'-')
                resetMailContent7 = resetMailContent6.replace("{{state}}",'-')
                resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Job Enquiry')
                mailSubject = 'Job Enquiry'
                mailcontent=resetMailContent8
            enquiry_id = MongoAPI.receiveEnquiry(data)
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - '+mailSubject
            data1['content']=mailcontent
            data1['to']='swami@skyzon.com'
            data1['cc']='support@farazon.com'
            data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
            if data['enquiry_type']=='job_enquiry':
                attachment=saved_file
            else:
                attachment=''
            emailId='support@farazon.com'
            data1['associate_id']=enquiry_id
            data1['associate_to']=data['enquiry_type']
            sendMail=MongoAPI.emailSubmit(0,enquiry_id,data1,emailId,attachment)
            return json.dumps({"code":200,"message" : "Thanks for submitting the form. Our team will get back to you shortly."}) 

@app.route('/partnerEnquiry', methods=['POST'])
def partnerEnquiry():
    if not request.form:
        return json.dumps({"msg": "Missing Data"})
    else:
        data = json.loads(json_util.dumps(request.form))
        gcaptcha=data['gcaptcha'].replace(" ","")
        captcha=data['captcha']
        if captcha != gcaptcha:
            return json.dumps({"code":600,"msg":"Wrong Captcha"})
        else:
            enquiry_id = MongoAPI.partnerEnquiry(data)
            Template=MongoAPI.getAdminEmailTemplate('sales_enquiry')
            resetMail=Template[0]['template_content']
            resetMailContent1 = resetMail.replace("{{name}}",data['contact_name'])
            resetMailContent2 = resetMailContent1.replace("{{company_name}}",data['company_name'])
            resetMailContent3 = resetMailContent2.replace("{{mobile}}",data['mobile'])
            resetMailContent4 = resetMailContent3.replace("{{email}}",data['email'])
            resetMailContent5 = resetMailContent4.replace("{{message}}",data['message'])
            resetMailContent6 = resetMailContent5.replace("{{city}}",data['city'])
            resetMailContent7 = resetMailContent6.replace("{{state}}",data['state'])
            resetMailContent8 = resetMailContent7.replace("{{enquiry_type}}",'Partner Request')
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - Partner Request'
            data1['content']=resetMailContent8
            data1['to']='swami@skyzon.com'
            data1['cc']='support@farazon.com'
            data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
            emailId='support@farazon.com'
            data1['associate_id']=enquiry_id
            data1['associate_to']='partner_enquiry'
            attachment=''
            sendMail=MongoAPI.emailSubmit(0,enquiry_id,data1,emailId,attachment)
            return json.dumps({"code":200,"message" : "Thanks for submitting the form. Our team will get back to you shortly."})

@app.route('/account_mapping', methods=['POST'])
@jwt_required 
def account_mapping():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    

    column =  request.json.get('column', None)
    customer_type =  request.json.get('customer_type', None)
    source =  request.json.get('source', None)
    assigned_to =  request.json.get('users', None)

    if not column:
        return jsonify({"msg": "Missing column parameter","code": 400})



    rowData  = defaultdict(list)

    for item in column:
        rowData[item] = column[item]


    account = MongoAPI.getAccountTmpList(ordId,rowData,current_user,customer_type,assigned_to,source)



    if rowData:
        return jsonify({"msg": "File(s) successfully uploaded","column":rowData,"count":account,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})


@app.route('/account_upload_view', methods=['GET'])
@jwt_required 
def account_upload_view():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    account = MongoAPI.getAccountTmp(ordId)

    users = MongoAPI.usersList(ordId)
    source = MongoAPI.fieldsSettingsData('source',ordId)
    customer_type = MongoAPI.fieldsSettingsData('customer_type',ordId)
        

    users = Uid.fix_array2(users)
    source = Uid.fix_array2(source)
    customer_type = Uid.fix_array2(customer_type)
        

    settings = {'users':users,'source':source,'customer_type':customer_type}

    if user:
        return jsonify({"msg": "File(s) successfully uploaded","account":account,"settings":settings,"fields":json.loads(json_util.dumps(company_field)),"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})




@app.route('/account_upload', methods=['POST'])
@jwt_required 
def account_upload():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'file' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            company = MongoAPI.getCompanyProfile(ordId)

            if not company['upload_count']:
                upload_count = 100
            else:
                upload_count = int(company['upload_count'])


            filenameData = []
            files = request.files.getlist('file')
            for file in files:
                if file and allowed_file(file.filename,app_config.CSV_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    file.save(os.path.join(app.config['UPLOAD_CSV_FOLDER'], filename1))
                    filenameData.append(filename1)
            
            #obj = Accounttmp(org_id=ordId)
            Accounttmp.objects(org_id=ordId).delete()
            #obj.delete()


            with open(app.config['UPLOAD_CSV_FOLDER']+filename1, 'r') as read_obj:
                # pass the file object to reader() to get the reader object
                csv_reader = reader(read_obj)
                # Iterate over each row in the csv using reader object
                company_models = app_config.company_models
                rowData  = defaultdict(list)
                r = 0
                for row in csv_reader:
                    if(upload_count>r):
                        i = 0
                        col = defaultdict(list)
                        col['org_id'] = ordId
                        col['create_by'] = current_user
                        col['sort_order'] = r
                        
                        for row1 in row:
                            #print (company_models[i])
                            col[company_models[i]] = row1
                            i = i+1
                        # row col variable is a list that represents a row in csv
                        col1 = json.loads(json_util.dumps(col))
                        account1 = Accounttmp(**col)
                        response = account1.save()

                        rowData[r] = col
                        r = r+1



            return jsonify({"msg": "File(s) successfully uploaded","count":r,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})

@app.route('/product_upload', methods=['POST'])
@jwt_required 
def product_upload():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    #product_field=''
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'file' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            company = MongoAPI.getCompanyProfile(ordId)

            if not company['upload_count']:
                upload_count = 100
            else:
                upload_count = int(company['upload_count'])


            filenameData = []
            files = request.files.getlist('file')
            for file in files:
                if file and allowed_file(file.filename,app_config.CSV_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    file.save(os.path.join(app.config['UPLOAD_CSV_FOLDER'], filename1))
                    filenameData.append(filename1)
            
            #obj = Accounttmp(org_id=ordId)
            Producttemp.objects(org_id=ordId).delete()
            #obj.delete()product_field


            with open(app.config['UPLOAD_CSV_FOLDER']+filename1, 'r') as read_obj:
                # pass the file object to reader() to get the reader object
                csv_reader = reader(read_obj)
                # Iterate over each row in the csv using reader object
                product_models = app_config.product_models
                rowData  = defaultdict(list)
                r = 0
                for row in csv_reader:
                    if(upload_count>r):
                        i = 0
                        col = defaultdict(list)
                        col['org_id'] = ordId
                        col['sort_order'] = r
                        
                        for row1 in row:
                            #print (company_models[i])
                            col[product_models[i]] = row1
                            i = i+1
                        # row col variable is a list that represents a row in csv
                        col1 = json.loads(json_util.dumps(col))
                        account1 = Producttemp(**col)
                        response = account1.save()

                        rowData[r] = col
                        r = r+1



            return jsonify({"msg": "File(s) successfully uploaded","count":r,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})



@app.route('/product_upload_view', methods=['GET'])
@jwt_required 
def product_upload_view():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    product = MongoAPI.getProductTmp(ordId)

    product_category = MongoAPI.fieldsSettingsData('product_category',ordId)
    product_category = Uid.fix_array2(product_category)
   

    settings = {'product_category':product_category}

    if user:
        return jsonify({"msg": "File(s) successfully uploaded","product":product,"settings":settings,"fields":json.loads(json_util.dumps(product_field)),"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})
        

@app.route('/product_mapping', methods=['POST'])
@jwt_required 
def product_mapping():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    column =  request.json.get('column', None)
    category =  request.json.get('product_category', '')
    if not column:
        return jsonify({"msg": "Missing column parameter","code": 400})
    




    rowData  = defaultdict(list)

    for item in column:
        rowData[item] = column[item]


    product = MongoAPI.getProductTmpList(ordId,rowData,current_user,category)

    if rowData:
        return jsonify({"msg": "File(s) successfully uploaded","column":rowData,"count":product,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})





@app.route('/contact_upload', methods=['POST'])
@jwt_required 
def contact_upload():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    #product_field=''
    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    if request.method == 'POST':

        if 'file' not in request.files:
            resp = jsonify({'msg' : 'No file part in the request','code':'400'})
            return resp
        else:
            company = MongoAPI.getCompanyProfile(ordId)

            if not company['upload_count']:
                upload_count = 100
            else:
                upload_count = int(company['upload_count'])


            filenameData = []
            files = request.files.getlist('file')
            for file in files:
                if file and allowed_file(file.filename,app_config.CSV_ALLOWED_EXTENSIONS):
                    filename = secure_filename(file.filename)
                    file_type = file.filename
                    file_format = splitpart(file_type,-1,'.')
                    file_name = splitpart(file_type,0,'.')
                    filename1 = file_name + time.strftime('_%d_%Y_%H_%M_%S') +'.'+file_format
                    file.save(os.path.join(app.config['UPLOAD_CSV_FOLDER'], filename1))
                    filenameData.append(filename1)
            
            #obj = Accounttmp(org_id=ordId)
            Contacttmp.objects(org_id=ordId).delete()
            #obj.delete()product_field


            with open(app.config['UPLOAD_CSV_FOLDER']+filename1, 'r') as read_obj:
                # pass the file object to reader() to get the reader object
                csv_reader = reader(read_obj)
                # Iterate over each row in the csv using reader object
                contact_models = app_config.contact_models
                rowData  = defaultdict(list)
                r = 0
                for row in csv_reader:
                    if(upload_count>r):
                        i = 0
                        col = defaultdict(list)
                        col['org_id'] = ordId
                        col['sort_order'] = r
                        
                        for row1 in row:
                            #print (company_models[i])
                            col[contact_models[i]] = row1
                            i = i+1
                        # row col variable is a list that represents a row in csv
                        col1 = json.loads(json_util.dumps(col))
                        contact1 = Contacttmp(**col)
                        response = contact1.save()

                        rowData[r] = col
                        r = r+1



            return jsonify({"msg": "File(s) successfully uploaded","count":r,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})



@app.route('/contact_upload_view', methods=['GET'])
@jwt_required 
def contact_upload_view():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    
    contact = MongoAPI.getContactTmp(ordId)

    users = MongoAPI.usersList(ordId)
    source = MongoAPI.fieldsSettingsData('source',ordId)
    users = Uid.fix_array2(users)
    source = Uid.fix_array2(source)

    settings = {'users':users,'source':source}

    if user:
        return jsonify({"msg": "File(s) successfully uploaded","contact":contact,"settings":settings,"fields":json.loads(json_util.dumps(contact_field)),"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})
        

@app.route('/contact_mapping', methods=['POST'])
@jwt_required 
def contact_mapping():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    

    assigned_to =  request.json.get('users', None)
    source =  request.json.get('source', None)
    column =  request.json.get('column', None)
    # source = MongoAPI.fieldsSettingsData('source',ordId)
    if not column:
        return jsonify({"msg": "Missing column parameter","code": 400})



    rowData  = defaultdict(list)

    for item in column:
        rowData[item] = column[item]


    contact = MongoAPI.getContactTmpList(ordId,rowData,current_user,assigned_to,source)

    if rowData:
        return jsonify({"msg": "File(s) successfully uploaded","column":rowData,"count":contact,"code": 200})

    else:
        return jsonify({"msg": "Invalid method for input","code": 400})


# **************************************** Admin Portal APIS ***************************************************

@app.route('/adminLogin', methods=['GET','POST'])
# @cross_origin()
def adminLogin():
    data = request.json
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    device_id =  request.json.get('device_id', None)
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    if not device_id:
        device_id=''

    # test=AdminAPI.createUser(email,password,'Administrator')
    response = AdminAPI.selectLogin(email,password)
    
    if response=='User Does Not Exists':
        response1 = AdminAPI.emailCheckNew(email)
        if response1=='Yes':
            return jsonify({"msg": "Invalid Password.","code": 400})
        else:
             return jsonify({"msg": "Invalid Email","code": 400})
    else:
        response1 = AdminAPI.emailCheckNew(email)
        if response1=='Yes':
            if response['status']=='active':
                response4 = Uid.fix_array3(response)
                expires = datetime.timedelta(days=1)
                access_token = create_access_token(identity=str(response['id']), expires_delta=expires)
                refresh_token = create_refresh_token(identity=str(response['id']), expires_delta=expires)
                data = {'result': response4,'access_token': access_token,'refresh_token': refresh_token}
                return jsonify({"msg": "You have been successfully Logged in.","code": 200,"data": data})
            else:
                return jsonify({"msg": "You account is inactive pls contact admin","code": 400,"data": data})
        else:
            return jsonify({"msg": "Invalid User Name or Password.","code": 400})


@app.route('/organizationList', methods=['GET'])
@jwt_required
def getOrganizationList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getOrganizationList(ordId,length,page,search,date_from,date_to,order,sort)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        org_total = AdminAPI.getOrgCountData()
        data = {'total_count':org_total,'items':response1}
        return data

@app.route('/organizationListMobile', methods=['GET'])
@jwt_required
def organizationListMobile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sort =  request.args.get('sort', None)
        orderdata = 'desc'
        order =  request.args.get('order', orderdata)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        if not order:
            order=orderdata
        
        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getOrganizationList(ordId,length,page,search,date_from,date_to,order,sort)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        active_companies = AdminAPI.getOrgCount('active')
        active_users = AdminAPI.getUserCount('active')
        data = {'organization':response1,'active_users':active_users,'active_companies':active_companies}

        if data:
            return jsonify({"msg": "Organization List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminfilters/<name>', methods=['GET'])
@jwt_required
def adminfilters(name):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    if name=='organization':
        data = {}
    return jsonify({"msg": "Filters List",'data': data,"code": 200})

@app.route('/organizationData/<id>', methods=['GET'])
@jwt_required
def getOrganization(id):
    current_user = get_jwt_identity()
    response1 = AdminAPI.getOrgDetails(id)
    response = Uid.fix_array3(response1)
    admin_user=AdminAPI.getOrgUser(response['org_id'],response['email'])
    data = response
    if data:
        return jsonify({"msg": "Organization details !","code": 200,"data": data})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/getAdminUser/<org_id>/<email>', methods=['GET'])
@jwt_required
def getAdminUser(org_id,email):
    current_user = get_jwt_identity()
    response1 = AdminAPI.getOrgUser(org_id,email)
    response = Uid.fix_array3(response1)
    data = response
    if data:
        return jsonify({"msg": "Admin User Details","code": 200,"data": data})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminNotes', methods=['POST'])
@jwt_required
def adminNotes():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId='0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"})

        note =  request.json.get('note', None)
        associate_id =  request.json.get('associate_id', None)
        associate_to =  request.json.get('associate_to', None)
      
        if not note:
            return jsonify({"msg": "Missing note parameter","code": 400})
        if not associate_id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})

        data1 = json.loads(json_util.dumps(request.json))
        data = data1
        org_id = '0'
        if org_id:
            response1 = MongoAPI.noteSubmit(0,current_user,data)
            response = json.loads(json_util.dumps(response1))
            data = {'id':response}
            if data:
                action = 'ADD_NOTE'
                associate_to = associate_to
                associate_id = associate_id
                via = 0
                extra_info = json.loads(json_util.dumps(data1))
                text_info = app_config.CREATE_INFO
                title = app_config.CREATE_TITLE
                MongoAPI.user_activity(ordId,current_user,action,associate_to,associate_id,via,extra_info,title,text_info)
                return jsonify({"msg": "Notes successfully created !","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminNotes', methods=['GET'])
@jwt_required
def getadminNotes():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = ''
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId='0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        if not request.args.get:
            return jsonify({"msg": "Missing JSON in request"})

        id =  request.args.get('associate_id', None)
        associate_to =  request.args.get('associate_to', None)
               
        if not id:
            return jsonify({"msg": "Missing associate id parameter","code": 400})
        if not associate_to:
            return jsonify({"msg": "Missing associate to parameter","code": 400})
        
               
        
        data1 = json.loads(json_util.dumps(request.json))

        data = data1
        
    
        org_id=ordId
        if org_id:
            response1 = MongoAPI.getNotes(0,current_user,ObjectId(id),associate_to)
            response = json.loads(json_util.dumps(response1))
            # response = response1.to_json()
            # print (response)
            data = {'notes':response}

            if data:
                # print (response)              
                return jsonify({"msg": "Notes list","code": 200,"data": data})
            else:
                return jsonify({"msg": "Oops,Something went wrong !","code": 400})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/orgUserList/<org_id>', methods=['GET'])
@jwt_required
def orgUserList(org_id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = org_id
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:



        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getOrganizationUserList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        org_total = AdminAPI.getOrgUserCountData(org_id)
        data = {'total_count':org_total,'items':response1}
        return data

@app.route('/orgUserListMobile/<org_id>', methods=['GET'])
@jwt_required
def orgUserListMobile(org_id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = org_id
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:



        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getOrganizationUserList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'org_users':response1}
        if data:
            return jsonify({"msg": "Org Users List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/orgUserCount/<org_id>', methods=['GET'])
@jwt_required
def orgUserCount(org_id):
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = org_id
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:

        org_total = AdminAPI.getOrgUserCountData(org_id)
        data = {'total_count':org_total}
        return data

@app.route('/changeCrmUserStatus/<id>', methods=['PUT'])
@jwt_required
def changeCrmUserStatus(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    status =  request.json.get('status', None)
    if not status:
        return jsonify({"msg": "Missing status parameter","code": 400})
    
    response = AdminAPI.changeCrmUserStatus(id,status)
    if response:
        return jsonify({"msg": "User Status changed successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeCrmUserPassword/<id>', methods=['PUT'])
@jwt_required
def changeCrmUserPassword(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    password =  request.json.get('status', None)
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    
    response = AdminAPI.changeCrmUserPassword(id,password)
    if response:
        return jsonify({"msg": "User Password changed successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeOrgStatus/<id>', methods=['PUT'])
@jwt_required
def changeOrgStatus(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    status =  request.json.get('status', None)
    if not status:
        return jsonify({"msg": "Missing status parameter","code": 400})
    
    response = AdminAPI.changeOrgStatus(id,status)
    if response:
        return jsonify({"msg": "Organization Status changed successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminUserDetails/<id>', methods=['GET'])
@jwt_required
def adminUserDetails(id):
    current_user = get_jwt_identity()
    user1 = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user1:
        if user2=='org_id':
            ordId = user1[user2]
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        user = AdminAPI.getAdminUserDetails(id)
        if user:
            return jsonify({"msg": "User details","code": 200,"user": user})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/liveUsersList', methods=['GET'])
@jwt_required
def liveUsersList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        status =  request.args.get('status', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not status:
            status='active'

        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort:
            sort = sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = AdminAPI.liveUsersList(ordId,length,page,search,status,sort,order)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)

        # data = {'deals':response1,'stage':stage_data2}

        user_total = AdminAPI.liveUsersListCount(ordId,status)
        data = {'total_count':user_total,'items':response1}
        return data


@app.route('/liveUsersListMobile', methods=['GET'])
@jwt_required
def liveUsersListMobile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sort =  request.args.get('sort', None)
        orderdata = 'desc'
        order =  request.args.get('order', orderdata)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        if not order:
            order=orderdata
        
        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = AdminAPI.liveUsersList(ordId,length,page,search,status,sort,order)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'live_users':response1}

        if data:
            return jsonify({"msg": "Live Users List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminUserList', methods=['GET'])
@jwt_required
def adminUserList():


    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
       
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        


        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort:
            sort = sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)


        response1 = AdminAPI.getAdminUserList(ordId,length,page,search,sort,order)
        # response1 = MongoAPI.getContacts(ordId,length,page,search,user,source,tag,date_from,date_to,sort_by)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array_sub(response)

        # data = {'deals':response1,'stage':stage_data2}

        user_total = AdminAPI.getAdminUserListCount()
        data = {'total_count':user_total,'items':response1}
        return data

@app.route('/addAdminUser', methods=['POST'])
@jwt_required
def addAdminUser():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    name =  request.json.get('name', None)
    phone = request.json.get('phone', None)
    
    if not name:
        return jsonify({"msg": "Missing name parameter","code": 400})
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    if not phone:
        return jsonify({"msg": "Missing phone parameter","code": 400})

    user1 = AdminAPI.userCheck(email)
    if user1=='Yes':
        return jsonify({"msg": "Email id is already registered","code": 400})
    else:
        org_id = int(ordId)
        # password = generate_password_hash(password)
        response = AdminAPI.createAdminUser(email,password,name,phone)
        response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "User Created Successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeAdminUserStatus/<id>', methods=['PUT'])
@jwt_required
def changeAdminUserStatus(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    status =  request.json.get('status', None)
    if not status:
        return jsonify({"msg": "Missing status parameter","code": 400})
    
    response = AdminAPI.changeAdminUserStatus(id,status)
    if response:
        return jsonify({"msg": "User Status changed successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/changeAdminUserPassword/<id>', methods=['PUT'])
@jwt_required
def changeAdminUserPassword(id):

    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)
    ordId = '0'
    password =  request.json.get('password', None)
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    
    response = AdminAPI.changeAdminUserPassword(id,password)
    if response:
        return jsonify({"msg": "User Password changed successfully!","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminUserUpdate/<id>', methods=['PUT'])
@jwt_required
def adminUserUpdate(id):

    email =  request.json.get('email', None)
    name =  request.json.get('name', None)
    phone = request.json.get('phone', None)

    
    if not name:
        return jsonify({"msg": "Missing name parameter","code": 400})
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not phone:
        return jsonify({"msg": "Missing phone parameter","code": 400})


    response = AdminAPI.adminUserUpdate(email,name,phone,id)
    if response=='Yes':
        return jsonify({"msg": "User successfully updated !","code": 200})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/ticketList', methods=['GET'])
@jwt_required
def ticketList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:



        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        source =  request.args.get('source', None)
        customer_type =  request.args.get('customer_type', None)
        user =  request.args.get('owner', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        
        if not source:
            source=''
        if not customer_type:
            customer_type=''
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        length = int(length)
        page = int(page)
        response1 = AdminAPI.getTicketList(ordId,length,page,search,user,source,customer_type,date_from,date_to,order,sort,current_user)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        org_total = AdminAPI.getTicketCountData()
        data = {'total_count':org_total,'items':response1}
        return data

@app.route('/enquiryList', methods=['GET'])
@jwt_required
def enquiryList():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:



        page =  request.args.get('page', None)
        sort =  request.args.get('sort', None)
        order =  request.args.get('order', None)
        length =  request.args.get('length', None)
        
        search =  request.args.get('search', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)


        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        
        if not order:
            order=''
        if not sort:
            sort=''

        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getEnquiryList(ordId,length,page,search,date_from,date_to,order,sort,current_user)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        org_total = AdminAPI.getEnquiryCountData()
        data = {'total_count':org_total,'items':response1}
        return data

@app.route('/enquiryListMobile', methods=['GET'])
@jwt_required
def enquiryListMobile():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = user[user2]
    ordId = int(ordId)
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        page =  request.args.get('page', None)
        length =  request.args.get('length', None)
        search =  request.args.get('search', None)
        date_from =  request.args.get('date_from', None)
        date_to =  request.args.get('date_to', None)
        sort =  request.args.get('sort', None)
        orderdata = 'desc'
        order =  request.args.get('order', orderdata)

        if not page:
            page=1
        if not length:
            length=10
        if not search:
            search=''
        if not order:
            order=orderdata
        
        if order=='desc':
            order=-1
        elif order=='asc':
            order=1
        else:
            order=1
        if not user:
            user=''
        if not date_from:
            date_from=''
        if not date_to:
            date_to=''
        if sort=='company_name':
            sort='account.company_name'
        elif sort:
            sort=sort
        else:
            sort='create_date'

        length = int(length)
        page = int(page)
        response1 = AdminAPI.getEnquiryList(ordId,length,page,search,date_from,date_to,order,sort)
        response = json.loads(json_util.dumps(response1))
        response1 = Uid.fix_array(response)
        data = {'enquiry_list':response1}

        if data:
            return jsonify({"msg": "Enquiry List","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminDashboard', methods=['GET'])
@jwt_required
def adminDashboard():
    current_user = get_jwt_identity()
    user = MongoAPI.authorizationCheck(current_user)

    ordId = '0'
    for user2 in user:
        if user2=='org_id':
            ordId = int(user[user2])

    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
        # today = date.now().date('%Y-%m-%d')

        # today
        y = datetime.datetime.today().strftime('%Y')
        m = datetime.datetime.today().strftime('%m')
        d = datetime.datetime.today().strftime('%d')
        today_start = datetime.datetime(int(y),int(m),int(d),0,0)
        today_end = datetime.datetime(int(y),int(m),int(d),23,59)

        # This Week 

        today = datetime.datetime.today()
        week_date_start = today - datetime.timedelta(days=today.weekday())
        wsy = week_date_start.strftime('%Y')
        wsm = week_date_start.strftime('%m')
        wsd = week_date_start.strftime('%d')
        week_start = datetime.datetime(int(wsy),int(wsm),int(wsd),0,0)
        week_date_end = week_date_start + datetime.timedelta(days=6)
        wey = week_date_end.strftime('%Y')
        wem = week_date_end.strftime('%m')
        wed = week_date_end.strftime('%d')
        week_end = datetime.datetime(int(wey),int(wem),int(wed),23,59)


        #this month

        today=datetime.datetime.now().date()
        start_date = today.replace(day=1)
        month = start_date.month
        year = start_date.year
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1
        next_month_start_date = start_date.replace(month=month, year=year)
        end_date = next_month_start_date - datetime.timedelta(days=1)
        sy = start_date.strftime('%Y')
        sm = start_date.strftime('%m')
        sd = start_date.strftime('%d')
        month_start = datetime.datetime(int(sy),int(sm),int(sd),0,0)
        ey = end_date.strftime('%Y')
        em = end_date.strftime('%m')
        ed = end_date.strftime('%d')
        month_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
        
        # Last 90 days
        n_day=(datetime.datetime.now()-datetime.timedelta(days=90))
        ny = n_day.strftime('%Y')
        nm = n_day.strftime('%m')
        nd = n_day.strftime('%d')
        n_start = datetime.datetime(int(ny),int(nm),int(nd),0,0)

        month_days = month_end - month_start
        month_signup=[]
        for i in range(month_days.days + 1):
            day_start=month_start + datetime.timedelta(days=i)
            day_end_convert=str(day_start).replace('00:00:00' , '23:59:00')
            day_end=datetime.datetime.strptime(day_end_convert, '%Y-%m-%d %H:%M:%S')
            this_day_count = AdminAPI.getSignupCount(day_start,day_end)
            this_day_unix=int(time.mktime(day_start.utctimetuple()) * 1000 + day_start.microsecond / 1000)
            this_array=[int(this_day_unix),int(this_day_count)]
            month_signup.append(this_array)

        week_days = week_end - week_start
        week_signup=[]
        for i in range(week_days.days + 1):
            day_start=week_start + datetime.timedelta(days=i)
            day_end_convert=str(day_start).replace('00:00:00' , '23:59:00')
            day_end=datetime.datetime.strptime(day_end_convert, '%Y-%m-%d %H:%M:%S')
            this_day_count = AdminAPI.getSignupCount(day_start,day_end)
            this_day_unix=int(time.mktime(day_start.utctimetuple()) * 1000 + day_start.microsecond / 1000)
            this_array=[int(this_day_unix),int(this_day_count)]
            week_signup.append(this_array)

        ninety_days = today_start - n_start
        ninety_signup=[]
        for i in range(ninety_days.days + 1):
            day_start=n_start + datetime.timedelta(days=i)
            day_end_convert=str(day_start).replace('00:00:00' , '23:59:00')
            day_end=datetime.datetime.strptime(day_end_convert, '%Y-%m-%d %H:%M:%S')
            this_day_count = AdminAPI.getSignupCount(day_start,day_end)
            this_day_unix=int(time.mktime(day_start.utctimetuple()) * 1000 + day_start.microsecond / 1000)
            this_array=[int(this_day_unix),int(this_day_count)]
            ninety_signup.append(this_array)

        today_signup = AdminAPI.getSignupCount(today_start,today_end)
        this_week_signup = AdminAPI.getSignupCount(week_start,week_end)
        this_month_signup = AdminAPI.getSignupCount(month_start,month_end)
        last_ninety_signup = AdminAPI.getSignupCount(n_start,today_end)
        total_companies = AdminAPI.getOrgCountData()
        active_companies = AdminAPI.getOrgCount('active')
        inactive_companies = AdminAPI.getOrgCount('inactive')
        active_users = AdminAPI.getUserCount('active')

        data = {
               'today_signup':today_signup,
               'this_week_signup':this_week_signup,
               'this_month_signup':this_month_signup,
               'last_ninety_signup':last_ninety_signup,
               'total_companies':total_companies,
               'active_companies':active_companies,
               'inactive_companies':inactive_companies,
               'active_users':active_users,
               'month_signup':month_signup,
               'week_signup':week_signup,
               'ninety_signup':ninety_signup
        }
        if data:        
            return jsonify({"msg": "dashboard","code": 200,"data": data})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/adminSignup', methods=['POST'])
def adminSignup():

    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    email =  request.json.get('email', None)
    password =  request.json.get('password', None)
    name =  request.json.get('name', None)
    device_id =  request.json.get('device_id', None)
    if not name:
        return jsonify({"msg": "Missing name parameter","code": 400})
    if not email:
        return jsonify({"msg": "Missing email parameter","code": 400})
    if not password:
        return jsonify({"msg": "Missing password parameter","code": 400})
    if not device_id:
        device_id=''
        app_type='web'
    else:
        app_type='mobile'
    
    user = AdminAPI.userCheck(email)
    if user=='Yes':
        return jsonify({"msg": "Email id is already registered","code": 400})
    else:
        response1 = AdminAPI.createUser(email,password,name)
        response = AdminAPI.selectLogin(email,password)
    if response1:

        data1 = defaultdict(list)
        data1['subject']='Farazon CRM Admin Account Signup'
        data1['content']='A new Admin account signup has been done.'
        data1['to']='swami@skyzon.com'
        data1['cc']=''
        data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
        # data1['attachment_list']=''
        emailId='info@farazon.com'
        attachment=''
        data1['associate_id']=response['id']
        data1['associate_to']='signup'
        sendMail=MongoAPI.emailSubmit('0',response['id'],data1,emailId,attachment)
        expires = datetime.timedelta(days=365)
        access_token = create_access_token(identity=str(response['id']), expires_delta=expires)
        refresh_token = create_refresh_token(identity=str(response['id']), expires_delta=expires)
        data = {'result': 'Done'}
        # reresult =  {'data': data,"code": 200,"msg":"Account registered successfully."}

        return jsonify({"msg": "Account registered successfully ! Contact Admin for account activation","code": 400,"data": data})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})


@app.route('/deleteData/<org_id>', methods=['GET'])
def deleteData(org_id):
    ordId = org_id
    if ordId=='':
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})
    else:
       
        if org_id:
            org_id = int(ordId)
            response1 = AdminAPI.deleteData(org_id)
            response = json.loads(json_util.dumps(response1))
            return jsonify({"msg": "Data Cleared!","code": 200})
        else:
            return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/checkEncryption/<password>', methods=['GET'])
def checkEncryption(password):
    # key = Fernet.generate_key()
    response1 = MongoAPI.dataEncryption(password)
    print(response1)
    return '1'

@app.route('/addNewAddon', methods=['POST'])
# @jwt_required
def addNewAddon():
    # current_user = get_jwt_identity()
    # user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    addon =  request.json.get('addon', None)
    addon_name =  request.json.get('addon_name', None)
    description =  request.json.get('description', None)
    response = AdminAPI.addNewAddon(addon,addon_name,description)
    response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "Addon added successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/mapAddon', methods=['POST'])
# @jwt_required
def mapAddon():
    # current_user = get_jwt_identity()
    # user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"})
    org_id =  request.json.get('org_id', None)
    addon =  request.json.get('addon', None)
    addon_id =  request.json.get('addon_id', None)
    status =  request.json.get('status', None)
    response = AdminAPI.mapAddon(org_id,addon,addon_id,status)
    response1 = json.loads(json_util.dumps(response))
    if response:
        return jsonify({"msg": "Addon activated Successfully","code": 200,"response": response1})
    else:
        return jsonify({"msg": "Oops,Something went wrong !","code": 400})

@app.route('/alladdonList', methods=['GET'])
@jwt_required
def alladdonList():
    current_user = get_jwt_identity()
    

    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    addons = AdminAPI.alladdonList()
    addons1 = Uid.fix_array(addons)
    # print (users)
    if addons1:
        return jsonify({"msg": "Addon List","code": 200,"data": addons1})
    else:
        return jsonify({"msg": "No Addon List","code": 400})

@app.route('/getOrgAddon/<org_id>', methods=['GET'])
@jwt_required
def getOrgAddon(org_id):
    current_user = get_jwt_identity()
    

    user = MongoAPI.authorizationCheck(current_user)
    ordId = ''
    addons = AdminAPI.getOrgAddon(org_id)
    addons1 = Uid.fix_array(addons)
    # print (users)
    if addons1:
        return jsonify({"msg": "Addon List","code": 200,"data": addons1})
    else:
        return jsonify({"msg": "No Addon List","code": 400})
