
import url_config

order_dict = {'asc': 1, 'desc': -1}

BASE_URL = url_config.BASE_URL
REDIRECT_URL = url_config.BASE_URL

PROFILE_ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
ALLOWED_EXTENSIONS = set(['txt','doc','docx','csv','xlsx','xls', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
RESUME_ALLOWED_EXTENSIONS = set(['doc','docx','pdf'])


UPLOAD_CRM_DOCS_FOLDER =  'uploads/crmDocs'

UPLOAD_FOLDER =  'uploads/'
UPLOAD_PRODUCT_FOLDER = 'uploads/product/'
UPLOAD_PROFILE_FOLDER = 'uploads/profile/'
UPLOAD_RESUME_FOLDER = 'uploads/resume/'
UPLOAD_COMPANY_FOLDER = 'uploads/company/'

UPLOAD_EMAIL_FOLDER = 'uploads/email/'
UPLOAD_DOCUMENT_FOLDER = 'uploads/document/'
UPLOAD_DEAL_FOLDER = 'uploads/deal/'


UPLOAD_OPEN_FOLDER =  'uploads/'
UPLOAD_OPEN_PROFILE_FOLDER = 'uploads/profile/'
UPLOAD_OPEN_COMPANY_FOLDER = 'uploads/company/'
UPLOAD_OPEN_EMAIL_FOLDER = 'uploads/email/'
UPLOAD_OPEN_CRM_DOCS_FOLDER = 'uploads/crmDocs/'
UPLOAD_OPEN_DOCUMENT_FOLDER = 'uploads/document/'
UPLOAD_COMMON_DOCUMENT_FOLDER = 'uploads/commonDocs/'
UPLOAD_OPEN_PRODUCT_FOLDER = 'uploads/product/'
UPLOAD_PDF_IMAGE_FOLDER = 'uploads/pdf_image/'
TEMPLATE_FOLDER = 'templates/'
MAIL_USE_TLS = False
MAIL_USE_SSL = True

# Create data
CREATE_TITLE = "Created"
CREATE_INFO = "Contact created"
# Create data
RESCHEDULE_TITLE = "Rescheduled"
RESCHEDULE_INFO = "Follow up created"
# Update data
UPDATE_TITLE = "Updated"
UPDATE_INFO = "Contact updated"
# Update data
SEND_EMAIL = "Send Email"
UPDATE_INFO = "Contact updated"
SEND_EMAIL_INFO = "Email"
DATE_FORMAT = '%d/%m/%Y'
company_models = ['company_name','phone','email','gstin','website','address1','address2','country','state','city','pincode','f1','f2','f3','f4','f5','sort_order']
product_models = ['product_name','model_no','uom','hsn_code','price','description','cgst','sgst','igst','sort_order']
contact_models = ['name','company_name','phone_number','email','description','whatsapp_no','secondary_email','designation','website','facebook','linkedin','twitter','skype','address1','address2','country','state','city','pincode','sort_order']
CSV_ALLOWED_EXTENSIONS = set(['csv'])
UPLOAD_CSV_FOLDER = 'uploads/csv/'


contact_project = {'assigned.name':1,'source_name.name':1,'countryId.id':1,'stateId.id':1,'account.company_name':1,'account.currency':1,'_id':1,'source':1,'org_id':1,'pincode':1,'name':1,'company_id':1,'assigned_to':1,'phone_number':1,'email':1,'description':1,'whatsapp_no':1,'secondary_email':1,'designation':1,'website':1,'facebook':1,'linkedin':1,'twitter':1,'skype':1,'address1':1,'address2':1,'country':1,'state':1,'city':1,'create_date':1,'tag':1,'modify_date':1,'key_contact':1,'custom_fields':1} 
account_contact_project = {'_id':1,'name':1} 
# company_project = {'assigned.name':1,'source_name.name':1,'customerType.name':1,'countryId.id':1,'stateId.id':1,'company_name':1,'_id':1,'phone':1,'org_id':1,'email':1,'assigned_to':1,'customer_type':1,'gstin':1,'website':1,'source':1,'address1':1,'address2':1,'country':1,'state':1,'city':1,'pincode':1,'create_date':1,'modify_date':1,'create_by':1} 
company_project = {'assigned.name':1,'source_name.name':1,'customerType.name':1,'countryId.id':1,'stateId.id':1,'company_name':1,'_id':1,'phone':1,'org_id':1,'email':1,'assigned_to':1,'customer_type':1,'gstin':1,'website':1,'source':1,'address1':1,'address2':1,'country':1,'state':1,'city':1,'pincode':1,'create_date':1,'modify_date':1,'create_by':1,'pricelist':1,'currency':1,'pricelist_name.title':1,}

note_Project = {'createBy.name':1,'_id':1,'org_id':1,'note':1,'associate_id':1,'associate_to':1,'create_date':1,'create_by':1} 
document_Project = {'createBy.name':1,'_id':1,'org_id':1,'document':1,'associate_id':1,'associate_to':1,'create_date':1,'create_by':1,'user_file_name':1} 
followUp_Project = {'assigned.name':1,'_id':1,'follow_up':1,'org_id':1,'name':1,'follow_type':1,'time':1,'date':1,'owner':1,'sms':1,'email':1,'associate_to':1,'associate_id':1,'priority':1,'status':1,'create_date':1,'create_by':1,'notes':1} 
email_Project = {'createBy.name':1,'_id':1,'email_id':1,'org_id':1,'fromEmail':1,'to':1,'cc':1,'bcc':1,'subject':1,'content':1,'attachment':1,'associate_id':1,'associate_to':1,'status':1,'create_date':1,'create_by':1} 
activity_Project = {'createBy.name':1,'_id':1,'action':1,'org_id':1,'associate_to':1,'associate_id':1,'via':1,'extra_info':1,'text_info':1,'title':1,'create_date':1} 
products_project = {'createBy.name':1,'category_name.name':1,'_id':1,'source':1,'org_id':1,'create_by':1,'model_no':1,'product_name':1,'hsn_code':1,'uom':1,'description':1,'cgst':1,'sgst':1,'igst':1,'category':1,'price':1,'status':1,'create_date':1,'modify_date':1,'create_by':1,'default_image':1,'images':1} 
deals_project = {'product_data.product_name':1,
'deal_no':1,
'modify_date':1,
'tag':1,'currency':1,
'stage_name.name':1,
'description':1,
'product_data.id':1,
'createBy.name':1,
'assigned.name':1,
'_id':1,
'contact.name':1,
'org_id':1,
'create_by':1,
'deal_name':1,
'contact_id':1,
'company_id':1,
'amount':1,
'stage':1,
'assigned_to':1,
'product':1,
'qty':1,
'target_date':1,
'create_date':1,
'create_by':1,
'modify_date':1,
'checklist':1,
'template_id':1,
'template_name':1,
'completed_percentage':1,
# 'account.company_name':1,
# 'account.source':1,
# 'account.website':1,
'account':1,
# 'account.currency'
'contact_detail':1,
'description':1,
'deal_status':1,
'custom_fields':1
}
deals_count_project = {'stage_name.name':1,'amount':1,'count':1}
quote_count_project = {'quote_stage_name.name':1,'total':1,'count':1}
quote_project = {'quote_stage_name':1,
                'deal.deal_name':1,
                'deal.deal_no':1,
                'assigned.name':1,
                'createBy.name':1,
                'account.company_name':1,
                'contact':1,
                'deal.deal_name':1,
                'deal.stage':1,
                '_id':1,
                'org_id':1,
                'subject':1,
                'deal_id':1,
                'quote__no':1,
                'currency':1,
                'contact_id':1,
                'company_id':1,
                'gstin':1,
                'ref_no':1,
                'valid_till':1,
                'quote_stage':1,
                'product':1,
                'discount_amount':1,
                'discount_percentage':1,
                'packing_amount':1,
                'packing_percentage':1,
                'shipping_charges':1,
                'installation_charges':1,
                'tax_type1':1,
                'tax_percentage1':1,
                'tax_amount1':1,
                'tax_type2':1,
                'tax_percentage2':1,
                'tax_amount2':1,
                'total':1,
                'terms_condition':1,
                'bank_details':1,
                'remark':1,
                'address1':1,
                'address2':1,
                'city':1,
                'state':1,
                'country':1,
                'pincode':1,
                'shipping_address1':1,
                'shipping_address2':1,
                'shipping_city':1,
                'shipping_state':1,
                'shipping_country':1,
                'shipping_pincode':1,
                'create_date':1,
                'create_by':1,
                'assigned_to':1,
                'deal._id':1,
                'quote_no':1,
                'payment_terms':1,
                'carrier':1,
                'field_setting':1,
                'delivery_period':1,
                'quote_category':1,
                'quote_revised':1,
                'revise_type':1,
                'revise_count':1,
                'same_as_address':1,
                'terms_condition_selected':1,
                'tax_type':1
                }

proforma_count_project = {'proforma_stage_lookup.name':1,'total':1,'count':1}

proforma_project = {'deal.deal_name':1,'field_setting':1,'deal.deal_no':1,'assigned.name':1,'createBy.name':1,'account.company_name':1,'contact':1,'deal.deal_name':1,'deal.deal_name':1,'_id':1,'org_id':1,'subject':1,
                'deal_id':1,
                'invoice_no':1,
                'quote_id':1,
                'proforma_no':1,
                'currency':1,
                'contact_id':1,
                'company_id':1,
                'gstin':1,
                'ref_no':1,
                'valid_till':1,
                'proforma_stage':1,
                'product':1,
                'discount_amount':1,
                'discount_percentage':1,
                'packing_amount':1,
                'packing_percentage':1,
                'shipping_charges':1,
                'installation_charges':1,
                'tax_type1':1,
                'tax_percentage1':1,
                'tax_amount1':1,
                'tax_type2':1,
                'tax_percentage2':1,
                'tax_amount2':1,
                'total':1,
                'terms_condition':1,
                'bank_details':1,
                'remark':1,
                'address1':1,
                'address2':1,
                'city':1,
                'state':1,
                'country':1,
                'pincode':1,
                'shipping_address1':1,
                'shipping_address2':1,
                'shipping_city':1,
                'shipping_state':1,
                'shipping_country':1,
                'shipping_pincode':1,
                'create_date':1,
                'create_by':1,
                'assigned_to':1,
                'deal._id':1,
                'quote_no':1,
                'payment_terms':1,
                'carrier':1,
                'proforma_stage_name.name':1,
                'delivery_period':1,
                'same_as_address':1,
                'terms_condition_selected':1,
                'quote_no':1,
                'proforma_date':1,
                'tax_type':1
                }


invoice_project = {'invoice_stage_name':1,'deal.deal_name':1,'deal.deal_no':1,'assigned.name':1,'createBy.name':1,'account.company_name':1,'contact':1,'deal.deal_name':1,'deal.deal_name':1,'_id':1,'org_id':1,'subject':1,
                'deal_id':1,
                'invoice_no':1,
                'currency':1,
                'contact_id':1,
                'company_id':1,
                'gstin':1,
                'ref_no':1,
                'valid_till':1,
                'invoice_stage':1,
                'product':1,
                'discount_amount':1,
                'discount_percentage':1,
                'packing_amount':1,
                'packing_percentage':1,
                'shipping_charges':1,
                'installation_charges':1,
                'tax_type1':1,
                'tax_percentage1':1,
                'tax_amount1':1,
                'tax_type2':1,
                'tax_percentage2':1,
                'tax_amount2':1,
                'total':1,
                'terms_condition':1,
                'bank_details':1,
                'remark':1,
                'address1':1,
                'address2':1,
                'city':1,
                'state':1,
                'country':1,
                'pincode':1,
                'shipping_address1':1,
                'shipping_address2':1,
                'shipping_city':1,
                'shipping_state':1,
                'shipping_country':1,
                'shipping_pincode':1,
                'create_date':1,
                'create_by':1,
                'assigned_to':1,
                'deal._id':1,
                'quote_no':1,
                'payment_terms':1,
                'carrier':1,
                'field_setting':1,
                'delivery_period':1,
                'same_as_address':1,
                'terms_condition_selected':1,
                'quote_no':1,
                'invoice_no':1,
                'quote_id':1,
                'tax_type':1
                }


user_project = {'email':1,'name':1,'org_id':1,'create_date':1,'status':1,'phone':1,'role':1,'report_to':1,'time_zone':1,'date_format':1,'profile_image':1,'currency':1,'mail_signature':1,'report.name':1,'roleData.role_name':1}
user_audit_project = {'user_id':1,'org_id':1,'create_date':1,'start_time':1,'end_time':1,'app_type':1,'user_detail':1,'org_detail':1}

user_audit_lookup = {'from': 'user','localField': 'user_id','foreignField': '_id','as': 'user_detail'}
org_lookup = {'from': 'organization','localField': 'org_id','foreignField': 'org_id','as': 'org_detail'}
categoryLookup = {'from': 'fields','localField': 'category','foreignField': '_id','as': 'category_name'}
user1_lookup = {'from': 'user','localField': 'assigned_to','foreignField': '_id','as': 'assigned'}

user_lookup = {'from': 'user','localField': 'assigned_to','foreignField': '_id','as': 'assigned'}
report_to_lookup = {'from': 'user','localField': 'report_to','foreignField': '_id','as': 'report'}
role_lookup = {'from': 'role','localField': 'role','foreignField': '_id','as': 'roleData'}

owner_lookup = {'from': 'user','localField': 'owner','foreignField': '_id','as': 'assigned'}
ownerLookup = {'from': 'user','localField': 'create_by','foreignField': '_id','as': 'createBy'}



note_lookup = {'from': 'user','localField': 'create_by','foreignField': '_id','as': 'createBy'}
account_lookup = {'from': 'account','localField': 'company_id','foreignField': '_id','as': 'account'}
contact_lookup = {'from': 'contact','localField': 'contact_id','foreignField': '_id','as': 'contact'}
contact_detail_lookup = {'from': 'contact','localField': 'contact_id','foreignField': '_id','as': 'contact_detail'}
fields_lookup = {'from': 'fields','localField': 'source','foreignField': '_id','as': 'source_name'}
deal_stage_lookup = {'from': 'fields','localField': 'stage','foreignField': '_id','as': 'stage_name'}
# deal_quote_stage_lookup = {'from': 'fields','localField': 'deal[0].stage','foreignField': '_id','as': 'stage_name'}
deal_lookup = {'from': 'deal','localField': 'deal_id','foreignField': '_id','as': 'deal'}
quote_stage_lookup = {'from': 'fields','localField': 'quote_stage','foreignField': '_id','as': 'quote_stage_name'}

proforma_stage_lookup = {'from': 'fields','localField': 'proforma_stage','foreignField': '_id','as': 'proforma_stage_name'}

invoice_stage_lookup = {'from': 'fields','localField': 'invoice_stage','foreignField': '_id','as': 'invoice_stage_name'}

customer_type_lookup = {'from': 'fields','localField': 'customer_type','foreignField': '_id','as': 'customerType'}
timelineLookup = {'from': 'user','localField': 'user_id','foreignField': '_id','as': 'createBy'}
countriesLookup = {'from': 'countries','localField': 'country','foreignField': 'name','as': 'countryId'}
statesLookup = {'from': 'states','localField': 'state','foreignField': 'name','as': 'stateId'}
sales_process_project = {'org_id':1,'process_name':1,'field_type':1,'list_value':1,'create_date':1,'create_by':1,'sub_process':1}
sales_sub_process_project = {'org_id':1,'parent_id':1,'process_name':1,'field_type':1,'list_value':1,'create_date':1,'create_by':1}
processLookup = {'from': 'sales_process','localField': 'parent_id','foreignField': '_id','as': 'parent_process'}
checklist_project = {'org_id':1,'process_name':1,'field_type':1,'list_value':1,'create_date':1,'create_by':1,'sub_checklist':1,'status':1,'createBy.name':1,'value':1}
checklist_sub_project = {'org_id':1,'process_name':1,'field_type':1,'list_value':1,'create_date':1,'create_by':1,'sub_checklist':1,'status':1,'createBy.name':1,'value':1}
checklistLookup = {'from': 'deals_sub_check_list','localField': '_id','foreignField': 'parent_id','as': 'sub_checklist'}
salesProcessLookup = {'from': 'sales_sub_process','localField': '_id','foreignField': 'parent_id','as': 'sub_process'}
dealCheckListLookup = {'from': 'deals_check_list','localField': '_id','foreignField': 'associate_id','as': 'checklist'}
fields_project = {'org_id':1,'name':1,'type':1}
organization_project = {'org_id':1,'organization_name':1,'logo':1,'address':1,'address2':1,'city':1,'state':1,'pcode':1,'country':1,'phone':1,'website':1,'email':1,'gstin':1,'pan_no':1,'currency':1,'create_date':1,'status':1,'start_date':1,'end_date':1} 
ticket_project = {'org_id':1,'user_id':1,'ticket_no':1,'subject':1,'message':1,'attachment':1,'user_detail':1,'org_detail':1,'create_date':1}
enquiry_project = {'enquiry_type':1,'name':1,'company_name':1,'mobile':1,'email':1,'city':1,'state':1,'country':1,'pincode':1,'message':1,'file_name':1,'create_date':1}
pricelist_project_list = {'createBy.name':1,'_id':1,'org_id':1,'create_by':1,'title':1,'create_date':1}
PricelistProduct  = {'inr_price':1,'usd_price':1,'erou_price':1}
pricelistLookup = {'from': 'pricelist','localField': 'pricelist','foreignField': '_id','as': 'pricelist_name'}
products_pricelist_project = {'product_id':1} 
productsPricelistProject = {'product.product_name':1,'product._id':1,'product_id':1,'product.price':1,'inr_price':1,'usd_price':1,'erou_price':1}

pricelist_products_lookup = {'from': 'product','localField': 'product_id','foreignField': '_id','as': 'product'}


