# from app import status

from flask import Flask
from flask import request, jsonify, json, Response, Flask,url_for
from db import initialize_db
from bson import json_util
from models import User,Organization,Countries,States,Cities,User1,Fields,Numbering,Module_name,User_module_name,Timezones,Contact,Contact1,Activity,Account,Follow_up,Note,Product,Deal,Email,Document,Quote,PdfSettings,Proforma,Role,Invoice,Email_template,Rename,SalesProcess,SalesSubProcess,DealsCheckList,DealsSubCheckList,SalesProcessTemplate,DealsStageMapping,AdminEmailTemplate,Reporting_To,SupportTicket,Enquiry,Partner,Accounttmp,Contacttmp,Producttemp,User_audit,AdminUser,CustomFields,QuoteBuilder,Pricelist,PricelistProduct,OrgAddon,AddOn
from bson import ObjectId
import datetime
from datetime import date
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, fresh_jwt_required,get_raw_jwt
)
from mongoengine.errors import NotUniqueError
from werkzeug.security import generate_password_hash, check_password_hash
import re
from common_config import Uid
from collections import defaultdict
from common_config import Uid
from flask_split import split
import app_config
import os
import decimal 
from cryptography.fernet import Fernet
import base64
from pytz import timezone 

# from flask_session import Session

app = Flask(__name__)

# SESSION_TYPE = 'user'
# Session(app)

from datatable import LastDataId

DATE_FORMAT1 = '%d/%m/%Y'

DATE_FORMAT_mdy = '%m/%d/%Y'

DATE_FORMAT2 = '%H:%M %p'

DATE_FORMAT3 = '%Y-%m-%d %H:%M:%S'

DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'

DATE_FORMAT4 = '%Y-%m-%d %H:%M:%S'

USER_DATE_FORMAT1 = '%m-%d-%y'

USER_DATE_FORMAT2 = '%d-%m-%Y'

USER_DATE_FORMAT3 = '%m-%d-%Y'

app.config.from_pyfile('config.cfg')
app.config['UPLOAD_PDF_IMAGE_FOLDER'] = app_config.UPLOAD_PDF_IMAGE_FOLDER

currency_code=defaultdict(list)
currency_code['inr']='&#8377'
currency_code['usd']='&#36'
currency_code['euro']='&#8364;'



initialize_db(app)

class MongoAPI:
    def selectLogin(email,password):
       
        try:
            s = User.objects.get(email=email,password=password)
            id1 = json.loads(json_util.dumps(s.user_id))
            id12 = json.loads(json_util.dumps(s))
            id = str(s.id)
            output1 = {'name' : s.name,'email' : s.email,'create_date' : s.create_date,'id':id,'role':s.role,'org_id':s.org_id,'status':s.status}
            output = output1
        except User.DoesNotExist:
            output = 'no such name'        
        return output

    def selectLoginMail(user_id):
       
        try:
            # password = generate_password_hash(password)
            # print (password)
            s = User.objects.get(user_id=ObjectId(user_id))
            id1 = json.loads(json_util.dumps(s.user_id))
            id12 = json.loads(json_util.dumps(s))
            id = str(s.id)
            output1 = {'name' : s.name,'email' : s.email,'create_date' : s.create_date,'id':id,'role':s.role,'org_id':s.org_id,'status':s.status}
            output = output1
        except User.DoesNotExist:
            output = 'no such name'        
        return output
    def emailCheck(email):
        try:
            s = User.objects.get(email=email)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   

        return output

    def userCheck(user_id):
        try:
            s = User.objects.get(user_id=ObjectId(user_id))
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   

        return output

    def updateDeviceID(org_id,user_id,device_id):
        update_order = User.objects(org_id=org_id,id=ObjectId(user_id)).update(device_id=device_id)

    def createOrganizationUser(email,password,name,org_id,phone,role,report_to):

        try:
            user = User(email=email,name=name,org_id=org_id,password=password,phone=phone,role=role,report_to=report_to)
            response = user.save()
            output1 = {'user_id' : response.id}
            delete_report_to =  Reporting_To.objects(user_id=response.id).delete()
            for report in report_to:
                insert_reporting = Reporting_To(user_id=response.id,reporting_to=report)
                insert_reporting_save = insert_reporting.save()
            return output1
        except NotUniqueError as e:
            return ''

    def userAudit(user_id,org_id,start_time,app_type):

        try:
            y = datetime.datetime.today().strftime('%Y')
            m = datetime.datetime.today().strftime('%m')
            d = datetime.datetime.today().strftime('%d')
            from_date = datetime.datetime(int(y),int(m),int(d),0,0)
            to_date = datetime.datetime(int(y),int(m),int(d),23,59)
            filter = {}
            filter['user_id'] = ObjectId(user_id)
            filter['start_time'] = {"$gte": from_date,"$lte": to_date}
            match = filter
            pipeline = [
                         
                            {'$match': match}
                       ]
            data = User_audit.objects.aggregate(*pipeline)
            check_data = json.loads(json_util.dumps(data))
            if len(check_data) == 0:
                user = User_audit(user_id=ObjectId(user_id),org_id=org_id,start_time=start_time,end_time=start_time,app_type=app_type)
                response = user.save()
                output=response.id
            else:
                output='1'
            return output
           
        except NotUniqueError as e:
            return ''

    def userLogoutWeb(user_id,org_id,end_time):
        try:
            y = datetime.datetime.today().strftime('%Y')
            m = datetime.datetime.today().strftime('%m')
            d = datetime.datetime.today().strftime('%d')
            from_date = datetime.datetime(int(y),int(m),int(d),0,0)
            to_date = datetime.datetime(int(y),int(m),int(d),23,59)
            app_type='web'
            filter = {}
            filter['user_id'] = ObjectId(user_id)
            filter['app_type'] = app_type
            filter['start_time'] = {"$gte": from_date,"$lte": to_date}
            match = filter
            pipeline = [    
                            {'$match': match}
                       ]
            data = User_audit.objects.aggregate(*pipeline)
            check_data = json.loads(json_util.dumps(data))
            for cd in check_data:
                l_id=cd['_id']['$oid']

            if len(check_data) != 0:
                user = User_audit.objects(id=ObjectId(l_id)).update(end_time=end_time)
                output = user_id
            else:
                output='1'
            return output
           
        except NotUniqueError as e:
            return ''

    def userLogoutMobile(user_id,org_id,end_time):
        try:
            y = datetime.datetime.today().strftime('%Y')
            m = datetime.datetime.today().strftime('%m')
            d = datetime.datetime.today().strftime('%d')
            from_date = datetime.datetime(int(y),int(m),int(d),0,0)
            to_date = datetime.datetime(int(y),int(m),int(d),23,59)
            app_type='mobile'
            filter = {}
            filter['user_id'] = ObjectId(user_id)
            filter['app_type'] = app_type
            filter['start_time'] = {"$gte": from_date,"$lte": to_date}
            match = filter
            pipeline = [    
                            {'$match': match}
                       ]
            data = User_audit.objects.aggregate(*pipeline)
            check_data = json.loads(json_util.dumps(data))
            for cd in check_data:
                l_id=cd['_id']['$oid']

            if len(check_data) != 0:
                user = User_audit.objects(id=ObjectId(l_id)).update(end_time=end_time)
                output = user_id
            else:
                output='1'
            return output
           
        except NotUniqueError as e:
            return ''
    

    def authorizationCheck(id):
        try:
            filter = {}
            filter['_id'] = ObjectId(id)
            filter['status'] = 'active'
            match = filter
            pipeline = [
                            {'$lookup': app_config.role_lookup},
                            {'$match'  : match}
                        ]
           
            settings = defaultdict(list)
            role = User.objects.aggregate(*pipeline)

            settings6=[]
            
            item4 = role
            
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    elif item1=='_id':
                        settings['id'] = item[item1]
                    elif item1=='org_id':
                        settings['org_id'] = int(item[item1])
                    elif item1=='roleData':
                        assigned = item[item1]
                        for item2 in assigned:
                            for item3 in item2:
                                if item3=='_id':
                                    role_key = str('role')+str(item3)
                                else:
                                    role_key = str('role_')+str(item3)
                                # print (role_key)
                                settings[role_key] = item2[item3]
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['roleData']:
                        settings['roleData']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['company_name']:
                        settings['company_name']=''            
            settings2 = json.loads(json_util.dumps(settings))
            
            # print (json.loads(settings2))
            settings2 = Uid.fix_array4(settings2)
            # print(settings2)
            # session['user'] = settings2

            # print (session['user'])

            return settings2
        except User.DoesNotExist:
            output = 'No'   

        return output

    def dataEncryption(message):
        try:
            passKey = Fernet.generate_key()
            fernet = Fernet(passKey)
            encString = fernet.encrypt(message.encode())
            data = {
                'passKey':passKey,
                'encString':encString
            }
            # print(data)
            return data
        except NotUniqueError as e:
            return '0'

    def createUser(email,dataEnc,name,org_id):

        try:
            data1 = defaultdict(list)
            data1['email'] = email
            data1['password'] = dataEnc
            # data1['passKey'] = dataEnc['passKey']
            data1['name'] = name
            data1['org_id'] = org_id
            user = User(**data1)
            response = user.save()
            output1 = {'user_id' : str(response.id)}
            return output1
        except NotUniqueError as e:
            return ''

    def addSettings(type,name,weightage,info,org_id,count,default=0):
        try:
            count = int(count)+1

            if default==1:
                user = Fields.objects(type=type,org_id=org_id).update(default=0)

            if count==1:
                fields = Fields(type=type,name=name,weightage=weightage,org_id=org_id,sort_order=count,default=1,info=info)
            else:
                fields = Fields(type=type,name=name,weightage=weightage,org_id=org_id,sort_order=count,default=default,info=info)

            response = fields.save()

            output1 = {'field_id' : response.id}

            return output1
        except Fields.DoesNotExist:
            return ''

    def reorderDealStage(org_id):
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='New Opportunity').update(sort_order=1,default=1)
     
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Quote Sent').update(sort_order=2,default=0)
      
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Expect').update(sort_order=3,default=0)
    
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Negotiation').update(sort_order=4,default=0)
 
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Commit').update(sort_order=5,default=0)
   
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Order in hand').update(sort_order=6,default=0)
    
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Won').update(sort_order=7,default=0)
     
        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Lost').update(sort_order=8,default=0)

        update_order = Fields.objects(org_id=org_id,type='deal_stage',name='Revised').update(sort_order=9,default=0)
     
    def reorderSettings(org_id,user_id,data):
        try:
            order = 1
            for s_id in data['settings_list']:
                update_settings = Fields.objects(org_id=org_id,type=data['settings_type'],id=s_id).update(sort_order=order)
                order = order+1
            return '1'
        except Fields.DoesNotExist:
            return ''

     

    def addAdminEmailTemplates(template_name,template_content):
        try:
            template = AdminEmailTemplate(template_name=template_name,template_content=template_content)
            response = template.save()
            output1 = {'template_id' : response.id}

            return output1
        except AdminEmailTemplate.DoesNotExist:
            return ''
    def addEmailTemplate(name,template,subject,org_id,count,default=0):
        try:
            count = int(count)+1

            if default==1:
                user = Email_template.objects(org_id=org_id).update(default=0)

            if count==1:
                fields = Email_template(name=name,template=template,subject=subject,org_id=org_id,sort_order=count,default=1)
            else:
                fields = Email_template(name=name,template=template,subject=subject,org_id=org_id,sort_order=count,default=default)

            response = fields.save()
            output1 = {'field_id' : response.id}
            return output1
        except Fields.DoesNotExist:
            return ''
    
    def addPDFSettingsNew(org_id,data1):
        try:
            data1['org_id'] = org_id
            # pdf_settings = PdfSettings(**data1)

            # response = pdf_settings.save()
            # print (data1)
            u2 = PdfSettings.from_json(json.dumps(data1))
            u2.save()


            output1 = {'field_id' : u2.id}
            return output1
        except PdfSettings.DoesNotExist:
            return ''

    def updatePDFSettings(org_id,data1):
        try:
            data1['pdf_modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)
            # if data1['name']:
            #     data5['name'] = data1['name']
            # if data1['type']:
            #     data5['type'] = data1['type']
            # if data1['Taxable']:
            #     data5['Taxable'] = data1['Taxable']
          
            # data5['modify_date'] = datetime.datetime.utcnow

            data3 = defaultdict(list)

            for data4 in data1:
                data3[data4] = data1[data4]
            # print(data3)
            PdfSettings.objects(org_id=org_id,doc_type=data1['doc_type']).update_one(**data3)

            # u2.save()
            output1 = data1['doc_type']
            return output1
        except NotUniqueError as e:
            return '0' 


    
    def quoteBuilderItem(org_id,data1):
        try:
            check_org = QuoteBuilder.objects(org_id=org_id).count()
            if check_org == 0:
                data1['org_id'] = org_id
                u2 = QuoteBuilder.from_json(json.dumps(data1))
                u2.save()
                output1 = {'field_id' : u2.id}
                return output1
            else:
                data3 = defaultdict(list)
                for data4 in data1:
                    data3[data4] = data1[data4]
                QuoteBuilder.objects(org_id=org_id).update_one(**data3)
                output1 = {'field_id' : org_id}
                return output1
        except QuoteBuilder.DoesNotExist:
            return ''

    def getQuoteBuilderSettings(org_id):
        try:

            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$match'  : match},

                        ]
            
            settings = defaultdict(list)

            role = QuoteBuilder.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    if item1=='prepared_by_path' or item1=='checked_by_path' or item1=='prepared_by_path' or item1=='authorized_signatory_path' or item1=='footer_img_file':
                        if item[item1]!='':
                            item[item1]=str(item[item1])
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def dealUpdate(org_id,id,data1):
        try:
            # print(id)
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['deal_name']:
                data5['deal_name'] = str(data1['deal_name'])
            if data1['company_id']:
                data5['company_id'] = ObjectId(str(data1['company_id']))
            if data1['contact_id']:
                data5['contact_id'] = ObjectId(str(data1['contact_id']))
            if data1['amount']:
                data5['amount'] = float(data1['amount'])
            if data1['stage']:
                data5['stage'] = ObjectId(str(data1['stage']))
            # if data1['assigned_to']:
            #     data5['assigned_to']= ObjectId(str(data1['assigned_to']))
            if data1['product']:
                data5['product'] = data1['product']
            # if data1['qty']:
            #     data5['qty']=data1['qty']
            if data1['target_date']:
                dt = datetime.datetime.strptime(data1['target_date'], '%Y-%m-%dT%H:%M:%S.000Z').strftime('%Y-%m-%d %H:%M:%S.%f')
                data5['target_date'] = dt
                
                 
            # if data1['close_date']:
            #     data5['close_date']=data1['close_date']
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']
            if data1['currency']:
                data5['currency']=data1['currency']
            if data1['description']:
                data5['description']=data1['description']
            if data1['custom_fields']:
                data5['custom_fields']=data1['custom_fields']
            data3 = defaultdict(list)
            for data4 in data5:
                data3[data4] = data1[data4]
            Deal.objects(id=ObjectId(id)).update_one(
                **data3
                )
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 
            # u2 = PdfSettings.from_json(json.dumps(data1))
            # u2.save()


            output1 = {'field_id' : u2.id}
            return output1
        except PdfSettings.DoesNotExist:
            return ''

    def closeDeal(org_id,id,data,user_id):
        try:
            data['modify_date'] = datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
            data['completed_date'] = datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
            filter = {}
            filter['org_id'] = org_id
            filter['type'] = 'deal_stage'
            if data['type']=='won':
                filter['name'] = 'Won'
            elif data['type']=='lost':
                filter['name'] = 'Lost'
          
            match = filter
            pipeline = [
                            {'$match'  : match}
                       ]
            settings = defaultdict(list)
            status = Fields.objects.aggregate(*pipeline)
            stage=''
            for item in status:
    
                if data['type'] == 'won':
                    stage=item['_id']
                    deal = Deal.objects(org_id=org_id,id=ObjectId(id)).update(stage=item['_id'],reason=data['reason'],amount=data['deal_value'],completed_percentage=100,deal_status='won',completed_date=data['completed_date'])
                    # checkList = DealsCheckList.objects(org_id=org_id,id=data['process_id'],associate_id=ObjectId(id)).update(status='completed',value='won',create_by=user_id)
                elif data['type'] == 'lost':
                    stage=item['_id']
                    deal = Deal.objects(org_id=org_id,id=ObjectId(id)).update(stage=item['_id'],reason=data['reason'],amount=data['deal_value'],completed_percentage=0,deal_status='lost',completed_date=data['completed_date'])
                    # checkList = DealsCheckList.objects(org_id=org_id,id=data['process_id'],associate_id=ObjectId(id)).update(status='completed',value='lost',create_by=user_id)


           

            deal_detail=MongoAPI.getDealDetails(org_id,id)
            deal_detail = Uid.fix_array3(deal_detail)
            # Update business type

            deal_filter = {}
            dkey = []
            dval = []
            deal_filter['org_id'] = org_id
            deal_filter['company_id'] = ObjectId(deal_detail['company_id'])
            deal_filter['business_type'] = 'new'
            for item1 in deal_filter:
                dkey.append(item1)
                dval.append(deal_filter[item1])
            match = deal_filter
            pipeline = [
                        {'$match'  : match},
                    ]
            deal_data = defaultdict(list)
            deal_data = Deal.objects.aggregate(*pipeline)
            ll=list(deal_data)
            if len(ll) == 0:
                 deal_update = Deal.objects(id=ObjectId(id)).update(business_type='new')
            elif len(ll) >= 1:
                deal_update = Deal.objects(id=ObjectId(id)).update(business_type='repeat')


            # Update Quote stage


            quote_filter = {}
            qkey = []
            qval = []
            quote_filter['org_id'] = org_id
            quote_filter['deal_id'] = ObjectId(id)
            
            for item1 in quote_filter:
                qkey.append(item1)
                qval.append(quote_filter[item1])
            match = quote_filter
            pipeline = [
                        {'$sort'   : {'create_date' : -1}},
                        {'$limit'  : 1},
                        {'$match'  : match},
                    ]
            quote = defaultdict(list)
            quoteData = Quote.objects.aggregate(*pipeline)
   
            # q_length=list(quoteData)
            # print('cccc')
            # print(q_length)
            for dd in quoteData:

                quote_id=dd['_id']

                if quote_id!='':
                    quote_update = Quote.objects(id=ObjectId(quote_id)).update(quote_stage=ObjectId(deal_detail['stage']))

            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 

    def addSettingsNew(type,name,org_id,count,data1,default=0):
        try:
            count = int(count)+1

            if not data1['info']:
                info =''
            else:
                info = data1['info']

            if default==1:
                user = Fields.objects(type=type,org_id=org_id).update(default=0)

            if count==1:
                if type=='deal_stage':
                    default_val=0
                    fields = Fields(type=type,name=name,org_id=org_id,default=default_val,info=info)
                else:
                    default_val=1
                    fields = Fields(type=type,name=name,org_id=org_id,sort_order=count,default=default_val,info=info)
            else:
                fields = Fields(type=type,name=name,org_id=org_id,sort_order=count,default=default,info=info)

            response = fields.save()
            output1 = {'field_id' : response.id}
            return output1
        except Fields.DoesNotExist:
            return ''

    def settingsDefault(type,org_id,id):
        try:
            data5 = defaultdict(list)
            data5['default'] = 0
            Fields.objects(org_id=org_id,type=type).update(**data5)


            data6 = defaultdict(list)
            data6['default'] = 1
            Fields.objects(id=ObjectId(id)).update_one(**data6)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 
    
    
    def settingsDefaultName(type,org_id,name):
        try:
            data5 = defaultdict(list)
            data5['default'] = 0
            Fields.objects(org_id=org_id,type=type).update(**data5)


            data6 = defaultdict(list)
            data6['default'] = 1
            Fields.objects(org_id=org_id,type=type,name=name).update_one(**data6)

            # u2.save()
            output1 = name
            return output1
        except NotUniqueError as e:
            return '0' 
    
    def addsalesSubProcessDefault(org_id,user_id,process_name,field_type):
        try:
           
            salesProcess = SalesSubProcess(org_id=org_id,create_by=user_id,process_name=process_name,field_type=field_type)
            response = salesProcess.save()
            output = org_id
            return output
        except NotUniqueError as e:
            return '0' 

    def addsalesProcessDefault(org_id,user_id,process_name,field_type):
        try:
           
            salesProcess = SalesProcess(org_id=org_id,create_by=user_id,process_name=process_name,field_type=field_type)
            response = salesProcess.save()
            output = org_id
            return output
        except NotUniqueError as e:
            return '0' 

    def emailTemplateDefault(org_id,id):
        try:
            data5 = defaultdict(list)
            data5['default'] = 0
            Email_template.objects(org_id=org_id).update(**data5)


            data6 = defaultdict(list)
            data6['default'] = 1
            Email_template.objects(id=ObjectId(id)).update_one(**data6)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def salesProcessDefault(org_id,id):
        try:
            data5 = defaultdict(list)
            data5['default'] = 0
            SalesProcessTemplate.objects(org_id=org_id).update(**data5)


            data6 = defaultdict(list)
            data6['default'] = 1
            SalesProcessTemplate.objects(id=ObjectId(id)).update_one(**data6)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def getPdfSettings(org_id,doc_type):
        try:

            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            filter['doc_type'] = doc_type
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # print(filter)
            pipeline = [

                            # {'$lookup' : app_config.fields_lookup},
                            # {'$lookup' : app_config.countriesLookup},
                            # {'$lookup' : app_config.statesLookup},
                            # {'$sort'   : {'create_date' : order_dict}},
                            {'$match'  : match},
                            # {'$project': app_config.fields_project}
                        ]
            
            settings = defaultdict(list)

            role = PdfSettings.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    if item1=='prepared_by_path' or item1=='checked_by_path' or item1=='prepared_by_path' or item1=='authorized_signatory_path' or item1=='footer_img_file':
                        if item[item1]!='':
                            item[item1]=str(item[item1])
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def emailSent(id):
        try:
            # print(id)
            data5 = defaultdict(list)
            Email.objects(id=id).update(status='1')
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 

    def addNumberingSettings(type,name,prefix,sequence,org_id):
        try:
            fields = Numbering(type=type,name=name,org_id=org_id,prefix=prefix,sequence=sequence)
            response = fields.save()
            output1 = {'field_id' : response.id}
            return output1
        except Fields.DoesNotExist:
            return ''

    def updateNumberingSettings(prefix,sequence,id):
        try:
            modify_date = datetime.datetime.utcnow
            user = Numbering.objects(id=id).update_one(prefix=prefix,sequence=sequence,modify_date=modify_date)
            output = 'Yes'
            output1 = {'id' : id}
            return output1
        except Fields.DoesNotExist:
            return ''
    
    def getNumberingSettings(type,id):
        try:         
            s1 = Numbering.objects.get(type=type,org_id=id).to_json()
            output = s1
            s = json.loads(output)
            
            settings = defaultdict(list)
            f_id = ''
            for item in s:
                if item=='_id':
                    idData = s[item]
                    for num in idData:
                        settings['id'] = idData['$oid']
                        f_id = idData['$oid']
                else:
                    settings[item] = str(s[item])
            output = json.loads(json_util.dumps(settings))
            
            return output
        except Numbering.DoesNotExist:
            return ''

    def getNumberingSettings2(type,id):
        try:         
            if type=='deal':
                type = 'deal'
                name = 'deal_no'
                prefix = 'DE'
                sequence = '1001'
                response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,id)
            elif type=='quote':
                type = 'quote'
                name = 'quote_no'
                prefix = 'QUO'
                sequence = '1001'
                response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,id)
            elif type=='proforma':
                type = 'proforma'
                name = 'proforma_no'
                prefix = 'PRO'
                sequence = '1001'
                response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,id)
            elif type=='invoice':
                type = 'invoice'
                name = 'invoice_no'
                prefix = 'INV'
                sequence = '1001'
                response = MongoAPI.addNumberingSettings(type,name,prefix,sequence,id)
            output = MongoAPI.getNumberingSettings(type,id)
            return output
        except Numbering.DoesNotExist:
            return ''

    def getNumberingSettingsNew(type,id):
        try:         
            # print (type)
            # print (id)
            s1 = Numbering.objects.get(type=type,org_id=id).to_json()
            output = s1
            s = json.loads(output)
            
            settings = defaultdict(list)
            for item in s:
                if item=='_id':
                    idData = s[item]
                    for num in idData:
                        settings['id'] = idData['$oid']
                else:
                    settings[item] = str(s[item])
            output = json.loads(json_util.dumps(settings))
            return output

        except Numbering.DoesNotExist:
            return ''
    
    def updateSettings(org_id,name,weightage,id,type,default,info):
        try:
            modify_date = datetime.datetime.utcnow
            if default==1:
                user = Fields.objects(type=type,org_id=org_id).update(default=0)
            
            user = Fields.objects(id=id).update_one(name=name,modify_date=modify_date,default=default,info=info,weightage=int(weightage))
            output = 'Yes'
            output1 = {'id' : id}
            return output1
        except Fields.DoesNotExist:
            return ''

    def deleteSettings(id):

        user = Fields.objects(id=id).first()
        if not user:
            return 'No'
        else:
            user.delete()
            return 'Yes'

    def deleteEmailTemplate(id):

        template = Email_template.objects(id=id).first()
        if not template:
            return 'No'
        else:
            template.delete()
            return 'Yes'

    def deleteDocumentCommon(id):

        document = Document.objects(id=id).first()
        if not document:
            return 'No'
        else:
            document.delete()
            return 'Yes'
    def deleteDocument(id):

        document = Document.objects(id=id).first()
        if not document:
            return 'No'
        else:
            document.delete()
            return 'Yes'

    def getEmailTemplates(org_id):
        try:
            
            filter = {}
            filter['org_id'] = org_id
            match = filter
            pipeline = [
                            {'$match': match},
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Email_template.objects.aggregate(*pipeline)
            settings6=[]
            
            item4 = role
            settings6 = []
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='default':
                        assigned = int(item[item1])
                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            # print (json.loads(settings2))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getEmailTemplateDetails(org_id,id):
        try:
            
            filter = {}
            filter['org_id'] = org_id
            filter['_id'] = ObjectId(id)
            match = filter
            pipeline = [
                            {'$match': match},
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Email_template.objects.aggregate(*pipeline)
            settings6=[]
            
            item4 = role
            
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='default':
                        assigned = int(item[item1])
                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}

            
            settings2 = json.loads(json_util.dumps(settings))
            # print (json.loads(settings2))
            return settings2
        except NotUniqueError as e:
            return '0'

    def checkSettings(type,name,org_id):
        try:
            
            fields = Fields.objects.get(type=type,name=name,org_id=org_id)
            if fields.name:
                output = 'Yes'
            else:
                output = 'No'
            return output
        except Fields.DoesNotExist:
            return 'No'
    def checkEmailTemplate(name,org_id):
        try:
            
            fields = Email_template.objects.get(name=name,org_id=org_id)
            if fields.name:
                output = 'Yes'
            else:
                output = 'No'
            return output
        except Email_template.DoesNotExist:
            return 'No'
    def emailTemplateCount(org_id):
        try:
            
            count = Email_template.objects.filter(org_id=org_id).count()
        
            return count
        except Email_template.DoesNotExist:
            return '0'
    
    def updateEmailTemplate(id,name,template,subject,org_id,default):
        try:
            modify_date = datetime.datetime.utcnow
            if default==1:
                user = Email_template.objects(org_id=org_id).update(default=0)


            user = Email_template.objects(id=id).update_one(name=name,template=template,subject=subject,org_id=org_id,modify_date=modify_date,default=default)
            # print (user)
            output = 'Yes'
            output1 = {'id' : id}
            return output1
        except Fields.DoesNotExist:
            return ''

    def checkPDFSettings(doc_type,org_id):
        try:
            fields = PdfSettings.objects.filter(doc_type=doc_type,org_id=org_id).first().to_json()
            output = json.loads(fields)
            
            if output['name']:
                # settings = json.loads(json_util.dumps(fields))
                output = output
            else:
                output = 'No'
            return output
        except PdfSettings.DoesNotExist:
            return 'No'

    def checkCustomPDFSettings(org_id):
        try:
            fields = QuoteBuilder.objects.filter(org_id=org_id).first().to_json()
            output = json.loads(fields)
            
            if output['name']:
                # settings = json.loads(json_util.dumps(fields))
                output = output
            else:
                output = 'No'
            return output
        except PdfSettings.DoesNotExist:
            return 'No'

    def checkEditSettings(type,name,org_id,id):
        try:
            field = Fields.objects.get(type=type,name=name,org_id=org_id,id__not__in=ObjectId(id))
            if not field['type']:
                output = 'No'
            else:
                output = 'Yes'
            return output
        except NotUniqueError as e:
            return 'No'



    def getTodayFollowUpCount(org_id,user_id,from_date,to_date,status):
        try:
            filter = {}
            filter['org_id'] = org_id
            filter['owner'] = ObjectId(user_id)
            if status=='all':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            if status=='today':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Completed':
                filter['status'] = 'Completed'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Overdue':
                filter['status'] = 'Open'
                yesterday = from_date - datetime.timedelta(days = 1)
                # print (yesterday)
                filter['date'] = {"$lte": yesterday}
            count = Follow_up.objects.filter(**filter).count()
            return count 
        except Fields.DoesNotExist:
            return '0'


    def getTodayFollowUp(org_id,user_id,from_date,to_date,status):
        try:

            filter = {}
            filter['org_id'] = org_id
            filter['owner'] = ObjectId(user_id)
            

            if status=='all':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Completed':
                filter['status'] = 'Completed'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Overdue':
                filter['status'] = 'Open'
                yesterday = from_date - datetime.timedelta(days = 1)
                # print (yesterday)
                filter['date'] = {"$lte": yesterday}

            match = filter

            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$project': app_config.followUp_Project},
                            {'$match': match}
                        ]

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getTodayFollowUpCount(org_id,user_id,from_date,to_date,status):
        try:

            filter = {}
            filter['org_id'] = org_id
            filter['owner'] = ObjectId(user_id)
            

            if status=='all':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Completed':
                filter['status'] = 'Completed'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Overdue':
                filter['status'] = 'Open'
                yesterday = from_date - datetime.timedelta(days = 1)
                # print (yesterday)
                filter['date'] = {"$lte": yesterday}

            match = filter

            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$project': app_config.followUp_Project},
                            {'$match': match},
                            {'$group' : {
                                             '_id'  : "org_id" ,
                                             'total' : { "$sum": "$org_id" },
                                            'count': {"$sum":1}
                                        }
                            },
                        ]

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
               count = int(item['count'])
            return count
        except NotUniqueError as e:
            return '0'
    
    def getUnnoticedCount(ordId,user_id,from_date):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['deal_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            filter['deal_status']='open'
          
            yesterday = from_date - datetime.timedelta(days = 1)
            filter['modify_date'] = {"$lte": yesterday}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            count = 0
            return_data=defaultdict(list)
            for item in role:
               count = int(item['count'])
               total = int(item['total'])
               return_data['count']=count
               return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'

    def getTodayUnnoticedCount(ordId,user_id,date_from,from_date):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['deal_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            filter['deal_status']='open'
            # stage1 = 'Won,Lost,lost,won'
            # stage = stage1.split(',')
            # filter['stage_name.name'] = {'$nin': stage}
            filter['create_date'] = {"$gte": date_from,"$lte": from_date}
            
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$match': match},
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            count = 0
            return_data = defaultdict(list)
            for item in role:
                if item['count']:
                    count = int(item['count'])
                    total = int(item['total'])
                else:
                    count = 0
                    total = 0              
                return_data['count']=count
                return_data['total']=TokenRefresh.human_format(total)
            return return_data

        except NotUniqueError as e:
            return '0'


    def getDashboardQuotesCount(ordId,user_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            filter['quote_stage_name.name'] = {'$nin': stage}
            match = filter


            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)
            return_data = defaultdict(list)
            role = Quote.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
                if item['count']:
                    count = int(item['count'])
                    total = int(item['total'])
                else:
                    count = 0
                    total = 0              
                return_data['count']=count
                return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'

    def getOverdueCount(ordId,user_id,from_date):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['deal_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            filter['deal_status']='open'
            # stage1 = 'Won,Lost,lost,won'
            # stage = stage1.split(',')
            # filter['stage_name.name'] = {'$nin': stage}
            
            yesterday = from_date - datetime.timedelta(days = 1)
            # print (yesterday)
            filter['target_date'] = {"$lte": yesterday}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            count = 0
            return_data=defaultdict(list)
            for item in role:
               count = int(item['count'])
               total = int(item['total'])
               return_data['count']=count
               return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'


    def getOpenDealsCount(ordId,user_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # filter['assigned_to'] = ObjectId(user_id)
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['deal_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            filter['deal_status']='open'
            # stage1 = 'Won,Lost,lost,won'
            # stage = stage1.split(',')
            # filter['stage_name.name'] = {'$nin': stage}
            
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            return_data=defaultdict(list)
           
            for item in role:
               count = int(item['count'])
               total = int(item['total'])
               return_data['count']=count
               return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'

            
    def getQuoteCreatedThisMonth(ordId,user_id):
        try:
            today=datetime.datetime.now().date()
            start_date = today.replace(day=1)
            month = start_date.month
            year = start_date.year
            if month == 12:
                month = 1
                year += 1
            else:
                month += 1
            next_month_start_date = start_date.replace(month=month, year=year)
            end_date = next_month_start_date - datetime.timedelta(days=1)
            sy = start_date.strftime('%Y')
            sm = start_date.strftime('%m')
            sd = start_date.strftime('%d')
            search_time_from = datetime.datetime(int(sy),int(sm),int(sd),0,0)
            ey = end_date.strftime('%Y')
            em = end_date.strftime('%m')
            ed = end_date.strftime('%d')
            search_time_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
       
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['create_date'] = {"$gte": search_time_from,"$lte": search_time_end}
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['quote_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
            filter['quote_stage']={"$in":other_stage_list}
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)
            return_data = defaultdict(list)
            role = Quote.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
                if item['count']:
                    count = int(item['count'])
                    total = int(item['total'])
                else:
                    count = 0
                    total = 0              
                return_data['count']=count
                return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'


    def getQuoteWonThisMonth(ordId,user_id):
        try:
            today=datetime.datetime.now().date()
            start_date = today.replace(day=1)
            month = start_date.month
            year = start_date.year
            if month == 12:
                month = 1
                year += 1
            else:
                month += 1
            next_month_start_date = start_date.replace(month=month, year=year)
            end_date = next_month_start_date - datetime.timedelta(days=1)
            sy = start_date.strftime('%Y')
            sm = start_date.strftime('%m')
            sd = start_date.strftime('%d')
            search_time_from = datetime.datetime(int(sy),int(sm),int(sd),0,0)
            ey = end_date.strftime('%Y')
            em = end_date.strftime('%m')
            ed = end_date.strftime('%d')
            search_time_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
       
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['create_date'] = {"$gte": search_time_from,"$lte": search_time_end}
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['quote_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            stage=MongoAPI.getWonLostDealStages('deal_stage',ordId,'won')

            filter['quote_stage']=stage
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)
            return_data = defaultdict(list)
            role = Quote.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
                if item['count']:
                    count = int(item['count'])
                    total = int(item['total'])
                else:
                    count = 0
                    total = 0              
                return_data['count']=count
                return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'


    def getQuoteLostThisMonth(ordId,user_id):
        try:
            today=datetime.datetime.now().date()
            start_date = today.replace(day=1)
            month = start_date.month
            year = start_date.year
            if month == 12:
                month = 1
                year += 1
            else:
                month += 1
            next_month_start_date = start_date.replace(month=month, year=year)
            end_date = next_month_start_date - datetime.timedelta(days=1)
            sy = start_date.strftime('%Y')
            sm = start_date.strftime('%m')
            sd = start_date.strftime('%d')
            search_time_from = datetime.datetime(int(sy),int(sm),int(sd),0,0)
            ey = end_date.strftime('%Y')
            em = end_date.strftime('%m')
            ed = end_date.strftime('%d')
            search_time_end = datetime.datetime(int(ey),int(em),int(ed),23,59)
       
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['create_date'] = {"$gte": search_time_from,"$lte": search_time_end}
            UserList=MongoAPI.getReportingUsers(ordId,user_id)
            reporting_user_list=UserList['quote_user_list']
            filter['assigned_to']={"$in":reporting_user_list}
            stage=MongoAPI.getWonLostDealStages('deal_stage',ordId,'lost')
            filter['quote_stage']=stage
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]
            
            settings = defaultdict(list)
            return_data = defaultdict(list)
            role = Quote.objects.aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
                if item['count']:
                    count = int(item['count'])
                    total = int(item['total'])
                else:
                    count = 0
                    total = 0              
                return_data['count']=count
                return_data['total']=TokenRefresh.human_format(total)
            return return_data
        except NotUniqueError as e:
            return '0'
   

    def fieldsSettingsCount(type,org_id):
        try:
            
            count = Fields.objects.filter(type=type,org_id=org_id).count()
        
            return count
        except Fields.DoesNotExist:
            return '0'

    def fieldsSettingsData(type,org_id):
        if type=='timezones':
            try:
                s = Timezones.objects.filter().to_json()
                output = json.loads(s)
                # print (output)
                return output
            except Fields.DoesNotExist:
                return '0'
        elif type=='country':
            try:
                s = Countries.objects.filter().to_json()
                output = json.loads(s)
                return output
            except Fields.DoesNotExist:
                return '0'
            
        else:
            try:
                s = Fields.objects.filter(type=type,org_id=org_id).order_by('sort_order').to_json()
                output = json.loads(s)
                stage_list=[]
                if type=='deal_stage':
                    for ds in output:
                        # print(ds)
                        if ds['name']!='Revised':
                            stage_list.append(ds)
                else:
                    stage_list=output

                return stage_list
            except Fields.DoesNotExist:
                return '0'

    # def fieldsSettingsDataDefault(type,org_id):
    #     try:
    #         s = Fields.objects.filter(type=type,org_id=int(org_id),default=1).to_json()
    #         output = json.loads(s)
    #         print (org_id)
    #         print (type)
    #         print (s)
    #         settings = defaultdict(list)
    #         id = ''
    #         for item in output:
    #             if item=='_id':
    #                 oid = output[item]
    #                 for item1 in oid:
    #                     id=str(oid['$oid'])
    #         return id
    #     except Fields.DoesNotExist:
    #         return '0'


    def fieldsSettingsDataDefault(type,org_id):
        try:
            s = Fields.objects.filter(type=type,org_id=int(org_id),default=1).first().to_json()
            output = json.loads(s)
           
            settings = defaultdict(list)
            id = ''
            for item in output:
                if item=='_id':
                    oid = output[item]
                    for item1 in oid:
                        id=str(oid['$oid'])
            return id
        except Fields.DoesNotExist:
            return '0'


    def fieldsSettingsDataDefaultRevised(type,org_id,name):
        try:
            

            # s = Fields.objects.filter(type=type,name=name,org_id=int(org_id),default=1).to_json()
            # output = json.loads(s)

            pipeline = [
                        {"$match" : {"type" :type,"name" :name,"org_id" :int(org_id)}}
                       ]
            output = Fields.objects.aggregate(*pipeline)

            # output = json.loads(s)

            # print ('org_id')
            # print (output)
            # print (org_id)
            
            

            settings = defaultdict(list)
            id = ''
            for item in output:
                for item1 in item:
                    if item1=='_id':
                        id = item[item1]
                        # print (id)
                        # for item2 in oid:
                        #   id=str(oid['$oid'])
            return id
        except Fields.DoesNotExist:
            return '0'

    def test(email,name,org_id,password):
        try:
            user = User1(email=email,name=name,org_id=org_id,password=password)
            response = user.save()
            return response.id
        except NotUniqueError as e:
            return ''


    def createOrganizationData(email,name):
        try:
            lastDataId = LastDataId.getOrganizationLastDataId('org_id')
            lastDataId = int(lastDataId)+1
            oneyear = datetime.datetime.now() + datetime.timedelta(days=365)
            response = Organization(organization_name=name,email=email,org_id=lastDataId,end_date=oneyear).save()
            id = lastDataId
        except NotUniqueError as e:
            id = '0'
        return id
    def updateOrganizationLogo(org_id,logo):
        try:
            response = Organization.objects(org_id=org_id).update_one(logo=logo)
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id
    def signUpUpdate(organization_name,currency,time_zone,date_format,phone,website,ordId):
        try:

            response = Organization.objects(org_id=ordId).update_one(organization_name=organization_name,currency=currency,time_zone=time_zone,date_format=date_format,phone=phone,website=website)
            id = ordId
        except NotUniqueError as e:
            id = '0'
        return id

    def resetPassword(user_id,password):
        try:
            password_string=str(password)
            response = User.objects(id=user_id).update_one(password=password_string)
            id = user_id
        except NotUniqueError as e:
            id = '0'
        return id

    def activateEmail(org_id):
        try:

            response = Organization.objects(org_id=org_id).update_one(email_activation='completed')
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id
    
    def updateUserLogo(id,profile_image):
        try:
            response = User.objects(id=id).update_one(profile_image=profile_image)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def getProfile(id):
        # try:
        #     s = User.objects.filter(id=id).to_json()
        #     output = json.loads(s)
           
        # except NotUniqueError as e:
        #     output = ''
        # return output
        try:
            filter = {}
            # filter['org_id'] = ordId   
            filter['_id'] = ObjectId(id)
            key = []
            val = []
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [    {'$lookup' : app_config.role_lookup},
                            {'$match': match}
                        ]
            settings = defaultdict(list)
            # print (pipeline)
            role = User.objects.aggregate(*pipeline)
            settings6=[]
            item4 = role
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='role':
                        assigned = item[item1]
                        settings['role']=str(assigned)
                    if item1=='_id':
                        item3 = item[item1]
                        settings['_id']=str(item3)
                    if item1=='user_id':
                        item3 = item[item1]
                        settings['user_id']=str(item3)
                        # for items in item3:
                        #     settings['_id']=str(items['$oid'])
                     
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}            
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return 'No'

    def profileUpdate(id,name,phone):
        try:
            response = User.objects(id=id).update_one(name=name,phone=phone)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def updateOrganization(org_id,company_name,phone,email,website):
        try:
            response = Organization.objects(org_id=org_id).update_one(organization_name=company_name,phone=phone,email=email,website=website)
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id
    def updateOrganizationData(org_id,address,address2,city,state,pcode,country,gstin,pan_no):
        try:
            response = Organization.objects(org_id=org_id).update_one(address=address,address2=address2,city=city,state=state,pcode=pcode,country=country,gstin=gstin,pan_no=pan_no)
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id
    
    def organizationUpdateData(org_id,currency,business_days,time_zone,start_time,date_format,end_time):
        try:
            response = Organization.objects(org_id=org_id).update_one(currency=currency,business_days=business_days,time_zone=time_zone,start_time=start_time,date_format=date_format,end_time=end_time)
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id

    def organizationInfo(org_id):
        try:
            s = Organization.objects.filter(org_id=org_id).to_json()
            output = json.loads(s)
            output = Uid.fix_array(output)
        except User.DoesNotExist:
            output = 'no such name'        
        return output

    
    def userCheck(email):
        try:
            s = User.objects.get(email=email)
            if s.email:
                output = 'Yes'
            else:
                output = 'No'
                
        except User.DoesNotExist:
            output = 'No'   

        return output

    
    
    def getUserDetails(ordId,id):

        try:
            filter = {}
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            key = []
            val = []
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$match': match}
                        ]
            settings = defaultdict(list)
            # print (pipeline)
            role = User.objects.aggregate(*pipeline)
            settings6=[]
            item4 = role
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='role':
                        assigned = item[item1]
                        settings['role']=str(assigned)
                     
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}            
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return 'No'

    def getUserPasswordDetails(user_id,password):
        try:
            s = User.objects.filter(password=password,id=ObjectId(user_id)).to_json()
            output = json.loads(s)
        except User.DoesNotExist:
            output = 'No'   

        return output

    def userUpdate(email,name,phone,id,role,report_to):
        try:
            modify_date = datetime.datetime.utcnow
            user_id = id
            user = User.objects(id=id).update_one(email=email,name=name,phone=phone,role=role,report_to=report_to)
            delete_report_to =  Reporting_To.objects(user_id=id).delete()
            for report in report_to:
                insert_reporting = Reporting_To(user_id=id,reporting_to=report)
                insert_reporting_save = insert_reporting.save()
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output

    def userRoleUpdate(id,role):
        try:
            modify_date = datetime.datetime.utcnow
            # print (id)
            user_id = ObjectId(id)
            role = ObjectId(role)
            user = User.objects(id=id).update_one(role=role)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output

    def userAdministratorRole(ordId):

        try:
            # password = generate_password_hash(password)
            # print (password)
            role_name = 'Administrator'
            s = Role.objects.get(org_id=int(ordId),role_name='Administrator')
            id12 = json.loads(json_util.dumps(s))
            id = str(s.id)
            output1 = id
            output = output1
        except Role.DoesNotExist:
            output = 0       
        return output

        
    
    def regionalSettingsUpdate(id,currency,time_zone,date_format):
        try:
            modify_date = datetime.datetime.utcnow
            
 
            user = User.objects(id=id).update_one(currency=currency,time_zone=time_zone,date_format=date_format,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output

    def mailSignatureUpdate(id,mail_signature):
        try:
            modify_date = datetime.datetime.utcnow
            user = User.objects(id=id).update_one(mail_signature=mail_signature,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output


    
    def changePassword(user_id,password):
        try:
            modify_date = datetime.datetime.utcnow
            user = User.objects(id=user_id).update_one(password=password)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   

        return output  
   
    def userChangeStatus(status,id):
        try:
            modify_date = datetime.datetime.utcnow
            user_id = id
            user = User.objects(id=id).update_one(status=status)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   

        return output

    def removePricelistCompany(id):
        try:
            modify_date = datetime.datetime.utcnow
            company_id = id
            user = Account.objects(id=ObjectId(id)).update_one(pricelist='')
            output = 'Yes'
        except Account.DoesNotExist:
            output = 'No'   

        return output

    def changeStatus(status,data):
        try:
            modify_date = datetime.datetime.utcnow
            id = data['associate_id']
            status = data['status']

            # print(id)
            # print(status)

            if data['associate_to']=='quote':
                Quote.objects(id=id).update_one(quote_stage=status,modify_date=modify_date)
            elif data['associate_to']=='proforma':
                Proforma.objects(id=id).update_one(proforma_stage=status,modify_date=modify_date)
            elif data['associate_to']=='invoice':
                Invoice.objects(id=id).update_one(invoice_stage=status,modify_date=modify_date)
            elif data['associate_to']=='deal':
                Deal.objects(id=id).update_one(stage=status,modify_date=modify_date)
            elif data['associate_to']=='product':
                Product.objects(id=id).update_one(status=status,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   

        return output

    


    def countriesList(id):
        try:
            user_id = id
            status = 'active'
            s = Countries.objects.filter().to_json()
            output = json.loads(s)
        except Countries.DoesNotExist:
            output = 'No'   

        return output


    def getCountryANDStateId(name,type):
        
        id = ''
        if name!='':
            if(type=='country'):
                try:
                    s = Countries.objects.get(name=name)
                    settings = defaultdict(list)
                    for item in s:
                        if item=='id':
                            id = s['id']
                except Countries.DoesNotExist:
                    id = ''
            elif(type=='state'):
                try:
                    if name=='':
                        s = States.objects.get(name=name)
                        settings = defaultdict(list)
                        for item in s:
                            if item=='id':
                                id = s['id']
                    else:
                        id = ''
                except States.DoesNotExist:
                    id = ''
            return id
        else:
            return ''
            
    def usersList(org_id):
        try:
            s = User.objects.filter(org_id=org_id).to_json()
            output = json.loads(s)
        except Countries.DoesNotExist:
            output = 'No'   

        return output

    def addOnList(org_id):
        try:
            s = OrgAddon.objects.filter(org_id=org_id,status='active').to_json()
            output = json.loads(s)
            addon_list=[]
            for addon in output:
                addon_list.append(addon['addon'])
            output = addon_list
        except OrgAddon.DoesNotExist:
            output = []   
        return output
    
    def statesList(user_id,id):
        try:
            user_id = id
            s = States.objects.filter(country_id=id).to_json()
            output = json.loads(s)
        except States.DoesNotExist:
            output = 'No'   

        return output

    def countriesNameList(name):
        try:
            user_id = id
            status = 'active'
            s = Countries.objects.filter(name=name).to_json()
            output = json.loads(s)
        except Countries.DoesNotExist:
            output = 'No'   

        return output
        
    def statesNameList(name):
        try:
            user_id = id
            status = 'active'
            s = States.objects.filter(name=name).to_json()
            output = json.loads(s)
        except Countries.DoesNotExist:
            output = 'No'   

        return output


    def numberingSettingsData(id):
        try:
            user_id = id
            s = Numbering.objects.filter(org_id=id).to_json()
            output = json.loads(s)
        except Numbering.DoesNotExist:
            output = 'No'   

        return output

    def modulesList(id):
        try:
            pipeline = [
                        {"$match" : {"module_name_id" :id}}
                       ]

            user_id = id
            cursor = Module_name.objects.aggregate(*pipeline)
            # for batch in cursor:
            output = json.loads(cursor)
        except States.DoesNotExist:
            output = 'No'   

        return output
    

    # def getDeals(ordId,length,page,search_string):
    #     try:
    #         sData = int(page)*int(length)
            
    #         length1 = int(page)-1
    #         sData1 = int(length)*int(length1) 
    #         s = Deal.objects.filter(org_id=ordId,deal_name__contains=search_string).limit(length).skip(sData1).to_json()
    #         output = json.loads(s)
    #     except States.DoesNotExist:
    #         output = 'No'   

    #     return output



    def citiesList(user_id,id):
        try:
            user_id = id
            s = Cities.objects.filter(state_id=id).to_json()
            output = json.loads(s)
        except States.DoesNotExist:
            output = 'No'   

        return output

    def selectedCitiesList(org_id,module):
        try:
            if module=='contact':
                s = Contact.objects.filter(org_id=org_id).to_json()
            elif module=='company':
                s = Account.objects.filter(org_id=org_id).to_json()
           
            output1 = json.loads(s)
            city_list=[]
            for dt in output1:
                if dt['city']!='':
                    city_list.append(dt['city'])
            res = []
            duplicate_check=[]
            for i in city_list:
                if i not in duplicate_check:
                    duplicate_check.append(i)
                    tt = dict()
                    tt['_id']=i
                    tt['name']=i
                    res.append(tt)
            output = res
        except Contact.DoesNotExist:
            output = 'No'   

        return output

    def selectedStatesList(org_id,module):
        try:
            if module=='contact':
                s = Contact.objects.filter(org_id=org_id).to_json()
            elif module=='company':
                s = Account.objects.filter(org_id=org_id).to_json()
           
            output1 = json.loads(s)
            city_list=[]
            for dt in output1:
                if dt['state']!='':
                    city_list.append(dt['state'])
            res = []
            duplicate_check=[]
            for i in city_list:
                if i not in duplicate_check:
                    duplicate_check.append(i)
                    tt = dict()
                    tt['_id']=i
                    tt['name']=i
                    res.append(tt)
            output = res
        except Contact.DoesNotExist:
            output = 'No'   

        return output

    def selectedCountriesList(org_id,module):
        try:
            if module=='contact':
                s = Contact.objects.filter(org_id=org_id).to_json()
            elif module=='company':
                s = Account.objects.filter(org_id=org_id).to_json()
           
            output1 = json.loads(s)
            city_list=[]
            for dt in output1:
                if dt['country']!='':
                    city_list.append(dt['country'])
            res = []
            duplicate_check=[]
            for i in city_list:
                if i not in duplicate_check:
                    duplicate_check.append(i)
                    tt = dict()
                    tt['_id']=i
                    tt['name']=i
                    res.append(tt)
            output = res
        except Contact.DoesNotExist:
            output = 'No'   

        return output

    def getAccountName(org_id,id):
        try:
            s = Account.objects.filter(id=id).to_json()
            account = json.loads(s)
            account = Uid.fix_array(account)
            # print (account)
            company_name = ''
            for contact2 in account:
                for contact3 in contact2:
                    if contact3=='company_name':
                        company_name = contact2[contact3]
            output = company_name
            
        except Account.DoesNotExist:
            output = ''   

        return output

    def getUserName(org_id,id):
        try:
            s = Fields.objects.filter(id=id).to_json()
            fields = json.loads(s)
            fields = Uid.fix_array(fields)
            # print (account)
            name = ''
            for contact2 in fields:
                for contact3 in contact2:
                    if contact3=='name':
                        name = contact2[contact3]
            output = name
            
        except Fields.DoesNotExist:
            output = ''   

        return output

    def getFieldsName(org_id,id):
        try:
            s = Fields.objects.filter(id=id).to_json()
            account = json.loads(s)
            account = Uid.fix_array(account)
            # print (account)
            name = ''
            for contact2 in account:
                for contact3 in contact2:
                    if contact3=='name':
                        name = contact2[contact3]
            output = name
            
        except Fields.DoesNotExist:
            output = ''   

        return output

    def getContacts2(ordId,length,page,search_string,owner,source,tag,date_from,date_to):
        try:
            sData = int(page)*int(length)
            
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            filter['org_id'] = ordId
            if search_string :
                filter1['name__contains'] = search_string
                # filter['name__contains'] = search_string
            if owner :
                filter['owner'] = ObjectId(owner)
            if source:
                filter['source'] = ObjectId(source)
            if tag:
                filter['tag'] = tag
            if date_from:

                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')

                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
                
                filter['create_date__gte'] = search_time
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')

                nextday=datetime.datetime(int(y),int(m),int(d),23,59)

                filter['create_date__lte'] = nextday
            
            # print (filter)

            s = Contact.objects(**filter1).limit(length).skip(sData1).to_json()
            output = json.loads(s)

        except States.DoesNotExist:
            output = 'No'   

        return output

    # def getAccounts(ordId,length,page,search_string):
    #     try:
    #         sData = int(page)*int(length)
            
    #         length1 = int(page)-1
    #         sData1 = int(length)*int(length1) 
    #         s = Account.objects.filter(org_id=ordId,company_name__contains=search_string).limit(length).skip(sData1).to_json()
    #         output = json.loads(s)
    #     except States.DoesNotExist:
    #         output = 'No'   

    #     return output


     


    
    def getAccounts(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,sort_by,current_user,city,state,country):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search_string :
            #     filter1['company_name__contains'] = search_string
                # filter['company_name__contains'] = search_string
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['company_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            if source:
                filter['source'] = ObjectId(source)
            if customer_type:
                filter['customer_type'] = ObjectId(customer_type)
            if city:
                filter['city'] = city
            if state:
                filter['state'] = state
            if country:
                filter['country'] = country
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)

            

            role = Account.objects(**filter1).aggregate(*pipeline)

            # print (role)



            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']

                    elif item1=='pricelist':
                        if item[item1]!='':
                             pricelist = MongoAPI.getPricelist(item[item1])
                             settings['pricelist_name'] = pricelist[0]['title']
                    # elif item1=='customer_type':
                    #     item3 = item[item1]
                    #     if item3:
                    #         print (item3)
                    #         for item2 in item3:
                    #             settings[item]=str(item2['$oid'])
                        # else:
                        #     settings[item]
                    # elif item1=='source':
                    #     item3 = item[item1]
                    #     for item1 in item3:
                    #         settings[item]=str(item3['$oid'])
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                            
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['customer_type_name']:
                        settings['customer_type_name']=''
                    if not settings['customer_type']:
                        settings['customer_type']=''
                    if not settings['contacts']:
                        contacts = MongoAPI.getAccountContactsList(ordId,settings['_id'])

                        response = json.loads(json_util.dumps(contacts))
                        response1 = Uid.fix_array(response)

                        settings['contacts'] = response1 
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getAccountContactsList(ordId,company_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['company_id'] = company_id
            match = filter
       
            pipeline = [
                        {'$match': match},
                        {'$project': app_config.account_contact_project}
                    ]
            settings = defaultdict(list)
            role = Contact.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                # settings = defaultdict(list)
                for item1 in item:
                    # print(item1)
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='_id':
                        # print (item[item1])
                        # assigned = item[item1]
                        # # assigned = item['_id']
                        # for item2 in assigned:
                        settings['_id'] = item[item1]
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getAccountsList(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user,city,state,country):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search_string :
            #     filter1['company_name__contains'] = search_string
                # filter['company_name__contains'] = search_string
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['company_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if city:
                filter['city'] = city
            if state:
                filter['state'] = state
            if country:
                filter['country'] = country
            if source:
                filter['source'] = ObjectId(source)
            if customer_type:
                filter['customer_type'] = ObjectId(customer_type)
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)

            

            role = Account.objects(**filter1).aggregate(*pipeline)

            # print (role)



            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                        settings['next_followup']=MongoAPI.getRecentFollowUp(ordId,assigned,'company')
                        settings['keycontact']=MongoAPI.getCompanyKeyContact(ordId,assigned)
                        settings['recentcontact']=MongoAPI.getCompanyRecentContacts(ordId,assigned)
                       
                                       
                    # elif item1=='customer_type':
                    #     item3 = item[item1]
                    #     if item3:
                    #         print (item3)
                    #         for item2 in item3:
                    #             settings[item]=str(item2['$oid'])
                        # else:
                        #     settings[item]
                    # elif item1=='source':
                    #     item3 = item[item1]
                    #     for item1 in item3:
                    #         settings[item]=str(item3['$oid'])
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    
                                
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['customer_type_name']:
                        settings['customer_type_name']=''
                    if not settings['customer_type']:
                        settings['customer_type']=''

                    
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    
    def pricelistCompaniesList(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['company_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            filter['pricelist'] = pricelist_id
            if source:
                filter['source'] = ObjectId(source)
            if customer_type:
                filter['customer_type'] = ObjectId(customer_type)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)
            role = Account.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                  
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getAccountsDetails(org_id,id):
        try:
            s = Account.objects.filter(id=ObjectId(id)).to_json()
            output = json.loads(s)
        except States.DoesNotExist:
            output = 'No'   

        return output


    def getAccountNameSearch(ordId,search_string):
        try:
           
            s = Account.objects.filter(org_id=ordId,company_name__contains=search_string).to_json()
            output = json.loads(s)
            # print (output)
        except States.DoesNotExist:
            output = 'No'   

        return output

    
    def user_activity(ordId,user_id,action,associate_to,associate_id,via,extra_info,text_info,title):
        try:
            u2 = Activity(org_id=ordId,user_id=user_id,action=action,associate_to=associate_to,associate_id=associate_id,via=via,extra_info=str(extra_info),title=str(title),text_info=text_info)
            u2.save()
            output1 = '1'
            return output1
        except States.DoesNotExist:
            output = 'No'   

        return output

    

    
    def renameSubmit(org_id):
        try:

            data1 = defaultdict(list)

            data1['org_id'] = int(org_id)
            data1['contact_singular'] = 'Contact'
            data1['contact_plural'] = 'Contacts'
            data1['company_singular'] = 'Company'
            data1['company_plural'] = 'Companies'
            data1['deal_singular'] = 'Deal'
            data1['deal_plural'] = 'Deals'            
            u2 = Rename.from_json(json.dumps(data1))
            u2.save()
            output1 = str(u2.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def getRenameDetails(org_id):
        try:
            
            filter = {}
            filter['org_id'] = org_id
            match = filter
            pipeline = [
                            {'$match': match},
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Rename.objects.aggregate(*pipeline)
            settings6=[]
            
            item4 = role
            
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}

            
            settings2 = json.loads(json_util.dumps(settings))
            # print (json.loads(settings2))
            return settings2
        except NotUniqueError as e:
            return '0'


    def companyNewSubmit(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)
            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]

            # id = TokenRefresh.idData()
            # print(settings)
            # print(data1) 
            
            u2 = Account.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'



    def contactUpdate(org_id,user_id,data1,id):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['name']:
                data5['name'] = str(data1['name'])
            if data1['company_id']:
                data5['company_id'] = ObjectId(str(data1['company_id']))
            if data1['company_name']:
                data5['company_name'] = str(data1['company_name'])
            if data1['phone_number']:
                data5['phone_number']=data1['phone_number']
            if data1['email']:
                data5['email']=data1['email']
            if data1['description']:
                data5['description']=data1['description']
            if data1['assigned_to']:
                data5['assigned_to']=ObjectId(str(data1['assigned_to']))
            if data1['source']:
                data5['source']=ObjectId(str(data1['source']))
            if data1['whatsapp_no']:
                data5['whatsapp_no']=data1['whatsapp_no']
            if data1['secondary_email']:
                data5['secondary_email']=data1['secondary_email']
            if data1['designation']:
                data5['designation']=data1['designation']
            if data1['website']:
                data5['website']=data1['website']
            if data1['facebook']:
                data5['facebook']=data1['facebook']
            if data1['linkedin']:
                data5['linkedin']=data1['linkedin']
            if data1['twitter']:
                data5['twitter']=data1['twitter']
            if data1['skype']:
                data5['skype']=data1['skype']
            if data1['address1']:
                data5['address1']=data1['address1']
            if data1['address2']:
                data5['address2']=data1['address2']
            if data1['country']:
                data5['country']=data1['country']
            if data1['state']:
                data5['state']=data1['state']
            if data1['city']:
                data5['city']=data1['city']
            if data1['pincode']:
                data5['pincode']=int(data1['pincode'])
            if data1['custom_fields']:
                data5['custom_fields']=data1['custom_fields']
            data5['modify_date'] = datetime.datetime.utcnow

            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Contact.objects(id=ObjectId(id)).update_one(**data3)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 

    def setKeyContact(org_id,company_id,id):
        try:
            data5 = defaultdict(list)
            data5['key_contact'] = 0
            Contact.objects(company_id=ObjectId(company_id)).update(**data5)


            data6 = defaultdict(list)
            data6['key_contact'] = 1
            Contact.objects(id=ObjectId(id)).update_one(**data6)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 


    def contactSubmit(org_id,user_id,data1,company_id,pincode,source):
        try:
            shopping_list = []
           
            for data in data1:
                shopping_list.append({data:data1[data]})
            id = TokenRefresh.idData()
            data1['owner'] = user_id
            data1['org_id'] = org_id
            data1['contact_id'] = id
            data1['company_id'] = company_id
            if pincode:
                data1['pincode'] = pincode
            else:
                del data1['pincode']
            if source!='':

                data1['source'] = ObjectId(source)
            else:
                del data1['source']
            # print (data1)
            # user = Contact(name=data1['name'],company_name=data1['company_name'],owner=user_id,org_id=data1['org_id'],contact_id=data1['contact_id'],company_id=data1['company_id'],email=data1['email'],description=data1['description'],assigned_to=data1['assigned_to'],source=source,whatsapp_no=data1['whatsapp_no'],secondary_email=data1['secondary_email'],designation=data1['designation'],facebook=data1['facebook'],linkedin=data1['linkedin'],twitter=data1['twitter'],skype=data1['skype'],website=data1['website'],address1=data1['address1'],address2=data1['address2'],country=data1['country'],state=data1['state'],city=data1['city'],pincode=pincode, phone_number=data1['phone_number'])
            user = Contact(**data1)
            response = user.save()
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def companySubmit(org_id,user_id,data1,pincode):
        try:
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['account_id'] = Uid.generateUUID()
            if pincode:
                data1['pincode'] = pincode
            else:
                del data1['pincode']
            # data1['assigned_to'] = ObjectId(data1['assigned_to'])
            # data1['customer_type'] = ObjectId(data1['customer_type'])
            # print (data1)
            u2 = Account.from_json(json.dumps(data1))
            u2.save()
            output1 = str(u2.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def companyUpdate(org_id,user_id,data1,id):
        try:


            data1['modify_date'] = datetime.datetime.utcnow   
            data5 = defaultdict(list)

            if data1['company_name']:
                data5['company_name'] = str(data1['company_name'])
            if data1['modify_date']:
                data5['modify_date'] = data1['modify_date']
            if data1['phone']:
                data5['phone']=data1['phone']
            if data1['email']:
                data5['email']=data1['email']
            # if data1['assigned_to']:
            #     data5['assigned_to']=ObjectId(str(data1['assigned_to']))
            if data1['customer_type']:
                data5['customer_type']=ObjectId(str(data1['customer_type']))
            if data1['gstin']:
                data5['gstin']=str(data1['gstin'])
            if data1['website']:
                data5['website']=data1['website']
            if data1['source']:
                data5['source']=ObjectId(str(data1['source']))
            if data1['address1']:
                data5['address1']=data1['address1']
            if data1['address2']:
                data5['address2']=data1['address2']
            if data1['country']:
                data5['country']=data1['country']
            if data1['state']:
                data5['state']=data1['state']
            if data1['city']:
                data5['city']=data1['city']
            if data1['pincode']:
                data5['pincode']=int(data1['pincode'])
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']
            if "pricelist" in data1:
                data5['pricelist']=data1['pricelist']

            if data1['currency']:
                data5['currency']=data1['currency']


            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]
            Account.objects(id=ObjectId(id)).update_one(
                **data3
                )
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'



    def getContacts1(ordId,length,page,search_string,owner,source,tag,date_from,date_to):
        try:
            sData = int(page)*int(length)
            
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            filter['org_id'] = ordId
            if search_string :
                filter1['name__contains'] = search_string
                # filter['name__contains'] = search_string
            if owner :
                filter['owner'] = ObjectId(owner)
            if source:
                filter['source'] = ObjectId(source)
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')

                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
                
                filter['create_date__gte'] = search_time
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')

                nextday=datetime.datetime(int(y),int(m),int(d),23,59)

                filter['create_date__lte'] = nextday
            
            # print (filter)

            s = Contact.objects(**filter).limit(length).skip(sData1).to_json()
            output = json.loads(s)

        except States.DoesNotExist:
            output = 'No'   

        return output
    
    def getContactDetails(org_id,id):
        try:
            
            filter = {}
            filter['org_id'] = org_id
            filter['_id'] = ObjectId(id)
            
            key = []
            val = []

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                            {'$lookup': app_config.user_lookup},
                            {'$lookup': app_config.account_lookup},
                            {'$lookup': app_config.fields_lookup},
                            {'$lookup': app_config.countriesLookup},
                            {'$lookup': app_config.statesLookup},
                            {'$match': match},
                            { '$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Contact.objects.aggregate(*pipeline)
            settings6=[]
            
            item4 = role
            
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            
                            settings['company_name'] = item2['company_name']
                            
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['company_name']:
                        settings['company_name']=''

            # print (settings)



        
                    
            
            settings2 = json.loads(json_util.dumps(settings))
            # print (json.loads(settings2))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getCompanyDetails(org_id,id):
        try:
            
            filter = {}
            filter['org_id'] = org_id
            filter['_id'] = ObjectId(id)
            
            key = []
            val = []

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                            {'$lookup': app_config.user1_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup': app_config.fields_lookup},
                            {'$lookup': app_config.countriesLookup},
                            {'$lookup': app_config.statesLookup},
                            # {'$lookup': app_config.pricelistLookup},
                            {'$match': match},
                            { '$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Account.objects.aggregate(*pipeline)
            settings6=[]
            
            item4 = role

            # print (role)
            
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    elif item1=='pricelist':
                        if item[item1]!='':
                             pricelist = MongoAPI.getPricelist(item[item1])
                             settings['pricelist_name'] = pricelist[0]['title']

                       
                        # assigned = item[item1]
                        
                        # for item2 in assigned:
                        #     settings['pricelist_name'] = item2['title']
                    if item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['customer_type_name']:
                        settings['customer_type_name']=''


            settings2 = json.loads(json_util.dumps(settings))
            # print (json.loads(settings2))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getCompanyContacts(ordId,id):
        try:
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['company_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {'create_date' : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            role = Contact.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getCompanyKeyContact(ordId,id):
        try:
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['company_id'] = ObjectId(id)
            filter['key_contact'] = 1
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            # {'$sort'   : {'create_date' : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            role = Contact.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDefault(ordId,field_type):
        try:
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['default'] = 1
            if field_type!='all':
                filter['type'] = field_type
           
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [

                            # {'$lookup' : app_config.fields_lookup},
                            # {'$lookup' : app_config.countriesLookup},
                            # {'$lookup' : app_config.statesLookup},
                            # {'$sort'   : {'create_date' : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.fields_project}
                        ]
            
            settings = defaultdict(list)

            role = Fields.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getAdminEmailTemplate(template_name):
        try:
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            filter = {}
            key = []
            val = []
            filter['template_name'] = template_name
          
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [

                            {'$match'  : match},
                        ]
            
            settings = defaultdict(list)

            role = AdminEmailTemplate.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    
    def getCompanyRecentContacts(ordId,id):
        try:
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['company_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            limit = 1

            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {'create_date' : order_dict}},
                            {'$match'  : match},
                            {'$limit'  : limit},
                            {'$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            role = Contact.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getContacts(ordId,length,page,search_string,owner,source,tag,date_from,date_to,sort_by,order,current_user,city,state,country):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            #if search_string :
                #filter1['name__contains'] = search_string
                # filter['name__contains'] = search_string
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['contact_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if source:
                filter['source'] = ObjectId(source)
            if tag:
                filter['tag'] = tag
            if city:
                filter['city'] = city
            if state:
                filter['state'] = state
            if country:
                filter['country'] = country
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort_by : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'phone_number': { "$regex": search_string, "$options" :'i'}}, 
                                {'name': { "$regex": search_string, "$options" :'i'}},
                                {'email': { "$regex": search_string, "$options" :'i'}},
                                ],
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Contact.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    def getCompanyContacts(ordId,company_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['company_id'] = ObjectId(company_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter


            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$match'  : match},
                            {'$project': app_config.contact_project}
                        ]
            
            settings = defaultdict(list)

            # print (pipeline)

            role = Contact.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getTimeline(ordId,id,associate_to):
        try:        
            filter = {}
            key = []
            val = []
            # filter['org_id'] = ordId
            filter['associate_id'] = ObjectId(id)
            filter['associate_to'] = associate_to

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.timelineLookup},
                            {'$match': match},
                            {'$project': app_config.activity_Project}
                        ]

            settings = defaultdict(list)

            role = Activity.objects.aggregate(*pipeline)
            # print (role)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))

            
            return settings2
        except NotUniqueError as e:
            return '0'



    def delete(org_id,current_user,id,associate_to):

        if associate_to=='contact':
            try:
                obj = Contact.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='company':
            try:
                obj = Account.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='deal':
            try:
                obj = Deal.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='quote':
            try:
                obj = Quote.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='proforma':
            try:
                obj = Proforma.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='invoice':
            try:
                obj = Invoice.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='product':
            try:
                obj = Product.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='custom_fields':
            try:
                obj = CustomFields.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        else:
            return '0'

    def clearDemoData(org_id):

        try:
                obj1 = Contact.objects(demo_data='true',org_id=org_id)
                obj1.delete()
                obj2 = Account.objects(demo_data='true',org_id=org_id)
                obj2.delete()
                obj3 = Deal.objects(demo_data='true',org_id=org_id)
                obj3.delete()
                obj4 = Product.objects(demo_data='true',org_id=org_id)
                obj4.delete()
                obj5 = Quote.objects(demo_data='true',org_id=org_id)
                obj5.delete()
                obj6 = Proforma.objects(demo_data='true',org_id=org_id)
                obj6.delete()
                obj7 = Invoice.objects(demo_data='true',org_id=org_id)
                obj7.delete()
                Organization.objects(org_id=org_id).update_one(demo_data_set='1')
                return '1'
        except NotUniqueError as e:
                return '0'

    

    def reassign(org_id,current_user,id,associate_to):

        if associate_to=='contact':
            try:
                obj = Contact.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='company':
            try:
                obj = Account.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='deal':
            try:
                obj = Deal.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
        elif associate_to=='quote':
            try:
                obj = Quote.objects(id=id)
                obj.delete()
                return '1'
            except NotUniqueError as e:
                return '0'
       
        else:
            return '0'

    def getReporting(user_id):
        try:        
            filter = {}
            key = []
            val = []
            filter['reporting_to'] = ObjectId(user_id)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$match': match},
                        ]

            settings = defaultdict(list)

            role = Reporting_To.objects.aggregate(*pipeline)
            # print (role)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                settings1=[]
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='user_id':
                        assigned = item[item1]
                        settings1.append(assigned)

                settings6.append(settings1)
            settings2 = json.loads(json_util.dumps(settings6))
            
            return settings2
        except NotUniqueError as e:
            return '0'

    def getReportingUsers(ordId,current_user):
        userData = MongoAPI.getUserDetails(ordId,current_user)
        role = MongoAPI.getRolesListDetails(ordId,userData['role'])
        roleData = Uid.fix_array_role(role)
        roleDetail = json.loads(json_util.dumps(roleData))
        # contact_user_list=[]
        # if roleDetail['contact_view_all'] == 1:
        #     contact_user_list=[]
        #     reportingUsersData=MongoAPI.getReporting(current_user)
        #     for report_user in reportingUsersData:
        #         this_user=str(report_user[0]['$oid'])
        #         contact_user_list.append(ObjectId(this_user))
        # elif roleDetail['contact_view_own'] == 1:
        #     contact_user_list=[]
        #     contact_user_list.append(ObjectId(current_user))
        # else:
        #     contact_user_list=[]
        # if roleDetail['contact_view_all']=='1':
        #     reporting_user_list=[]
        #     reportingUsersData=MongoAPI.getReporting(current_user)
        #     # reportingUsers=Uid.fix_array3(reportingUsersData)
        #     for report_user in reportingUsersData:
        #         # print(report_user)
        #         this_user=str(report_user[0]['$oid'])
        #         # print(this_user)
        #         reporting_user_list.append(ObjectId(this_user))
        #     reporting_user_list.append(ObjectId(current_user))
        # elif roleDetail['contact_view_own']=='1':
        #     reporting_user_list=[]
        #     reporting_user_list.append(ObjectId(current_user))

        # else:
        #     reporting_user_list=[]
  
        if roleDetail['role_name']=='Administrator':
            user_list=[]
            contact_user_list=[]
            company_user_list=[]
            deal_user_list=[]
            invoice_user_list=[]
            product_user_list=[]
            proforma_user_list=[]
            quote_user_list=[]
            reportingUsersData=MongoAPI.usersList(ordId)
            reportingUsersData=Uid.fix_array(reportingUsersData)
            for report_user in reportingUsersData:
                this_user=str(report_user['_id'])
                user_list.append(ObjectId(this_user))
            user_list.append(ObjectId(current_user))
            contact_user_list=user_list
            company_user_list=user_list
            deal_user_list=user_list
            invoice_user_list=user_list
            product_user_list=user_list
            proforma_user_list=user_list
            quote_user_list=user_list
        else:

            if roleDetail['contact_view_all']=='1':
                contact_user_list=[]
                reportingUsersData=MongoAPI.getReporting(current_user)
                for report_user in reportingUsersData:
                    this_user=str(report_user[0]['$oid'])
                    contact_user_list.append(ObjectId(this_user))
                contact_user_list.append(ObjectId(current_user))
            elif roleDetail['contact_view_own']=='1':
                contact_user_list=[]
                contact_user_list.append(ObjectId(current_user))

            else:
                contact_user_list=[]

            
            if roleDetail['company_view_all']=='1':
                company_user_list=[]
                reportingUsersData=MongoAPI.getReporting(current_user)
                # reportingUsers=Uid.fix_array3(reportingUsersData)
                for report_user in reportingUsersData:
                    # print(report_user)
                    this_user=str(report_user[0]['$oid'])
                    # print(this_user)
                    company_user_list.append(ObjectId(this_user))
                company_user_list.append(ObjectId(current_user))
            elif roleDetail['company_view_own']=='1':
                company_user_list=[]
                company_user_list.append(ObjectId(current_user))

            else:
                company_user_list=[]

            
            if roleDetail['deal_view_all']=='1':
                deal_user_list=[]
                reportingUsersData=MongoAPI.getReporting(current_user)
                # reportingUsers=Uid.fix_array3(reportingUsersData)
                for report_user in reportingUsersData:
                    # print(report_user)
                    this_user=str(report_user[0]['$oid'])
                    # print(this_user)
                    deal_user_list.append(ObjectId(this_user))
                deal_user_list.append(ObjectId(current_user))
            elif roleDetail['deal_view_own']=='1':
                deal_user_list=[]
                deal_user_list.append(ObjectId(current_user))

            else:
                deal_user_list=[]

            if roleDetail['invoice_view_all']:
                if roleDetail['invoice_view_all']=='1':
                    invoice_user_list=[]
                    reportingUsersData=MongoAPI.getReporting(current_user)
                
                    for report_user in reportingUsersData:
            
                        this_user=str(report_user[0]['$oid'])
                
                        invoice_user_list.append(ObjectId(this_user))
                    invoice_user_list.append(ObjectId(current_user))
                elif roleDetail['invoice_view_own']=='1':
                    invoice_user_list=[]
                    invoice_user_list.append(ObjectId(current_user))

                else:
                    invoice_user_list=[]
                

            if roleDetail['invoice_view_all']:
                if roleDetail['invoice_view_all']=='1':
                    product_user_list=[]
                    reportingUsersData=MongoAPI.getReporting(current_user)
            
                    for report_user in reportingUsersData:
                
                        this_user=str(report_user[0]['$oid'])
                    
                        product_user_list.append(ObjectId(this_user))
                    product_user_list.append(ObjectId(current_user))
                elif roleDetail['invoice_view_own']=='1':
                    product_user_list=[]
                    product_user_list.append(ObjectId(current_user))

                else:
                    product_user_list=[]

            
            if roleDetail['proforma_view_all']=='1':
                proforma_user_list=[]
                reportingUsersData=MongoAPI.getReporting(current_user)
                # reportingUsers=Uid.fix_array3(reportingUsersData)
                for report_user in reportingUsersData:
                    # print(report_user)
                    this_user=str(report_user[0]['$oid'])
                    # print(this_user)
                    proforma_user_list.append(ObjectId(this_user))
                proforma_user_list.append(ObjectId(current_user))
            elif roleDetail['proforma_view_own']=='1':
                proforma_user_list=[]
                proforma_user_list.append(ObjectId(current_user))

            else:
                proforma_user_list=[]

            
            if roleDetail['proforma_view_all']=='1':
                quote_user_list=[]
                reportingUsersData=MongoAPI.getReporting(current_user)
                # reportingUsers=Uid.fix_array3(reportingUsersData)
                for report_user in reportingUsersData:
                    # print(report_user)
                    this_user=str(report_user[0]['$oid'])
                    # print(this_user)
                    quote_user_list.append(ObjectId(this_user))
                quote_user_list.append(ObjectId(current_user))
            elif roleDetail['proforma_view_own']=='1':
                quote_user_list=[]
                quote_user_list.append(ObjectId(current_user))

            else:
                quote_user_list=[]

        data={
            'contact_user_list':contact_user_list,
            'company_user_list':company_user_list,
            'deal_user_list':deal_user_list,
            'quote_user_list':quote_user_list,
            'proforma_user_list':proforma_user_list,
            'invoice_user_list':invoice_user_list
        }
        return data

    def getContactsList(ordId,length,page,search_string,owner,source,tag,date_from,date_to,sort,order,current_user,city,state,country):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId

            # userData = MongoAPI.getUserDetails(ordId,current_user)
            # role = MongoAPI.getRolesListDetails(ordId,userData['role'])
            # roleData = Uid.fix_array_role(role)
            # roleDetail = json.loads(json_util.dumps(roleData))
            
            # if roleDetail['contact_view_all']=='1':
            #     reporting_user_list=[]
            #     reportingUsersData=MongoAPI.getReporting(current_user)
            #     for report_user in reportingUsersData:
            #         this_user=str(report_user[0]['$oid'])
            #         reporting_user_list.append(ObjectId(this_user))
            #     reporting_user_list.append(ObjectId(current_user))
            # elif roleDetail['contact_view_own']=='1':
            #     reporting_user_list=[]
            #     reporting_user_list.append(ObjectId(current_user))

            # else:
            #     reporting_user_list=[]

            # UserList=MongoAPI.getReportingUsers(ordId,current_user)
            # reporting_user_list=UserList['contact_user_list']
            # filter['owner']={"$in":reporting_user_list}
            # if search_string :
            #     filter1['name__contains'] = search_string
            # if owner :
            #     filter['owner'] = ObjectId(owner)
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['contact_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if source:
                filter['source'] = ObjectId(source)
            if city:
                filter['city'] = city
            if state:
                filter['state'] = state
            if country:
                filter['country'] = country
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print(match)
            pipeline = [
                
                        {'$lookup': app_config.user_lookup},
                        {'$lookup': app_config.account_lookup},
                        {'$lookup': app_config.fields_lookup},
                        {'$match': match},
                        {'$match':{
                            '$or': [
                            {'name': { "$regex": search_string, "$options" :'i'}}, 
                            {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                            {'phone_number': { "$regex": search_string, "$options" :'i'}}, 
                            {'email': { "$regex": search_string, "$options" :'i'}}, 
                            ],
                            
                        }},
                        {"$sort": {sort: order}},
                        { '$skip' : sData1 },
                        { '$limit' : length },
                        { '$project': app_config.contact_project}
                    ]
            print (pipeline)
                
            


            settings = defaultdict(list)

            role = Contact.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                        settings['next_followup']=MongoAPI.getRecentFollowUp(ordId,assigned,'contact')
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getContactsCount(org_id):
        try:
            contact_total = Contact.objects(org_id=org_id).count()
            return contact_total
        except NotUniqueError as e:
            return '0'
    

    def getProductsCount(org_id):
        try:
            product_total = Product.objects(org_id=org_id).count()
            return product_total
        except NotUniqueError as e:
            return '0'

    # def getProductsCount(org_id):
    #     try:
    #         product_total = Product.objects(org_id=org_id).count()
    #         return product_total
    #     except NotUniqueError as e:
    #         return '0'
    

    def getUsersNewListCountData(org_id,status):
        try:
            user_total = User.objects(org_id=org_id,status=status).count()
            return user_total
        except NotUniqueError as e:
            return '0'
    def getDealsCountData(org_id,search,assigned_to,stage,tag,date_from,date_to,current_user):
        try:
           
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if search :
                filter1['deal_name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',org_id)
                if stage!='active':
                    filter['stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['stage']={"$in":other_stage_list}
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            print(match)
            pipeline = [
                            
                            {'$lookup' : app_config.dealCheckListLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
            settings = defaultdict(list)
            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            count=len(settings2)
            return count
        except NotUniqueError as e:
            return '0'
        # try:
        #     deal_total = Deal.objects(org_id=org_id).count()
        #     return deal_total
        # except NotUniqueError as e:
        #     return '0'
    def getInvoiceCountData(org_id):
        try:
            deal_total = Invoice.objects(org_id=org_id).count()
            return deal_total
        except NotUniqueError as e:
            return '0'
    def getProformaCountData(org_id):
        try:
            deal_total = Proforma.objects(org_id=org_id).count()
            return deal_total
        except NotUniqueError as e:
            return '0'
            
    def getDealsAssociateCountData(org_id,id,associate_to):
        try:
            if associate_to=='contact':
                deal_total = Deal.objects(org_id=org_id,contact_id=ObjectId(id)).count()
            elif associate_to=='company':
                deal_total = Deal.objects(org_id=org_id,company_id=ObjectId(id)).count()
            
            return deal_total
        except NotUniqueError as e:
            return '0'
    
    def getEmailAssociateCount(org_id,id,associate_to):
        try:
            total = Email.objects(org_id=org_id,associate_id=ObjectId(id),associate_to=associate_to).count()
            return total
        except NotUniqueError as e:
            return '0'

    def getQuotesCountData(ordId,search,assigned_to,stage,date_from,date_to,current_user,quote_category):
        try:
         
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if search :
                filter1['subject__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['quote_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
                if stage!='active':
                    filter['quote_stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['quote_stage']={"$in":other_stage_list}
            if quote_category :
                filter['quote_category'] = quote_category
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                          
                            {'$match'  : match},
                         
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings2 = len(settings6)
            return settings2
        except NotUniqueError as e:
            return '0'
        # try:
        #     quote_total = Quote.objects(org_id=org_id).count()
        #     return quote_total
        # except NotUniqueError as e:
        #     return '0'
    
    def getQuotesCountDataAssociate(org_id,id,associate_to):
        try:
            if associate_to=='contact':
                quote_total = Quote.objects(org_id=org_id,contact_id=ObjectId(id)).count()
            elif associate_to=='company':
                quote_total = Quote.objects(org_id=org_id,company_id=ObjectId(id)).count()
            return quote_total
        except NotUniqueError as e:
            return '0'

    def getAccountsCountData(org_id):
        try:
            deal_total = Account.objects(org_id=org_id).count()
            return deal_total
        except NotUniqueError as e:
            return '0'

    def getPriceListAccountsCountData(org_id,pricelist_id):
        try:
            deal_total = Account.objects(org_id=org_id,pricelist=pricelist_id).count()
            return deal_total
        except NotUniqueError as e:
            return '0'

    def priceListNewProduct(price_id,product_id):

        try:
            pricelist = PricelistProduct(price_id=ObjectId(price_id),product_id=ObjectId(product_id),inr_price='',usd_price='',erou_price='')
            response = pricelist.save()
            output1 = {'pricelist_id' : response.id}
            return output1
        except NotUniqueError as e:
            return ''



    def getFollowUp(org_id,user_id,id,associate_to):
        try:

            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to

            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$match': match},
                            { '$project': app_config.followUp_Project}
                        ]

            

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getFollowUpAll():
        try:

            filter = {}
            key = []
            val = []
            # filter['org_id'] = org_id
            
            # if id :
            #     filter['associate_id'] = id
            # if associate_to:
            #     filter['associate_to'] = associate_to

            y = datetime.datetime.today().strftime('%Y')
            m = datetime.datetime.today().strftime('%m')
            d = datetime.datetime.today().strftime('%d')
            search_time = datetime.datetime(int(y),int(m),int(d),0,0)
            nextday = datetime.datetime(int(y),int(m),int(d),23,59)
            # if date_from:
            #     y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
            #     m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
            #     d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
            #     search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            # if date_to:
            #     y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
            #     m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
            #     d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                # nextday=datetime.datetime(int(y),int(m),int(d),23,59)
            filter['notification']='0'
            filter['date'] = {"$gte": search_time,"$lte": nextday}
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$match': match},
                            { '$project': app_config.followUp_Project}
                        ]

            

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getRecentFollowUp(org_id,id,associate_to):
        try:

            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to

            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            y = datetime.datetime.today().strftime('%Y')
            m = datetime.datetime.today().strftime('%m')
            d = datetime.datetime.today().strftime('%d')
            search_time = datetime.datetime(int(y),int(m),int(d),0,0)
            filter['date'] = {"$gte": search_time}
            filter['status'] = 'Open'
            match = filter
            length = 1
            sort_by = 'create_date'
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match': match},
                            {'$limit'  : length},
                            { '$project': app_config.followUp_Project}
                        ]

            

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'


    def getTodayFollowUp(org_id,user_id,from_date,to_date,status):
        try:

            filter = {}
            filter['org_id'] = org_id
            filter['owner'] = ObjectId(user_id)
            

            if status=='all':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Completed':
                filter['status'] = 'Completed'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Overdue':
                filter['status'] = 'Open'
                yesterday = from_date - datetime.timedelta(days = 1)
                # print (yesterday)
                filter['date'] = {"$lte": yesterday}

            match = filter

            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$project': app_config.followUp_Project},
                            {'$match': match}
                        ]

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'



    def getTodayFollowUpData(org_id,length,page,user_id,from_date,to_date,sort,order,status):
        try:
            
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)


            filter = {}
            filter['org_id'] = org_id
            filter['owner'] = ObjectId(user_id)
            

            if status=='all':
                filter['status'] = 'Open'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Completed':
                filter['status'] = 'Completed'
                filter['date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Overdue':
                filter['status'] = 'Open'
                yesterday = from_date - datetime.timedelta(days = 1)
                # print (yesterday)
                filter['date'] = {"$lte": yesterday}
            match = filter
            pipeline = [
                            {'$lookup' : app_config.owner_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},

                        ]

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'



    def getEmail(org_id,user_id,id,associate_to):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to

            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.ownerLookup},
                            {'$match': match},
                            {'$project': app_config.email_Project}
                        ]

            

            role = Email.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    if item1=='attachment':
                        attachment1 = item[item1]
                        attachment = []
                        for item3 in attachment1:
                            attachment2 = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_EMAIL_FOLDER)+str(item3)
                            attachment.append(attachment2)
                        settings['attachment'] = attachment
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']=''



                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getEmailAll():
        try:
            test = Email.objects(org_id=94).delete()
            # test2 =Email.objects().update(status='1')
            filter = {}
            key = []
            val = []
            # filter['org_id'] = org_id
            # if id :
            filter['status'] = '0'
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.ownerLookup},
                            {'$match': match},
                            {'$project': app_config.email_Project}
                       ]
            role = Email.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    # if item1=='create_date':
                    #     create_date1 = item[item1]
                    #     create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                    #     create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                    #     settings['create_date'] = create_date
                    #     settings['create_time'] = create_time
                    # if item1=='date':
                    #     create_date1 = item[item1]
                    #     create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                    #     settings['date'] = create_date
                    # if item1=='attachment':
                    #     if len(item[item1]) > 0:
                    #         attachment1 = item[item1]
                    #         attachment = []
                    #         for item3 in attachment1:
                    #             # attachment2 = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_EMAIL_FOLDER)+str(item3)
                    #             attachment2 = str(app_config.UPLOAD_OPEN_EMAIL_FOLDER)+str(item3)
                    #             # attachment2 =''+str(app_config.UPLOAD_OPEN_EMAIL_FOLDER)+str(item3)
                    #             attachment.append(attachment2)
                    #         settings['attachment'] = attachment
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']=''



                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    
    def getEmailAssociate(ordId,length,page,sort,order,id,associate_to):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = int(ordId)
            filter['associate_to'] = associate_to
            filter['associate_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [

                            
                            {'$lookup': app_config.ownerLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.email_Project}
                        ]
            settings = defaultdict(list)

            role = Email.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    if item1=='attachment':
                        attachment1 = item[item1]
                        attachment = []
                        for item3 in attachment1:
                            attachment2 = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_EMAIL_FOLDER)+str(item3)
                            attachment.append(attachment2)
                        settings['attachment'] = attachment
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getFollowUp1(org_id,user_id,id,associate_to):
        try:
            s = Follow_up.objects.filter(org_id=org_id,associate_id=id,associate_to=associate_to).to_json()
            followup = json.loads(s)
            followup = Uid.fix_array(followup)
            output = followup
            
        except Follow_up.DoesNotExist:
            output = ''   

        return output


    def getCompanyProfile(org_id):
        try:
            s = Organization.objects.filter(org_id=org_id).to_json()
            organization = json.loads(s)
            organization = Uid.fix_array1(organization)
            output = organization
            
        except Organization.DoesNotExist:
            output = ''   

        return output

    def getEmailDetail(id):
        try:
            s = Email.objects.filter(id=ObjectId(id)).to_json()
            email = json.loads(s)
            email = Uid.fix_array1(email)
            output = email
            
        except Email.DoesNotExist:
            output = ''   

        return output

    
    def getUserWithEmail(email):
        try:
            filter = {}
            key = []
            val = []
            filter['email'] = email
            match = filter


            pipeline = [

                            {'$match': match},
                        ]
            role = User.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']='' 
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'
        
    

    def followUpCompleted(org_id,user_id,id,notes):
        try:

            modify_date = datetime.datetime.utcnow
            completed_on = modify_date
            status = 'Completed'
            follow_up = Follow_up.objects(id=ObjectId(id)).update_one(notes=notes,completed_on=completed_on,modify_date=modify_date,status=status)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def followUpReschedule(org_id,current_user,id,time,date):

        try:

            modify_date = datetime.datetime.utcnow
            date=datetime.datetime.strptime(str(date),'%d-%m-%Y').strftime('%Y-%m-%d')
            follow_up = Follow_up.objects(id=ObjectId(id)).update_one(time=time,date=date,modify_date=modify_date,notification="0")
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'
            
    def updateFieldSettings(org_id,current_user,data):

        try:
            output1=1
            follow_up = Fields.objects(org_id=org_id,type='field_settings').update_one(settings=data)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'
    def updatePriceList(id,data):

        try:
            output1=1
            pricelist = Pricelist.objects(id=ObjectId(id)).update_one(title=data['title'])
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def followUpSubmit(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['follow_up'] = Uid.generateUUID()
            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                if data=='date':
                    data1['date']=datetime.datetime.strptime(str(data1[data]),'%d-%m-%Y').strftime('%Y-%m-%d')
                else:
                    data1[data] = data1[data]
            u2 = Follow_up.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def documentSubmit(org_id,user_id,id,associate_to,document,user_file_name):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1 = defaultdict(list)

            data1['associate_id'] = id
            data1['associate_to'] = associate_to
            data1['user_id'] = user_id
            data1['org_id'] = org_id
            data1['document_id'] = Uid.generateUUID()
            data1['document'] = document
            data1['create_by'] = user_id
            data1['user_file_name'] = user_file_name
            

            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]
            
            u2 = Document.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def pdfDocumentUpdate(ordId,doc_id,field_type,filename1):
        try:
            filename=str(app_config.BASE_URL)+str(app.config['UPLOAD_PDF_IMAGE_FOLDER'])+str(ordId)+'/'+str(filename1)

            if field_type=='prepared_by':
                doc = PdfSettings.objects(org_id=ordId,id=ObjectId(doc_id)).update(prepared_by_path=filename)
            elif field_type=='checked_by':
                doc = PdfSettings.objects(org_id=ordId,id=ObjectId(doc_id)).update(checked_by_path=filename)
            elif field_type=='authorized_signatory':
                doc = PdfSettings.objects(org_id=ordId,id=ObjectId(doc_id)).update(authorized_signatory_path=filename)  
            elif field_type=='footer_img':
                doc = PdfSettings.objects(org_id=ordId,id=ObjectId(doc_id)).update(footer_img_file=filename)
            
            # filename=str(app_config.BASE_URL)+str(app.config['UPLOAD_PDF_IMAGE_FOLDER'])+str(ordId)+'/'+str(filename1)
            output1 = filename
            return output1
        except Fields.DoesNotExist:
            return ''
    def assignMail(org_id,user_id,associate_to,associate_id,old_assigned_name,assigned_to,url):
        user = MongoAPI.getUserDetails(org_id,assigned_to)
        if associate_to=='contact':
            response1 = MongoAPI.getContactDetails(org_id,associate_id)
            templateData=MongoAPI.getAdminEmailTemplate('assign_contact')
            template=templateData[0]['template_content']
            template1 = template.replace("{{assigned_by}}",old_assigned_name)
            template1 = template1.replace("{{user_name}}",user['name'])
            template1 = template1.replace("{{contact_name}}",response1['name'])
            template1 = template1.replace("{{company_name}}",response1['company_name'])
            template1 = template1.replace("{{contact_no}}",response1['phone_number'])
            template1 = template1.replace("{{contact_email}}",response1['email'])
            template1 = template1.replace("{{contact_description}}",response1['description'])
            
            template1 = template1.replace("{{contact_detail_url}}",url)
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - Contact Assign Notification'
            data1['content']=template1
            data1['to']=user['email']
            data1['cc']=''
            data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
            # data1['attachment_list']=''
            emailId='info@farazon.com'
            attachment=''
            data1['associate_id']=associate_id
            data1['associate_to']='reassign_contact'
            sendMail=MongoAPI.emailSubmit(org_id,user_id,data1,emailId,attachment)
        elif associate_to=='company':
            response1 = MongoAPI.getCompanyDetails(org_id,associate_id)
            templateData=MongoAPI.getAdminEmailTemplate('assign_company')
            template=templateData[0]['template_content']
            template1 = template.replace("{{assigned_by}}",old_assigned_name)
            template1 = template1.replace("{{user_name}}",user['name'])
            # template1 = template1.replace("{{contact_name}}",response1['name'])
            template1 = template1.replace("{{company_name}}",response1['company_name'])
            template1 = template1.replace("{{contact_no}}",response1['phone'])
            template1 = template1.replace("{{contact_email}}",response1['email'])
            # template1 = template1.replace("{{contact_description}}",response1['description'])
            
            template1 = template1.replace("{{company_detail_url}}",url)
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - Company Assign Notification'
            data1['content']=template1
            data1['to']=user['email']
            data1['cc']=''
            data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
            # data1['attachment_list']=''
            emailId='info@farazon.com'
            attachment=''
            data1['associate_id']=associate_id
            data1['associate_to']='reassign_company'
            sendMail=MongoAPI.emailSubmit(org_id,user_id,data1,emailId,attachment)
        elif associate_to=='deal':
            response1 = MongoAPI.getDealDetails(org_id,associate_id)
            templateData=MongoAPI.getAdminEmailTemplate('assign_deal')
            template=templateData[0]['template_content']
            template1 = template.replace("{{assigned_by}}",old_assigned_name)
            template1 = template1.replace("{{user_name}}",user['name'])
            template1 = template1.replace("{{contact_name}}",response1['contact_detail'][0]['name'])
            template1 = template1.replace("{{company_name}}",response1['company_name'])
            template1 = template1.replace("{{deal_currency}}",response1['currency'])
            amt = (format(float(response1['amount']),".2f"))
            template1 = template1.replace("{{deal_amount}}",amt)
            template1 = template1.replace("{{deal_description}}",response1['description'])
            # template1 = template1.replace("{{contact_description}}",response1['description'])
            
            template1 = template1.replace("{{deal_detail_url}}",url)
            data1 = defaultdict(list)
            data1['subject']='Farazon CRM - Deal Assign Notification'
            data1['content']=template1
            data1['to']=user['email']
            data1['cc']=''
            data1['bcc']='developer2@skyzon.com,tester@skyzon.com'
            # data1['attachment_list']=''
            emailId='info@farazon.com'
            attachment=''
            data1['associate_id']=associate_id
            data1['associate_to']='reassign_deal'
            sendMail=MongoAPI.emailSubmit(org_id,user_id,data1,emailId,attachment)

    def emailSubmit(org_id,user_id,data1,emailId,attachment):
        try:
            shopping_list = []
            insertData = defaultdict(list)

            to_list = data1['to'].split(',')
            data2=json.loads(json_util.dumps(data1))
          
            # if hasattr(data2,'cc'):
            # print('------------------------------------------')
            if data2['cc'] != '':
                cc_list = data2['cc'].split(',')
                data2['cc'] = cc_list
                
            else:
                cc_list = []
                data2['cc'] = []
            # else:
            #     cc_list = []
            
            # if hasattr(data2,'bcc'):
            if data2['bcc'] !='':
                bcc_list = data2['bcc'].split(',')
                data2['bcc'] = bcc_list
            else:
                bcc_list = []
                data2['bcc'] = []
            # else:
            #     bcc_list = []
              
            insertData['email_id'] = Uid.generateUUID()
            insertData['org_id'] = org_id
            insertData['fromEmail'] = emailId
            insertData['to'] = to_list
            insertData['cc'] = cc_list
            insertData['bcc'] = bcc_list
            insertData['subject'] = data1['subject']
            insertData['content'] = data1['content']
            insertData['attachment'] = attachment.split(",")
            insertData['associate_id'] =  data1['associate_id']
            insertData['associate_to'] =  data1['associate_to']
            insertData['create_by'] =  user_id
            u2 = Email.from_json(json.dumps(insertData))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def createDirectory(org_id):
        try:
            access =  0o777
            path1='uploads/commonDocs/'+str(org_id)
            os.mkdir(path1, access)

            path2='uploads/company/'+str(org_id)
            os.mkdir(path2, access)

            path3='uploads/crmDocs/'+str(org_id)
            os.mkdir(path3, access)

            path4='uploads/deal/'+str(org_id)
            os.mkdir(path4, access)

            path5='uploads/document/'+str(org_id)
            os.mkdir(path5, access)

            path6='uploads/email/'+str(org_id)
            os.mkdir(path6, access)

            path7='uploads/pdf_image/'+str(org_id)
            os.mkdir(path7, access)

            path8='uploads/product/'+str(org_id)
            os.mkdir(path8, access)

            path9='uploads/profile/'+str(org_id)
            os.mkdir(path9, access)

            return '1'
        except NotUniqueError as e:
            return '0'

    def ticketSubmit(org_id,user_id,ticket_no,data1,emailId,attachment):
        try:
            shopping_list = []
            settings = defaultdict(list)

            to_list = data1['to'].split(',')

            if hasattr(data1, 'cc'):
                cc_list = data1['cc'].split(',')
                data1['cc'] = cc_list

            if hasattr(data1, 'bcc'):
                bcc_list = data1['bcc'].split(',')
                data1['bcc'] = bcc_list

            if hasattr(data1, 'attachment_list'):
                data1.pop('attachment_list')

            settings['org_id'] = org_id
            settings['user_id'] = user_id
            settings['ticket_no'] = ticket_no
            settings['subject'] = data1['subject']
            settings['message'] = data1['content']
            settings['attachment'] = attachment.split(",")
            settings['status'] = 'open'
            settings['create_by'] = user_id
            u2 = SupportTicket.from_json(json.dumps(settings))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def noteSubmit(org_id,user_id,data1):
        try:
            print('ttttttttttttttttttttttttttt')
            shopping_list = []
            settings = defaultdict(list)

            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['note_id'] = Uid.generateUUID()


            for data in data1:
               
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]
            
            u2 = Note.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def getNotes(org_id,user_id,id,associate_to):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to           
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            sort_by = 'create_date'
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup': app_config.note_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match': match},
                            { '$project': app_config.note_Project}
                        ]
            role = Note.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']='' 
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'


    def getDocument(org_id,user_id,id,associate_to):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to           
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.note_lookup},
                            {'$match': match},
                            { '$project': app_config.document_Project}
                        ]
            role = Document.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='document':
                        attachment2 = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_DOCUMENT_FOLDER)+str(item[item1])
                        settings['document'] = attachment2
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']='' 
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDocumentCommon(org_id,user_id,id,associate_to):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if id :
                filter['associate_id'] = id
            if associate_to:
                filter['associate_to'] = associate_to           
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.note_lookup},
                            {'$match': match},
                            { '$project': app_config.document_Project}
                        ]
            role = Document.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='document':
                        attachment2 = str(app_config.BASE_URL)+str(app_config.UPLOAD_COMMON_DOCUMENT_FOLDER)+str(org_id)+'/'+str(item[item1])
                        settings['document'] = attachment2
                    # if item1=='create_date':
                    #     create_date1 = item[item1]
                    #     create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                    #     create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                    #     settings['create_date'] = create_date
                    #     settings['create_time'] = create_time
                    # else:
                    #     data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']='' 
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'



    def getProductsList(org_id):
        try:
            s = Product.objects.filter(org_id=org_id,status='active').to_json()
            followup = json.loads(s)
            followup = Uid.fix_array(followup)
            output = followup
            
        except Product.DoesNotExist:
            output = ''   

        return output
    
    
    def productSubmit(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)

            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['product_id'] = Uid.generateUUID()
            # data1['product_id'] = int(data1['product_id'])


            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]
            
            u2 = Product.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'
    def productUpdate(org_id,user_id,data1,id):
        try:


            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['product_name']:
                data5['product_name'] = str(data1['product_name'])
            if data1['category']:
                data5['category'] = ObjectId(str(data1['category']))
            if data1['model_no']:
                data5['model_no'] = str(data1['model_no'])
                
            if data1['price']:
                data5['price'] = str(data1['price'])
            if data1['hsn_code']:
                data5['hsn_code'] = str(data1['hsn_code'])
            if data1['uom']:
                data5['uom']= str(data1['uom'])

            if data1['cgst']:
                data5['cgst']= str(data1['cgst'])
            if data1['sgst']:
                data5['sgst']= str(data1['sgst'])
            if data1['igst']:
                data5['igst']= str(data1['igst'])
                

            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']
            if data1['description']:
                data5['description']=data1['description']


            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Product.objects(id=ObjectId(id)).update_one(**data3)
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def renameUpdate(org_id,user_id,data1,id):
        try:


            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['contact_singular']:
                data5['contact_singular'] = str(data1['contact_singular'])
            if data1['contact_plural']:
                data5['contact_plural'] = str(data1['contact_plural'])
            if data1['company_singular']:
                data5['company_singular'] = str(data1['company_singular'])
                
            if data1['company_plural']:
                data5['company_plural'] = str(data1['company_plural'])
            if data1['deal_singular']:
                data5['deal_singular'] = str(data1['deal_singular'])
            if data1['deal_plural']:
                data5['deal_plural']= str(data1['deal_plural'])
            if data1['modify_date']:
                data5['modify_date'] = data1['modify_date']


            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Rename.objects(id=ObjectId(id)).update_one(**data3)
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def dealUpdate111(org_id,id,data1):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['deal_name']:
                data5['deal_name'] = str(data1['deal_name'])
            if data1['company_id']:
                data5['company_id'] = ObjectId(str(data1['company_id']))
            if data1['contact_id']:
                data5['contact_id'] = ObjectId(str(data1['contact_id']))
                
            if data1['amount']:
                data5['amount'] = float(data1['amount'])
            if data1['stage']:
                data5['stage'] = ObjectId(str(data1['stage']))
            if data1['assigned_to']:
                data5['assigned_to']= ObjectId(str(data1['assigned_to']))
            if data1['product']:
                data5['product'] = data1['product']
            # if data1['qty']:
            #     data5['qty']=data1['qty']
            if data1['target_date']:
                data5['target_date']=data1['target_date']
            # if data1['close_date']:
            #     data5['close_date']=data1['close_date']
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']
            if data1['currency']:
                data5['currency']=data1['currency']

            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Deal.objects(id=ObjectId(id)).update_one(
                **data3
                )

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'


    def dealChangeStage(org_id,id,data1):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['stage']:
                data5['stage'] = ObjectId(str(data1['stage']))
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']

            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Deal.objects(id=ObjectId(id)).update_one(
                **data3
                )

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def proformaStatusChange(org_id,id,data1):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['proforma_stage']:
                data5['proforma_stage'] = ObjectId(str(data1['proforma_stage']))
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']

            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Proforma.objects(id=ObjectId(id)).update_one(
                **data3
                )

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'


    def invoiceStatusChange(org_id,id,data1):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            if data1['invoice_stage']:
                data5['invoice_stage'] = ObjectId(str(data1['invoice_stage']))
            if data1['modify_date']:
                data5['modify_date']=data1['modify_date']

            data3 = defaultdict(list)

            for data4 in data5:
                data3[data4] = data1[data4]

            Invoice.objects(id=ObjectId(id)).update_one(
                **data3
                )

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0'

    def dealSubmit(org_id,user_id,data1,no):
        try:
            shopping_list = []
            settings = defaultdict(list)


            data1['deal_no'] = no
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['deal_id'] = Uid.generateUUID()

            # print(data1)
            if not data1['target_date']:
                data1['target_date'] =  datetime.date.today() + datetime.timedelta(days = 5)
                
            if data1['assigned_to'] == '':
                data1['assigned_to'] = user_id

            stage_data=MongoAPI.getDefault(org_id,'deal_stage')
            for dd in stage_data:
                data1['stage']=dd['_id']
            # t=hasattr(data1['product'])
            # print(t)
            # if not hasattr(data1['product']):
            #     data1['product']=[{}]

            # if not data1['product']:
            #     data1['product']=[{}]
            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]

            u2 = Deal.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'


    def dealSubmitNew(org_id,user_id,data2,deal_stage,no):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1 = defaultdict(list)
            user_id = str(user_id)
            deal_stage = str(deal_stage)


            data1['deal_no'] = no
            data1['create_by'] = ObjectId(user_id)
            data1['org_id'] = int(org_id)
            data1['stage'] = ObjectId(deal_stage)
            data1['deal_id'] = Uid.generateUUID()
            data1['assigned_to'] = data2['assigned_to']
            data1['product'] = data2['product']
            data1['target_date'] = data2['valid_till']
            data1['currency'] = data2['currency']
            data1['deal_name'] = data2['subject']
            data1['contact_id'] = data2['contact_id']
            data1['company_id'] = data2['company_id']
            data1['amount'] = float(data2['total'])

            for data in data1:
                # print (data)
                # shopping_list.append(data:data1[data])
                settings[data] = data1[data]
            
            u2 = Deal(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def addCustomFields(org_id,user_id,data1):
        try:

            settings = defaultdict(list)


            data1['create_by'] = user_id
            data1['org_id'] = org_id


            for data in data1:
                settings[data] = data1[data]
            
            u2 = CustomFields.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def getCustomFields(ordId,module):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['module'] = module
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            pipeline = [
                            {'$match'  : match},
                       ]
            
            settings = defaultdict(list)
            role = CustomFields.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getCustomFieldsDetails(ordId,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            pipeline = [
                            {'$match'  : match},
                       ]
            
            settings = defaultdict(list)
            role = CustomFields.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def updateCustomFields(org_id,data1,id):
        try:
            data1['modify_date'] = datetime.datetime.now(timezone("Asia/Kolkata")).strftime('%Y-%m-%d %H:%M:%S.%f')
            data3 = defaultdict(list)
            for data4 in data1:
                data3[data4] = data1[data4]
            CustomFields.objects(id=ObjectId(id)).update_one(**data3)
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 
    
    def salesProcess(org_id,user_id,data1):
        try:

            settings = defaultdict(list)


            data1['create_by'] = user_id
            data1['org_id'] = org_id


            for data in data1:
                settings[data] = data1[data]
            
            u2 = SalesProcess.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def salesProcessUpdate(org_id,user_id,data1,id):
        try:
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            # data1['modify_date'] = datetime.datetime.utcnow
            for data in data1:
                settings[data] = data1[data]
            response = SalesProcess.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def salesSubProcessUpdate(org_id,user_id,data1,id):
        try:
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            # data1['modify_date'] = datetime.datetime.utcnow
            for data in data1:
                settings[data] = data1[data]
            response = SalesSubProcess.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def salesProcessTemplate(org_id,user_id,data1):
        try:
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            for data in data1:
                settings[data] = data1[data]
            u2 = SalesProcessTemplate.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'
    
    def salesProcessTemplateUpdate(org_id,data1,id):
        try:
            data1['modify_date'] = datetime.datetime.utcnow

            data5 = defaultdict(list)

            
            if data1['template_name']:
                data5['template_name']=data1['template_name']
            if data1['process_list']:
                data5['process_list']=data1['process_list']
            data5['modify_date'] = datetime.datetime.utcnow

            data3 = defaultdict(list)

            for data4 in data1:
                data3[data4] = data1[data4]
            # print(data3)
            SalesProcessTemplate.objects(id=ObjectId(id)).update_one(**data3)

            # u2.save()
            output1 = id
            return output1
        except NotUniqueError as e:
            return '0' 
    
    
    def deleteSalesTemplate(id):

        template = SalesProcessTemplate.objects(id=id).first()
        if not template:
            return 'No'
        else:
            template.delete()
            return 'Yes'

    def updateCheckList(org_id,user_id,id,data1,deal_id):
        try:
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['status'] = 'completed'
          
            # data1['modify_date'] = datetime.datetime.utcnow
            for data in data1:
                settings[data] = data1[data]
               
            response = DealsCheckList.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            response1 = MongoAPI.updateDealStage(org_id,deal_id)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def updateCheckListData(org_id,associate_id,process_data):
        try:
            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(process_data['process_id'])
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = DealsCheckList.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)

            for sett in settings6:
                sett['data_list'].append(process_data['list_value'])
                upresponse = DealsCheckList.objects(org_id=org_id,id=ObjectId(process_data['process_id'])).update(data_list=sett['data_list'])
            id = 1
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def updateCheckListSubData(org_id,associate_id,process_data):
        try:
            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(process_data['process_id'])
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = DealsSubCheckList.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)
            # print(settings6)
            for sett in settings6:
                sett['data_list'].append(process_data['list_value'])
                upresponse = DealsSubCheckList.objects(org_id=org_id,id=ObjectId(process_data['process_id'])).update(data_list=sett['data_list'])
            id = 1
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def dealDocumentsRemove(org_id,process_id,index):
        try:
            index1 =int(index)
            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(process_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = DealsCheckList.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)

            for sett in settings6:
                
                # sett['data_list']=sett['data_list'].remove(sett['data_list'][index])
                del sett['data_list'][index1]
                upresponse = DealsCheckList.objects(org_id=org_id,id=ObjectId(process_id)).update(data_list=sett['data_list'])
            id = 1
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    

    def dealDocumentsSubRemove(org_id,process_id,index):
        try:
            index1 =int(index)
            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(process_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = DealsSubCheckList.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)

            for sett in settings6:
                
                # sett['data_list']=sett['data_list'].remove(sett['data_list'][index])
                del sett['data_list'][index1]
                upresponse = DealsSubCheckList.objects(org_id=org_id,id=ObjectId(process_id)).update(data_list=sett['data_list'])
            id = 1
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def updateCheckListSub(org_id,user_id,id,data1):
        try:
            settings = defaultdict(list)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['status'] = 'completed'
            for data in data1:
                settings[data] = data1[data]
            response = DealsSubCheckList.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)

            # Complete Main Process
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)
            subData=[]
            subCheckList = DealsSubCheckList.objects.aggregate(*pipeline)
            for sub in subCheckList:
                sub_data = defaultdict(list)
                for item1 in sub:
                    sub_data[item1] = sub[item1]
                subData.append(sub_data)
            subData = json.loads(json_util.dumps(subData))
            subData = Uid.fix_array(subData)
            filter2 = {}
            key1 = []
            val1 = []
            filter2['parent_id']=ObjectId(subData[0]['parent_id'])
            match2 = filter2
            pipeline2 = [
                {
                    '$match':match2
                }
            ]
            subData2=[]
            subCheckListAll = DealsSubCheckList.objects.aggregate(*pipeline2)
            for sub in subCheckListAll:
                sub_data2 = defaultdict(list)
                for item1 in sub:
                    sub_data2[item1] = sub[item1]
                subData2.append(sub_data2)
            subData2 = json.loads(json_util.dumps(subData2))

            subProcessLength=len(subData2)
            completedProcessLength=0
            for allsub in subData2:
                if allsub['status']=='completed':
                    completedProcessLength = completedProcessLength + 1
            if subProcessLength==completedProcessLength:
                updateMainProcess = DealsCheckList.objects(org_id=org_id,id=ObjectId(subData[0]['parent_id'])).update(status='completed',create_by=ObjectId(user_id))

            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def updateDealStage(org_id,deal_id):
        try:
            settings = defaultdict(list)
            deal_detail=MongoAPI.getDealDetails(org_id,deal_id)
            deal_detail = Uid.fix_array3(deal_detail)
            # template=MongoAPI.getSalesProcessTemplateDetail(org_id,deal_detail['template_id'])
            deal_stage=MongoAPI.getDealStageMapping(org_id,deal_id)
            stage_id=''
            quote_id=''  
            for stage_data in deal_stage:
                for stages in stage_data['stage_mapping']:
                    
                    stage_status='new'
                    process_length=len(stages['process_list'])
                    process_completed=0
                    for process_id in stages['process_list']:

                        check_list=MongoAPI.getDealCheckList(org_id,deal_id,process_id)
                        check_list=Uid.fix_array(check_list)
                        for cl in check_list:
                         
                            if cl['process_id'] == process_id and cl['status'] == 'completed':
                                process_completed = process_completed+1
                            if process_length == process_completed:
                                deal_update = Deal.objects(id=ObjectId(deal_id)).update(stage=ObjectId(stages['_id']),completed_percentage=stages['weightage'])
                                stage_id=stages['_id']

            # Update Quote stage
            deal_detail=MongoAPI.getDealDetails(org_id,deal_id)
            deal_detail = Uid.fix_array3(deal_detail)
            print(deal_detail['stage'])


            quote_filter = {}
            qkey = []
            qval = []
            quote_filter['org_id'] = org_id
            quote_filter['deal_id'] = ObjectId(deal_id)
            
            for item1 in quote_filter:
                qkey.append(item1)
                qval.append(quote_filter[item1])
            match = quote_filter
            pipeline = [
                        {'$sort'   : {'create_date' : -1}},
                        {'$limit'  : 1},
                        {'$match'  : match},
                    ]
            quote = defaultdict(list)
            quoteData = Quote.objects.aggregate(*pipeline)
            for dd in quoteData:
                quote_id=dd['_id']
            if quote_id!=''and stage_id!='':
                # print(quote_id)
                # print(stages['_id'])
                quote_update = Quote.objects(id=ObjectId(quote_id)).update(quote_stage=ObjectId(deal_detail['stage']))
            output1 = 1
            return output1
        except NotUniqueError as e:
            return '0'

    def deleteSalesProcess(id):

        template = SalesProcess.objects(id=id).first()
        if not template:
            return 'No'
        else:
            template.delete()
            return 'Yes'

    def deleteSalesSubProcess(id):

        template = SalesSubProcess.objects(id=id).first()
        if not template:
            return 'No'
        else:
            template.delete()
            return 'Yes'
    
    def salesSubProcess(org_id,user_id,data1):
        try:

            settings = defaultdict(list)


            data1['create_by'] = user_id
            data1['org_id'] = org_id


            for data in data1:
                settings[data] = data1[data]
            
            u2 = SalesSubProcess.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def insertCheckList(org_id,process,associate_to,associate_id):
        try:
            checkList = DealsCheckList.objects(associate_id=ObjectId(associate_id))
            checkList.delete()
            sub_checkList = DealsSubCheckList.objects(associate_id=ObjectId(associate_id))
            sub_checkList.delete()
            settings = defaultdict(list)
            process_list = []
            sub_process_list = []
            for data1 in process:

                for data in data1['process_list']:
                    # print(data)
                    new_process = {}
                    new_process['org_id']=org_id
                    new_process['associate_id']=ObjectId(associate_id)
                    new_process['associate_to']=associate_to
                    new_process['process_id']=data['_id']
                    new_process['process_name']=data['process_name']
                    new_process['field_type']=data['field_type']
                    new_process['list_value']=data['list_value']
                    new_process['status']='new'
                    u2 = DealsCheckList(**new_process)
                    u2.save()
                    response = u2
                    output1 = str(response.id)
                    if len(data['sub_process']) > 0:
                        for sub_data in data['sub_process']:
                            new_sub_process = {}
                            new_sub_process['org_id']=org_id
                            new_sub_process['associate_id']=ObjectId(associate_id)
                            new_sub_process['associate_to']=associate_to
                            new_sub_process['parent_id']=output1
                            new_sub_process['process_name']=sub_data['process_name']
                            new_sub_process['field_type']=sub_data['field_type']
                            new_sub_process['list_value']=sub_data['list_value']
                            new_sub_process['status']='new'
                            u3 = DealsSubCheckList(**new_sub_process)
                            u3.save()
                
                stage_mapping = {}
                stage_mapping['org_id']=org_id
                stage_mapping['associate_id']=ObjectId(associate_id)
                stage_mapping['associate_to']=associate_to
                stage_mapping['stage_mapping']=data1['stage_mapping']
                u4 = DealsStageMapping(**stage_mapping)
                u4.save()

                template = Deal.objects(id=ObjectId(associate_id)).update(template_id=data1['_id'],template_name=data1['template_name'])
        except NotUniqueError as e:
            return '0'
    

    def numberingUpdate(id,count):
        try:
            settings = defaultdict(list)
            settings['sequence'] = int(count)
            settings['sequence'] = int(count)
            response = Numbering.objects(id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'


    def getProformaDetails(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.proforma_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.proforma_project}
                        ]
           
            settings = defaultdict(list)

            role = Proforma.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            for item3 in item2:
                                if item3=='_id':
                                    contact_key = str('contact')+str(item3)
                                else:
                                    contact_key = str('contact_')+str(item3)
                                # print (contact_key)
                                settings[contact_key] = item2[item3]
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='proforma_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['proforma_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''

                settings6.append(settings)

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'


    def convertToProforma(ordId,quotes,id,no,user_id,proforma_stage):
        try:
            shopping_list = []
            settings = defaultdict(list)

            assigned_to = str(user_id)
            deal_id = str(quotes['deal_id'])

            quote_id = str(quotes['quote_id'])
            

            
            quotes['assigned_to'] = ObjectId(assigned_to)
            quotes['proforma_no'] = str(no)
            quotes['create_by'] = user_id
            quotes['org_id'] = ordId
            quotes['deal_id'] = ObjectId(deal_id)
            quotes['quote_id'] = ObjectId(id)

            # print (quotes['quote_stage'])

            del quotes['quote_id']
            del quotes['valid_till']
            del quotes['quote_stage']
            del quotes['_id']
            # del quotes['quote_no']
            # del quotes['create_date']
            # del quotes['quote_revised']
            # del quotes['revise_type']
            # del quotes['revise_count']
            # del quotes['quote_category']
            # del quotes['shipping']
            
            quotes['proforma_stage'] = ObjectId(proforma_stage)
            quotes['total'] = float(quotes['total'])

            quotes['quote_id'] = ObjectId(str(id))

            for quote in quotes:
                if quote!='quote_revised' and quote!='shipping' and quote!='revise_type' and quote!='revise_count' and quote!='quote_category' and quote!='create_date':
                    settings[quote] = quotes[quote]
            settings['modify_date'] = datetime.datetime.utcnow
            u2 = Proforma(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'


    def convertToInvoice(ordId,quotes,id,no,user_id,invoice_stage,convert_type):
        try:
            shopping_list = []
            settings = defaultdict(list)
            quotes=json.loads(json_util.dumps(quotes))
            assigned_to = str(user_id)
            deal_id = str(quotes['deal_id'])
            if convert_type=='quote':
                quote_id = str(quotes['_id'])
            elif convert_type=='proforma':
                quote_id = str(quotes['quote_id'])


            
            # print(quotes)

            
            quotes['assigned_to'] = ObjectId(assigned_to)
            quotes['invoice_no'] = str(no)
            quotes['create_by'] = user_id
            quotes['org_id'] = ordId
            quotes['deal_id'] = ObjectId(deal_id)
            quotes['quote_id'] = ObjectId(id)

            # print (quotes['quote_stage'])

            # del quotes['quote_id']
            # del quotes['valid_till']
            # del quotes['quote_stage']
            # del quotes['_id']
            # del quotes['proforma_stage']
            # del quotes['proforma_no']
            # del quotes['create_date']

            # for quote in quotes:
            #     print(quote)\
            if 'proforma_date' in quotes:
                del quotes['proforma_date']
            if '_id' in quotes:
                del quotes['_id']
            if 'create_date' in quotes:
                del quotes['create_date']
            if 'proforma_stage' in quotes:
                del quotes['proforma_stage']
            # if 'proforma_no' in quotes:
            #     del quotes['proforma_no']
            if 'quote_revised' in quotes:
                del quotes['quote_revised']
            if 'revise_type' in quotes:
                del quotes['revise_type']
            if 'revise_count' in quotes:
                del quotes['revise_count']
            if 'quote_category' in quotes:
                del quotes['quote_category']
            if 'shipping' in quotes:
                del quotes['shipping']
            # if 'quote_no' in quotes:
            #     del quotes['quote_no']
            
            if 'valid_till' in quotes:
                del quotes['valid_till']
                
            if 'revise_type' in quotes:
                del quotes['revise_type']

            if 'revise_count' in quotes:
                del quotes['revise_count']

            if 'quote_stage' in quotes:
                del quotes['quote_stage']

            if 'quote_category' in quotes:
                del quotes['quote_category']
            

            quotes['invoice_stage'] = ObjectId(invoice_stage)
            quotes['total'] = float(quotes['total'])

            quotes['quote_id'] = ObjectId(str(id))
            
            
            for quote in quotes:

                if quote!='quote_revised' and quote!='shipping':
                    settings[quote] = quotes[quote]
            settings['modify_date'] = datetime.datetime.utcnow
            u2 = Invoice(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'


    def quoteSubmit(org_id,user_id,quote_stage,data1,deal_id,no,revise_type):
        try:
            shopping_list = []
            settings = defaultdict(list)

            assigned_to = str(data1['assigned_to'])
            deal_id = str(deal_id)
            # quote_stage = str(quote_stage)
            
            data1['revise_type'] = revise_type
            data1['assigned_to'] = ObjectId(assigned_to)
            data1['quote_no'] = str(no)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['deal_id'] = ObjectId(deal_id)
            data1['quote_stage'] = ObjectId(quote_stage)
            data1['total'] = float(data1['total'])
            

            for data in data1:
                settings[data] = data1[data]
            
            

            u2 = Quote(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def invoiceSubmit(org_id,user_id,invoice_stage,data1,no):
        try:
            shopping_list = []
            settings = defaultdict(list)

            assigned_to = str(data1['assigned_to']) 
            invoice_stage = str(invoice_stage)
            
            data1['assigned_to'] = ObjectId(assigned_to)
            data1['invoice_no'] = str(no)
            data1['create_by'] = user_id
            data1['org_id'] = org_id
            data1['invoice_stage'] = ObjectId(invoice_stage)
            data1['total'] = float(data1['total'])
            

            for data in data1:
                settings[data] = data1[data]
            
            

            u2 = Invoice(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def insertPDFSettings(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)
            user_id = str(user_id)
            data1['org_id'] = org_id
            for data in data1:
                settings[data] = data1[data]
            u2 = PdfSettings(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'
    

    def insertFieldSettings(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)
            user_id = str(user_id)
            data1['org_id'] = org_id
            for data in data1:
                settings[data] = data1[data]
            u2 = Fields(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'


    def roleSubmit(org_id,user_id,data1):
        try:
            shopping_list = []
            settings = defaultdict(list)

            user_id = str(user_id)
            data1['create_by'] = ObjectId(user_id)
            data1['org_id'] = org_id
            for data in data1:
                settings[data] = data1[data]
            u2 = Role(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'


    def roleUpdate(org_id,data1,id):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1['modify_date'] = datetime.datetime.utcnow
            for data in data1:
                settings[data] = data1[data]
            response = Role.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'
    

    def quoteUpdateStage(org_id,id,quote_stage):
        try:
            modify_date = datetime.datetime.utcnow
            quote_stage = ObjectId(quote_stage)
            # print (quote_stage)
            
            response = Quote.objects(org_id=org_id,id=ObjectId(id)).update_one(modify_date=modify_date,quote_stage=quote_stage)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'
    def quoteUpdateReviseCount(org_id,id,count):
        try:
            # modify_date = datetime.datetime.utcnow
            revise_count = count
            # print (quote_stage)
            
            response = Quote.objects(org_id=org_id,id=ObjectId(id)).update_one(revise_count=revise_count)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def quoteUpdate(org_id,user_id,data1,id):
        try:
       
            settings = defaultdict(list)
            data1['modify_date'] = datetime.datetime.utcnow
            data1['total'] = float(data1['total'])
            for data in data1:
                settings[data] = data1[data]
            response = Quote.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def invoiceUpdate(org_id,user_id,data1,id):
        try:
           
            settings = defaultdict(list)
            data1['modify_date'] = datetime.datetime.utcnow
            data1['total'] = float(data1['total'])
            for data in data1:
                settings[data] = data1[data]
            response = Invoice.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    def getRolesCountData(org_id):
        try:
            role_total = Role.objects(org_id=org_id).count()
            return role_total
        except NotUniqueError as e:
            return '0'

    def getRolesList(ordId,length,page,search,sort,order):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if search :
                filter1['role_name__contains'] = search
                # filter['name__contains'] = search
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match}
                        ]
           
            settings = defaultdict(list)

            role = Role.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getRolesListDetails(ordId,id):
        try:
           
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$match'  : match}
                        ]
           
            settings = defaultdict(list)
            # print (pipeline)
            role = Role.objects.aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['createBy']:
                        settings['createBy']=''
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'

    def newCompany(org_id):
        try:
            # Contact / Company
            source = {'Direct Mail','Facebook','Linked In','Referral','Whatsapp','Indiamart','Cold Call','Trade India','Trade Fair'}
            tag = {'Working - Contacted','Interested','Not Qualified','Future Requirement','No Response'}
            customer_type = {'End Customer','Partner','Distributor','OEM'}
            application = {'Application 1','Application 2'}
            industry = {'Agriculture And Allied Industries','Automobiles','Auto Components','Aviation','Banking','Biotechnology','Cement','Chemicals','Consumer Durables','Defence Manufacturing','E-commerce','Education And Training','Electronics System Design & Manufacturing','Engineering And Capital Goods','Financial Services','Fmcg','Gems And Jewellery','Healthcare','Infrastructure','Insurance','It & Bpm','Manufacturing','Media And Entertainment','Medical Devices','Metals And Mining','Msme','Oil And Gas','Pharmaceuticals','Ports','Power','Railways','Real Estate','Renewable Energy','Retail','Roads','Science And Technology','Services','Steel','Telecommunications','Textiles','Tourism And Hospitality'}
            # Deal
            stage = {'New Opportunity','Quote Sent','Expect (50%)','Negotiation (70%)','Commit (90%)','Order in hand (98%)','Won','Lost'}
            deal_stage = {'New Opportunity','Order Confirmed','Looking for low cost','PO Received','Waiting for PO','Need Technical Support','Installation Required','Decision pending with customer','Won','Lost'}
            deal_stage_weightage = {0,10,20,30,40,50,60,70,100,90}
            won_reason = {'Brand Name','Repeat business','Quick Delivery','Referred By Existing Customer'}
            lost_reason = {'Price is High','Project Postponed','Budget not available','Old Enquiries','No Response'}
            # Product & Tax
            product_category= {'Category A','Category B'}
            tax_type= {'SGST','CGST','IGST','UGST'}
            tax_percentage = {'9','18','12','6'}
            currency ={'INR','USD','EURO','AUD','GBP','MYR','AED','QR',}
            # Quote
            quote_stage = {'Created','Sent','Revised','Won','Lost'}
            category= {'Sales','Service'}
            validity= {'30 Days','15 Days','60 Days','1 Week'}
            shipping= {'By Road','Air Cargo','By Sea','Deliver by hand','Customer Pickup'}
            payment_terms= {'100% Advance','70 % advance 30% before dispatch','50% Advance with PO & Balance 50% before dispatch','Against Delivery','Net 30 Days'}
            # Proforma & Invoice
            invoice_stage = {'Created','Sent','Won','Lost'}
            proforma_stage = {'Created','Sent','Won','Lost'}
            # print (source)
            settings = {'source':source,'tag':tag,'customer_type':customer_type,'application':application,'industry':industry,'stage':stage,'deal_stage':deal_stage,'won_reason':won_reason,'lost_reason':lost_reason,'product_category':product_category,'tax_type':tax_type,'tax_percentage':tax_percentage,'currency':currency,'quote_stage':quote_stage,'category':category,'validity':validity,'shipping':shipping,'payment_terms':payment_terms,'invoice_stage':invoice_stage,'proforma_stage':proforma_stage}
            
            for key in settings:
                settings1 = settings[key]
                for key2 in settings1:
                    count = MongoAPI.fieldsSettingsCount(key2,org_id)
                    response = MongoAPI.addSettings(key2,settings1[key2],0,'',org_id,count,default=0)

            return response
        except NotUniqueError as e:
            return '0'

    def getProducts(ordId,length,page,search_string,category,sort_by,date_from,date_to,status):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search_string :
            #     filter1['product_name__contains'] = search_string
                # filter['name__contains'] = search_string
            if status!='all' and status!='' :
                filter['status'] = status
            if category :
                filter['category'] = ObjectId(category)
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.categoryLookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'product_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'model_no': { "$regex": search_string, "$options" :'i'}}, 
                                {'hsn_code': { "$regex": search_string, "$options" :'i'}}, 
                                {'description': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.products_project}
                        ]
            
            settings = defaultdict(list)

            role = Product.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='category_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['category_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['category_name']:
                        settings['category_name']=''
                    if not settings['createBy']:
                        settings['createBy']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getProductsListData(ordId,length,page,search_string,category,sort,order,status):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search_string :
            #     filter1['product_name__contains'] = search_string
                # filter['name__contains'] = search_string
            if category :
                filter['category'] = ObjectId(category)
            if status!='all' and status!='' :
                filter['status'] = status
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.categoryLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'product_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'model_no': { "$regex": search_string, "$options" :'i'}}, 
                                {'hsn_code': { "$regex": search_string, "$options" :'i'}}, 
                                {'description': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.products_project}
                        ]
            
            settings = defaultdict(list)

            role = Product.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='category_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['category_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['category_name']:
                        settings['category_name']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['path']:
                        settings['path'] = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_PRODUCT_FOLDER) 

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def productsListPricelist(ordId,length,page,search_string,category,sort,order,status,pricelist_id):
        try:

            Products = MongoAPI.getPricelistProductsList(ordId,sort,order,pricelist_id)
            existing_list=[]
            for productData in Products:
                existing_list.append(ObjectId(productData['product_id']))
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)   
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = {'$nin': existing_list}
            if category :
                filter['category'] = ObjectId(category)
            filter['status'] = 'active'
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.categoryLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'product_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'model_no': { "$regex": search_string, "$options" :'i'}}, 
                                {'hsn_code': { "$regex": search_string, "$options" :'i'}}, 
                                {'description': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.products_project}
                        ]
            
            settings = defaultdict(list)

            role = Product.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='category_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['category_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['category_name']:
                        settings['category_name']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['path']:
                        settings['path'] = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_PRODUCT_FOLDER) 

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getAccountsListPriceList(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            existing_list=[]
            existing_list.append(pricelist_id)
            filter['pricelist'] = {'$nin': existing_list}
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['company_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            # if source:
            #     filter['source'] = ObjectId(source)
            if customer_type:
                if customer_type!='all':
                    filter['customer_type'] = ObjectId(customer_type)
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)
            role = Account.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    else:
                        data = {item1:item4}
                 
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getAccountsListPriceList1(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user,pricelist_id):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            existing_list=[]
            existing_list.append(pricelist_id)
            filter['pricelist'] = {'$nin': existing_list}
            if owner :
                filter['assigned_to'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['company_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            # if source:
            #     filter['source'] = ObjectId(source)
            if customer_type:
                if customer_type!='all':
                    filter['customer_type'] = ObjectId(customer_type)

            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'company_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            # {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)
            role = Account.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='_id':
                        assigned = str(item[item1])
                        settings['_id'] = assigned
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    else:
                        data = {item1:item4}
                 
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getProductDetails(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
           

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.categoryLookup},
                            {'$match'  : match},
                            {'$project': app_config.products_project}
                        ]

            settings = defaultdict(list)

            role = Product.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='category_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['category_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['category_name']:
                        settings['category_name']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['path']:
                        settings['path'] = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_PRODUCT_FOLDER)+str(ordId)+'/'

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'
    def getDealDetails(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_detail_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
           
            settings = defaultdict(list)

            role = Deal.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    # elif item1=='contact_detail':
                    #     assigned = item[item1]
                    #     for item2 in assigned:
                    #         settings['contact_detail'] = Uid.fix_array3(item2['name'])
                    if item1=='create_date':
                        create_date1 = item[item1]
                        # create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(MongoAPI.getUserDateFormat(ordId))
                        create_date_age = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date_age)
                    if item1=='modify_date':
                        modify_date1 = item[item1]
                        # create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(MongoAPI.getUserDateFormat(ordId))
                        settings['target_date'] = target_date
                        #  if item1=='target_date':
                        # target_date = item[item1]
                        # target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT).strftime(MongoAPI.getUserDateFormat(ordId))
                        # settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''

                settings6.append(settings)
            
     

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getQuotesCount(ordId,id):

        # try:         
        #     count = Quote.objects.filter(quote_revised=id).count()
        #     return count
        # except NotUniqueError as e:
        #     return ''
        

        try:
           
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if stage:
            filter['quote_stage'] = ObjectId(id)
            
            # print (filter)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print (filter)
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.quote_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.quote_count_project},
                            {'$group' : {   '_id'  : "$quote_stage_lookup.name" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            }
                        ]
            settings = defaultdict(list)
            role = Quote.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='total':
                        # print (item[item1])
                        settings[item1] = TokenRefresh.human_format(item[item1])
                    # elif item1=='_id':
                    #     string=''
                    #     for x in item[item1]:
                    #         string+= str(x)
                    #     # settings['stage'] = item[item1]
                    #     settings['stage_name'] = string

                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings))
            # settings2=''
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getProformaCount(ordId,stage):
        try:
           
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if stage:
            filter['proforma_stage'] = ObjectId(stage)
            
            # print (filter)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print (filter)
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.proforma_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.proforma_count_project},
                            {'$group' : {   '_id'  : "$proforma_stage_lookup.name" ,
                                            'total' : { "$sum": "$total" },
                                            'count': {"$sum":1}
                                        }
                            }
                        ]
            settings = defaultdict(list)
            role = Proforma.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='total':
                        # print (item[item1])
                        settings[item1] = TokenRefresh.human_format(item[item1])
                    # elif item1=='_id':
                    #     string=''
                    #     for x in item[item1]:
                    #         string+= str(x)
                    #     # settings['stage'] = item[item1]
                    #     settings['stage_name'] = string

                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings))
            # settings2=''
            return settings2
        except NotUniqueError as e:
            return '0'


    def getProformas(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort_by,current_user):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if search :
                filter1['subject__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['proforma_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            if stage:
                filter['proforma_stage'] = ObjectId(stage)
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.proforma_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.proforma_project}
                        ]
           
            settings = defaultdict(list)

            role = Proforma.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='proforma_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['proforma_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def proformasUpdate(org_id,user_id,data1,id):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1['modify_date'] = datetime.datetime.utcnow
            data1['total'] = float(data1['total'])
            for data in data1:
                settings[data] = data1[data]
            response = Proforma.objects(org_id=org_id,id=ObjectId(id)).update_one(**settings)
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'

    


    def getDelasQuotesList(settingsData):
        try:
            
            ordId = settingsData['ordId']
            length = settingsData['length']
            page = settingsData['page']
            search = settingsData['search']
            assigned_to = settingsData['assigned_to']
            stage = settingsData['stage']
            sort = settingsData['sort']
            order = settingsData['order']
            associate_id = settingsData['associate_id']
            associate_to = settingsData['associate_to']

            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if associate_to=='contact':
            #     filter['contact_id'] = ObjectId(associate_id)
            # elif associate_to=='company':
            #     filter['company_id'] = ObjectId(associate_id)
            if search :
                filter1['deal_name__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            if stage:
                filter['stage'] = ObjectId(stage)

            if associate_to=='deal':
                filter['deal_id'] = ObjectId(associate_id)
            elif associate_to=='contact':
                filter['contact_id'] = ObjectId(associate_id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(associate_id)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getDealsCount1(settingsData):
        try:
            
            ordId = settingsData['ordId']
            length = settingsData['length']
            page = settingsData['page']
            search = settingsData['search']
            assigned_to = settingsData['assigned_to']
            stage = settingsData['stage']
            sort = settingsData['sort']
            order = settingsData['order']
            associate_id = settingsData['associate_id']
            associate_to = settingsData['associate_to']

            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if associate_to=='contact':
                filter['contact_id'] = ObjectId(associate_id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(associate_id)
            if search :
                filter1['deal_name__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            if stage:
                filter['stage'] = ObjectId(stage)

            if associate_to=='deal':
                filter['deal_id'] = ObjectId(associate_id)
            elif associate_to=='contact':
                filter['contact_id'] = ObjectId(associate_id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(associate_id)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            count=len(settings2)
            return count
        except NotUniqueError as e:
            return '0'




    def getQuotesList(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user,quote_category):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['subject__contains'] = search

                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['quote_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
                if stage!='active':
                    filter['quote_stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['quote_stage']={"$in":other_stage_list}
            if quote_category :
                filter['quote_category'] = quote_category
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])

            match = filter
            print(match)
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'quote_no': { "$regex": search, "$options" :'i'}}, 
                                {'subject': { "$regex": search, "$options" :'i'}}, 
                                {'company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact_name': { "$regex": search, "$options" :'i'}}, 
                                {'stage_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            }},
                            {'$limit' : length },
                            {'$project': app_config.quote_project},
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getQuotesListAssociate(ordId,length,page,sort,order,id,associate_to):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if associate_to=='contact':
                filter['contact_id'] = ObjectId(id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
     
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getQuotes(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort_by,current_user,quote_category):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['subject__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['quote_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
                if stage!='active':
                    filter['quote_stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['quote_stage']={"$in":other_stage_list}
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            if quote_category :
                filter['quote_category'] = quote_category
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {'create_date' : -1}},
                            {'$match'  : match},
                             {'$match':{
                                '$or': [
                                {'quote_no': { "$regex": search, "$options" :'i'}}, 
                                {'subject': { "$regex": search, "$options" :'i'}}, 
                                {'company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact_name': { "$regex": search, "$options" :'i'}}, 
                                {'stage_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            }},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    

    def getQuoteDetailsCount(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            for item3 in item2:
                                if item3=='_id':
                                    contact_key = str('contact')+str(item3)
                                else:
                                    contact_key = str('contact_')+str(item3)
                                settings[contact_key] = item2[item3]
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''

                settings6.append(settings)
            
     

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getFullQuoteDetails(ordId,id):

        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$match'  : match},
                           
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name'] 

                    if item1=='create_date':
                        assigned = item[item1]
                        settings[item1] = assigned
                                         
                    else:
                        data = {item1:item4}

                settings6.append(settings)
            
     

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getFullProformaDetails(ordId,id):

        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$match'  : match}
                        ]
           
            settings = defaultdict(list)

            role = Proforma.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name'] 

                    if item1=='create_date':
                        assigned = item[item1]
                        settings[item1] = assigned
                                         
                    else:
                        data = {item1:item4}

                settings6.append(settings)
            
     

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getQuoteDetails(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='deal':
                        stage=MongoAPI.getFieldsName(ordId,item[item1][0]['stage'])
                        settings[item1][0]['deal_stage_name']=stage
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            for item3 in item2:
                                if item3=='_id':
                                    contact_key = str('contact')+str(item3)
                                else:
                                    contact_key = str('contact_')+str(item3)
                                # print (contact_key)
                                settings[contact_key] = item2[item3]
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='currency':
                        assigned = item[item1]
                        if assigned=='INR':
                            settings['currency_code'] = currency_code['inr']
                        elif assigned=='USD':
                            settings['currency_code'] = currency_code['usd']
                        elif assigned=='EURO':
                            settings['currency_code'] = currency_code['euro']

                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    if item1=='shipping_charges':
                        settings['shipping_charges']=(format(float(item[item1]),".2f"))
                    if item1=='installation_charges':
                        settings['installation_charges']=(format(float(item[item1]),".2f"))
                    if item1=='packing_amount':
                        settings['packing_amount']=(format(float(item[item1]),".2f"))
                    if item1=='discount_amount':
                        settings['discount_amount']=(format(float(item[item1]),".2f"))
                    if item1=='tax_amount1':
                        settings['tax_amount1']=(format(float(item[item1]),".2f"))
                    if item1=='tax_amount2':
                        settings['tax_amount2']=(format(float(item[item1]),".2f"))
                    if item1=='total':
                        settings['total']=(format(float(item[item1]),".2f"))
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''

                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getInvoiceDetails(ordId,id):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.invoice_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.invoice_project}
                        ]
           
            settings = defaultdict(list)

            role = Invoice.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            for item3 in item2:
                                if item3=='_id':
                                    contact_key = str('contact')+str(item3)
                                else:
                                    contact_key = str('contact_')+str(item3)
                                # print (contact_key)
                                settings[contact_key] = item2[item3]
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='invoice_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['invoice_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''

                settings6.append(settings)

            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getDealStageCount(ordId,stage):
        try:
            filter = {}
            key = []
            val = []
                
            filter['org_id'] = ordId
            filter['stage'] = ObjectId(stage)
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_count_project},
                            {'$group' : {   '_id'  : "$stage_name.name" ,
                                            'total' : { "$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            }
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='_id':
                        string=''
                        for x in item[item1]:
                            string+= str(x)
                        settings['stage_name'] = string
                    elif item1=='total':
                        settings[item1] = TokenRefresh.human_format(item[item1])
                    elif item1=='count':
                        assigned = item[item1]
                    else:
                        data = {item1:item4}
                if not settings['total']:
                    settings['total']='0'
                if not settings['count']:
                    settings['count']='0'
                # settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getDealsStage(ordId,length,skip,search,assigned_to,stage,tag,date_from,date_to,sort_by):
        try:
            filter = {}
            key = []
            val = []

            if skip:
                skip = skip
            else:
                skip = 0

            filter1 = {}
                
            filter['org_id'] = ordId
            if search :
                filter1['deal_name__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            if stage:
                filter['stage'] = ObjectId(stage)
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$skip'   : skip},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getUserDateFormat(org_id):

        organization = MongoAPI.organizationInfo(org_id)
        organization = Uid.fix_array1(organization)

        if organization['date_format']=="mm-dd-yy":
            return USER_DATE_FORMAT1
        elif organization['date_format']=="dd-mm-yyyy":
            return USER_DATE_FORMAT2
        elif organization['date_format']=="mm-dd-yyyy":
            return USER_DATE_FORMAT3
    

    def getDeals(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort_by,current_user,city,state,country):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['deal_name__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
                if stage!='active':
                    filter['stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['stage']={"$in":other_stage_list}
            if tag:
                filter['tag'] = tag
            if city:
                filter['city'] = city
            if state:
                filter['state'] = state
            if country:
                filter['country'] = country
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$lookup' : app_config.contact_detail_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                              {'$match':{
                                '$or': [
                                {'deal_name': { "$regex": search, "$options" :'i'}}, 
                                {'account.company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact.contact_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date1 = datetime.datetime.strptime(str(target_date), '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                        settings['target_date'] = target_date1
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getSalesProcess(ordId):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            # {'$lookup' : app_config.salesProcessLookup},
                            {'$match'  : match},
                            {'$project': app_config.sales_process_project}
                        ]
            
            settings = defaultdict(list)

            role = SalesProcess.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getSalesProcessDetail(ordId,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.sales_process_project}
                        ]
            
            settings = defaultdict(list)

            role = SalesProcess.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    settings['sub_process']=MongoAPI.getSalesSubProcessList(ordId,id)
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getSalesSubProcess(ordId):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup' : app_config.processLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.sales_sub_process_project}
                        ]
            
            settings = defaultdict(list)

            role = SalesSubProcess.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getSalesSubProcessDetail(ordId,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup' : app_config.processLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.sales_sub_process_project}
                        ]
            
            settings = defaultdict(list)

            role = SalesSubProcess.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getSalesSubProcessList(ordId,parent_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['parent_id'] = ObjectId(parent_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$lookup' : app_config.processLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.sales_sub_process_project}
                        ]
            
            settings = defaultdict(list)

            role = SalesSubProcess.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            settings6 = Uid.fix_array_sub(settings6)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getSubCheckList(ordId,associate_id,associate_to,parent_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['associate_id'] = ObjectId(associate_id)
            filter['associate_to'] = associate_to
            filter['parent_id'] = parent_id
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            # print(match)    
            pipeline = [
                            {'$lookup' : app_config.checklistLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$match'  : match},
                        
                    ]
            
            settings = defaultdict(list)

            role = DealsSubCheckList.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        if len(assigned):
                            for item2 in assigned:
                                if item2['name']:
                                    settings[item1] = item2['name']
                                else:
                                    settings[item1] = ''
                        else:
                            settings[item1] = ''
                    if item1=='create_date':
                        create_date1 = item[item1]
                        # print(item[item1])
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'

    def getCheckList(ordId,associate_id,associate_to):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['associate_id'] = ObjectId(associate_id)
            filter['associate_to'] = associate_to
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            # print(match)    
            pipeline = [
                            # {'$lookup' : app_config.checklistLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$match'  : match},
                            # {'$project': app_config.checklist_project}
                       ]
            
            settings = defaultdict(list)

            role = DealsCheckList.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # settings['sub_checkList'] = []
                for item1 in item:
                    # print(item1)
                    settings[item1] = item[item1]
                    
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        if len(assigned):
                            for item2 in assigned:
                                if item2['name']:
                                    settings[item1] = item2['name']
                                else:
                                    settings[item1] = ''
                        else:
                            settings[item1] = ''

                        

                        
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='_id':
                        settings['sub_checklist']=MongoAPI.getSubCheckList(ordId,associate_id,associate_to,item[item1])
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

        

    def getSalesProcessTemplate(ordId):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$match'  : match},
                        ]
            
            settings = defaultdict(list)

            role = SalesProcessTemplate.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
           
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getSalesProcessTemplateDetail(ordId,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$match'  : match},
                       ]
            
            
            settings = defaultdict(list)

            role = SalesProcessTemplate.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    # if item1=='process_list':
                    #     process_list=[]
                    #     for pl in item[item1]:
                    #         process_data=MongoAPI.getSalesProcessDetail(ordId,pl)
                    #         for p in process_data:
                    #             process_list.append(p)
                    #     settings['process_list']=process_list
                            
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            # print(settings6)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getSalesProcessTemplateDefault(ordId):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['default'] = 1
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$match'  : match},
                       ]
            settings = defaultdict(list)

            role = SalesProcessTemplate.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                          
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            # print(settings6)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealStageMapping(ordId,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['associate_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$match'  : match},
                       ]
            
            settings = defaultdict(list)

            role = DealsStageMapping.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                          
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        settings[item1]=item[item1]
                # print(settings)
                settings6.append(settings)
            
            # print(settings6)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealCheckList(ordId,deal_id,id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['process_id'] = ObjectId(id)
            filter['associate_id'] = ObjectId(deal_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            pipeline = [
                            {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = DealsCheckList.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                          
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    else:
                        settings[item1]=item[item1]
                # print(settings)
                settings6.append(settings)
            
            # print(settings6)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getUnnoticed(ordId,user_id,from_date):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            
            yesterday = from_date - datetime.timedelta(days = 3)
            # print (yesterday)
            filter['modify_date'] = {"$lte": yesterday}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    

    def getOverdue(ordId,user_id,from_date):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            
            yesterday = from_date - datetime.timedelta(days = 1)
            # print (yesterday)
            filter['target_date'] = {"$lte": yesterday}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getOpenDeals(ordId,user_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    
    

    def openDealsCount(ordId,user_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['deal_status'] = 'open'
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]   
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            if role:
                settings6=[]
                count = 0
                deal_data=list(role)
                t=len(deal_data)
                if t > 0:
                    for item in deal_data:
                        if item['count'] and item['total']:
                            count = int(item['count'])
                            total = int(item['total'])
                        else:
                            count = 0
                            total = 0
                else:
                    count = 0
                    total = 0
                return {'count':count,'total':total}
            else:
                return {'count':0,'total':0}
        except NotUniqueError as e:
            return '0'

    def createdThisMonthDealsCount(ordId,user_id,search_time,nextday):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]   
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            if role:
                settings6=[]
                count = 0
                deal_data=list(role)
                t=len(deal_data)
                if t > 0:
                    for item in deal_data:
                        if item['count'] and item['total']:
                            count = int(item['count'])
                            total = int(item['total'])
                        else:
                            count = 0
                            total = 0
                else:
                    count = 0
                    total = 0
                return {'count':count,'total':total}
            else:
                return {'count':0,'total':0}
            
        except NotUniqueError as e:
            return '0'

    def wonDealsCount(ordId,user_id,search_time,nextday,list_type):
        try:
            
            filter = {}
            filter1 = {}
            key = []
            val = []

            won_stage=MongoAPI.getWonLostDealStages('deal_stage',ordId,'won')
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['stage'] = ObjectId(won_stage)
            if list_type == 'this_month':
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
           
            match = filter
            # print(match)
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]   
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            if role:
                settings6=[]
                count = 0
                deal_data=list(role)
                t=len(deal_data)
                if t > 0:
                    for item in deal_data:
                        if item['count'] and item['total']:
                            count = int(item['count'])
                            total = TokenRefresh.human_format(item['total'])
                        else:
                            count = 0
                            total = 0
                else:
                    count = 0
                    total = 0
                return {'count':count,'total':total}
            else:
                return {'count':0,'total':0}
            
        except NotUniqueError as e:
            return '0'

    def lostDealsCount(ordId,user_id,search_time,nextday,list_type):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            lost_stage=MongoAPI.getWonLostDealStages('deal_stage',ordId,'lost')
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            filter['stage'] = ObjectId(lost_stage)
            if list_type == 'this_month':
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : {"$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1,'total':1}}
                        ]   
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            if role:
                settings6=[]
                count = 0
                deal_data=list(role)
                t=len(deal_data)
                if t > 0:
                    for item in deal_data:
                        if item['count'] and item['total']:
                            count = int(item['count'])
                            total = TokenRefresh.human_format(item['total'])
                        else:
                            count = 0
                            total = 0
                else:
                    count = 0
                    total = 0
                return {'count':count,'total':total}
            else:
                return {'count':0,'total':0}
            
        except NotUniqueError as e:
            return '0'

    def getEndOpenDealsData(ordId ,length,page,user_id,sort,order,search_time,nextday):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won,Revised'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            filter['target_date'] = {"$gte": search_time,"$lte": nextday}
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getEndOpenDealsDataCount(ordId,user_id,search_time,nextday):
        try:
            
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won,Revised'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            filter['target_date'] = {"$gte": search_time,"$lte": nextday}
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$group' : {
                                            '_id'  : "org_id" ,
                                            'total' : { "$sum": "$org_id" },
                                            'count': {"$sum":1}
                                        }
                            },
                            {'$project': {'count':1}}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            count = 0
            for item in role:
               count = int(item['count'])
            return count
        except NotUniqueError as e:
            return '0'

    def overdueDealsData(ordId ,length,page,user_id,sort,order,search_time):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            filter['target_date'] = {"$lt": search_time}
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
          
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def unnoticedDealsData(ordId ,length,page,user_id,sort,order,search_time):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)

            yesterday = search_time - datetime.timedelta(days = 5)
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won,Revised'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            yesterday = search_time - datetime.timedelta(days = 3)
            filter['modify_date'] = {"$lte": yesterday}
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getOpenQuotesData(ordId,length,page,user_id,sort,order):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1)      
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            # stage1 = 'Won,Lost,lost,won'
            # stage = stage1.split(',')
            other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
            filter['quote_stage'] = {'$in': other_stage_list}
            match = filter
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.quote_project}
                        ]
            
            settings = defaultdict(list)

            role = Quote.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
            
    def getOpenQuotes(ordId,user_id):
        try:
          
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if user_id :
            #     filter['assigned_to'] = ObjectId(user_id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
           
            settings = defaultdict(list)

            role = Quote.objects.aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getOpenQuotes111(ordId,user_id):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['assigned_to'] = ObjectId(user_id)
            stage1 = 'Won,Lost,lost,won'
            stage = stage1.split(',')
            
            filter['stage_name.name'] = {'$nin': stage}

            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            sort_by = 'create_date'

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            
            pipeline = [
                        {'$lookup' : app_config.ownerLookup},
                        {'$lookup' : app_config.user_lookup},
                        {'$lookup' : app_config.account_lookup},
                        {'$lookup' : app_config.contact_lookup},
                        {'$lookup' : app_config.deal_lookup},
                        {'$lookup' : app_config.quote_stage_lookup}, 
                        {'$sort'   : {sort_by : order_dict}},
                        {'$match'  : match},
                        {'$project': app_config.quote_project}
                    ]
            
            settings = defaultdict(list)

            role = Quote.objects.aggregate(*pipeline)

            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']

                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']


                        settings['country_id'] = MongoAPI.getCountryANDStateId(country,'country')
                        settings['state_id'] = MongoAPI.getCountryANDStateId(state,'state')

                        settings['shipping_country_id'] = MongoAPI.getCountryANDStateId(shipping_country,'country')
                        settings['shipping_state_id'] = MongoAPI.getCountryANDStateId(shipping_state,'state')


                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    if not settings['date_aging']:
                        settings['date_aging']=''
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealsListData(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort,order,id,associate_to):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if associate_to=='contact':
                filter['contact_id'] = ObjectId(id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(id)
            if search :
                filter1['deal_name__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            if stage:
                filter['stage'] = ObjectId(stage)
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getDealsListDataDb(ordId,length,page,sort,order,id,associate_to):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId

            if associate_to=='contact':
                filter['contact_id'] = ObjectId(id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            # print (match)
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    def getOtherDealStages(type,org_id):
        stages=MongoAPI.fieldsSettingsData(type,org_id)
        # print(stages)
        settings = defaultdict(list)
        stage_list=[]
        for stage_data in stages:
            if stage_data['name']!='WON' and stage_data['name']!='LOST' and stage_data['name']!='Won' and stage_data['name']!='Lost' and stage_data['name']!='won' and stage_data['name']!='lost' and stage_data['name']!='Revised':
                this_id=str(stage_data['_id']['$oid'])
                stage_list.append(ObjectId(this_id))
        return stage_list

    def getWonLostDealStages(type,org_id,stage_type):
        stages=MongoAPI.fieldsSettingsData(type,org_id)
        # print(stages)
        settings = defaultdict(list)
        won_stage=''
        lost_stage=''
        for stage_data in stages:
            if stage_data['name']=='WON' or stage_data['name']=='Won' or stage_data['name']=='won':
                won_stage=str(stage_data['_id']['$oid'])
            if stage_data['name']=='LOST' or stage_data['name']=='Lost' or stage_data['name']=='lost':
                lost_stage=str(stage_data['_id']['$oid'])
        if stage_type == 'won':
            return won_stage
        elif stage_type == 'lost':
            return lost_stage
    
    def getOtherProformaStages(type,org_id):
        stages=MongoAPI.fieldsSettingsData(type,org_id)
        # print(stages)
        settings = defaultdict(list)
        stage_list=[]
        for stage_data in stages:
            if stage_data['name']!='WON' and stage_data['name']!='LOST' and stage_data['name']!='Won' and stage_data['name']!='Lost' and stage_data['name']!='won' and stage_data['name']!='lost' and stage_data['name']!='Completed' and stage_data['name']!='completed' and stage_data['name']!='COMPLETED':
                this_id=str(stage_data['_id']['$oid'])
                stage_list.append(ObjectId(this_id))
        return stage_list
    

    def getSettingsByName(type,org_id,name):
        stages=MongoAPI.fieldsSettingsData(type,org_id)
        # print(stages)
        settings = defaultdict(list)
        for stage_data in stages:
            if stage_data['name']==name:
                result=str(stage_data['_id']['$oid'])
                return result

    def insertDemoData(org_id,user_id):
        Organization.objects(org_id=org_id).update_one(demo_data_set='0')
        # Get Product Category
        product_category = MongoAPI.getSettingsByName('product_category',org_id,'Category A')
        # Insert Product
        product_array1={
            "product_id": "af6f6230-c5dc-11eb-8f74-e74b69a17044","org_id": org_id,"product_name": "Beats Studio Heaphones",
            "model_no": "XB900N",
            "hsn_code": "510510",
            "uom": "",
            "images": [
                ["61vz89Or3HL_05_2021_14_34_47.jpg"],
                ["51jHg2I2EnL_05_2021_14_36_05.jpg"],
                ["51xViTHCV2L_05_2021_14_36_05.jpg"],
                ["711ydwKnUuL_05_2021_14_36_19.jpg"]
            ],
            "default_image": "https://farazon.com:8444/uploads/product/103/61vz89Or3HL_05_2021_14_34_47.jpg",
            "description": "High Performance wireless noise canceling&nbsp; headphones",
            "cgst": "6",
            "sgst": "6",
            "igst": "12",
            "category": ObjectId(product_category),
            "price": "1000",
            "status": "active",
            "create_by":ObjectId(user_id),
            "demo_data":'true'

        }
        product_data_set = defaultdict(list)
        for data in product_array1:
            product_data_set[data] = product_array1[data]
        u2 = Product(**product_data_set)
        u2.save()
        product_array2={
        "product_id": "58935d8e-c5de-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "product_name": "BenQ Bezeless Monitor",
        "model_no": "227U4PA",
        "hsn_code": "510510",
        "uom": "",
        "images": [
            ["71f8+16FLwL_05_2021_18_11_28.jpg"],
            ["713ddwwC9QL_05_2021_18_11_36.jpg"],
            ["51zf-g+Wo2L_05_2021_18_11_42.jpg"]
        ],
        "default_image": "https://farazon.com:8444/uploads/product/103/713ddwwC9QL_05_2021_18_11_36.jpg",
        "description": "HDRi: Intelligent clarity adjustment and brightness control plus emulated HDR make&nbsp; easy image stunning and clear",
        "cgst": "6",
        "sgst": "6",
        "igst": "12",
        "category":ObjectId(product_category),
        "price": "35000",
        "status": "active",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
       }
        product_data_set = defaultdict(list)
        for data in product_array2:
            product_data_set[data] = product_array2[data]
        u2 = Product(**product_data_set)
        u2.save()
        product_array3={
        "product_id": "f6eca04e-c5de-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "product_name": "Logitech Wireless Mouse Black",
        "model_no": "B170",
        "hsn_code": "510510",
        "uom": "KG",
        "images": [
            ["51kdFjyPRAL_05_2021_14_48_33.jpg"],
            ["613Ol2QWTyL_05_2021_14_48_43.jpg"],
            ["71q1vOVrIJL_05_2021_14_48_57.jpg"],
            ["61zdxYNIhBL_05_2021_14_49_10.jpg"]
        ],
        "default_image": "https://farazon.com:8444/uploads/product/103/51kdFjyPRAL_05_2021_14_48_33.jpg",
        "description": "Dual connectivity. Connect the way you like via Bluetooth wireless technology or via the included tiny USB receiver.Modern, slim and beautiful Pebble shape Logitech Pebble has stand-out simplicity with a design that is nice to hold, feels great in your hand and is easy to carry around",
        "cgst": "6",
        "sgst": "6",
        "igst": "12",
        "category": ObjectId(product_category),
        "price": "500",
        "status": "active",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
       }
        product_data_set = defaultdict(list)
        for data in product_array3:
            product_data_set[data] = product_array3[data]
        u2 = Product(**product_data_set)
        u2.save()
        product_array4={
        "product_id": "b4d88596-c5df-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "product_name": "Logitech Wireless Keyboard",
        "model_no": "K380 ",
        "hsn_code": "510510",
        "uom": "",
        "images": [
            ["617zMW8uyYL_05_2021_14_53_53.jpg"],
            ["711K9wz4mEL_05_2021_14_54_40.jpg"],
            ["711mg+W+DXL_05_2021_14_54_40.jpg"],
            ["71ML23qmvfL_05_2021_14_55_00.jpg"]
        ],
        "default_image": "https://farazon.com:8444/uploads/product/103/617zMW8uyYL_05_2021_14_53_53.jpg",
        "description": "SILENTTOUCH TECHNOLOGY: This Logitech MK295 wireless keyboard and mouse combo features advanced optical sensor and delivers the same typing and click experience with 90% less noise",
        "cgst": "6",
        "sgst": "6",
        "igst": "12",
        "category": ObjectId(product_category),
        "price": "3000",
        "status": "active",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
       }
        product_data_set = defaultdict(list)
        for data in product_array4:
            product_data_set[data] = product_array4[data]
        u2 = Product(**product_data_set)
        u2.save()
        product_array5={
        "product_id": "f4f3f96e-d4c4-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "product_name": "Microsoft Surface Go",
        "model_no": "S11003",
        "hsn_code": "510510",
        "uom": "",
        "images": [
            ["71b4rUzpaxL_24_2021_13_50_10.jpg"],
            ["61u8t1lcfaL_24_2021_13_50_26.jpg"],
            ["71tc8PYA-KL_24_2021_13_50_26.jpg"]
        ],
        "default_image": "https://farazon.com:8444/uploads/product/103/71b4rUzpaxL_24_2021_13_50_10.jpg",
        "description": "Processor: Intel Pentium Gold Processor 4425Y gives your daily tasks and apps an added boost.Operating System: It comes with Windows 10 Home in S mode to bring you the powerful Windows features you use most at exceptional value.Memory &amp; Storage : 8GB DDR3 RAM with Intel UHD Graphics 615 Graphics| Storage: 128GB SSD",
        "cgst": "9",
        "sgst": "9",
        "igst": "18",
        "category":  ObjectId(product_category),
        "price": "65000",
        "status": "active",
        "create_by": ObjectId(user_id),
        "demo_data":'true'
        }
        product_data_set = defaultdict(list)
        for data in product_array5:
            product_data_set[data] = product_array5[data]
        u2 = Product(**product_data_set)
        u2.save()
        
        # Users.objects.order_by('-id').first()
        
        #Get source and customer type
        source = MongoAPI.getSettingsByName('source',org_id,'Cold Call')
        customer_type = MongoAPI.getSettingsByName('source',org_id,'End Customer')
        # Insert Company
        # company1 and contact 1
        company1={
        "org_id": org_id,
        "account_id": "833772",
        "company_name": "Digital Ocean Technologies",
        "phone": "9788892136",
        "email": "john@digitalocean.com",
        "assigned_to":ObjectId(user_id),
        "status": "",
        "gstin": "",
        "website": "",
        "source":ObjectId(source),
        "address1": "5587 Green Avenue",
        "address2": "",
        "country": "India",
        "state": "Tamil Nadu",
        "city": "Coimbatore",
        "pincode": "641026",
        "create_by":  ObjectId(user_id),
        "customer_type": ObjectId(customer_type),
        "demo_data":'true'
        }
        company1_id=''
        company_data_set = defaultdict(list)
        for data in company1:
            company_data_set[data] = company1[data]
        u2 = Account(**company_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_company = Account.objects().aggregate(*pipeline)
        for data in recent_company:
            company1_id=data['_id']

        contact1={
        "contact_id": 839094,
        "org_id": org_id,
        "name": "John Miller",
        "company_id":ObjectId(company1_id),
        "company_name": "Digital Ocean Technologies",
        "phone_number": "9788892136",
        "email": "john@digitalocean.com",
        "description": "Customer is interested in A Series products. He wants to see the technical spec of the product",
        "assigned_to":  ObjectId(user_id),
        "source":ObjectId(source),
        "pincode": "641035",
        "owner":ObjectId(user_id),
        "key_contact": 1,
        "demo_data":'true'
        }
        contact1_id=''
        contact_data_set = defaultdict(list)
        for data in contact1:
            contact_data_set[data] = contact1[data]
        u2 = Contact(**contact_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_contact = Contact.objects().aggregate(*pipeline)
        for data in recent_contact:
            contact1_id=data['_id']

        # Company 2 and contact 2 

        company2={
        "org_id": org_id,
        "account_id": "1f98ab52-d4e7-11eb-8f74-e74b69a17044",
        "company_name": "AimGlobal Technologies",
        "phone": "7200402290",
        "email": "aimglobal@india.com",
        "assigned_to":ObjectId(user_id),
        "status": "",
        "gstin": "",
        "website": "",
        "source":ObjectId(source),
        "address1": "1644 Mandan Road",
        "address2": "Tumkur Road",
        "country": "India",
        "state": "Madhya Pradesh",
        "city": "Baihar",
        "pincode": "600016",
        "create_by":  ObjectId(user_id),
        "customer_type": ObjectId(customer_type),
        "demo_data":'true'
        }
        company2_id=''
        company_data_set = defaultdict(list)
        for data in company2:
            company_data_set[data] = company2[data]
        u2 = Account(**company_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_company = Account.objects().aggregate(*pipeline)
        for data in recent_company:
            company2_id=data['_id']
            
        contact2={
        "contact_id": 839094,
        "org_id": org_id,
        "name": "Akshay Kumar",
        "company_id":ObjectId(company2_id),
        "company_name": "Acme India Private Limited",
        "phone_number": "7339333107",
        "email": "akshay@acmeindia.com",
        "description": "Customer is interested in A Series products. He wants to see the technical spec of the product",
        "assigned_to":  ObjectId(user_id),
        "source":ObjectId(source),
        "pincode": "641035",
        "owner":ObjectId(user_id),
        "key_contact": 1,
        "demo_data":'true'
        }
        contact2_id=''
        contact_data_set = defaultdict(list)
        for data in contact2:
            contact_data_set[data] = contact2[data]
        u2 = Contact(**contact_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_contact = Contact.objects().aggregate(*pipeline)
        for data in recent_contact:
            contact2_id=data['_id']

        # Company 3 and contact 3

        company3={
        "org_id": org_id,
        "account_id": "386cc810-d4ca-11eb-8f74-e74b69a17044",
        "company_name": "Hilton Tech Corp",
        "phone": "7373976000",
        "email": "info@hiltontech.com",
        "assigned_to":ObjectId(user_id),
        "status": "",
        "gstin": "",
        "website": "",
        "source":ObjectId(source),
        "address1": "1 Beacon Street,",
        "address2": " 33rd Floor",
        "country": "India",
        "state": "Kerala",
        "city": "Alappuzha",
        "pincode": "890677",
        "create_by":  ObjectId(user_id),
        "customer_type": ObjectId(customer_type),
        "demo_data":'true'
        }
        company3_id=''
        company_data_set = defaultdict(list)
        for data in company3:
            company_data_set[data] = company3[data]
        u2 = Account(**company_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_company = Account.objects().aggregate(*pipeline)
        for data in recent_company:
            company3_id=data['_id']
            
        contact3={
        "contact_id": 839095,
        "org_id": org_id,
        "name": "Mithun Shukla",
        "company_id":ObjectId(company3_id),
        "company_name": "Hilton Tech Corp",
        "phone_number": "9842152903",
        "email": "mithun@hiltontech.com",
        "description": "Customer is interested in A Series products. He wants to see the technical spec of the product",
        "assigned_to":  ObjectId(user_id),
        "source":ObjectId(source),
        "pincode": "641035",
        "owner":ObjectId(user_id),
        "key_contact": 1,
        "demo_data":'true'
        }
        contact3_id=''
        contact_data_set = defaultdict(list)
        for data in contact3:
            contact_data_set[data] = contact3[data]
        u2 = Contact(**contact_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_contact = Contact.objects().aggregate(*pipeline)
        for data in recent_contact:
            contact3_id=data['_id']
        # company 4 and contact 4 
        company4={
        "org_id": org_id,
        "account_id": "61648",
        "company_name": "Creative Technologies",
        "phone": "9842152903",
        "email": "alexa@creativetech.com",
        "assigned_to":ObjectId(user_id),
        "status": "",
        "gstin": "",
        "website": "www.creativecorp.com",
        "source":ObjectId(source),
        "address1": "106 A, Paradise Valley,",
        "address2": "Tumkur Road",
        "country": "India",
        "state": "Delhi",
        "city": "Karol Bāgh",
        "pincode": "600016",
        "create_by":  ObjectId(user_id),
        "customer_type": ObjectId(customer_type),
        "demo_data":'true'
        }
        company4_id=''
        company_data_set = defaultdict(list)
        for data in company4:
            company_data_set[data] = company4[data]
        u2 = Account(**company_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_company = Account.objects().aggregate(*pipeline)
        for data in recent_company:
            company4_id=data['_id']
            
        contact4={
        "contact_id": 839095,
        "org_id": org_id,
        "name": "Alexa Michael",
        "company_id":ObjectId(company4_id),
        "company_name": "Creative Technologies",
        "phone_number": "9842152706",
        "email": "alexa@creativetech.com",
        "description": "Customer interested to buy A Series products in 2 weeks",
        "assigned_to":  ObjectId(user_id),
        "source":ObjectId(source),
        "pincode": "641035",
        "owner":ObjectId(user_id),
        "key_contact": 1,
        "demo_data":'true'
        }
        contact4_id=''
        contact_data_set = defaultdict(list)
        for data in contact4:
            contact_data_set[data] = contact4[data]
        u2 = Contact(**contact_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_contact = Contact.objects().aggregate(*pipeline)
        for data in recent_contact:
            contact4_id=data['_id']

        # company 5 and contact 5
        company5={
        "org_id": org_id,
        "account_id": "249070",
        "company_name": "Acme Corp Private Limited",
        "phone": "9842152903",
        "email": "victoria@acmecorp.com",
        "assigned_to":ObjectId(user_id),
        "status": "",
        "gstin": "",
        "website": "",
        "source":ObjectId(source),
        "address1": "",
        "address2": "",
        "country": "",
        "state": "",
        "city": "",
        "pincode": "641035",
        "create_by":  ObjectId(user_id),
        "customer_type": ObjectId(customer_type),
        "demo_data":'true'
        }
        company5_id=''
        company_data_set = defaultdict(list)
        for data in company5:
            company_data_set[data] = company5[data]
        u2 = Account(**company_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_company = Account.objects().aggregate(*pipeline)
        for data in recent_company:
            company5_id=data['_id']
            
        contact5={
        "contact_id": 839095,
        "org_id": org_id,
        "name": "Victoria John",
        "company_id":ObjectId(company5_id),
        "company_name": "Acme Corp Private Limited",
        "phone_number": "9842152903",
        "email": "victoria@acmecorp.com",
        "description": "Customer asked B series products and interested to buy in 1 week",
        "assigned_to":  ObjectId(user_id),
        "source":ObjectId(source),
        "pincode": "641035",
        "owner":ObjectId(user_id),
        "key_contact": 1,
        "demo_data":'true'
        }
        contact5_id=''
        contact_data_set = defaultdict(list)
        for data in contact5:
            contact_data_set[data] = contact5[data]
        u2 = Contact(**contact_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_contact = Contact.objects().aggregate(*pipeline)
        for data in recent_contact:
            contact5_id=data['_id']
        
        deal_stage=MongoAPI.getSettingsByName('deal_stage',org_id,'New Opportunity')

        #deal 1 and Quote 1
        date=datetime.datetime.utcnow
        target_date=datetime.datetime.utcnow
        # target_date = date - datetime.timedelta(days = 1)
        deal1={
        "deal_id": "27341e3a-dfbb-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "deal_no": "DE/2",
        "deal_name": "Quote for Beats Studio Headphones",
        "currency": "INR",
        "contact_id": ObjectId(contact1_id),
        "company_id": ObjectId(company1_id),
        "amount": 1120,
        "stage": ObjectId(deal_stage),
        "assigned_to": ObjectId(user_id),
        "description": "",
        "target_date":target_date,
        "product": [{
            "_id": "60bb3d8727236dcd2e4d9097",
            "category_name": "Category A",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:01 AM",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "XB900N",
            "org_id": org_id,
            "packing_percentage": "",
            "packing_price": "",
            "price": "1000",
            "product_name": "Beats Studio Heaphones",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "total": 1000,
            "uom": "",
            "description": "High Performance wireless noise canceling&nbsp; headphones",
            "taxable_value": 1000
        }],
        "qty": [],
        "tag": [],
        "completed_percentage": 0,
        "deal_status": "open",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
        }
        deal1_id=''
        deal_data_set = defaultdict(list)
        for data in deal1:
            deal_data_set[data] = deal1[data]
        u2 = Deal(**deal_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_deal = Deal.objects().aggregate(*pipeline)
        for data in recent_deal:
            deal1_id=data['_id']
        

        quote1={
            "org_id": org_id,
            "quote_no": "QUO/1",
            "revise_type": "parent",
            "revise_count": 0,
            "subject": "Quote for Beats Studio Headphones",
            "deal_id": ObjectId(deal1_id),
            "currency": "INR",
            "contact_id":ObjectId(contact1_id),
            "company_id":ObjectId(company1_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "valid_till": target_date,
            "quote_stage":ObjectId(deal_stage),
            "product": [{
                "_id": "60bb3d8727236dcd2e4d9097",
                "category_name": "Category A",
                "cgst": "6",
                "createBy": "Swami",
                "create_date": "05/06/2021",
                "create_time": "09:01 AM",
                "discount_percentage": "",
                "discount_price": "",
                "hsn_code": "510510",
                "igst": "12",
                "model_no": "XB900N",
                "org_id": org_id,
                "packing_percentage": "",
                "packing_price": "",
                "price": "1000",
                "product_name": "Beats Studio Heaphones",
                "qty": 1,
                "sgst": "6",
                "status": "active",
                "summary": "",
                "tax_amount1": "",
                "tax_amount2": "",
                "tax_percentage1": "",
                "tax_percentage2": "",
                "tax_type1": "",
                "tax_type2": "",
                "total": 1000,
                "uom": "",
                "description": "High Performance wireless noise canceling&nbsp; headphones",
                "taxable_value": 1000
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "installation_charges": 0,
            "tax_type": "same_state",
            "tax_type1": "CGST",
            "tax_percentage1": 6,
            "tax_amount1": 60,
            "tax_type2": "SGST",
            "tax_percentage2": 6,
            "tax_amount2": 60,
            "total": 1120,
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "same_as_address": "true",
            "address1": "5587 Green Avenue",
            "address2": "",
            "city": "Coimbatore",
            "state": "Tamil Nadu",
            "country": "India",
            "pincode": "641026",
            "shipping_address1": "5587 Green Avenue",
            "shipping_address2": "",
            "shipping_city": "Coimbatore",
            "shipping_state": "Tamil Nadu",
            "shipping_country": "India",
            "shipping_pincode": "641026",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by": ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "quote_category": "sales",
            "demo_data":'true'
        }

        quote1_id=''
        quote_data_set = defaultdict(list)
        for data in quote1:
            quote_data_set[data] = quote1[data]
        u2 = Quote(**quote_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_quote = Quote.objects().aggregate(*pipeline)
        for data in recent_quote:
            quote1_id=data['_id']
        proforma_stage=MongoAPI.getSettingsByName('proforma_stage',org_id,'Created')
        proforma1={
            "org_id": org_id,
            "proforma_date":date,
            "proforma_no": "PRO/1",
            "quote_id":ObjectId(quote1_id),
            "quote_no": "QUO/1",
            "subject": "Quote for Beats Studio Headphones",
            "deal_id":ObjectId(deal1_id),
            "currency": "INR",
            "contact_id":ObjectId(contact1_id),
            "company_id": ObjectId(company1_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "proforma_stage":ObjectId(proforma_stage),
            "product": [{
                "_id": "60bb3d8727236dcd2e4d9097",
                "category_name": "Category A",
                "cgst": "6",
                "createBy": "Swami",
                "create_date": "05/06/2021",
                "create_time": "09:01 AM",
                "discount_percentage": "",
                "discount_price": "",
                "hsn_code": "510510",
                "igst": "12",
                "model_no": "XB900N",
                "org_id": org_id,
                "packing_percentage": "",
                "packing_price": "",
                "price": "1000",
                "product_name": "Beats Studio Heaphones",
                "qty": 1,
                "sgst": "6",
                "status": "active",
                "summary": "",
                "tax_amount1": "",
                "tax_amount2": "",
                "tax_percentage1": "",
                "tax_percentage2": "",
                "tax_type1": "",
                "tax_type2": "",
                "total": 1000,
                "uom": "",
                "description": "High Performance wireless noise canceling&nbsp; headphones",
                "taxable_value": 1000
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "installation_charges": 0,
            "tax_type1": "CGST",
            "tax_percentage1": 6,
            "tax_amount1": 60,
            "tax_type2": "SGST",
            "tax_percentage2": 6,
            "tax_amount2": 60,
            "total": 1120,
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "address1": "5587 Green Avenue",
            "address2": "",
            "city": "Coimbatore",
            "state": "Tamil Nadu",
            "country": "India",
            "pincode": "641026",
            "shipping_address1": "5587 Green Avenue",
            "shipping_address2": "",
            "shipping_city": "Coimbatore",
            "shipping_state": "Tamil Nadu",
            "shipping_country": "India",
            "shipping_pincode": "641026",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by":ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "same_as_address": "true",
            "tax_type": "same_state",
            "demo_data":'true'
        }
        proforma1_id=''
        proforma_data_set = defaultdict(list)
        for data in proforma1:
            proforma_data_set[data] = proforma1[data]
        u2 = Proforma(**proforma_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_proforma = Proforma.objects().aggregate(*pipeline)
        for data in recent_proforma:
            proforma1_id=data['_id']
        invoice_stage=MongoAPI.getSettingsByName('invoice_stage',org_id,'Created')
        invoice1 = {
        "org_id": org_id,
        "invoice_no": "INV/1",
        "quote_id":ObjectId(quote1_id),
        "quote_no": "QUO/1",
        "proforma_no": "PRO/1",
        "subject": "Quote for Beats Studio Headphones",
        "deal_id":ObjectId(deal1_id),
        "currency": "INR",
        "contact_id": ObjectId(contact1_id),
        "company_id": ObjectId(company1_id),
        "gstin": "",
        "ref_no": "13245RFDGF",
        "invoice_stage": ObjectId(invoice_stage),
        "product": [{
            "_id": "60bb3d8727236dcd2e4d9097",
            "category_name": "Category A",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:01 AM",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "XB900N",
            "org_id": org_id,
            "packing_percentage": "",
            "packing_price": "",
            "price": "1000",
            "product_name": "Beats Studio Heaphones",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "total": 1000,
            "uom": "",
            "description": "High Performance wireless noise canceling&nbsp; headphones",
            "taxable_value": 1000
        }],
        "discount_amount": 0,
        "discount_percentage": 0,
        "packing_amount": 0,
        "packing_percentage": 0,
        "shipping_charges": 0,
        "installation_charges": 0,
        "tax_type1": "CGST",
        "tax_percentage1": 6,
        "tax_amount1": 60,
        "tax_type2": "SGST",
        "tax_percentage2": 6,
        "tax_amount2": 60,
        "total": 1120,
        "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
        "bank_details": "",
        "remark": "",
        "address1": "5587 Green Avenue",
        "address2": "",
        "city": "Coimbatore",
        "state": "Tamil Nadu",
        "country": "India",
        "pincode": "641026",
        "shipping_address1": "5587 Green Avenue",
        "shipping_address2": "",
        "shipping_city": "Coimbatore",
        "shipping_state": "Tamil Nadu",
        "shipping_country": "India",
        "shipping_pincode": "641026",
        "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
        "field_setting": [{
            "bank": "false",
            "discount": "none",
            "discount_mode": "",
            "installation": "false",
            "packing": "none",
            "packing_mode": "",
            "remarks": "false",
            "round_off": "false",
            "shipping": "false",
            "tax": "overall",
            "total_in_pdf": "false",
            "uom": "false"
        }],
        "carrier": "By Road",
        "delivery_period": "2 Days",
        "create_by": ObjectId(user_id),
        "assigned_to":ObjectId(user_id),
        "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
        "same_as_address": "true",
        "tax_type": "same_state",
        "demo_data":'true'
        }

        invoice1_id=''
        invoice_data_set = defaultdict(list)
        for data in invoice1:
            invoice_data_set[data] = invoice1[data]
        u2 = Invoice(**invoice_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_invoice = Invoice.objects().aggregate(*pipeline)
        for data in recent_invoice:
            invoice1_id=data['_id']

        deal2={
        "deal_id": "645d69ba-dfbb-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "deal_no": "DE/3",
        "deal_name": "Quote for BenQ Bezeless Monitor",
        "currency": "INR",
        "contact_id": ObjectId(contact2_id),
        "company_id": ObjectId(company2_id),
        "amount": 1120,
        "stage": ObjectId(deal_stage),
        "assigned_to": ObjectId(user_id),
        "description": "",
        "target_date":target_date,
         "product": [{
        "_id": "60bb4050eeabdbac9d60505a",
        "category_name": "Category A",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:13 AM",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "227U4PA",
        "org_id": org_id,
        "packing_percentage": "",
        "packing_price": "",
        "price": "35000",
        "product_name": "BenQ Bezeless Monitor ",
        "qty": "2",
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "total": 70000,
        "uom": "",
        "description": "HDRi: Intelligent clarity adjustment and brightness control plus emulated HDR make&nbsp; easy image stunning and clear",
        "taxable_value": 70000
       }],
        "qty": [],
        "tag": [],
        "completed_percentage": 0,
        "deal_status": "open",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
        }
        deal2_id=''
        deal_data_set = defaultdict(list)
        for data in deal2:
            deal_data_set[data] = deal2[data]
        u2 = Deal(**deal_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_deal = Deal.objects().aggregate(*pipeline)
        for data in recent_deal:
            deal2_id=data['_id']
        

        quote2={
            "org_id": org_id,
            "quote_no": "QUO/2",
            "revise_type": "parent",
            "revise_count": 0,
            "subject": "Quote for BenQ Bezeless Monitor",
            "deal_id": ObjectId(deal2_id),
            "currency": "INR",
            "contact_id":ObjectId(contact2_id),
            "company_id":ObjectId(company2_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "valid_till": target_date,
            "quote_stage":ObjectId(deal_stage),
            "product": [{
                "_id": "60bb4050eeabdbac9d60505a",
                "category_name": "Category B",
                "cgst": "6",
                "createBy": "Swami",
                "create_date": "05/06/2021",
                "create_time": "09:13 AM",
                "description": "HDRi: Intelligent clarity adjustment and brightness control plus emulated HDR make&nbsp; easy image stunning and clear",
                "discount_percentage": "",
                "discount_price": "",
                "hsn_code": "510510",
                "igst": "12",
                "model_no": "227U4PA",
                "org_id": "103",
                "packing_percentage": "",
                "packing_price": "",
                "price": "35000",
                "product_name": "BenQ Bezeless Monitor ",
                "qty": "2",
                "sgst": "6",
                "status": "active",
                "summary": "",
                "tax_amount1": "",
                "tax_amount2": "",
                "tax_percentage1": "",
                "tax_percentage2": "",
                "tax_type1": "",
                "tax_type2": "",
                "taxable_value": 70000,
                "total": 70000,
                "uom": ""
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "installation_charges": 0,
            "tax_type": "same_state",
            "tax_type1": "CGST",
            "tax_percentage1": 6,
            "tax_amount1": 4200,
            "tax_type2": "SGST",
            "tax_percentage2": 6,
            "tax_amount2": 4200,
            "total": 78400,
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "same_as_address": "true",
            "address1": "1644 Mandan Road",
            "address2": "",
            "city": "Baihar",
            "state": "Madhya Pradesh",
            "country": "India",
            "pincode": "600016",
            "shipping_address1": "1644 Mandan Road",
            "shipping_address2": "",
            "shipping_city": "Baihar",
            "shipping_state": "Madhya Pradesh",
            "shipping_country": "India",
            "shipping_pincode": "600016",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by": ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "quote_category": "sales",
            "demo_data":'true'
        }

        quote2_id=''
        quote_data_set = defaultdict(list)
        for data in quote2:
            quote_data_set[data] = quote2[data]
        u2 = Quote(**quote_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_quote = Quote.objects().aggregate(*pipeline)
        for data in recent_quote:
            quote2_id=data['_id']
        proforma_stage=MongoAPI.getSettingsByName('proforma_stage',org_id,'Created')
        proforma2={
            "org_id": org_id,
            "proforma_date":date,
            "proforma_no": "PRO/2",
            "quote_id":ObjectId(quote2_id),
            "quote_no": "QUO/2",
            "subject": "Quote for BenQ Bezeless Monitor",
            "deal_id":ObjectId(deal2_id),
            "currency": "INR",
            "contact_id":ObjectId(contact2_id),
            "company_id": ObjectId(company2_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "proforma_stage":ObjectId(proforma_stage),
            "product": [{
            "_id": "60bb4050eeabdbac9d60505a",
            "category_name": "Category B",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:13 AM",
            "description": "HDRi: Intelligent clarity adjustment and brightness control plus emulated HDR make&nbsp; easy image stunning and clear",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "227U4PA",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "35000",
            "product_name": "BenQ Bezeless Monitor ",
            "qty": "2",
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 70000,
            "total": 70000,
            "uom": ""
        }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "installation_charges": 0,
            "tax_type1": "CGST",
            "tax_percentage1": 6,
            "tax_amount1": 4200,
            "tax_type2": "SGST",
            "tax_percentage2": 6,
            "tax_amount2": 4200,
            "total": 78400,
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "address1": "1644 Mandan Road",
            "address2": "",
            "city": "Baihar",
            "state": "Madhya Pradesh",
            "country": "India",
            "pincode": "600016",
            "shipping_address1": "1644 Mandan Road",
            "shipping_address2": "",
            "shipping_city": "Baihar",
            "shipping_state": "Madhya Pradesh",
            "shipping_country": "India",
            "shipping_pincode": "600016",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by":ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "same_as_address": "true",
            "tax_type": "same_state",
            "demo_data":'true'
        }
        proforma2_id=''
        proforma_data_set = defaultdict(list)
        for data in proforma2:
            proforma_data_set[data] = proforma2[data]
        u2 = Proforma(**proforma_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_proforma = Proforma.objects().aggregate(*pipeline)
        for data in recent_proforma:
            proforma2_id=data['_id']
        invoice_stage=MongoAPI.getSettingsByName('invoice_stage',org_id,'Created')
        invoice2 = {
        "org_id": org_id,
        "invoice_no": "INV/2",
        "quote_id":ObjectId(quote2_id),
        "quote_no": "QUO/2",
        "proforma_no": "PRO/2",
        "subject": "Quote for BenQ Bezeless Monitor",
        "deal_id":ObjectId(deal2_id),
        "currency": "INR",
        "contact_id": ObjectId(contact2_id),
        "company_id": ObjectId(company2_id),
        "gstin": "",
        "ref_no": "13245RFDGF",
        "invoice_stage": ObjectId(invoice_stage),
        "product": [{
        "_id": "60bb4050eeabdbac9d60505a",
        "category_name": "Category B",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:13 AM",
        "description": "HDRi: Intelligent clarity adjustment and brightness control plus emulated HDR make&nbsp; easy image stunning and clear",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "227U4PA",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "35000",
        "product_name": "BenQ Bezeless Monitor ",
        "qty": "2",
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "taxable_value": 70000,
        "total": 70000,
        "uom": ""
    }],
        "discount_amount": 0,
        "discount_percentage": 0,
        "packing_amount": 0,
        "packing_percentage": 0,
        "shipping_charges": 0,
        "installation_charges": 0,
        "tax_type1": "CGST",
        "tax_percentage1": 6,
        "tax_amount1": 4200,
        "tax_type2": "SGST",
        "tax_percentage2": 6,
        "tax_amount2": 4200,
        "total": 78400,
        "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
        "bank_details": "",
        "remark": "",
        "address1": "1644 Mandan Road",
        "address2": "",
        "city": "Baihar",
        "state": "Madhya Pradesh",
        "country": "India",
        "pincode": "600016",
        "shipping_address1": "1644 Mandan Road",
        "shipping_address2": "",
        "shipping_city": "Baihar",
        "shipping_state": "Madhya Pradesh",
        "shipping_country": "India",
        "shipping_pincode": "600016",
        "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
        "field_setting": [{
            "bank": "false",
            "discount": "none",
            "discount_mode": "",
            "installation": "false",
            "packing": "none",
            "packing_mode": "",
            "remarks": "false",
            "round_off": "false",
            "shipping": "false",
            "tax": "overall",
            "total_in_pdf": "false",
            "uom": "false"
        }],
        "carrier": "By Road",
        "delivery_period": "2 Days",
        "create_by": ObjectId(user_id),
        "assigned_to":ObjectId(user_id),
        "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
        "same_as_address": "true",
        "tax_type": "same_state",
        "demo_data":'true'
        }

        invoice2_id=''
        invoice_data_set = defaultdict(list)
        for data in invoice2:
            invoice_data_set[data] = invoice2[data]
        u2 = Invoice(**invoice_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_invoice = Invoice.objects().aggregate(*pipeline)
        for data in recent_invoice:
            invoice2_id=data['_id']

        deal3={
        "deal_id": "af4afa32-dfbb-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "deal_no": "DE/4",
        "deal_name": "Quote for Logitech Wireless Mouse Black",
        "currency": "INR",
        "contact_id": ObjectId(contact3_id),
        "company_id": ObjectId(company3_id),
        "amount": 1680,
        "stage": ObjectId(deal_stage),
        "assigned_to": ObjectId(user_id),
        "description": "",
        "target_date":target_date,
         "product": [{
        "_id": "60bb415a27236dcd2e4d90a2",
        "category_name": "Category C",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:18 AM",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "B170",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "500",
        "product_name": "Logitech Wireless Mouse Black",
        "qty": "3",
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "total": 1500,
        "uom": "KG",
        "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
        "taxable_value": 1500
       }],
        "qty": [],
        "tag": [],
        "completed_percentage": 0,
        "deal_status": "open",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
        }
        deal3_id=''
        deal_data_set = defaultdict(list)
        for data in deal3:
            deal_data_set[data] = deal3[data]
        u2 = Deal(**deal_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_deal = Deal.objects().aggregate(*pipeline)
        for data in recent_deal:
            deal3_id=data['_id']
        

        quote3={
            "org_id": org_id,
            "quote_no": "QUO/3",
            "revise_type": "parent",
            "revise_count": 0,
            "subject": "Quote for Logitech Wireless Mouse Black",
            "deal_id": ObjectId(deal3_id),
            "currency": "INR",
            "contact_id":ObjectId(contact3_id),
            "company_id":ObjectId(company3_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "valid_till": target_date,
            "quote_stage":ObjectId(deal_stage),
            "product": [{
            "_id": "60bb415a27236dcd2e4d90a2",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:18 AM",
            "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "B170",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "500",
            "product_name": "Logitech Wireless Mouse Black",
            "qty": "3",
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 1500,
            "total": 1500,
            "uom": "KG"
        }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "installation_charges": 0,
            "tax_type": "other_state",
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 180,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 1680,
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "same_as_address": "true",
            "address1": "1 Beacon Street,",
            "address2": " 33rd Floor",
            "city": "Alappuzha",
            "state": "Kerala",
            "country": "India",
            "pincode": "890677",
            "shipping_address1": "1 Beacon Street,",
            "shipping_address2": " 33rd Floor",
            "shipping_city": "Alappuzha",
            "shipping_state": "Kerala",
            "shipping_country": "India",
            "shipping_pincode": "890677",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by": ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "quote_category": "sales",
            "demo_data":'true'
        }

        quote3_id=''
        quote_data_set = defaultdict(list)
        for data in quote3:
            quote_data_set[data] = quote3[data]
        u2 = Quote(**quote_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_quote = Quote.objects().aggregate(*pipeline)
        for data in recent_quote:
            quote3_id=data['_id']
        proforma_stage=MongoAPI.getSettingsByName('proforma_stage',org_id,'Created')
        proforma3={
            "org_id": org_id,
            "proforma_date":date,
            "proforma_no": "PRO/3",
            "quote_id":ObjectId(quote3_id),
            "quote_no": "QUO/3",
            "subject": "Quote for Logitech Wireless Mouse Black",
            "deal_id":ObjectId(deal3_id),
            "currency": "INR",
            "contact_id":ObjectId(contact3_id),
            "company_id": ObjectId(company3_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "proforma_stage":ObjectId(proforma_stage),
            "product": [{
            "_id": "60bb415a27236dcd2e4d90a2",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:18 AM",
            "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "B170",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "500",
            "product_name": "Logitech Wireless Mouse Black",
            "qty": "3",
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 1500,
            "total": 1500,
            "uom": "KG"
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "installation_charges": 0,
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 180,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 1680,
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "address1": "1 Beacon Street,",
            "address2": " 33rd Floor",
            "city": "Alappuzha",
            "state": "Kerala",
            "country": "India",
            "pincode": "890677",
            "shipping_address1": "1 Beacon Street,",
            "shipping_address2": " 33rd Floor",
            "shipping_city": "Alappuzha",
            "shipping_state": "Kerala",
            "shipping_country": "India",
            "shipping_pincode": "890677",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by":ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "same_as_address": "true",
            "tax_type": "other_state",
            "demo_data":'true'
        }
        proforma3_id=''
        proforma_data_set = defaultdict(list)
        for data in proforma3:
            proforma_data_set[data] = proforma3[data]
        u2 = Proforma(**proforma_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_proforma = Proforma.objects().aggregate(*pipeline)
        for data in recent_proforma:
            proforma3_id=data['_id']
        invoice_stage=MongoAPI.getSettingsByName('invoice_stage',org_id,'Created')
        invoice3 = {
        "org_id": org_id,
        "invoice_no": "INV/3",
        "quote_id":ObjectId(quote3_id),
        "quote_no": "QUO/3",
        "proforma_no": "PRO/3",
        "subject": "Quote for Logitech Wireless Mouse Black",
        "deal_id":ObjectId(deal3_id),
        "currency": "INR",
        "contact_id": ObjectId(contact3_id),
        "company_id": ObjectId(company3_id),
        "gstin": "",
        "ref_no": "13245RFDGF",
        "invoice_stage": ObjectId(invoice_stage),
        "product": [{
        "_id": "60bb415a27236dcd2e4d90a2",
        "category_name": "Category A",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:18 AM",
        "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "B170",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "500",
        "product_name": "Logitech Wireless Mouse Black",
        "qty": "3",
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "taxable_value": 1500,
        "total": 1500,
        "uom": "KG"
        }],
        "discount_amount": 0,
        "discount_percentage": 0,
        "packing_amount": 0,
        "packing_percentage": 0,
        "shipping_charges": 0,
        "installation_charges": 0,
        "tax_type1": "IGST",
        "tax_percentage1": 12,
        "tax_amount1": 180,
        "tax_type2": "",
        "tax_percentage2": 0,
        "tax_amount2": 0,
        "total": 1680,
        "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
        "bank_details": "",
        "remark": "",
        "address1": "1 Beacon Street,",
        "address2": " 33rd Floor",
        "city": "Alappuzha",
        "state": "Kerala",
        "country": "India",
        "pincode": "890677",
        "shipping_address1": "1 Beacon Street,",
        "shipping_address2": " 33rd Floor",
        "shipping_city": "Alappuzha",
        "shipping_state": "Kerala",
        "shipping_country": "India",
        "shipping_pincode": "890677",
        "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
        "field_setting": [{
            "bank": "false",
            "discount": "none",
            "discount_mode": "",
            "installation": "false",
            "packing": "none",
            "packing_mode": "",
            "remarks": "false",
            "round_off": "false",
            "shipping": "false",
            "tax": "overall",
            "total_in_pdf": "false",
            "uom": "false"
        }],
        "carrier": "By Road",
        "delivery_period": "2 Days",
        "create_by": ObjectId(user_id),
        "assigned_to":ObjectId(user_id),
        "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
        "same_as_address": "true",
        "tax_type": "other_state",
        "demo_data":'true'
        }

        invoice3_id=''
        invoice_data_set = defaultdict(list)
        for data in invoice3:
            invoice_data_set[data] = invoice3[data]
        u2 = Invoice(**invoice_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_invoice = Invoice.objects().aggregate(*pipeline)
        for data in recent_invoice:
            invoice3_id=data['_id']

        deal4={
        "deal_id": "495c2d3a-dfbc-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "deal_no": "DE/4",
        "deal_name": "Quote for Microsoft Surface Go",
        "currency": "INR",
        "contact_id": ObjectId(contact4_id),
        "company_id": ObjectId(company4_id),
        "amount": 72800,
        "stage": ObjectId(deal_stage),
        "assigned_to": ObjectId(user_id),
        "description": "",
        "target_date":target_date,
        "product": [{
        "_id": "60d4402b68afee44801f7b9a",
        "category_name": "Category C",
        "cgst": "9",
        "createBy": "Swami",
        "create_date": "24/06/2021",
        "create_time": "08:19 AM",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "18",
        "model_no": "S11003",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "65000",
        "product_name": "Microsoft Surface Go",
        "qty": 1,
        "sgst": "9",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "total": 65000,
        "uom": "",
        "description": "<ul><li>Intel Pentium Gold Processor 4425Y gives your daily tasks and apps an added boost</li></ul>",
        "taxable_value": 65000
       }],
        "qty": [],
        "tag": [],
        "completed_percentage": 0,
        "deal_status": "open",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
        }
        deal4_id=''
        deal_data_set = defaultdict(list)
        for data in deal4:
            deal_data_set[data] = deal4[data]
        u2 = Deal(**deal_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_deal = Deal.objects().aggregate(*pipeline)
        for data in recent_deal:
            deal4_id=data['_id']
        

        quote4={
            "org_id": org_id,
            "quote_no": "QUO/4",
            "revise_type": "parent",
            "revise_count": 0,
            "subject": "Quote for Microsoft Surface Go",
            "deal_id": ObjectId(deal4_id),
            "currency": "INR",
            "contact_id":ObjectId(contact4_id),
            "company_id":ObjectId(company4_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "valid_till": target_date,
            "quote_stage":ObjectId(deal_stage),
            "product": [{
            "_id": "60d4402b68afee44801f7b9a",
            "category_name": "Category C",
            "cgst": "9",
            "createBy": "Swami",
            "create_date": "24/06/2021",
            "create_time": "08:19 AM",
            "description": "Intel Pentium Gold Processor 4425Y gives your daily tasks and apps an added boost",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "18",
            "model_no": "S11003",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "65000",
            "product_name": "Microsoft Surface Go",
            "qty": 1,
            "sgst": "9",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 65000,
            "total": 65000,
            "uom": ""
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "installation_charges": 0,
            "tax_type": "other_state",
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 7800,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 72800,
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "same_as_address": "true",
            "address1": "567, Park Ave",
            "address2": "West Street",
            "city": "Manipal",
            "state": "Karnataka",
            "country": "India",
            "pincode": "9087657",
            "shipping_address1": "567, Park Ave",
            "shipping_address2": "West Street",
            "shipping_city": "Manipal",
            "shipping_state": "Karnataka",
            "shipping_country": "India",
            "shipping_pincode": "9087657",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by": ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "quote_category": "sales",
            "demo_data":'true'
        }

        quote4_id=''
        quote_data_set = defaultdict(list)
        for data in quote4:
            quote_data_set[data] = quote4[data]
        u2 = Quote(**quote_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_quote = Quote.objects().aggregate(*pipeline)
        for data in recent_quote:
            quote4_id=data['_id']
        proforma_stage=MongoAPI.getSettingsByName('proforma_stage',org_id,'Created')
        proforma4={
            "org_id": org_id,
            "proforma_date":date,
            "proforma_no": "PRO/4",
            "quote_id":ObjectId(quote4_id),
            "quote_no": "QUO/4",
            "subject": "Quote for Microsoft Surface Go",
            "deal_id":ObjectId(deal4_id),
            "currency": "INR",
            "contact_id":ObjectId(contact4_id),
            "company_id": ObjectId(company4_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "proforma_stage":ObjectId(proforma_stage),
            "product": [{
            "_id": "60d4402b68afee44801f7b9a",
            "category_name": "Category C",
            "cgst": "9",
            "createBy": "Swami",
            "create_date": "24/06/2021",
            "create_time": "08:19 AM",
            "description": "Intel Pentium Gold Processor 4425Y gives your daily tasks and apps an added boost",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "18",
            "model_no": "S11003",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "65000",
            "product_name": "Microsoft Surface Go",
            "qty": 1,
            "sgst": "9",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 65000,
            "total": 65000,
            "uom": ""
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "installation_charges": 0,
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 7800,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 72800,
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "address1": "567, Park Ave",
            "address2": "West Street",
            "city": "Manipal",
            "state": "Karnataka",
            "country": "India",
            "pincode": "9087657",
            "shipping_address1": "567, Park Ave",
            "shipping_address2": "West Street",
            "shipping_city": "Manipal",
            "shipping_state": "Karnataka",
            "shipping_country": "India",
            "shipping_pincode": "9087657",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by":ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "same_as_address": "true",
            "tax_type": "other_state",
            "demo_data":'true'
        }
        proforma4_id=''
        proforma_data_set = defaultdict(list)
        for data in proforma4:
            proforma_data_set[data] = proforma4[data]
        u2 = Proforma(**proforma_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_proforma = Proforma.objects().aggregate(*pipeline)
        for data in recent_proforma:
            proforma4_id=data['_id']
        invoice_stage=MongoAPI.getSettingsByName('invoice_stage',org_id,'Created')
        invoice4 = {
        "org_id": org_id,
        "invoice_no": "INV/4",
        "quote_id":ObjectId(quote4_id),
        "quote_no": "QUO/4",
        "proforma_no": "PRO/4",
        "subject": "Quote for Microsoft Surface Go",
        "deal_id":ObjectId(deal4_id),
        "currency": "INR",
        "contact_id": ObjectId(contact4_id),
        "company_id": ObjectId(company4_id),
        "gstin": "",
        "ref_no": "13245RFDGF",
        "invoice_stage": ObjectId(invoice_stage),
        "product": [{
        "_id": "60d4402b68afee44801f7b9a",
        "category_name": "Category C",
        "cgst": "9",
        "createBy": "Swami",
        "create_date": "24/06/2021",
        "create_time": "08:19 AM",
        "description": "Intel Pentium Gold Processor 4425Y gives your daily tasks and apps an added boost",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "18",
        "model_no": "S11003",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "65000",
        "product_name": "Microsoft Surface Go",
        "qty": 1,
        "sgst": "9",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "taxable_value": 65000,
        "total": 65000,
        "uom": ""
        }],
        "discount_amount": 0,
        "discount_percentage": 0,
        "packing_amount": 0,
        "packing_percentage": 0,
        "shipping_charges": 0,
        "installation_charges": 0,
        "tax_type1": "IGST",
        "tax_percentage1": 12,
        "tax_amount1": 7800,
        "tax_type2": "",
        "tax_percentage2": 0,
        "tax_amount2": 0,
        "total": 72800,
        "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
        "bank_details": "",
        "remark": "",
        "address1": "567, Park Ave",
        "address2": "West Street",
        "city": "Manipal",
        "state": "Karnataka",
        "country": "India",
        "pincode": "9087657",
        "shipping_address1": "567, Park Ave",
        "shipping_address2": "West Street",
        "shipping_city": "Manipal",
        "shipping_state": "Karnataka",
        "shipping_country": "India",
        "shipping_pincode": "9087657",
        "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
        "field_setting": [{
            "bank": "false",
            "discount": "none",
            "discount_mode": "",
            "installation": "false",
            "packing": "none",
            "packing_mode": "",
            "remarks": "false",
            "round_off": "false",
            "shipping": "false",
            "tax": "overall",
            "total_in_pdf": "false",
            "uom": "false"
        }],
        "carrier": "By Road",
        "delivery_period": "2 Days",
        "create_by": ObjectId(user_id),
        "assigned_to":ObjectId(user_id),
        "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
        "same_as_address": "true",
        "tax_type": "other_state",
        "demo_data":'true'
        }

        invoice4_id=''
        invoice_data_set = defaultdict(list)
        for data in invoice4:
            invoice_data_set[data] = invoice4[data]
        u2 = Invoice(**invoice_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_invoice = Invoice.objects().aggregate(*pipeline)
        for data in recent_invoice:
            invoice4_id=data['_id']

        deal5={
        "deal_id": "ff3f985e-dfbb-11eb-8f74-e74b69a17044",
        "org_id": org_id,
        "deal_no": "DE/5",
        "deal_name": "Quote for Logitech Wireless Keyboard",
        "currency": "INR",
        "contact_id": ObjectId(contact5_id),
        "company_id": ObjectId(company5_id),
        "amount": 3920,
        "stage": ObjectId(deal_stage),
        "assigned_to": ObjectId(user_id),
        "description": "",
        "target_date":target_date,
        "product": [{
        "_id": "60bb429827236dcd2e4d90a6",
        "category_name": "Category C",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:23 AM",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "K380 ",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "3000",
        "product_name": "Logitech Wireless Keyboard",
        "qty": 1,
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "total": 3000,
        "uom": "",
        "description": "<ul><li>Logitech MK295 wireless keyboard features advanced optical sensor and delivers the same typing and click experience with 90% less noise</li></ul>",
        "taxable_value": 3000
        }, {
        "_id": "60bb415a27236dcd2e4d90a2",
        "category_name": "Category C",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:18 AM",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "B170",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "500",
        "product_name": "Logitech Wireless Mouse Black",
        "qty": 1,
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "total": 500,
        "uom": "KG",
        "description": "<ul><li>Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern</li></ul>",
        "taxable_value": 500
        }],
        "qty": [],
        "tag": [],
        "completed_percentage": 0,
        "deal_status": "open",
        "create_by":ObjectId(user_id),
        "demo_data":'true'
        }
        deal5_id=''
        deal_data_set = defaultdict(list)
        for data in deal5:
            deal_data_set[data] = deal5[data]
        u2 = Deal(**deal_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_deal = Deal.objects().aggregate(*pipeline)
        for data in recent_deal:
            deal5_id=data['_id']
        

        quote5={
            "org_id": org_id,
            "quote_no": "QUO/5",
            "revise_type": "parent",
            "revise_count": 0,
            "subject": "Quote for Logitech Wireless Keyboard",
            "deal_id": ObjectId(deal5_id),
            "currency": "INR",
            "contact_id":ObjectId(contact5_id),
            "company_id":ObjectId(company5_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "valid_till": target_date,
            "quote_stage":ObjectId(deal_stage),
            "product": [{
            "_id": "60bb429827236dcd2e4d90a6",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:23 AM",
            "description": "Logitech MK295 wireless keyboard features advanced optical sensor and delivers the same typing and click experience with 90% less noise",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "K380 ",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "3000",
            "product_name": "Logitech Wireless Keyboard",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 3000,
            "total": 3000,
            "uom": ""
            }, {
            "_id": "60bb415a27236dcd2e4d90a2",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:18 AM",
            "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "B170",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "500",
            "product_name": "Logitech Wireless Mouse Black",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 500,
            "total": 500,
            "uom": "KG"
             }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "installation_charges": 0,
            "tax_type": "other_state",
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 420,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 3920,
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "same_as_address": "true",
            "address1": "106 A, Paradise Valley,",
            "address2": "Tumkur Road",
            "city": "Karol Bāgh",
            "state": "Delhi",
            "country": "India",
            "pincode": "600016",
            "shipping_address1": "106 A, Paradise Valley,",
            "shipping_address2": "Tumkur Road",
            "shipping_city": "Karol Bāgh",
            "shipping_state": "Delhi",
            "shipping_country": "India",
            "shipping_pincode": "600016",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by": ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "quote_category": "sales",
            "demo_data":'true'
        }

        quote5_id=''
        quote_data_set = defaultdict(list)
        for data in quote5:
            quote_data_set[data] = quote5[data]
        u2 = Quote(**quote_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_quote = Quote.objects().aggregate(*pipeline)
        for data in recent_quote:
            quote5_id=data['_id']
        proforma_stage=MongoAPI.getSettingsByName('proforma_stage',org_id,'Created')
        proforma5={
            "org_id": org_id,
            "proforma_date":date,
            "proforma_no": "PRO/5",
            "quote_id":ObjectId(quote5_id),
            "quote_no": "QUO/5",
            "subject": "Quote for Logitech Wireless Keyboard",
            "deal_id":ObjectId(deal5_id),
            "currency": "INR",
            "contact_id":ObjectId(contact5_id),
            "company_id": ObjectId(company5_id),
            "gstin": "",
            "ref_no": "13245RFDGF",
            "proforma_stage":ObjectId(proforma_stage),
            "product": [{
            "_id": "60bb429827236dcd2e4d90a6",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:23 AM",
            "description": "Logitech MK295 wireless keyboard features advanced optical sensor and delivers the same typing and click experience with 90% less noise",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "K380 ",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "3000",
            "product_name": "Logitech Wireless Keyboard",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 3000,
            "total": 3000,
            "uom": ""
            }, {
            "_id": "60bb415a27236dcd2e4d90a2",
            "category_name": "Category C",
            "cgst": "6",
            "createBy": "Swami",
            "create_date": "05/06/2021",
            "create_time": "09:18 AM",
            "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
            "discount_percentage": "",
            "discount_price": "",
            "hsn_code": "510510",
            "igst": "12",
            "model_no": "B170",
            "org_id": "103",
            "packing_percentage": "",
            "packing_price": "",
            "price": "500",
            "product_name": "Logitech Wireless Mouse Black",
            "qty": 1,
            "sgst": "6",
            "status": "active",
            "summary": "",
            "tax_amount1": "",
            "tax_amount2": "",
            "tax_percentage1": "",
            "tax_percentage2": "",
            "tax_type1": "",
            "tax_type2": "",
            "taxable_value": 500,
            "total": 500,
            "uom": "KG"
            }],
            "discount_amount": 0,
            "discount_percentage": 0,
            "packing_amount": 0,
            "packing_percentage": 0,
            "shipping_charges": 0,
            "installation_charges": 0,
            "tax_type1": "IGST",
            "tax_percentage1": 12,
            "tax_amount1": 420,
            "tax_type2": "",
            "tax_percentage2": 0,
            "tax_amount2": 0,
            "total": 3920,
            "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
            "bank_details": "",
            "remark": "",
            "address1": "106 A, Paradise Valley,",
            "address2": "Tumkur Road",
            "city": "Karol Bāgh",
            "state": "Delhi",
            "country": "India",
            "pincode": "600016",
            "shipping_address1": "106 A, Paradise Valley,",
            "shipping_address2": "Tumkur Road",
            "shipping_city": "Karol Bāgh",
            "shipping_state": "Delhi",
            "shipping_country": "India",
            "shipping_pincode": "600016",
            "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
            "field_setting": [{
                "bank": "false",
                "discount": "none",
                "discount_mode": "",
                "installation": "false",
                "packing": "none",
                "packing_mode": "",
                "remarks": "false",
                "round_off": "false",
                "shipping": "false",
                "tax": "overall",
                "total_in_pdf": "false",
                "uom": "false"
            }],
            "carrier": "By Road",
            "delivery_period": "2 Days",
            "create_by":ObjectId(user_id),
            "assigned_to": ObjectId(user_id),
            "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
            "same_as_address": "true",
            "tax_type": "other_state",
            "demo_data":'true'
        }
        proforma5_id=''
        proforma_data_set = defaultdict(list)
        for data in proforma5:
            proforma_data_set[data] = proforma5[data]
        u2 = Proforma(**proforma_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_proforma = Proforma.objects().aggregate(*pipeline)
        for data in recent_proforma:
            proforma5_id=data['_id']
        invoice_stage=MongoAPI.getSettingsByName('invoice_stage',org_id,'Created')
        invoice5 = {
        "org_id": org_id,
        "invoice_no": "INV/5",
        "quote_id":ObjectId(quote5_id),
        "quote_no": "QUO/5",
        "proforma_no": "PRO/5",
        "subject": "Quote for Logitech Wireless Keyboard",
        "deal_id":ObjectId(deal5_id),
        "currency": "INR",
        "contact_id": ObjectId(contact5_id),
        "company_id": ObjectId(company5_id),
        "gstin": "",
        "ref_no": "13245RFDGF",
        "invoice_stage": ObjectId(invoice_stage),
        "product": [{
        "_id": "60bb429827236dcd2e4d90a6",
        "category_name": "Category C",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:23 AM",
        "description": "Logitech MK295 wireless keyboard features advanced optical sensor and delivers the same typing and click experience with 90% less noise",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "K380 ",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "3000",
        "product_name": "Logitech Wireless Keyboard",
        "qty": 1,
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "taxable_value": 3000,
        "total": 3000,
        "uom": ""
        }, {
        "_id": "60bb415a27236dcd2e4d90a2",
        "category_name": "Category C",
        "cgst": "6",
        "createBy": "Swami",
        "create_date": "05/06/2021",
        "create_time": "09:18 AM",
        "description": "Dual connectivity. Connect the way like via Bluetooth wireless technology or via the included tiny USB receiver Modern",
        "discount_percentage": "",
        "discount_price": "",
        "hsn_code": "510510",
        "igst": "12",
        "model_no": "B170",
        "org_id": "103",
        "packing_percentage": "",
        "packing_price": "",
        "price": "500",
        "product_name": "Logitech Wireless Mouse Black",
        "qty": 1,
        "sgst": "6",
        "status": "active",
        "summary": "",
        "tax_amount1": "",
        "tax_amount2": "",
        "tax_percentage1": "",
        "tax_percentage2": "",
        "tax_type1": "",
        "tax_type2": "",
        "taxable_value": 500,
        "total": 500,
        "uom": "KG"
        }],
        "discount_amount": 0,
        "discount_percentage": 0,
        "packing_amount": 0,
        "packing_percentage": 0,
        "shipping_charges": 0,
        "installation_charges": 0,
        "tax_type1": "IGST",
        "tax_percentage1": 12,
        "tax_amount1": 420,
        "tax_type2": "",
        "tax_percentage2": 0,
        "tax_amount2": 0,
        "total": 3920,
        "terms_condition": "<ul style=\"color: rgb(85, 85, 85); font-family: &quot;Open Sans&quot;, HelveticaNeue, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 14px;\"><li><b>Standard Warranty 1 Year</b>&nbsp;</li><li><b>Installation charges extra as applicable</b></li><li><b>Customization charges extra based on requirement</b></li></ul>",
        "bank_details": "",
        "remark": "",
        "address1": "106 A, Paradise Valley,",
        "address2": "Tumkur Road",
        "city": "Karol Bāgh",
        "state": "Delhi",
        "country": "India",
        "pincode": "600016",
        "shipping_address1": "106 A, Paradise Valley,",
        "shipping_address2": "Tumkur Road",
        "shipping_city": "Karol Bāgh",
        "shipping_state": "Delhi",
        "shipping_country": "India",
        "shipping_pincode": "600016",
        "payment_terms": "50% Advance with PO & Balance 50% before dispatch",
        "field_setting": [{
            "bank": "false",
            "discount": "none",
            "discount_mode": "",
            "installation": "false",
            "packing": "none",
            "packing_mode": "",
            "remarks": "false",
            "round_off": "false",
            "shipping": "false",
            "tax": "overall",
            "total_in_pdf": "false",
            "uom": "false"
        }],
        "carrier": "By Road",
        "delivery_period": "2 Days",
        "create_by": ObjectId(user_id),
        "assigned_to":ObjectId(user_id),
        "terms_condition_selected": "60bb38b2e7ac839b8d42dfc0",
        "same_as_address": "true",
        "tax_type": "other_state",
        "demo_data":'true'
        }

        invoice5_id=''
        invoice_data_set = defaultdict(list)
        for data in invoice5:
            invoice_data_set[data] = invoice5[data]
        u2 = Invoice(**invoice_data_set)
        u2.save()
        pipeline = [
                            {'$sort'   : {'create_date' : -1}},
                            {'$match':{'org_id':org_id}},
                            {'$limit'  : 1},
                   ]
        recent_invoice = Invoice.objects().aggregate(*pipeline)
        for data in recent_invoice:
            invoice5_id=data['_id']

        return 1   


    def getDealsListAll(ordId,length,page,search,assigned_to,stage,tag,date_from,date_to,sort,order,current_user,city,state,country):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            


            # if search :
            #     filter1['deal_name__contains'] = search
            # if assigned_to :
            #     filter['assigned_to'] = ObjectId(assigned_to)
            if city:
                filter['account.city'] = city
            if state:
                filter['account.state'] = state
            if country:
                filter['account.country'] = country
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if stage:
                other_stage_list=MongoAPI.getOtherDealStages('deal_stage',ordId)
                if stage!='active':
                    filter['stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['stage']={"$in":other_stage_list}
            if tag:
                filter['tag'] = tag
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            
                            {'$lookup' : app_config.dealCheckListLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'deal_name': { "$regex": search, "$options" :'i'}}, 
                                {'account.company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact.contact_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            # print (match)
            settings = defaultdict(list)

            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getInvoiceListData(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['subject__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['invoice_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if stage:
                other_stage_list=MongoAPI.getOtherProformaStages('invoice_stage',ordId)
                if stage!='active':
                    filter['invoice_stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['invoice_stage']={"$in":other_stage_list}
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.invoice_stage_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'invoice_no': { "$regex": search, "$options" :'i'}}, 
                                {'subject': { "$regex": search, "$options" :'i'}}, 
                                {'company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact_name': { "$regex": search, "$options" :'i'}}, 
                                {'stage_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.invoice_project}
                        ]
            
            settings = defaultdict(list)

            role = Invoice.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='invoice_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['invoice_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['invoice_stage_name']:
                        settings['invoice_stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'



    def getProformaListData(ordId,length,page,search,assigned_to,stage,date_from,date_to,sort,order,current_user):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['subject__contains'] = search
                # filter['name__contains'] = search
            if assigned_to :
                filter['assigned_to'] = ObjectId(assigned_to)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['proforma_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            if stage:
                other_stage_list=MongoAPI.getOtherProformaStages('proforma_stage',ordId)
                if stage!='active':
                    filter['proforma_stage'] = ObjectId(stage)
                elif stage=='active':
                    filter['proforma_stage']={"$in":other_stage_list}
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.proforma_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'proforma_no': { "$regex": search, "$options" :'i'}}, 
                                {'subject': { "$regex": search, "$options" :'i'}}, 
                                {'company_name': { "$regex": search, "$options" :'i'}}, 
                                {'contact_name': { "$regex": search, "$options" :'i'}}, 
                                {'stage_name': { "$regex": search, "$options" :'i'}}, 
                                ],
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.proforma_project}
                        ]
            settings = defaultdict(list)

            role = Proforma.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='proforma_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['proforma_stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['proforma_stage_name']:
                        settings['proforma_stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getUsersNewList(ordId,length,page,search,status,sort,order):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if search :
            #     filter1['name__contains'] = search
                # filter['name__contains'] = search
            if status :
                filter['status'] = status
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.report_to_lookup},
                            {'$lookup' : app_config.role_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'name': { "$regex": search, "$options" :'i'}}, 
                                {'email': { "$regex": search, "$options" :'i'}},
                                ],
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.user_project}
                        ]
            
            settings = defaultdict(list)

            role = User.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='report':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='report_to':
                        settings['reporting_to']=[] 
                        assigned = item[item1]
                        for item2 in assigned:
                            
                            this_user=MongoAPI.getUserDetails(ordId,item2)
                            # this_user2 = Uid.fix_array3(this_user)
                            settings['reporting_to'].append(this_user)
                    elif item1=='roleData':
                        assigned = item[item1]
                        assignedLen = len(list(assigned))
                        if assignedLen:
                            for item2 in assigned:
                                # print (assigned)
                                settings['roleData'] = item2['role_name']
                        else:
                            settings['roleData'] = ''
                    
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
             

                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['report']:
                        settings['report']=''
                    if not settings['roleData']:
                        settings['roleData']=''
                    

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    
    def getFilterUser(ordId,userlist):
        try:
          
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['_id']={"$in":userlist}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])

            match = filter
            pipeline = [
                            # {'$lookup' : app_config.report_to_lookup},
                            # {'$lookup' : app_config.role_lookup},
                            {'$match'  : match},

                            {'$project': app_config.user_project}
                        ]
            
            settings = defaultdict(list)

            role = User.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='report':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='report_to':
                        settings['reporting_to']=[] 
                        assigned = item[item1]
                        for item2 in assigned:
                            
                            this_user=MongoAPI.getUserDetails(ordId,item2)
                            # this_user2 = Uid.fix_array3(this_user)
                            settings['reporting_to'].append(this_user)
                    elif item1=='roleData':
                        assigned = item[item1]
                        assignedLen = len(list(assigned))
                        if assignedLen:
                            for item2 in assigned:
                                # print (assigned)
                                settings['roleData'] = item2['role_name']
                        else:
                            settings['roleData'] = ''
                    
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
             

                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['report']:
                        settings['report']=''
                    if not settings['roleData']:
                        settings['roleData']=''
                    

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    


    def getDealsNewListData(ordId,length,page,sort,order,id,associate_to):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = int(ordId)
            
            if associate_to=='contact':
                filter['contact_id'] = ObjectId(id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(id)

                # filter['name__contains'] = search
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.deals_project}
                        ]
            
            settings = defaultdict(list)
            
            role = Deal.objects.aggregate(*pipeline)


            settings6=[]
            for item in role:
                # print (item)
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''
                    # print (settings)
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealsList(ordId,user_id,id,associate_to):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if associate_to=='contact':
                filter['contact_id'] = ObjectId(id)
            elif associate_to=='company':
                filter['company_id'] = ObjectId(id)

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            sort_by = 'create_date'
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']

            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}

            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        ]
            settings = defaultdict(list)

            role = Deal.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['stage_name']:
                        settings['stage_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['contact']:
                        settings['contact']=''
                    if not settings['contact_name']:
                        settings['contact_name']=''
                    if not settings['company_name']:
                        settings['company_name']=''

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealsCount(ordId,stage):
        try:
           
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            # if stage:
            filter['stage'] = ObjectId(stage)
                
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print (filter)
            # fields_lookup = {'from': 'product','startWith':'$product_name','connectFromField': 'product','connectToField': '_id','maxDepth':50,'as': 'product_data'}
            pipeline = [
                            # {'$lookup' : app_config.ownerLookup},
                            # {'$lookup' : app_config.user_lookup},
                            # {'$lookup' : app_config.account_lookup},
                            # {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_count_project},
                            {'$group' : {   '_id'  : "$stage_name.name" ,
                                            'total' : { "$sum": "$amount" },
                                            'count': {"$sum":1}
                                        }
                            }
                            
                            
                        ]
            settings = defaultdict(list)
            role = Deal.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                # print (item)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='total':
                        # print (item[item1])
                        settings[item1] = TokenRefresh.human_format(item[item1])
                    elif item1=='_id':
                        string=''
                        for x in item[item1]:
                            string+= str(x)
                        # settings['stage'] = item[item1]
                        settings['stage_name'] = string
                            
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings))
            # settings2=''
            return settings2
        except NotUniqueError as e:
            return '0'

    def reassignUpdate(org_id,id,associate_to,assigned_to):
        try:
            if associate_to=='deal':
                response = Deal.objects(org_id=org_id,id=id).update_one(assigned_to=assigned_to)
            elif associate_to=='contact':
                response = Contact.objects(org_id=org_id,id=id).update_one(assigned_to=assigned_to)
            elif associate_to=='company':
                response = Account.objects(org_id=org_id,id=id).update_one(assigned_to=assigned_to)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id
    
    def idData():
        number = datetime.datetime.now().microsecond
        return number

    def tagUpdate(org_id,id,associate_to,tag):
        try:
            if associate_to=='deal':
                response = Deal.objects(org_id=org_id,id=id).update_one(tag=tag)
            else:
                response = Contact.objects(org_id=org_id,id=id).update_one(tag=tag)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id
    
    def idData():
        number = datetime.datetime.now().microsecond
        return number
    
    def companyStatusChangeUpdate(id,status):
        try:
            modify_date = datetime.datetime.utcnow
            user = Account.objects(id=id).update_one(status=status,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output
    def productUpload(org_id,id,filenameData):
        try:
            modify_date = datetime.datetime.utcnow   

            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = Product.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)

            for sett in settings6:
                # print(sett)
                sett['images'].append(filenameData)
                # print(sett['images'])
                # upresponse = DealsCheckList.objects(org_id=org_id,id=ObjectId(process_data['process_id'])).update(data_list=sett['data_list'])
                user = Product.objects(id=id).update_one(images=sett['images'],modify_date=modify_date)         
            # user = Product.objects(id=id).update_one(images=filenameData,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output

    def productImageRemove(org_id,id,index):
        try:
            index1 =int(index)
            data1=defaultdict(list)
            settings = defaultdict(list)
            # data1['create_by'] = user_id
            data1['org_id'] = org_id
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            # filter['associate_id'] = ObjectId(associate_id)
            filter['_id'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                        {'$match'  : match},
                       ]
           
            settings = defaultdict(list)

            role = Product.objects.aggregate(*pipeline)

            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                settings6.append(settings)
            settings6 = json.loads(json_util.dumps(settings6))
            # settings6 = Uid.fix_array(settings6)

            for sett in settings6:
                
                # sett['data_list']=sett['data_list'].remove(sett['data_list'][index])
                index2=int(index)
                del sett['images'][index2]
                upresponse = Product.objects(org_id=org_id,id=ObjectId(id)).update(images=sett['images'])
            id = 1
            output1 = str(id)
            return output1
        except NotUniqueError as e:
            return '0'
    
    
    def productImageDefault(id,default_image):
        try:
            modify_date = datetime.datetime.utcnow
            user = Product.objects(id=id).update_one(default_image=default_image,modify_date=modify_date)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output
    
    def convertAmountWords(num):
        num = decimal.Decimal(num)
        decimal_part = num - int(num)
        num = int(num)

        if decimal_part:
            return MongoAPI.convertAmountWords(num) + " point " + (" ".join(MongoAPI.convertAmountWords(i) for i in str(decimal_part)[2:]))

        under_20 = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen']
        tens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']
        above_100 = {100: 'Hundred', 1000: 'Thousand', 100000: 'Lakhs', 10000000: 'Crores'}

        if num < 20:
            return under_20[num]

        if num < 100:
            return tens[num // 10 - 2] + ('' if num % 10 == 0 else ' ' + under_20[num % 10])

        # find the appropriate pivot - 'Million' in 3,603,550, or 'Thousand' in 603,550
        pivot = max([key for key in above_100.keys() if key <= num])

        return MongoAPI.convertAmountWords(num // pivot) + ' ' + above_100[pivot] + ('' if num % pivot==0 else ' ' + MongoAPI.convertAmountWords(num % pivot))

    def pdfCalculate(pdfData,quote):

        pageSettings = defaultdict(list)
        # pageSettings={}
        pageSettings['sin_packing']= 0
        pageSettings['sin_partno']= 0
        pageSettings['sin_dis_percent']= 0 
        pageSettings['sin_discount']= 0 
        pageSettings['sin_vat']= 0 
        pageSettings['tax_type2']= 0 
        pageSettings['sin_addproduct']= 0 
        pageSettings['sin_packing']= 0
        pageSettings['sin_packing1']= 0
        pageSettings['sin_type2']= 0 
    
    
        products = quote['product'] 
        sub_total = 0
        # print(products)
        for product in products: 
            # for productData in product: 
            sub_total = float(sub_total)+float(product['total'])
            
           
            if product['packing_percentage'] or pageSettings['sin_packing']==1: 
                pageSettings['sin_packing'] = 1 
            else: 
                pageSettings['sin_packing'] = 0 
                
            if product['packing_price'] or pageSettings['sin_packing1']==1: 
                pageSettings['sin_packing1'] = 1 
            else: 
                pageSettings['sin_packing1'] = 0 

            if product['hsn_code'] or pageSettings['sin_partno']==1: 
                pageSettings['sin_partno'] = 1 
                

            if product['model_no'] or pageSettings['sin_partno1']==1: 
                pageSettings['sin_partno1'] = 1
            else: 
                pageSettings['sin_partno1'] = 0 
            if product['discount_percentage'] or pageSettings['sin_dis_percent']==1: 
                pageSettings['sin_dis_percent'] = 1 
            else: 
                pageSettings['sin_dis_percent'] = 0 
                

            if product['discount_price'] or pageSettings['sin_discount']==1: 
                pageSettings['sin_discount'] = 1
            else: 
                pageSettings['sin_discount'] = 0
                

            if product['tax_type1'] or pageSettings['sin_vat']==1: 
                pageSettings['sin_vat'] = 1 
            else: 
                pageSettings['sin_vat'] = 0 
                

            if product['tax_type2'] or pageSettings['tax_type2']==1: 
                pageSettings['tax_type2'] = 1 
            else: 
                pageSettings['tax_type2'] = 0 
                

            if product['uom'] or pageSettings['sin_addproduct']==1: 
                pageSettings['sin_addproduct'] = 1 
            else: 
                pageSettings['sin_addproduct'] = 0 
           
                 
        pageSettings['col_colspan1'] = 0

        pageSettings['item_sno'] = 6 
        pageSettings['item_description'] = 56 
        pageSettings['item_price'] = 18 
    
        pageSettings['item_amount'] = 21 
        pageSettings['part_no'] = 0 
    
        pageSettings['wtax3'] = 5 
        pageSettings['tax_amount3'] = 7 
    
        pageSettings['wtax4'] = 5 
        pageSettings['tax_amount4'] = 7 
    
    
        pageSettings['item_custom'] = 7 
        pageSettings['partNo1']=0 
        pageSettings['item_qty'] =0 

        if pdfData['qty']=='1': 
            pageSettings['item_qty'] =7 
            pageSettings['item_description'] = pageSettings['item_description']-6 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1'] + 1 
          
        else: 
            pageSettings['item_qty'] = 0 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1  
         
        
        if pdfData['part_no']=='1': 
            pageSettings['part_no']=1
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1  
          
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1 

        if pdfData['hsn']=='1': 
            pageSettings['hsn']=1
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
            
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1  
       
        
         

        # if pdfData['part_no'] and pageSettings['sin_partno']==1:            
        #     pageSettings['part_no'] = 9.5  
        #     pageSettings['item_description'] = pageSettings['item_description']-6   
        #     pageSettings['item_amount'] = pageSettings['item_amount']-1  
        #     pageSettings['item_price'] = pageSettings['item_price']-2.5    
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1  
         


        if pdfData['packing']=='1': 
            pageSettings['packing'] = 7 
            pageSettings['item_description'] = pageSettings['item_description']-3 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']-2 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
            
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1 

        if pdfData['packing1']=='1': 
            pageSettings['packing1'] = 7 
            pageSettings['item_description'] = pageSettings['item_description']-3 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']-2 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1  
     
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1   
         
        if pdfData['Taxable']=='1': 
            pageSettings['taxb']=8  
            pageSettings['item_description'] =pageSettings['item_description']-4  
            pageSettings['item_amount'] = pageSettings['item_amount']-2  
            pageSettings['item_price'] = pageSettings['item_price']-2  
            pageSettings['item_qty'] = pageSettings['item_qty']-1  
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1  
          
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1   
         
 
        if pdfData['discount']=='1': 
            pageSettings['item_discount'] = 6 
            pageSettings['item_description'] = pageSettings['item_description']-3 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
           
           
        # else: 
            # pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1  
         
        if pdfData['discount_price']=='1': 
            pageSettings['discount_price'] = 8.5 
            pageSettings['item_description'] = pageSettings['item_description']-3.5 
            pageSettings['item_amount'] = pageSettings['item_amount']-4 
            pageSettings['item_price'] = pageSettings['item_price']-1 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
        
            
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1 
         
    
        if pdfData['discount_price'] and pageSettings['sin_discount']==1 and pdfData['discount']==0 and pageSettings['sin_dis_percent']==0: 
            pageSettings['discount_price'] = 12 
            pageSettings['item_description'] = pageSettings['item_description']-3 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']-1 
         

    
        if pdfData['tax'] == '1': 
            pageSettings['wtax'] = 8 
            pageSettings['item_description'] = pageSettings['item_description']-5 
            pageSettings['item_amount'] = pageSettings['item_amount']-1 
            pageSettings['item_price'] = pageSettings['item_price']-2 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
       
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1   
    
            # if pdfData['discount_price'] and pageSettings['sin_discount']==1: 
            #     # pageSettings['discount_price'] = pageSettings['discount_price']+7 
            #     pageSettings['item_description'] = pageSettings['item_description']-3 
            #     pageSettings['item_amount'] = pageSettings['item_amount']-2 
            #     pageSettings['item_price'] = pageSettings['item_price']-2 
            # elif pdfData['Taxable']: 
            #     pageSettings['taxb'] = pageSettings['taxb']+7 
            #     pageSettings['item_description'] = pageSettings['item_description']-3 
            #     pageSettings['item_amount'] = pageSettings['item_amount']-2 
            #     pageSettings['item_price'] = pageSettings['item_price']-2 
            # else: 
            #     if pdfData['part_no'] and pageSettings['sin_partno']==1:            
            #         pageSettings['part_no'] = pageSettings['part_no']+7 
            #         pageSettings['item_description'] = pageSettings['item_description']-3 
            #         pageSettings['item_amount'] = pageSettings['item_amount']-2 
            #         pageSettings['item_price'] = pageSettings['item_price']-2 

        if pdfData['tax_amount']=='1':    
            pageSettings['tax_amount'] = 7.5 
            pageSettings['item_description'] = pageSettings['item_description']-4 
            pageSettings['item_amount'] = pageSettings['item_amount']-3.5 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
      
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1 
          
    

        if pdfData['tax2']=='1': 
            pageSettings['cwtax2'] = 8 
            pageSettings['citem_description'] = pageSettings['item_description']-4 
            pageSettings['citem_amount'] = pageSettings['item_amount']-2 
            pageSettings['citem_price'] = pageSettings['item_price']-2 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1 
       
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1
        
          
        if pdfData['tax2_amount']=='1': 
            pageSettings['tax_amount2'] = 7  
            pageSettings['item_description'] = pageSettings['item_description']-3 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']-2 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1
            
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1
                  
        pageSettings['sin_type3'] =12 


        if pdfData['item_custom']=='1':        
             
            pageSettings['item_description'] = pageSettings['item_description']-4 
            pageSettings['item_amount'] = pageSettings['item_amount']-2 
            pageSettings['item_price'] = pageSettings['item_price']-1 
            pageSettings['col_colspan1'] = pageSettings['col_colspan1']+1
         
        # else: 
        #     pageSettings['col_colspan1'] = pageSettings['col_colspan1']-1
         
    
        if pdfData['discounted_price']: 
            if pageSettings['discount_price']==0: 
                pageSettings['item_description'] = pageSettings['item_description']-4 
                pageSettings['item_amount'] = pageSettings['item_amount']-4 
                pageSettings['discount_price'] = 7 
             
            pageSettings['item_custom'] = pageSettings['item_custom']+4 
            pageSettings['item_price'] = pageSettings['item_price']-4 
            pageSettings['discount_price'] = pageSettings['discount_price']+pageSettings['item_price']
            if pageSettings['discount_price']: 
                pageSettings['discount_price'] = pageSettings['discount_price'] 

        pageSettings['col_colspan1'] = pageSettings['col_colspan1']+3
        pageSettings['col_colspan2'] = pageSettings['col_colspan1']-1 
        
        #testetetetetet
       

        # print(pageSettings['col_colspan1'])
        # print(pageSettings['col_colspan2'])
        # print(pageSettings['col_colspan3'])
        # settings2 = json.loads(json_util.dumps(pageSettings))
        # settings2 = Uid.fix_array4(pageSettings)
        # print (pageSettings)
        total_roundoff=(round(float(quote['total'])))
  
        sub_total = (format(float(sub_total),".2f"))
        if pdfData['total_round_off']=='1':
            amt_in_words = MongoAPI.convertAmountWords(total_roundoff)
        else:
            amt_in_words = MongoAPI.convertAmountWords(quote['total'])
        
        total_roundoff=(format(float(total_roundoff),".2f"))

        
        data = {'sub_total':sub_total,'pageSettings':pageSettings,'amt_in_words':amt_in_words,'total_roundoff':total_roundoff}
        # settings2 = json.loads(json_util.dumps(data))
        return data

    def receiveEnquiry(data1):
        try:
            settings = defaultdict(list)
            # data1 = defaultdict(list)
            for data in data1:
                settings[data] = data1[data]
            del settings['captcha']
            del settings['gcaptcha']
            u2 = Enquiry(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'

    def partnerEnquiry(data1):
        try:
            settings = defaultdict(list)
            # data1 = defaultdict(list)
            for data in data1:
                settings[data] = data1[data]
            del settings['captcha']
            del settings['gcaptcha']
            u2 = Partner(**settings)
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'
    def getAccountTmp(id):
        try:         
            s = Accounttmp.objects.get(org_id=id,sort_order=0)
            #output = s1
            #s = json.loads(json.loads(json_util.dumps(output)))
            
            output = {'address1' : str(s.address1),'address2' : str(s.address2),'city' : str(s.city),'company_name':str(s.company_name),'country':str(s.country),'email':str(s.email)
            ,'f1':str(s.f1),'f2':str(s.f2),'f3':str(s.f3),'f4':str(s.f4),'f5':str(s.f5),'gstin':str(s.gstin),'phone':str(s.phone),'pincode':str(s.pincode),'state':str(s.state),'website':str(s.website)
            
            }
            col1 = defaultdict(list)
            for col in output:
                if(output[col]):
                    col1[col] = output[col]
                    #print(output[col])

            return col1
        except Accounttmp.DoesNotExist:
            return ''

    def getAccountTmpList(ordId,rowData,create_by,customer_type,assigned_to,source):
        try:
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            
            filter['org_id'] = ordId
            #filter['city'] = 'phone2'
            match = filter

            AccountTmpList = defaultdict(list)
            AccountTmpList2 = defaultdict(list)
            AccountTmpList1 = []
            for rowData1 in rowData:
                AccountTmpList[rowData1] = 1
                AccountTmpList1.append(rowData1)
                AccountTmpList2[rowData1] = rowData[rowData1]


            pipeline = [
                        {'$match': match},
                        { '$project': AccountTmpList}
                       
                    ]

            # { '$project': AccountTmp}
                # { '$project': app_config.account_contact_project}
            


            settings = defaultdict(list)

            role = Accounttmp.objects(**filter1).aggregate(*pipeline)
            r = 0
            accountData1 = []
            for item in role:
                accountData = defaultdict(list)
                r = r+1
                for item1 in item:
                    for rowData1 in rowData:
                        if(rowData1==item1):
                            if(rowData1=='pincode'):
                                accountData[rowData[rowData1]] = str(item[item1])
                            elif (rowData1=='zip'):
                                accountData[rowData[rowData1]] = str(item[item1])
                            elif (rowData1=='email'):
                                accountData[rowData[rowData1]] = str(item[item1])
                            else:
                                accountData[rowData[rowData1]] = str(item[item1])
                    accountData7 = defaultdict(list)
                    for accountData6 in accountData:
                        if(accountData6=='pincode'):
                            accountData7[accountData6] = accountData[accountData6]
                        elif(accountData6=='zip'):
                            accountData7[accountData6] = int(accountData[accountData6])
                        else:
                            accountData7[accountData6] = str(accountData[accountData6])
                    accountData['org_id'] = ordId
                    if not customer_type:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['customer_type'] = ObjectId(customer_type)

                    if not source:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['source'] = ObjectId(source)

                    if not assigned_to:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['assigned_to'] = ObjectId(assigned_to)
                    accountData['create_by'] = ObjectId(create_by)
                    
                    

                accountData1.append(accountData)
                user = Account(**accountData7)
                response = user.save()

            return r
        except NotUniqueError as e:
            return '0'

    def getProductTmp(id):
        try:         
            s = Producttemp.objects.get(org_id=id,sort_order=0)
            #output = s1
            #s = json.loads(json.loads(json_util.dumps(output)))
            
            output = {
                        'product_name' : str(s.product_name),'model_no' : str(s.model_no),'hsn_code' : str(s.hsn_code),'uom':str(s.uom),'description':str(s.description),'cgst':str(s.cgst),'sgst':str(s.sgst),'igst':str(s.igst),'price':str(s.price)
                     }
            col1 = defaultdict(list)
            for col in output:
                if(output[col]):
                    col1[col] = output[col]
                    #print(output[col])

            return col1
        except Producttemp.DoesNotExist:
            return ''

    def getContactTmp(id):
        try:         
            s = Contacttmp.objects.get(org_id=id,sort_order=0)
            output = {
                        'name' : str(s.name),'company_name' : str(s.company_name),'phone_number' : str(s.phone_number),'email':str(s.email),'description':str(s.description),'whatsapp_no':str(s.whatsapp_no),'secondary_email':str(s.secondary_email),'designation':str(s.designation),'website':str(s.website),'facebook':str(s.facebook),'linkedin':str(s.linkedin),'twitter':str(s.twitter),'skype':str(s.skype),'address1':str(s.address1),'address2':str(s.address2),'country':str(s.country),'state':str(s.state),'city':str(s.city),'pincode':str(s.pincode)
                     }
            col1 = defaultdict(list)
            for col in output:
                if(output[col]):
                    col1[col] = output[col]
                    #print(output[col])

            return col1
        except Contacttmp.DoesNotExist:
            return ''



    def getProductTmpList(ordId,rowData,create_by,category):
        try:
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            
            filter['org_id'] = ordId
            #filter['city'] = 'phone2'
            match = filter

            AccountTmpList = defaultdict(list)
            AccountTmpList2 = defaultdict(list)
            AccountTmpList1 = []
            for rowData1 in rowData:
                AccountTmpList[rowData1] = 1
                AccountTmpList1.append(rowData1)
                AccountTmpList2[rowData1] = rowData[rowData1]
            pipeline = [
                        {'$match': match},
                        { '$project': AccountTmpList}
                       
                    ]
            settings = defaultdict(list)

            role = Producttemp.objects(**filter1).aggregate(*pipeline)
            r = 0
            accountData1 = []
            for item in role:
                accountData = defaultdict(list)
                r = r+1
                for item1 in item:
                    for rowData1 in rowData:
                        if(rowData1==item1):
                            if(rowData1=='pincode'):
                                accountData[rowData[rowData1]] = str(item[item1])
                            else:
                                accountData[rowData[rowData1]] = str(item[item1])
                    accountData7 = defaultdict(list)
                    for accountData6 in accountData:
                        if(accountData6=='pincode'):
                            accountData7[accountData6] = accountData[accountData6]
                        else:
                            accountData7[accountData6] = str(accountData[accountData6])
                    accountData['org_id'] = ordId
                    if not category:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['category'] = ObjectId(category)
                id = TokenRefresh.idData()
                accountData['product_id'] = str(id)
                accountData['create_by'] = ObjectId(create_by)
                accountData1.append(accountData)
                user = Product(**accountData)
                response = user.save()
            return r
        except NotUniqueError as e:
            return '0'


    def getContactTmpList(ordId,rowData,create_by,assigned_to,source):
        try:
            
            filter = {}
            filter1 = {}
            key = []
            val = []
            
            filter['org_id'] = ordId
            #filter['city'] = 'phone2'
            match = filter

            AccountTmpList = defaultdict(list)
            AccountTmpList2 = defaultdict(list)
            AccountTmpList1 = []
            for rowData1 in rowData:
                AccountTmpList[rowData1] = 1
                AccountTmpList1.append(rowData1)
                AccountTmpList2[rowData1] = rowData[rowData1]
            pipeline = [
                        {'$match': match},
                        {'$project': AccountTmpList}
                       
                    ]
            settings = defaultdict(list)

            role = Contacttmp.objects(**filter1).aggregate(*pipeline)
            r = 0
            accountData1 = []
            for item in role:
                accountData = defaultdict(list)
                r = r+1
                for item1 in item:
                    for rowData1 in rowData:
                        if(rowData1==item1):
                            if(rowData1=='pincode'):
                                accountData[rowData[rowData1]] = str(item[item1])
                            elif(rowData[rowData1]=='company_name'):
                                account = MongoAPI.getAccountsNameDetails(ordId,str(item[item1]))
                                accountData[rowData[rowData1]] = str(item[item1])
                                company_id=''
                                for acc in account:
                                    for acc1 in acc:
                                        if acc1=='_id':
                                            company_id=str(acc[acc1])
                                            
                                            if company_id:
                                                accountData['company_id'] = ObjectId(company_id)
                               
                            else:
                                accountData[rowData[rowData1]] = str(item[item1])
                    accountData7 = defaultdict(list)
                    for accountData6 in accountData:
                        if(accountData6=='pincode'):
                            accountData7[accountData6] = accountData[accountData6]
                        else:
                            accountData7[accountData6] = str(accountData[accountData6])
                    accountData['org_id'] = ordId
                    if not assigned_to:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['assigned_to'] = ObjectId(assigned_to)

                    if not source:
                        accountData['org_id'] = ordId
                    else:
                        accountData['org_id'] = ordId
                        accountData['source'] = ObjectId(source)
                    id = TokenRefresh.idData()
                    accountData['contact_id'] = int(id)
                    accountData['owner'] = ObjectId(create_by)
                accountData1.append(accountData)
                user = Contact(**accountData)
                response = user.save()
            return r
        except NotUniqueError as e:
            return '0'

    def getAccountsNameDetails(org_id,name):
        try:
            s = Account.objects.filter(org_id=int(org_id),company_name=str(name)).limit(1).to_json()
            followup = json.loads(s)
            followup = Uid.fix_array(followup)
            output = followup
            print('ttttttttttttttttttt')
            print(name)
            print(output)
            print('ssssssssssssssssssssss')
        except Account.DoesNotExist:
            output = 'No'   

        return output

    def PricelistCheck(title,ordId):
        try:
            s = Pricelist.objects.get(title=title,org_id=ordId)
            # id1 = json.loads(json_util.dumps(s.org_id))
            # id12 = json.loads(json_util.dumps(s))
            # id = str(s.id)
            if(s.org_id):
                output = 'Yes'
        except Pricelist.DoesNotExist:
            output = 'No'        
        return output

    def createPricelist(title,org_id,user_id):
        try:
            shopping_list = []
            settings = defaultdict(list)
            data1 = defaultdict(list)

            data1['title'] = title
            data1['org_id'] = org_id
            data1['create_by'] = user_id

            for data in data1:
                settings[data] = data1[data]
            
            u2 = Pricelist.from_json(json.dumps(data1))
            u2.save()
            response = u2
            output1 = str(response.id)
            return output1
        except NotUniqueError as e:
            return '0'
    def getProductsListNew(ordId,sort,order):
        try:

            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['status'] = 'active'
            

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.products_pricelist_project}
                        ]
            
            settings = defaultdict(list)

            role = Product.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    data = {item1:item4}
                    

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def createPricelistProducts(products,id):
        
        f_id = ''
        for product in products:
            settings = defaultdict(list)
            for prod in product:
                if prod=='_id':
                    settings['product_id'] = ObjectId(product[prod])
            settings['price_id'] = ObjectId(id)
            output = json.loads(json_util.dumps(settings))
            u2 = PricelistProduct.from_json(json.dumps(output))
            u2.save()
        return 'Yes'

    def getPricelistDetails(product_id,pricelist,currency):
        try:
            filter = {}
            
            if product_id:
                filter['product_id'] = ObjectId(product_id)
            if pricelist:
                filter['price_id'] = ObjectId(pricelist)
            
            match = filter
            pipeline = [
                            {'$match'  : match},
                            {'$project': app_config.PricelistProduct}
                        ]
            settings = defaultdict(list)

            role = PricelistProduct.objects.aggregate(*pipeline)
            output = ''
            for item1 in role:

                if currency=='INR':
                    output1=item1['inr_price']
                elif currency=='USD':
                    output1=item1['usd_price']
                elif currency=='EURO':
                    output1=item1['erou_price']
                #print (item1['inr_price'])
                output = output1    
        except PricelistProduct.DoesNotExist:
            output = ''        
        return output
    def getPricelistProductsList(ordId,sort,order,id):
        try:

            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['price_id'] = ObjectId(id)
            

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$lookup': app_config.pricelist_products_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.productsPricelistProject}
                        ]
            
            settings = defaultdict(list)

            role = PricelistProduct.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    if(item1=='product'):
                        products = Uid.fix_array2(item[item1])
                        for product3 in products:
                            product1 = defaultdict(list)
                            for product in product3:
                                if item=='_id':
                                    item3 = product3[product]
                                    for item12 in item3:
                                        product1['id'] = str(item3['$oid'])
                                else:
                                    product1[product] = product3[product]
                            settings['product'] = product1
                        
                    elif (item1=='_id'):
                        settings['_id'] = str(item[item1])
                    elif (item1=='product_id'):
                        settings['product_id'] = str(item[item1])
                    else:
                        settings[item1] = item[item1]
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    def getPricelistProductsDataList(ordId,sort,order):
        try:

            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter

            pipeline = [
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$project': app_config.products_pricelist_project}
                        ]
            
            settings = defaultdict(list)

            role = Product.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    data = {item1:item4}
                    

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def pricelistProductUpdate(id,column,value):
        try:
            modify_date = datetime.datetime.utcnow
            if(column=='inr_price'):
                user = PricelistProduct.objects(id=id).update_one(inr_price=value,modify_date=modify_date)
            elif(column=='usd_price'):
                user = PricelistProduct.objects(id=id).update_one(usd_price=value,modify_date=modify_date)
            elif(column=='erou_price'):
                user = PricelistProduct.objects(id=id).update_one(erou_price=value,modify_date=modify_date)
            output = 'Yes'
        except PricelistProduct.DoesNotExist:
            output = 'No'   
        return output

    def getPricelistList(ordId,length,page,search_string,owner,source,tag,date_from,date_to,sort,order,current_user):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId

            
            if search_string :
                filter1['title__contains'] = search_string
            # if owner :
            #     filter['owner'] = ObjectId(owner)
            if owner :
                filter['create_by'] = ObjectId(owner)
            else:
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['contact_user_list']
                filter['create_by']={"$in":reporting_user_list}
            
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print(match)
            pipeline = [
                
                        {'$lookup': app_config.ownerLookup},
                        {'$match': match},
                        {"$sort": {sort: order}},
                        { '$skip' : sData1 },
                        { '$limit' : length },
                        { '$project': app_config.pricelist_project_list}
                    ]
            print (pipeline)
                
            


            settings = defaultdict(list)

            role = Pricelist.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='source_name':
                        assigned = item[item1]
                        
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getPricelist(id):
        
        try:
            s = Pricelist.objects.filter(id=ObjectId(id)).to_json()
            output = json.loads(s)
        except User.DoesNotExist:
            output = 'No'   

        return output

    def getPricelistDetail(products,id):
        
        try:
            s = Pricelist.objects.filter(id=ObjectId(id)).to_json()
            output = json.loads(s)
        except User.DoesNotExist:
            output = 'No'   

        return output

    def getPricelistData(org_id):
        try:
            s = Pricelist.objects.filter(org_id=int(org_id)).to_json()
            output = json.loads(s)
            # print (output)
            return output
        except Pricelist.DoesNotExist:
            return '0'

    def getPricelistCompaniesList(ordId,id,length,page,search,sort_by):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId

            if search :
                filter1['subject__contains'] = search
            if id:
                filter['pricelist'] = ObjectId(id)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            order_dict1 = app_config.order_dict
            order_dict = order_dict1['desc']
            
            pipeline = [
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.customer_type_lookup},
                            {'$lookup' : app_config.fields_lookup},
                            {'$lookup' : app_config.countriesLookup},
                            {'$lookup' : app_config.statesLookup},
                            {'$sort'   : {sort_by : order_dict}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.company_project}
                        ]
            
            settings = defaultdict(list)

            

            role = Account.objects(**filter1).aggregate(*pipeline)

            # print (role)



            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    # elif item1=='customer_type':
                    #     item3 = item[item1]
                    #     if item3:
                    #         print (item3)
                    #         for item2 in item3:
                    #             settings[item]=str(item2['$oid'])
                        # else:
                        #     settings[item]
                    # elif item1=='source':
                    #     item3 = item[item1]
                    #     for item1 in item3:
                    #         settings[item]=str(item3['$oid'])
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                            
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['source_name']:
                        settings['source_name']=''
                    if not settings['assigned']:
                        settings['assigned']=''
                    if not settings['stateId']:
                        settings['stateId']=''
                    if not settings['countryId']:
                        settings['countryId']=''
                    if not settings['pincode']:
                        settings['pincode']=''
                    if not settings['customer_type_name']:
                        settings['customer_type_name']=''
                    if not settings['customer_type']:
                        settings['customer_type']=''
                    if not settings['contacts']:
                        contacts = MongoAPI.getAccountContactsList(ordId,settings['_id'])

                        response = json.loads(json_util.dumps(contacts))
                        response1 = Uid.fix_array(response)

                        settings['contacts'] = response1 
            
                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def updateCompanyPricelist(pricelist_id,company_list):
        try:
            modify_date = datetime.datetime.utcnow
            for company_id in company_list:
                update = Account.objects(id=ObjectId(company_id)).update_one(pricelist=pricelist_id,modify_date=modify_date)
            output1 = '1'
            return output1
        except NotUniqueError as e:
            return '0'

    def getProductNewDetails(ordId,id,pricelist,currency):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = ordId
            filter['_id'] = ObjectId(id)
           

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.categoryLookup},
                            {'$match'  : match},
                            {'$project': app_config.products_project}
                        ]
            settings = defaultdict(list)
            role = Product.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='category_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['category_name'] = item2['name']
                    elif item1=='price':
                        print('price')
                        prd_price=item[item1]
                        price = MongoAPI.getPricelistDetails(item['_id'],pricelist,currency)
                        if price:
                            if int(price)>0:
                                settings['price'] = price
                            else:
                                settings['price'] = item[item1]
                        else:
                            settings['price'] = item[item1]
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}
                    if not settings['category_name']:
                        settings['category_name']=''
                    if not settings['createBy']:
                        settings['createBy']=''
                    if not settings['path']:
                        settings['path'] = str(app_config.BASE_URL)+str(app_config.UPLOAD_OPEN_PRODUCT_FOLDER)+str(ordId)+'/'

            settings2 = json.loads(json_util.dumps(settings))


            return settings2
        except NotUniqueError as e:
            return '0'

# Reports

    def getDealsCountReport(org_id,user_id,from_date,to_date,status,current_user):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if user_id != 'all':
                filter['assigned_to']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

 
            
            # filter['assigned_to'] = ObjectId(user_id)
            if status=='won':
                filter['deal_status'] = 'won'
                filter['completed_date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='lost':
                filter['deal_status'] = 'lost'
                filter['completed_date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='all':
                filter['create_date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='pipeline':
                filter['create_date'] = {"$gte": from_date,"$lte": to_date}
                status_list = 'Won,Lost,lost,won'
                status = status_list.split(',')
                filter['deal_status'] = {'$nin': status}
            elif status=='new_business':
                filter['deal_status'] = 'won'
                filter['business_type'] = 'new'
                filter['completed_date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='repeat_business':
                filter['deal_status'] = 'won'
                filter['business_type'] = 'repeat'
                filter['completed_date'] = {"$gte": from_date,"$lte": to_date}
            elif status=='Negotiation':
                
                fields = Fields.objects.get(type='deal_stage',name='Negotiation',org_id=org_id)
                filter['stage'] = ObjectId(fields.id)
            elif status=='Expect':
                fields = Fields.objects.get(type='deal_stage',name='Expect',org_id=org_id)
                filter['stage'] = ObjectId(fields.id)
  
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print(match)
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Deal.objects.aggregate(*pipeline)
            final_data = defaultdict(list)
            total_amount = 0
            count = 0
            total = 0
            for data in result:
                count = count + 1
                for data1 in data:
                    if data1=='amount':
                        total=float(total)+float(data[data1])
            total = total
            total_amount=TokenRefresh.human_format(total)
            return_result={
                'count':count,
                'total':total,
                'total_amt':total_amount
            }
            # print(return_result)
            return return_result 
        except Deal.DoesNotExist:
            return '0'

    def getDealsCountReportStage(org_id,user_id,from_date,to_date,stage,current_user):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if user_id != 'all':
                filter['assigned_to']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            filter['stage'] = ObjectId(stage)
            # filter['completed_date'] = {"$gte": from_date,"$lte": to_date}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Deal.objects.aggregate(*pipeline)
            final_data = defaultdict(list)
            total_amount = 0
            count = 0
            total = 0
            for data in result:
                count = count + 1
                for data1 in data:
                    if data1=='amount':
                        total=float(total)+float(data[data1])
            total = total
            total_amount=TokenRefresh.human_format(total)
            return_result={
                'count':count,
                'total':total,
                'total_amt':total_amount
            }
            # print(return_result)
            return return_result 
        except Deal.DoesNotExist:
            return '0'


    def getQuotesCountReport(org_id,user_id,from_date,to_date,status,current_user):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if user_id != 'all':
                filter['assigned_to']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}

            
            if status=='all':
                filter['create_date'] = {"$gte": from_date,"$lte": to_date}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Quote.objects.aggregate(*pipeline)
            final_data = defaultdict(list)
            total_amount = 0
            count = 0
            for data in result:
                count = count + 1
                for data1 in data:
                    if data1=='total':
                        total_amount=float(total_amount)+float(data[data1])
            total_amount=TokenRefresh.human_format(total_amount)
            return_result={
                'count':count,
                'total':total_amount
            }
            return return_result 
        except Quote.DoesNotExist:
            return '0'

    def getFollowUpCountReport(org_id,user_id,from_date,to_date,status,current_user):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if user_id != 'all':
                filter['owner']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['owner']={"$in":reporting_user_list}
            
            if status=='open':
                filter['status'] = 'Open'
                filter['create_date'] = {"$gte": from_date,"$lte": to_date}
            elif status =='all':
                
                filter['create_date'] = {"$gte": from_date,"$lte": to_date}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Follow_up.objects.aggregate(*pipeline)
            final_data = defaultdict(list)
            total_amount = 0
            count = 0
            for data in result:
                count = count + 1
                
            return_result={
                'count':count,
            }
            return return_result 
        except Quote.DoesNotExist:
            return '0'

    def getTagContactCount(org_id,user_id,tag,current_user):
        try:
            
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            if user_id != 'all':
                filter['owner']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(org_id,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['owner']={"$in":reporting_user_list}

                
            filter['tag'] = tag
            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            # print(match)
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Contact.objects.aggregate(*pipeline)
            final_data = defaultdict(list)

            count = 0

            for data in result:
                count = count + 1
            return_result={
                'count':count
            }
            # print(return_result)
            return return_result 
        except Deal.DoesNotExist:
            return '0'

    def getNotesCountReport(org_id,user_id,from_date,to_date):
        try:
            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            filter['create_by'] = ObjectId(user_id)
            filter['create_date'] = {"$gte": from_date,"$lte": to_date}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            
            pipeline = [
                       
                            {'$match'  : match}
               
                       ]
            result = Note.objects.aggregate(*pipeline)
            final_data = defaultdict(list)
            total_amount = 0
            count = 0
            for data in result:
                count = count + 1
                
            return_result={
                'count':count,
            }
            return return_result 
        except Quote.DoesNotExist:
            return '0'

    def getQuotesListReport(ordId,user_id,current_user):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if user_id != 'all':
                filter['assigned_to']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            
            stage1 = 'Won,Lost,lost,won,Revised,revised'
            stage = stage1.split(',')
            filter['stage_name.name'] = {'$nin': stage}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_lookup},
                            {'$lookup' : app_config.quote_stage_lookup}, 
                            {'$sort'   : {'create_date' : -1}},
                            {'$match'  : match},
                            {'$project': app_config.quote_project}
                        ]
            settings = defaultdict(list)
            role = Quote.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='quote_stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['quote_stage_name'] = item2['name']
                    elif item1=='country':
                        country = item['country']
                        state = item['state']
                        shipping_country = item['shipping_country']
                        shipping_state = item['shipping_state']

                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='valid_till':
                        valid_till = item[item1]
                        valid_till = datetime.datetime.strptime(str(valid_till), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['valid_till'] = valid_till
                    else:
                        data = {item1:item4}
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getDealsBusinessReport(ordId,assigned_to,from_date,to_date,business_type,current_user):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if assigned_to != 'all':
                filter['assigned_to']=assigned_to
            elif assigned_to == 'all':
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            filter['business_type'] = business_type
            filter['completed_date'] = {"$gte": from_date,"$lte": to_date}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.dealCheckListLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        
                        ]
            settings = defaultdict(list)
            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']

                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getDealsLostReport(ordId,assigned_to,from_date,to_date,current_user):
        try:
            filter = {}
            filter1 = {}
            key = []
            val = []
            filter['org_id'] = ordId
            if assigned_to != 'all':
                filter['assigned_to']=assigned_to
            elif assigned_to == 'all':
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['assigned_to']={"$in":reporting_user_list}
            filter['deal_status'] = 'lost'
            filter['completed_date'] = {"$gte": from_date,"$lte": to_date}

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.dealCheckListLookup},
                            {'$lookup' : app_config.ownerLookup},
                            {'$lookup' : app_config.user_lookup},
                            {'$lookup' : app_config.account_lookup},
                            {'$lookup' : app_config.contact_lookup},
                            {'$lookup' : app_config.deal_stage_lookup},
                            {'$match'  : match},
                            {'$project': app_config.deals_project}
                        
                        ]
            settings = defaultdict(list)
            role = Deal.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='createBy':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='contact':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['contact_name'] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='stage_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['stage_name'] = item2['name']
                    # elif item1=='contact_detail':
                    #     assigned = item[item1]
                    #     for item2 in assigned:
                    #         settings['contact_detail'] = Uid.fix_array3(item2['name'])
                    if item1=='create_date':
                        create_date1 = item[item1]
                        # create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(MongoAPI.getUserDateFormat(ordId))
                        create_date_age = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date_age)
                    if item1=='modify_date':
                        modify_date1 = item[item1]
                        # create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(MongoAPI.getUserDateFormat(ordId))
                        settings['target_date'] = target_date

                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'


    def getFollowUpReports(org_id,user_id,from_date,to_date,followup_status,current_user):
        try:

            filter = {}
            key = []
            val = []
            filter['org_id'] = org_id
            
            if user_id != 'all':
                filter['owner']=user_id
            elif user_id == 'all':
                UserList=MongoAPI.getReportingUsers(ordId,current_user)
                reporting_user_list=UserList['deal_user_list']
                filter['owner']={"$in":reporting_user_list}
            filter['status'] = followup_status
            filter['date'] = {"$gte": from_date,"$lte": to_date}

            
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.owner_lookup},
                            {'$match': match},
                            {'$project': app_config.followUp_Project}
                        ]

            

            role = Follow_up.objects.aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    if item1=='date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT3).strftime(DATE_FORMAT1)
                        settings['date'] = create_date
                    else:
                        data = {item1:item4}
                    if not settings['assigned']:
                        settings['assigned']=''
                    
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            settings2 = Uid.fix_array(settings2)
            return settings2
        except NotUniqueError as e:
            return '0'



class TokenRefresh():
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}
    def idData():
        number = datetime.datetime.now().microsecond
        return number
    def days_between(d1, d2):
        d1 = datetime.strptime(d1, "%Y-%m-%d")
        d2 = datetime.strptime(d2, "%Y-%m-%d")
        return abs((d2 - d1).days)
    def calculate_age(born):
        y = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%Y')
        m = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%m')
        d = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%d')

        today = date.today().strftime('%d/%m/%Y')
        y1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%Y')
        m1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%m')
        d1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%d')

        d1 = date(int(y1),int(m1),int(d1))
        d0 = date(int(y),int(m),int(d))
        delta = d1 - d0
        days1 = delta.days
        if days1==0:
            days1='Today'
        elif days1==1:
            days1='1 Day'
        elif days1>0:
            days2 = str(days1)
            days1 = days2+str(' Days')
        return days1

    def calculate_remaining(born):
        y = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%Y')
        m = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%m')
        d = datetime.datetime.strptime(born, "%d/%m/%Y").strftime('%d')

        today = date.today().strftime('%d/%m/%Y')
        y1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%Y')
        m1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%m')
        d1 = datetime.datetime.strptime(today, "%d/%m/%Y").strftime('%d')

        d1 = date(int(y1),int(m1),int(d1))
        d0 = date(int(y),int(m),int(d))
        delta = d0 - d1
        days1 = delta.days
        if days1==0:
            days1='Today'
        elif days1==1:
            days1='1 Day'
        elif days1>0:
            days2 = str(days1)
            days1 = days2+str(' Days')
        return days1
    def fix_json_array(obj, attr):
        arr = getattr(obj, attr)
        if isinstance(arr, list) and len(arr) > 1 and arr[0] == '{':
            arr = arr[1:-1]
            arr = ''.join(arr).split(",")
            setattr(obj,attr, arr)

    def human_format(num):
        magnitude = 0
        while abs(num) >= 1000:
            magnitude += 1
            num /= 1000.0
            
        # add more suffixes if you need them
        return '%.0f%s' % (num, ['', 'K','L','C', 'G', 'T', 'P'][magnitude])

    
             
         
        