from app import app
from flask import Flask ,render_template,make_response
from flask import request, jsonify, json, Response, Flask,url_for, send_from_directory
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, fresh_jwt_required,get_raw_jwt
)

from flask_restful import Resource
from flask_cors import CORS
from flask_mail import Mail, Message
import socket, smtplib,ssl, time, select

from bson import ObjectId
from bson import json_util
import datetime
from db import initialize_db
from werkzeug.utils import secure_filename
from werkzeug.datastructures import  FileStorage
from os.path import join, dirname, realpath
import os
from werkzeug.security import generate_password_hash, check_password_hash
from common_config import Uid,Pdf

import app_config 
from collections import defaultdict
from models import User,Organization,Countries,States,Cities,User1,Fields,Numbering,Module_name,User_module_name,Timezones,Contact,Contact1,Activity,Account,Follow_up,Note,Product,Deal,Email,Document,Quote,PdfSettings

@app.route('/new', methods=['GET', 'POST'])
def pdf_template2():
    name='karthik'
    location='location'
    return render_template('certificate.html',name=name,location=location)

@app.route('/users2', methods=['GET'])
def get_users2():
    output = User.objects().to_json()
    users = json.loads(json_util.dumps(output))
    return Response(output, mimetype="application/json", status=200)