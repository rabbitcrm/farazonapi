# from app import status
from flask import Flask
from flask import request, jsonify, json, Response, Flask,url_for
from db import initialize_db
from bson import json_util
# from models import User,Organization,Countries,States,Cities,User1,Fields,Numbering,Module_name,User_module_name,Timezones,Contact,Contact1,Activity,Account,Follow_up,Note,Product,Deal,Email,Document,Quote,PdfSettings,Proforma,Role,Invoice,Email_template,Rename,SalesProcess,SalesSubProcess,DealsCheckList,DealsSubCheckList,SalesProcessTemplate,DealsStageMapping,AdminEmailTemplate,Reporting_To,SupportTicket,Enquiry,Partner,Accounttmp,Contacttmp,Producttemp,User_audit,AdminUser
from models import AdminUser, Contacttmp, Organization,User, User_audit,SupportTicket,Enquiry,Contact,Account,Deal,Quote,Product,Proforma,Follow_up,Activity,Email,Document,Invoice,Role,PdfSettings,Numbering,Accounttmp,Contacttmp,Producttemp,Rename,SalesProcess,SalesSubProcess,DealsCheckList,DealsSubCheckList,SalesProcessTemplate,DealsStageMapping,Fields,Note,Numbering,SupportTicket,User_audit,Email_template,AddOn,OrgAddon
from bson import ObjectId
import datetime
from datetime import date
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    jwt_refresh_token_required, create_refresh_token,
    get_jwt_identity, fresh_jwt_required,get_raw_jwt
)
from mongoengine.errors import NotUniqueError
from werkzeug.security import generate_password_hash, check_password_hash
import re
from common_config import Uid
from collections import defaultdict
from common_config import Uid
from flask_split import split
import app_config
import os
import decimal 
from mongodb import TokenRefresh
from cryptography.fernet import Fernet

# from flask_session import Session

app = Flask(__name__)

# SESSION_TYPE = 'user'
# Session(app)

from datatable import LastDataId

DATE_FORMAT1 = '%d/%m/%Y'

DATE_FORMAT_mdy = '%m/%d/%Y'

DATE_FORMAT2 = '%H:%M %p'

DATE_FORMAT3 = '%Y-%m-%d %H:%M:%S'

DATE_FORMAT = '%Y-%m-%d %H:%M:%S.%f'

DATE_FORMAT4 = '%Y-%m-%d %H:%M:%S'

USER_DATE_FORMAT1 = '%m-%d-%y'

USER_DATE_FORMAT2 = '%d-%m-%Y'

USER_DATE_FORMAT3 = '%m-%d-%Y'

app.config.from_pyfile('config.cfg')
app.config['UPLOAD_PDF_IMAGE_FOLDER'] = app_config.UPLOAD_PDF_IMAGE_FOLDER

initialize_db(app)

class AdminAPI:
    def selectLogin(email,password):
       
        try:
            

            s = AdminUser.objects.get(email=email,password=password)
            id1 = json.loads(json_util.dumps(s.admin_user_id))
            id12 = json.loads(json_util.dumps(s))
            id = str(s.id)
            output1 = {'name' : s.name,'email' : s.email,'create_date' : s.create_date,'id':id,'status':s.status}
            output = output1
        except AdminUser.DoesNotExist:
            output = 'User Does Not Exists'        
        return output

    def emailCheck(email):
        try:
            s = AdminUser.objects.get(email=email)
            output = 'Yes'
        except AdminUser.DoesNotExist:
            output = 'No'   

        return output
    
    def emailCheckNew(email):
        try:
            s = AdminUser.objects.get(email=email,approved='1')
            output = 'Yes'
        except AdminUser.DoesNotExist:
            output = 'No'   

        return output

    def createUser(email,password,name):
        try:
            data1 = defaultdict(list)
            data1['email'] = email
            data1['password'] = password
            data1['name'] = name
            user = AdminUser(**data1)
            response = user.save()
            output1 = {'user_id' : str(response.id)}
            return output1
        except NotUniqueError as e:
            return ''

    def getOrganizationList(ordId,length,page,search_string,date_from,date_to,order,sort):  
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'organization_name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.organization_project}
                        ]
            
            settings = defaultdict(list)
            role = Organization.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                    elif item1=='org_id':
                        assigned = item[item1]
                        settings['no_of_users']=AdminAPI.getOrgUserCount(item[item1])
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        # settings['date_aging'] = TokenRefresh.calculate_age(create_date2)

                    elif item1=='start_date':
                        start_date1 = item[item1]
                        start_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        start_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        start_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['start_date'] = start_date
                        settings['start_time'] = start_time
                        settings['date_aging'] = TokenRefresh.calculate_age(start_date2)
                    
                    elif item1=='end_date':
                        end_date1 = item[item1]
                        end_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        end_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        end_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['end_date'] = start_date
                        settings['end_time'] = start_time
                        settings['remaining_days'] = TokenRefresh.calculate_age(end_date2)

              
                    else:
                        data = {item1:item4}
                   
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getOrgCountData():
        try:
            org_total = Organization.objects().count()
            return org_total
        except NotUniqueError as e:
            return '0'

    def getOrgUserCount(org_id):
        try:
            users_count = User.objects(org_id=org_id).count()
            return users_count
        except NotUniqueError as e:
            return '0'

    def getOrgDetails(id):
        try:
            
            filter = {}
            filter['_id'] = ObjectId(id)
            key = []
            val = []
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup': app_config.countriesLookup},
                            {'$lookup': app_config.statesLookup},
                            {'$match': match},
                            {'$project': app_config.organization_project}
                       ]
            
            settings = defaultdict(list)
            role = Organization.objects.aggregate(*pipeline)
            settings6=[]
            item4 = role
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='org_id':
                        assigned = item[item1]
                        # admin_user = AdminAPI.getOrgUser(item[item1],item['email'])
                        admin_user=''
                        settings['admin_user']=admin_user
                        settings['no_of_users']=AdminAPI.getOrgUserCount(item[item1])
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='customerType':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['customer_type_name'] = item2['name']
                    if item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    elif item1=='start_date':
                        start_date1 = item[item1]
                        start_date = datetime.datetime.strptime(str(start_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        start_date2 = datetime.datetime.strptime(str(start_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        start_time = datetime.datetime.strptime(str(start_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['start_date'] = start_date
                        settings['start_time'] = start_time
                        settings['date_aging'] = TokenRefresh.calculate_age(start_date2)
                    
                    elif item1=='end_date':
                        end_date1 = item[item1]
                        end_date = datetime.datetime.strptime(str(end_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        end_date2 = datetime.datetime.strptime(str(end_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        end_time = datetime.datetime.strptime(str(end_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['end_date'] = end_date
                        settings['end_time'] = end_time
                        settings['remaining_days'] = TokenRefresh.calculate_remaining(end_date2)
                    else:
                        data = {item1:item4}
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getOrgUser(org_id,email):
        try:
            filter = {}
            filter['org_id'] = int(org_id)
            filter['email'] = email
            key = []
            val = []
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$match': match},
                            {'$project': app_config.user_project}
                       ]
            settings = defaultdict(list)
            role = User.objects.aggregate(*pipeline)
            settings6=[]
            item4 = role
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getOrganizationUserList(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user):

        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            filter['org_id'] = int(ordId)
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.role_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'name': { "$regex": search_string, "$options" :'i'}}, 
                                {'phone': { "$regex": search_string, "$options" :'i'}}, 
                                {'email': { "$regex": search_string, "$options" :'i'}}, 
                                ],
                            
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.user_project}
                        ]
            
            settings = defaultdict(list)
            role = User.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='roleData':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['role_name'] = item2['role_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                    elif item1=='org_id':
                        assigned = item[item1]
                        settings['no_of_users']=AdminAPI.getOrgUserCount(item[item1])
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date2)
                    else:
                        data = {item1:item4}
                   
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getOrgUserCountData(org_id):
        try:
            org_total = User.objects(org_id=int(org_id)).count()
            return org_total
        except NotUniqueError as e:
            return '0'

    def changeCrmUserStatus(id,status):
        try:
            response = User.objects(id=ObjectId(id)).update_one(status=status)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def changeCrmUserPassword(id,password):
        try:
            response = User.objects(id=ObjectId(id)).update_one(password=password)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def changeOrgStatus(org_id,status):
        try:
            response = Organization.objects(org_id=int(org_id)).update_one(status=status)
            user_update =User.objects(org_id=org_id).update(status=status)
            id = org_id
        except NotUniqueError as e:
            id = '0'
        return id
    def getAdminUserDetails(id):

        try:
            filter = {}
            filter['_id'] = ObjectId(id)
            key = []
            val = []
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$match': match}
                        ]
            settings = defaultdict(list)
            # print (pipeline)
            role = AdminUser.objects.aggregate(*pipeline)
            settings6=[]
            item4 = role
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    if item1=='role':
                        assigned = item[item1]
                        settings['role']=str(assigned)
                     
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                    else:
                        data = {item1:item4}            
            settings2 = json.loads(json_util.dumps(settings))
            return settings2
        except NotUniqueError as e:
            return 'No'

    def liveUsersList(ordId,length,page,search,status,sort,order):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            # filter['org_id'] = ordId
            # if search :
            #     filter1['name__contains'] = search
                # filter['name__contains'] = search

            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.org_lookup},
                            {'$lookup' : app_config.user_audit_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.user_audit_project}
                        ]
            settings = defaultdict(list)

            role = User_audit.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
                    if item1=='start_time':
                        start_time1 = item[item1]
                        start_date = datetime.datetime.strptime(str(start_time1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        start_time = datetime.datetime.strptime(str(start_time1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['start_date'] = start_date
                        settings['start_time'] = start_time
                        st_time = datetime.datetime.strptime(start_time, "%H:%M %p")
                        st_time1=st_time.strftime("%r")
                        settings['start_time'] = st_time1

                    if item1=='end_time':
                        end_time1 = item[item1]
                        end_date = datetime.datetime.strptime(str(end_time1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        end_time = datetime.datetime.strptime(str(end_time1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['end_date'] = end_date
                        settings['end_time'] = end_time
                        et_time = datetime.datetime.strptime(end_time, "%H:%M %p")
                        et_time1=et_time.strftime("%r")
                        settings['end_time'] = et_time1
                        diff = et_time - st_time
                        hours = int(diff.seconds // (60 * 60))
                        settings['duration'] = hours
                    if item1=='user_detail':
                        user_create_date1 = item[item1][0]['create_date']
                        user_create_date = datetime.datetime.strptime(str(user_create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        user_create_time = datetime.datetime.strptime(str(user_create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['user_create_date'] = user_create_date
                        settings['user_create_time'] = user_create_time


                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                  

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def liveUsersListCount(org_id,status):
        try:
            user_total = User_audit.objects().count()
            return user_total
        except NotUniqueError as e:
            return '0'

    def getAdminUserList(ordId,length,page,search,sort,order):
        try:
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
    
            # if search :
            #     filter1['name__contains'] = search
                # filter['name__contains'] = search
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [
                            {'$lookup' : app_config.report_to_lookup},
                            {'$lookup' : app_config.role_lookup},
                            # {'$graphLookup' : fields_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$match':{
                                '$or': [
                                {'name': { "$regex": search, "$options" :'i'}}, 
                                {'email': { "$regex": search, "$options" :'i'}},
                                ],
                            }},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.user_project}
                        ]
            
            settings = defaultdict(list)

            role = AdminUser.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='currency':
                        settings[item1] = item[item1]
                    elif item1=='report':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    
                    if item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        settings['date_aging'] = TokenRefresh.calculate_age(create_date)
             

                    if item1=='target_date':
                        target_date = item[item1]
                        target_date = datetime.datetime.strptime(str(target_date), DATE_FORMAT4).strftime(DATE_FORMAT1)
                        settings['target_date'] = target_date
                    else:
                        data = {item1:item4}
                    if not settings['report']:
                        settings['report']=''
                    if not settings['roleData']:
                        settings['roleData']=''
                    

                settings6.append(settings)
            
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'

    def getAdminUserListCount():
        try:
            user_total = AdminUser.objects().count()
            return user_total
        except NotUniqueError as e:
            return '0'

    def userCheck(email):
        try:
            s = AdminUser.objects.get(email=email)
            if s.email:
                output = 'Yes'
            else:
                output = 'No'
                
        except AdminUser.DoesNotExist:
            output = 'No'   

        return output

    # def userCheckNew(email):
    #     try:
    #         s = AdminUser.objects.get(email=email,approved=1)
    #         if s.email:
    #             output = 'Yes'
    #         else:
    #             output = 'No'
                
    #     except AdminUser.DoesNotExist:
    #         output = 'No'   

    #     return output

    def createUser(email,password,name):

        try:
            data1 = defaultdict(list)
            data1['email'] = email
            data1['password'] = password
            data1['name'] = name
            user = AdminUser(**data1)
            response = user.save()
            output1 = {'user_id' : str(response.id)}
            return output1
        except NotUniqueError as e:
            return ''

    def createAdminUser(email,password,name,phone):

        try:
            user = AdminUser(email=email,name=name,password=password,phone=phone)
            response = user.save()
            output1 = {'user_id' : response.id}
            return output1
        except NotUniqueError as e:
            return ''

    def changeAdminUserStatus(id,status):
        try:
            response = AdminUser.objects(id=ObjectId(id)).update_one(status=status)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def changeAdminUserPassword(id,password):
        try:
            response = AdminUser.objects(id=ObjectId(id)).update_one(password=password)
            id = id
        except NotUniqueError as e:
            id = '0'
        return id

    def adminUserUpdate(email,name,phone,id):
        try:
            modify_date = datetime.datetime.utcnow
            user_id = id
            user = AdminUser.objects(id=id).update_one(email=email,name=name,phone=phone)
            output = 'Yes'
        except User.DoesNotExist:
            output = 'No'   
        return output

    def getTicketList(ordId,length,page,search_string,owner,source,customer_type,date_from,date_to,order,sort,current_user):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [    {'$lookup' : app_config.org_lookup},
                            {'$lookup' : app_config.user_audit_lookup},
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.ticket_project}
                        ]
            
            settings = defaultdict(list)
            role = SupportTicket.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                    elif item1=='org_id':
                        assigned = item[item1]
                        settings['no_of_users']=AdminAPI.getOrgUserCount(item[item1])
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        # settings['date_aging'] = TokenRefresh.calculate_age(create_date2)

                    elif item1=='start_date':
                        start_date1 = item[item1]
                        start_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        start_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        start_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['start_date'] = start_date
                        settings['start_time'] = start_time
                        settings['date_aging'] = TokenRefresh.calculate_age(start_date2)
                    
                    elif item1=='end_date':
                        end_date1 = item[item1]
                        end_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        end_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        end_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['end_date'] = start_date
                        settings['end_time'] = start_time
                        settings['remaining_days'] = TokenRefresh.calculate_age(end_date2)

              
                    else:
                        data = {item1:item4}
                   
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getTicketCountData():
        try:
            ticket_total = SupportTicket.objects().count()
            return ticket_total
        except NotUniqueError as e:
            return '0'

    def getEnquiryList(ordId,length,page,search_string,date_from,date_to,order,sort):
        try: 
            sData = int(page)*int(length)
            length1 = int(page)-1
            sData1 = int(length)*int(length1) 
            filter = {}
            filter1 = {}
            key = []
            val = []
            if date_from:
                y = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_from, "%d/%m/%Y").strftime('%d')
                search_time=datetime.datetime(int(y),int(m),int(d),0,0)
            if date_to:
                y = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%Y')
                m = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%m')
                d = datetime.datetime.strptime(date_to, "%d/%m/%Y").strftime('%d')
                nextday=datetime.datetime(int(y),int(m),int(d),23,59)
                filter['create_date'] = {"$gte": search_time,"$lte": nextday}
            for item1 in filter:
                key.append(item1)
                val.append(filter[item1])
            match = filter
            pipeline = [  
                            {'$sort'   : {sort : order}},
                            {'$match'  : match},
                            {'$skip'   : sData1},
                            {'$limit'  : length},
                            {'$project': app_config.enquiry_project}
                        ]
            
            settings = defaultdict(list)
            role = Enquiry.objects(**filter1).aggregate(*pipeline)
            settings6=[]
            for item in role:
                settings = defaultdict(list)
                for item1 in item:
                    settings[item1] = item[item1]
                    item4 = str(item[item1])
                    data = {item1:item4}
                    if item1=='assigned':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings[item1] = item2['name']
                    elif item1=='account':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['company_name'] = item2['company_name']
                    elif item1=='countryId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['country_id'] = item2['id']
                    elif item1=='stateId':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['state_id'] = item2['id']
                    elif item1=='source_name':
                        assigned = item[item1]
                        for item2 in assigned:
                            settings['source_name'] = item2['name']
                    elif item1=='_id':
                        assigned = item[item1]
                    elif item1=='org_id':
                        assigned = item[item1]
                        settings['no_of_users']=AdminAPI.getOrgUserCount(item[item1])
                    elif item1=='modify_date':
                        modify_date1 = item[item1]
                        modify_date = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        modify_time = datetime.datetime.strptime(str(modify_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['modify_date'] = modify_date
                        settings['modify_time'] = modify_time
                        settings['last_modified'] = TokenRefresh.calculate_age(modify_date)

                    elif item1=='create_date':
                        create_date1 = item[item1]
                        create_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        create_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        create_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['create_date'] = create_date
                        settings['create_time'] = create_time
                        # settings['date_aging'] = TokenRefresh.calculate_age(create_date2)

                    elif item1=='start_date':
                        start_date1 = item[item1]
                        start_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        start_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        start_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['start_date'] = start_date
                        settings['start_time'] = start_time
                        settings['date_aging'] = TokenRefresh.calculate_age(start_date2)
                    
                    elif item1=='end_date':
                        end_date1 = item[item1]
                        end_date = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT_mdy)
                        end_date2 = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT1)
                        end_time = datetime.datetime.strptime(str(create_date1), DATE_FORMAT).strftime(DATE_FORMAT2)
                        settings['end_date'] = start_date
                        settings['end_time'] = start_time
                        settings['remaining_days'] = TokenRefresh.calculate_age(end_date2)

              
                    else:
                        data = {item1:item4}
                   
                settings6.append(settings)
            settings2 = json.loads(json_util.dumps(settings6))
            return settings2
        except NotUniqueError as e:
            return '0'
    
    def getEnquiryCountData():
        try:
            ticket_total = Enquiry.objects().count()
            return ticket_total
        except NotUniqueError as e:
            return '0'

    def getSignupCount(from_date,to_date):
        try:

            filter = {}
            filter['create_date'] = {"$gte": from_date,"$lte": to_date}
            match = filter
            pipeline = [
                            {'$project': app_config.organization_project},
                            {'$match': match},

                       ]
            data = Organization.objects.aggregate(*pipeline)
            count_array=json.loads(json_util.dumps(data))
            if len(count_array) == 0:
                count = 0
            else:
                count =len(count_array)
            return count
        except NotUniqueError as e:
            return '0'

    def getOrgCount(status):
        try:
            total = Organization.objects(status=status).count()
            return total
        except NotUniqueError as e:
            return '0'
    def getUserCount(status):
        try:
            total = AdminUser.objects(status=status).count()
            return total
        except NotUniqueError as e:
            return '0'

    def deleteData(org_id):
        try:
            obj1 = Contact.objects(org_id=org_id)
            obj1.delete()
            # obj2 = Account.objects(org_id=org_id)
            # obj2.delete()
            # obj3 = Deal.objects(org_id=org_id)
            # obj3.delete()
            # obj4 = Quote.objects(org_id=org_id)
            # obj4.delete()
            # obj5 = Proforma.objects(org_id=org_id)
            # obj5.delete()
            # obj6 = Invoice.objects(org_id=org_id)
            # obj6.delete()
            # obj7 = Follow_up.objects(org_id=org_id)
            # obj7.delete()
            # obj8 = Email.objects(org_id=org_id)
            # obj8.delete()
            # obj9 = Document.objects(org_id=org_id)
            # obj9.delete()
            # obj10 = Organization.objects(org_id=org_id)
            # obj10.delete()
            # obj11 = Role.objects(org_id=org_id)
            # obj11.delete()
            # obj12 = PdfSettings.objects(org_id=org_id)
            # obj12.delete()
            # obj13 = Numbering.objects(org_id=org_id)
            # obj13.delete()
            # obj13 = Activity.objects(org_id=org_id)
            # obj13.delete()
            # obj14 = Accounttmp.objects(org_id=org_id)
            # obj14.delete()
            # obj15 = Contacttmp.objects(org_id=org_id)
            # obj15.delete()
            # obj16 = Producttemp.objects(org_id=org_id)
            # obj16.delete()
            # obj17 = Rename.objects(org_id=org_id)
            # obj17.delete()
            # obj18 = SalesProcess.objects(org_id=org_id)
            # obj18.delete()
            # obj19 = SalesSubProcess.objects(org_id=org_id)
            # obj19.delete()
            # obj20 = DealsCheckList.objects(org_id=org_id)
            # obj20.delete()
            # obj21 = DealsSubCheckList.objects(org_id=org_id)
            # obj21.delete()
            # obj22 = SalesProcessTemplate.objects(org_id=org_id)
            # obj22.delete()
            # obj23 = DealsStageMapping.objects(org_id=org_id)
            # obj23.delete()
            # obj24 = Fields.objects(org_id=org_id)
            # obj24.delete()
            # obj25 = Note.objects(org_id=org_id)
            # obj25.delete()
            # obj26 = Numbering.objects(org_id=org_id)
            # obj26.delete()
            # obj27 = Product.objects(org_id=org_id)
            # obj27.delete()
            # obj28 = SupportTicket.objects(org_id=org_id)
            # obj28.delete()
            # obj29 = User.objects(org_id=org_id)
            # obj29.delete()
            # obj30 = User_audit.objects(org_id=org_id)
            # obj30.delete()
            # obj31 = Email_template.objects(org_id=org_id)
            # obj31.delete()
            return '1'
        except NotUniqueError as e:
            return '0'

    def addNewAddon(addon,addon_name,description):
        try:
            addon = AddOn(addon=addon,addon_name=addon_name,description=description)
            response = addon.save()
            output1 = {'addon_id' : response.id}
            return output1
        except NotUniqueError as e:
            return ''
    def mapAddon(org_id,addon,addon_id,status):
        try:
            check_addon= AdminAPI.addonCheck(org_id,addon_id)
            if check_addon=='Yes':
                addon = OrgAddon.objects(org_id=org_id,addon_id=ObjectId(addon_id)).update_one(status=status)
                output1 ='1'
            else:
                addon = OrgAddon(org_id=org_id,addon=addon,addon_id=ObjectId(addon_id),status=status)
                response = addon.save()
                output1 = {'addon_id' : response.id}
            return output1
        except NotUniqueError as e:
            return ''

    def addonCheck(org_id,addon_id):
        try:
            s = OrgAddon.objects.get(org_id=org_id,addon_id=ObjectId(addon_id))
            output = 'Yes'
        except OrgAddon.DoesNotExist:
            output = 'No'   

        return output

    def alladdonList():
        try:
            s = AddOn.objects.filter().to_json()
            output = json.loads(s)
        except AddOn.DoesNotExist:
            output = 'No'   
        return output

    def getOrgAddon(org_id):
        try:
            s = OrgAddon.objects.filter(org_id=org_id).to_json()
            output = json.loads(s)
        except OrgAddon.DoesNotExist:
            output = 'No'   
        return output
    

